import React from 'react';
import {Layout} from '../../components/common';
import { Switch, Route } from 'react-router-dom';
import routes from '../../routes';
import Monitor from '../MonitorPage';
import PatientPage from '../PatientPage';

const MonitorWrapper = () => {
    return (
        <Layout title={'card_title_monitor'}>
            <Switch>
                {/*<Route exact {...routes.monitor} component={Monitor} />*/}
                {/*<Route {...routes.patient} component={PatientPage} />*/}
            </Switch>
        </Layout>
    );
};

export default MonitorWrapper;
