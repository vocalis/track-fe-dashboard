/*
 *
 * App constants
 *
 */

enum ActionTypes {
    DEFAULT_ACTION = 'app/App/DEFAULT_ACTION',
    STATUS_ALL_ACTION = 'app/App/STATUS_ALL_ACTION',
    STATUS_ALL_ACTION_SUCESS = 'app/App/STATUS_ALL_ACTION_SUCESS',
}

export const API_STATUS_ALL = 'statusAll';
export default ActionTypes;
