import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';

import App from '../index';

const renderer = createRenderer();

describe('<App />', () => {
  it('should render and match the snapshot', () => {
    // @ts-ignore
    renderer.render(<App history={{}} />);
    const renderedOutput = renderer.getRenderOutput();
    expect(renderedOutput).toMatchSnapshot();
  });
});
