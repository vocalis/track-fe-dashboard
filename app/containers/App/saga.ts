
import { call, put, takeLatest } from 'redux-saga/effects';
import ActionTypes, {API_STATUS_ALL} from './constants';
import {getData, getStatusAllData, postData} from '../../utils/network';
import {API_LOGIN_ENDPOINT} from '../AuthProvider/constants';
import {getAuthenticatedUser} from '../../models/model';
import {getIsAuthenticated} from '../../utils/commonFunc';
import {saveDataToStorage} from '../../utils/cookies';
import {authenticateActionFails, authenticateActionSuccess} from '../AuthProvider/actions';
import {statusAllActionSucess} from './actions';

export default function* appSaga() {
    yield takeLatest(ActionTypes.STATUS_ALL_ACTION, getStatusAll);
}
function *getStatusAll() {
    const response = yield call(getStatusAllData, API_STATUS_ALL, {});
    if (!response.error) {
       const statusAllArray = response.data.map(item => {
           return  item.split(' ')[2];
        });
       yield  put (statusAllActionSucess(statusAllArray));
    }
}
