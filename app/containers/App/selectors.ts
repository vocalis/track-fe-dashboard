import { createSelector } from 'reselect';
import { ApplicationRootState } from 'types';
import {initialState} from './reducer';

const selectRoute = (state: ApplicationRootState) => state.router;

const selectAppDomain = (state: ApplicationRootState) =>
    state.app || initialState;

const makeSelectLocation = () =>
    createSelector(selectRoute, routeState => routeState.location);

const makeSelectStatusAll = () =>
    createSelector(
        selectAppDomain,
        substate => {
            return substate.statusAll;
        },
    );
export { makeSelectLocation, makeSelectStatusAll };
