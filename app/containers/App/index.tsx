/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React, {useEffect, useState} from 'react';
import {
    Switch,
    Route,
    withRouter,
    RouteComponentProps,
    RouteProps,
} from 'react-router-dom';
import PrivateRoute from '../../routes/PrivateRouter';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import UserManagement from 'containers/UserManagement';
import GlobalStyle from '../../global-styles';
import routes from '../../routes';
import TriagePage from '../TriagePage';
import ScreenPage from '../ScreenPage';
import Help from '../../components/Help';
import About from '../../components/About';
import Contact from '../../components/Contact';
import PrivacyNotice from '../../components/PrivacyNotice';
import TermsOfService from '../../components/TermsOfService';
import Login from '../Login';
import {useInjectReducer} from '../../utils/injectReducer';
import reducer from './reducer';
import MonitorWrapper from '../MonitorWrapper';
import {useDispatch, useSelector} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import PublicRoute from '../../routes/PublicRoute';
import AuthProvider from '../AuthProvider';
import {makeSelectGetUserRole, makeSelectIsAuthenticated, makeSelectIsRefresh} from '../AuthProvider/selectors';
import {getDataFromStorage} from '../../utils/cookies';
import FormDetails from '../FormDetails';
import {authenticateActionSuccess, refreshTokenAction} from '../AuthProvider/actions';
import {getIsAuthenticated, stringToNumber} from '../../utils/commonFunc';
import {selectTabAction} from '../SideBar/actions';
import {help, mapingPathsToTabId} from '../../routes/paths';
import PatientPage from '../PatientPage';
import {userRole} from '../../utils/constants';
import Monitor from '../MonitorPage';
import {useInjectSaga} from '../../utils/injectSaga';
import saga from './saga';
import {statusAllAction} from './actions';
import {makeSelectStatusAll} from './selectors';

const packageJson = require('../../../package.json');

interface AppProps {
    history: RouteProps;
}

const key = 'app';
const stateSelector = createStructuredSelector({
    isAuthenticated: makeSelectIsAuthenticated(),
    isRefresh: makeSelectIsRefresh(),
    role: makeSelectGetUserRole(),
    statusAll: makeSelectStatusAll(),
});

function App(props: AppProps) {
    const {history} = props;
    const dispatch = useDispatch();
    const {isAuthenticated, role, isRefresh, statusAll} = useSelector(stateSelector);
    useInjectReducer({key, reducer});
    useInjectSaga({key, saga: saga});
    const [checkAuth, setCheckAuth] = useState(false);
    useEffect(() => {
        const env = window['_env_'];
        if (statusAll && statusAll.length > 0) {
            statusAll.join('_');
            console.log(`version ${packageJson && packageJson.version}.${env.BUILD}_${statusAll.join('_')}`);
        }
    }, [statusAll]);
    useEffect(() => {
        dispatch(statusAllAction());
        const {role, accessToken, expiresAt, refreshToken, useId} = getDataFromStorage();
        const isAuthenticated = getIsAuthenticated(accessToken, expiresAt);
        setCheckAuth(true);
        if (history && history.location && history.location.pathname) {
            const id = mapingPathsToTabId(history.location.pathname);
            dispatch(selectTabAction(id));
            if (accessToken && expiresAt && isAuthenticated) {
                dispatch(
                    authenticateActionSuccess(
                        useId,
                        accessToken,
                        refreshToken,
                        expiresAt,
                        isAuthenticated,
                        false,
                        role,
                        '',
                    ),
                );
            } else {
                if (accessToken && refreshToken && expiresAt && !isAuthenticated) {
                    dispatch(refreshTokenAction(true));
                }
            }
        }
    }, []);

    return (
        <AuthProvider>
            <Switch>
                <Route {...routes.help} component={Help}/>
                <Route {...routes.privacyNotice} component={PrivacyNotice}/>
                <Route {...routes.termsOfService} component={TermsOfService}/>
                <Route {...routes.about} component={About}/>
                <Route {...routes.contact} component={Contact}/>
            </Switch>
            {checkAuth && !isRefresh && (
                <Switch>
                    <PublicRoute
                        isAuthenticated={isAuthenticated}
                        {...routes.login}
                        component={Login}
                    />
                    <PrivateRoute
                        isAuthenticated={isAuthenticated}
                        {...routes.monitor}
                        component={Monitor}
                        authenticatedUserRole={role}
                    />
                    <PrivateRoute
                        isAuthenticated={isAuthenticated}
                        {...routes.patient}
                        component={PatientPage}
                        authenticatedUserRole={role}
                    />
                    <PrivateRoute
                        isAuthenticated={isAuthenticated}
                        {...routes.triage}
                        component={TriagePage}
                        authenticatedUserRole={role}
                    />
                    <PrivateRoute
                        isAuthenticated={isAuthenticated}
                        {...routes.MonitorWrapper}
                        component={MonitorWrapper}
                        authenticatedUserRole={role}
                    />
                    <PrivateRoute
                        isAuthenticated={isAuthenticated}
                        {...routes.screen}
                        component={ScreenPage}
                        authenticatedUserRole={role}
                    />

                    <PrivateRoute
                        isAuthenticated={isAuthenticated}
                        {...routes.medicalStaffManagementCreate}
                        component={FormDetails}
                        authenticatedUserRole={role}
                    />
                    <PrivateRoute
                        isAuthenticated={isAuthenticated}
                        {...routes.medicalStaffManagementEdit}
                        component={FormDetails}
                        authenticatedUserRole={role}
                    />
                    <PrivateRoute
                        isAuthenticated={isAuthenticated}
                        {...routes.medicalStaffManagement}
                        component={UserManagement}
                        authenticatedUserRole={role}
                    />
                    <PrivateRoute
                        isAuthenticated={isAuthenticated}
                        {...routes.patientManagementCreate}
                        component={FormDetails}
                        authenticatedUserRole={role}
                    />
                    <PrivateRoute
                        isAuthenticated={isAuthenticated}
                        {...routes.patientManagementEdit}
                        component={FormDetails}
                        authenticatedUserRole={role}
                    />
                    <PrivateRoute
                        isAuthenticated={isAuthenticated}
                        authenticatedUserRole={role}
                        {...routes.patientsManagement}
                        component={UserManagement}
                    />
                    <Route component={NotFoundPage}/>
                </Switch>
            )}
            <GlobalStyle/>
        </AuthProvider>
    );
}

export default App;
