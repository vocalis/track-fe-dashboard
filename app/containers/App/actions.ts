import { action } from 'typesafe-actions';
import ActionTypes from './constants';

export const defaultAction = () => action(ActionTypes.DEFAULT_ACTION);
export const statusAllAction = () => action(ActionTypes.STATUS_ALL_ACTION);
export const statusAllActionSucess = (statusAll: []) =>
    action(ActionTypes.STATUS_ALL_ACTION_SUCESS, {statusAll});
