import {fromJS} from 'immutable';
import ActionTypes from './constants';
import {ContainerActions, ContainerState} from './types';

export const initialState: ContainerState = fromJS({
    url: '',
    statusAll: [],
});

const AppReducer = (state: ContainerState = initialState, action: ContainerActions): ContainerState => {
    switch (action.type) {
        case ActionTypes.DEFAULT_ACTION:
            return state;
        case ActionTypes.STATUS_ALL_ACTION_SUCESS:
            return {url: state.url, statusAll: action.payload.statusAll};
        default:
            return state;
    }
};

export default AppReducer;
