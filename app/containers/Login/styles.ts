import styled from 'styled-components';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const TextFieldsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding-right: 60px;
  padding-left: 59px;
  color: #38abb6;
  font-size: 20px;
  font-family: Roboto-Regular;
  font-weight: bold;
`;
const LoginLayout = styled.div`
  width: 100%;
  background: #ffffff;
  box-shadow: 0px 3px 18px #00000029;
  border: 1px solid #eaeaea;
  opacity: 1;
  border-radius: 9px;
`;
const Container = styled.div`
  display: grid;
  width: 100%;
  height: 100%;
  background: transparent linear-gradient(185deg, #38abb6 0%, #1c565b 100%) 0% no-repeat padding-box;
  grid-template-columns: [col1-start] auto [col1-end] 655px [col2-end] auto [col3-end];
  grid-template-rows:
    [row1-start] 78px
    [row1-end] 613px
    [row2-end] 60.61px
    [row3-end] 60.61px
    [row4-end] 65.61px
    [row5-end] calc(100vh - 878px)
    [row6-end];
  opacity: 1;
`;

const FooterWrapper = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: space-around;
  background: #fdfdfd;
  grid-column-start: col1-start;
  grid-column-end: col3-end;
  grid-row-start: row4-end;
  grid-row-end: row6-end;
`;

const Span = styled.div<{
  fontSize: string;
  marginTop: string;
  height: string;
  color: string;
}>`
    text-align: center;
    width: 100%;
    height: ${(props) => props.height};
    font-size: ${(props) => props.fontSize};
    margin-top:  ${(props) => props.marginTop};
    color: ${(props) => props.color};
    opacity: 1;
    font-family: Roboto-Regular;
        font-weight: normal;
}
`;
const LogoWrapper = styled.div<{ paddingTop: string; paddingBottom: string }>`
  text-align: center;
  padding-top: ${(props) => props.paddingTop};
  padding-bottom: ${(props) => props.paddingBottom};
`;
const Logo = styled.img`
  width: 218px;
  height: 53px;
`;

const Img = styled.img`
  height: 100%;
  width: 100%;
  background: #fdfdfd;
  grid-column-start: col1-start;
  grid-column-end: col3-end;
  grid-row-start: row2-end;
  grid-row-end: row4-end;
`;
const Div = styled.div<{ marginTop: string }>`
  margin-top: ${(props) => props.marginTop};
`;
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    buttom: {
      background: '#38ABB6',
    },
    disableButtom: {
      background: '#EAEAEA',
    },
  }),
);
const LoginLayoutWrapper = styled.div`
  grid-column-start: col1-end;
  grid-column-end: col2-end;
  grid-row-start: row1-end;
  grid-row-end: row1-end;
  position: relative;
`;

const ButtonWrapper = styled.div<{ marginTop: string; marginBottom: string }>`
  margin-top: ${(props) => props.marginTop};
  margin-bottom: ${(props) => props.marginBottom};
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
`;

const ByWrapper = styled.span`
  font-size: 16px;
  font-family: Roboto-Regular;
  color: #707070;
  opacity: 1;
  margin-right: 16px;
`;
const TermsOfUseWrapper = styled.span`
     position: absolute;
    background: #FDFDFD;
       left: 10px;
    right: 10px;
    top: 10px;
    overflow-y: auto;
      height: calc(100% - 30px);
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    padding: 44px;
    align-items: center;
`;
const CloseIconWrapper = styled.div`
    width: 100%;
    position: fixed;
    left: 24px;
    top: 24px;
`;
export {
  TextFieldsWrapper,
  LoginLayout,
  Container,
  Span,
  TermsOfUseWrapper,
  LogoWrapper,
  Logo,
  FooterWrapper,
  Div,
  ButtonWrapper,
  LoginLayoutWrapper,
  Img,
  ByWrapper,
  CloseIconWrapper,
};
