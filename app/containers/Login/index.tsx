/*
 *
 * Login
 *
 */

import React, {memo, useState} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  TextFieldsWrapper,
  LoginLayout,
  Container,
  Span,
  LogoWrapper,
  Logo,
  FooterWrapper,
  Div,
  ButtonWrapper,
  useStyles,
  LoginLayoutWrapper,
    Img,
    ByWrapper,
  TermsOfUseWrapper,
  CloseIconWrapper,
} from './styles';
import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectLogin from './selectors';
import reducer from './reducer';
import saga from './saga';
import TextField from '../../components/TextField';
import { authenticateAction } from '../AuthProvider/actions';
import { FormattedMessage } from 'react-intl';
import Button from '../../components/Button';
import {makeSelectLoginErrorMessage} from '../AuthProvider/selectors';
import {Modal} from '@material-ui/core';
import TermsOfUseContainer from '../../components/TermsOfService/TermsOfUseContainer';
const vtrackLogo = require('images/VocalisTrack-Logo-RGB.png');
const vLogo = require('images/Vocalis-logo.svg');
const lines = require('images/lines@2x.png');
const stateSelector = createStructuredSelector({
  login: makeSelectLogin(),
  loginErrorMessage: makeSelectLoginErrorMessage(),
});

interface Props {}
function Login(props: Props) {
  useInjectReducer({ key: 'login', reducer: reducer });
  useInjectSaga({ key: 'login', saga: saga });
  const classes = useStyles();
  const {loginErrorMessage } = useSelector(stateSelector);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [agree, setAgree] = useState(false);
  const [showCard, setShowCard] = useState(false);
  const dispatch = useDispatch();
  const closeIcon = require('images/close.svg');
  const handlechanges = (id: string , value: string) => {
   if (id === 'email') {
     setEmail(value);
   } else {
     setPassword(value);
   }
  };
  const loginClicked = (e) => {
    e.preventDefault();
    if (agree) {
      const loginUserData = {
        email,
        password,
      };
      dispatch(authenticateAction(loginUserData, true));
    } else {
        if (email !== '' && password !== '') {
          setAgree(true);
        }
    }
  };
  const openTermsOfuse = () => {
    setShowCard(!showCard);
  };
  return (
      <Container>

        <LoginLayoutWrapper>

        <Modal
            open={showCard}
            onClose={openTermsOfuse}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            disableBackdropClick
        >
 <TermsOfUseWrapper >
    <CloseIconWrapper>
      <img style={{cursor : 'pointer'}} onClick={openTermsOfuse} src={closeIcon} />
    </CloseIconWrapper>
    <TermsOfUseContainer/>
           </TermsOfUseWrapper>
            </Modal>

            <LoginLayout>
              <Span
                  fontSize="24px"
                  marginTop="45px"
                  height="51px"
                    color="#353535"
              ><FormattedMessage id={'Sign_IN_TO'}/>
              </Span>
              <LogoWrapper  paddingTop="19.8px" paddingBottom="0px">
                <Logo src={vtrackLogo} alt="logo" />
              </LogoWrapper>
              <form onSubmit={loginClicked}>
                <TextFieldsWrapper>
                  <Div   marginTop="54px">
                    <TextField
                        onChange={handlechanges}
                        label={<FormattedMessage id="EMAIL" />}
                        id="email"
                        type="string"
                        placeHolder="FILL_IN_YOUR_Email"
                        value={email}
                        fontFamily="Roboto-Regular"
                        isSubmitted={true}
                        autoFocus
                        error={loginErrorMessage}
                        hideErrorMessage={true}
                    />
                  </Div>
                  <Div   marginTop="50px">
                    <TextField
                        onChange={handlechanges}
                        label={<FormattedMessage id="PASSWORD" />}
                        id="password"
                        type="password"
                        placeHolder="FILL_IN_YOUR_PASSWORD"
                        value={password}
                        fontFamily="Roboto-Regular"
                        error={loginErrorMessage}
                        isSubmitted={true}
                        errorPossition="-34px"
                    />
                  </Div>
                  <Span color="#707070" fontSize="16px" marginTop="90px"  height="19px" >
                      <FormattedMessage
                          id={'BY_SIGNING_IN_I_ACCEPT_THE_TERMS_OF_USE'}
                          values={{termsOfUse:
                          <span onClick={openTermsOfuse} style={{color : '#38ABB6', cursor: 'pointer' }}>
                              <FormattedMessage
                                  id={'TERMS_OF_USE'}/>
                          </span>,

                          }}
                      />
                  </Span>
                  <ButtonWrapper marginTop="39.5px" marginBottom="67.5px">
                    <Button
                        onClick={loginClicked}
                        title={agree ? 'SIGN_IN' : 'I_AGREE_LET_ME_SIGN_IN'}
                        width="260px"
                        height="42px"
                        className={(email === '' || password === '') ? classes.disableButtom : classes.buttom}
                        disableButton={(email === '' || password === '')}
                    />
                  </ButtonWrapper>
                </TextFieldsWrapper>
              </form>
            </LoginLayout>

        </LoginLayoutWrapper>
          <Img src={lines} alt="Lines"/>
            <FooterWrapper>
            <LogoWrapper  paddingTop="0" paddingBottom="20px">
                <ByWrapper> By</ByWrapper>
              <a href="https://vocalishealth.com/" target="_blank">
              <Logo  src={vLogo} alt="logo" />
              </a>
            </LogoWrapper>
        </FooterWrapper>
  </Container>
);
}
export default memo(Login);


