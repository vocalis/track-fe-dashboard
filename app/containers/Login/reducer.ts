/*
 *
 * Login reducer
 *
 */

import ActionTypes from './constants';
import { ContainerState, ContainerActions } from './types';

export const initialState: ContainerState = {
  isLoading: false,
};

function loginReducer(
  state: ContainerState = initialState,
  action: ContainerActions,
): ContainerState {
      return state;
  }

export default loginReducer;
