import { createSelector } from 'reselect';
import { initialState } from './reducer';
import {ApplicationRootState} from 'types';

const selectTriagePageDomain = (state: ApplicationRootState) =>
    state.triage || initialState;

const makeSelectTriageData = () => createSelector(selectTriagePageDomain, substate => substate.data);

const makeSelectTriageLoading = () => createSelector(selectTriagePageDomain, substate => substate.loading);

export {
    makeSelectTriageData,
    makeSelectTriageLoading,
};
