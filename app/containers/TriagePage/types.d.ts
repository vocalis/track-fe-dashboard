import {ActionType} from 'typesafe-actions';
import * as actions from './actions';
import { ApplicationRootState } from 'types';
import {ServerTriage} from '../../models/server';

/* --- STATE --- */
interface TriagePageState {
    readonly data: ServerTriage[];
    readonly loading: boolean;
}

/* --- ACTIONS --- */
type TriagePageActions = ActionType<typeof actions>;

/* --- EXPORTS --- */
type RootState = ApplicationRootState;
type ContainerState = TriagePageState;
type ContainerActions = TriagePageActions;

export { RootState, ContainerState, ContainerActions };
