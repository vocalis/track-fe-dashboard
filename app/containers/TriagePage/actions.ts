import { action } from 'typesafe-actions';
import ActionTypes from './constants';
import {ServerTriage} from '../../models/server';

export const defaultAction = () => action(ActionTypes.DEFAULT_ACTION);

export const loadTriageDataAction = () => action(ActionTypes.LOAD_TRIAGE_DATA_ACTION);

export const loadTriageDataSuccessAction = (data: ServerTriage[]) =>
    action(ActionTypes.LOAD_TRIAGE_DATA_SUCCESS_ACTION, data);

export const loadTriageDataFailureAction = error => action(ActionTypes.LOAD_TRIAGE_DATA_FAILURE_ACTION, error);

export const setTriageLoadingAction = isLoading => action(ActionTypes.TRIAGE_SET_LOADING_ACTION, isLoading);
