import React, {useEffect, useState} from 'react';
import {Layout, Table} from 'components/common';
import {TriageTableRegularItem, TriageTablePVRStem, AlertIcon, TriageTableNameItem} from './styles';
import {HeaderItem} from 'components/common/Table';
import {useInjectSaga} from '../../utils/injectSaga';
import saga from './saga';
import reducer from './reducer';
import {loadTriageDataAction, setTriageLoadingAction} from './actions';
import { useDispatch, useSelector} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import {makeSelectTriageData, makeSelectTriageLoading} from './selectors';
import {useInjectReducer} from '../../utils/injectReducer';
import {StyledLinerProgress} from '../../components/common/LinerProgress';

const alertIcon = require('images/ic_warning.svg');



const key = 'triage';

const stateSelector = createStructuredSelector({
    data: makeSelectTriageData(),
    loading: makeSelectTriageLoading(),
});

const TriagePage = () => {
    const dispatch = useDispatch();
    const {data, loading} = useSelector(stateSelector);
    useInjectReducer({ key, reducer });
    useInjectSaga({ key, saga });
    const [showSlider, setShowSlider] = useState(false);
    const [sliderVal, setSliderVal] = useState(0);
    useEffect(() => {
        dispatch(loadTriageDataAction());
        return () => {dispatch(setTriageLoadingAction(true)); };
    }, []);
    const onAlertClick = () => {
        setShowSlider(!showSlider);
    };
    const onChangeSliderValue = (value) => {
        setSliderVal(value);
    };
    const headers: HeaderItem[] = [
        {title: 'table_header_id', weight: 10, align: 'center'},
        {title: 'table_header_patient_name', weight: 15, align: 'flex-start'},
        {title: 'table_header_pcc', weight: 45, align: 'flex-start'},
        {title: 'table_header_vrs', weight: 20, align: 'center'},
        {title: 'table_header_alert',
         weight: 10, align: 'center',
         onClick: onAlertClick,
         showSlider: showSlider,
         onChangeSliderValue: onChangeSliderValue,
        },
    ];
    const getRows = (): JSX.Element[][] => {
        if (!data) {
            return [];
        }
        return data.map((row, index) => {
            let pcc: string = '';
            row.pcc.forEach((item, index) => {
                pcc += item;
                if (index !== row.pcc.length - 1) {
                    pcc += ', ';
                }
            });
            const isvisable = row.score > sliderVal;
            return ([
                    <TriageTableRegularItem key={`row_${index}_id`}>{row.id}</TriageTableRegularItem>,
                    <TriageTableNameItem key={`row_${index}_name`}>{row.name}</TriageTableNameItem>,
                    <TriageTableRegularItem key={`row_${index}_pcc`}>{pcc}</TriageTableRegularItem>,
                    <TriageTablePVRStem key={`row_${index}_score`}>{row.score}</TriageTablePVRStem>,
                    <AlertIcon src={alertIcon}  isVisible={isvisable && row.alert} key={`row_${index}_alert`}/>]);
    });
    };

    return (
        <Layout title={'card_title_triage'}>
            {
                loading ?
                    <StyledLinerProgress color="secondary"  />
                    :
                    <Table headers={headers} rows={getRows()} />
            }
        </Layout>
    );
};

export default TriagePage;
