/*
 *
 * TriagePage constants
 *
 */

export const API_TRIAGE_ENDPOINT = 'triage';

enum ActionTypes {
    DEFAULT_ACTION = 'app/Triage/DEFAULT_ACTION',
    LOAD_TRIAGE_DATA_ACTION = 'app/Triage/LOAD_TRIAGE_DATA_ACTION',
    LOAD_TRIAGE_DATA_SUCCESS_ACTION = 'app/Triage/LOAD_TRIAGE_DATA_SUCCESS_ACTION',
    LOAD_TRIAGE_DATA_FAILURE_ACTION = 'app/Triage/LOAD_TRIAGE_DATA_FAILURE_ACTION',
    TRIAGE_SET_LOADING_ACTION = 'app/Triage/TRIAGE_SET_LOADING_ACTION',
}

export default ActionTypes;
