import { fromJS } from 'immutable';
import ActionTypes from '../TriagePage/constants';
import {ContainerState, ContainerActions} from './types';

export const initialState: ContainerState = fromJS({
    data: [],
    loading: false,
});

const triageScreenReducer = (state: ContainerState = initialState, action: ContainerActions): ContainerState => {
        switch (action.type) {
            case ActionTypes.LOAD_TRIAGE_DATA_ACTION:
                return {loading: true, data: state.data};
            case ActionTypes.LOAD_TRIAGE_DATA_SUCCESS_ACTION:
                return {loading: false, data: action.payload};
            case ActionTypes.LOAD_TRIAGE_DATA_FAILURE_ACTION:
                return {loading: false, data: []};
            case ActionTypes.TRIAGE_SET_LOADING_ACTION:
                return {loading: action.payload, data: state.data};
            case ActionTypes.DEFAULT_ACTION:
            default:
                return state;
        }
};

export default triageScreenReducer;
