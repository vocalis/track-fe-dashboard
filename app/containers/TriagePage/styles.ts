import styled from 'styled-components';
import styles from 'styles/values';


const TriageTableRegularItem = styled.div`
  width: 100%;
  color: ${styles.colors.SHADE.GRAY.TABLE_TEXT};
  font-size: 15px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const TriageTableNameItem = styled.div`
  width: 100%;
  color: ${styles.colors.SHADE.GRAY.TABLE_TEXT};
  font-size: 15px;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
`;

const TriageTablePCCItem = styled.div`
  width: 100%;
  font-size: 15px;
  color: ${styles.colors.SHADE.RED.ERROR};
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
`;

const TriageTablePVRStem = styled.div`
  width: 100%;
  font-size: 18px;
  font-weight: bold;
  color: ${styles.colors.SHADE.GRAY.TABLE_TEXT};
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const AlertIcon = styled.img<{isVisible: boolean}>`
  width: 1.250vw;
  visibility: ${props => props.isVisible ? 'visible' : 'hidden'};
`;

export {TriageTableRegularItem, TriageTableNameItem, TriageTablePCCItem, TriageTablePVRStem, AlertIcon};
