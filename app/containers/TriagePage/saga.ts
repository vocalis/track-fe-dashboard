import { call, put, select, takeLatest, all } from 'redux-saga/effects';
import ActionTypes, {API_TRIAGE_ENDPOINT} from './constants';
import {getData} from '../../utils/network';
import {loadTriageDataFailureAction, loadTriageDataSuccessAction} from './actions';

function* actionWatcher() {
    yield takeLatest(ActionTypes.LOAD_TRIAGE_DATA_ACTION, loadTriageData);
}

export default function* triageSaga() {
    yield all([actionWatcher()]);
}

function* loadTriageData(action) {
    const response = yield call(getData, API_TRIAGE_ENDPOINT, {});
    if (!response.error) {
        yield put(loadTriageDataSuccessAction(response.data));
    } else {
        yield put(loadTriageDataFailureAction(response.error));
    }
}
