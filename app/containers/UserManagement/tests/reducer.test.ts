import userManagementReducer from '../reducer';
// import { someAction } from '../actions';
import { ContainerState } from '../types';

describe('userManagementReducer', () => {
  let state: {};
  beforeEach(() => {
    state = {
      message: '',
      loading: false,
      patients: {},
      medicalStaff: {},
    };
  });

  it('returns the initial state', () => {
    const expectedResult = state;
    expect(userManagementReducer(undefined, {} as any)).toEqual(expectedResult);
  });

  /**
   * Example state change comparison
   *
   * it('should handle the someAction action correctly', () => {
   *   const expectedResult = {
   *     loading = true;
   *   );
   *
   *   expect(appReducer(state, someAction())).toEqual(expectedResult);
   * });
   */
});
