import styled from 'styled-components';
import styles from 'styles/values';
import {string} from 'prop-types';


const FormikWrapper = styled.div`
  width: 100%;
  height: calc(100% - 120px);
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  padding-right: 160px;
   background: #FFFFFF;
padding-left: ${styles.sizes.APP_SIDE_PADDING};
`;
const HeaderWrapper = styled.div`

           @media ${styles.sizes.MEDIUM_SCREEN}{
        padding-left: ${styles.sizes.HELP_SIDE_PADDINGE_LEFT};
        padding-right: ${styles.sizes.HELP_SIDE_PADDINGE_ROGHT};
         }
   @media ${styles.sizes.SMALL_SCREEN}{
   padding-right: ${styles.sizes.HELP_SIDE_PADDINGE_ROGHT_SMALL_SCREEN};
  padding-left: ${styles.sizes.HELP_SIDE_PADDINGE_LEFT_SMALL_SCREEN};
  }
     @media ${styles.sizes.LARGE_SCREEN}{
   padding-right: ${styles.sizes.APP_SIDE_PADDING_RIGHT_LARGE_SCREEN};
  padding-left: ${styles.sizes.APP_SIDE_PADDING_LEFT_LARGE_SCREEN};
  }
  width: 100%;
  height: 120px;
display: flex;
flex-direction: row;
    align-items: center;
 background: #FDFDFD;
opacity: 1;
border-bottom: 2px solid #00000007;
`;
const Contanier = styled.div`
width:100%;
height: calc( 100% - 120px);

           @media ${styles.sizes.MEDIUM_SCREEN}{
        padding-left: ${styles.sizes.HELP_SIDE_PADDINGE_LEFT};
        padding-right: ${styles.sizes.HELP_SIDE_PADDINGE_ROGHT};
         }
   @media ${styles.sizes.SMALL_SCREEN}{
   padding-right: ${styles.sizes.HELP_SIDE_PADDINGE_ROGHT_SMALL_SCREEN};
  padding-left: ${styles.sizes.HELP_SIDE_PADDINGE_LEFT_SMALL_SCREEN};
  }
     @media ${styles.sizes.LARGE_SCREEN}{
   padding-right: ${styles.sizes.APP_SIDE_PADDING_RIGHT_LARGE_SCREEN};
  padding-left: ${styles.sizes.APP_SIDE_PADDING_LEFT_LARGE_SCREEN};
  }
`;

const TextWrapper = styled.div`
text-align: left;
font-weight: lighter;
font-size: 18px;
font-family: Roboto-Light;
letter-spacing: 0px;
color: #353535;
opacity: 1;

margin-top: 39px;
margin-bottom: 57px;
`;
const Img = styled.img<{opacity?: string}>`
opacity: ${props => props.opacity ? props.opacity : '1'};
`;
const ImgWrapper = styled.div`
width:88px;
height: 70px;
margin-right: 22px;
    align-self: center;
`;
const TitleWrapper = styled.div`
width: calc(100% - 100px - 300px);
font-size: 35px;
  font-weight: bold;
      align-self: center;
`;
const ActionWrapper = styled.div`
width: 300px;
    display: flex;
    flex-direction: row;
        align-items: center;
        justify-content: flex-end;
`;
const ButtonWrapper = styled.div`
margin-left: 23px;
`;
const Span = styled.div<{color?: string, letterSpacing: string }>`
  color: ${props => props.color};
  letter-spacing: ${props => props.letterSpacing};
  font-family: Roboto-Regular;
  font-weight: normal;
  font-size: 18px;
  display: flex;
     align-items: center;
`;
const Title = styled.div`
  color:#707070;
  font-weight: bold;
  font-family: Roboto-Bold;
  font-size:42px;
`;
const PaginationWrapper =  styled.div<{count: number}>`
margin-top: 51px;
display: flex;
flex-direction: row;
    justify-content: ${props => props.count > 0 ? 'space-between' : 'flex-end'} ;
`;
const SearchWrapper = styled.div`
margin-bottom: 27px;
justify-content: space-between;
display: flex;
flex-direction: row;
`;
const Div = styled.div`
display: flex;
flex-direction: row;
justify-content: space-between;
`;

const AddActionWrapper = styled.div`
display: flex;
flex-direction: column;
justify-content: center;
margin-bottom: 27px;
`;
const TextFieldWrapper = styled.div`
display: flex;
position: relative;
width: 100%;
`;

const  IconWrapper = styled.div<{direction: string}>`
position: absolute;
z-index: 4;
    top: 8px;
${props => props.direction}
`;
const SelecetWrapper = styled.div`
display: flex;
flex-direction: row;
align-items: center;
font-size: 18px;
font-family: Roboto-Light;
      font-size: 18px;
      font-family: Roboto-Light;
      color:#353535;
letter-spacing: 0px;
color: #353535;
opacity: 1;
height: 38px;
width: 305px;
justify-content: space-between;
margin-top: 11px;
`;
export {
    FormikWrapper,
    HeaderWrapper,
    Contanier,
    Img,
    TitleWrapper,
    ActionWrapper,
    ButtonWrapper,
    ImgWrapper,
    Span,
    Title,
    TextWrapper,
    SearchWrapper,
    PaginationWrapper,
    Div,
    AddActionWrapper,
    IconWrapper,
    TextFieldWrapper,
    SelecetWrapper,
};
