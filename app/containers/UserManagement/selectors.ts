import { createSelector } from 'reselect';
import { ApplicationRootState } from 'types';
import { initialState } from './reducer';

/**
 * Direct selector to the userManagement state domain
 */

const selectUserManagementDomain = (state: ApplicationRootState) => {
  return state.userManagement || initialState;
};

/**
 * Other specific selectors
 */

/**
 * Default selector used by UserManagement
 */

const makeSelectUserManagement = () =>
  createSelector(
    selectUserManagementDomain,
    substate => {
      return substate;
    },
  );
const makeSelectMedicalStaffData = () =>
    createSelector(
        selectUserManagementDomain,
        substate => {
            return substate.medicalStaff.data;
        },
    );
const makeSelectMedicalStaffParams = () =>
    createSelector(
        selectUserManagementDomain,
        substate => {
            return substate.medicalStaff.params;
        },
    );
const makeSelectPatientsData = () =>
    createSelector(
        selectUserManagementDomain,
        substate => {
            return substate.patients.data;
        },
    );
const makeSelectPatientsParams = () =>
    createSelector(
        selectUserManagementDomain,
        substate => {
            return substate.patients.params;
        },
    );
const makeSelectIsLoading = () =>
    createSelector(
        selectUserManagementDomain,
        substate => {
            return substate.loading;
        },
    );
const makeSelectMessage = () =>
    createSelector(
        selectUserManagementDomain,
        substate => {
            return substate.message;
        },
    );
export default makeSelectUserManagement;
export {
    selectUserManagementDomain,
    makeSelectIsLoading,
    makeSelectMedicalStaffData,
    makeSelectMedicalStaffParams,
    makeSelectMessage,
    makeSelectPatientsData,
    makeSelectPatientsParams,
};
