import { ActionType } from 'typesafe-actions';
import * as actions from './actions';
import { ApplicationRootState } from 'types';
import {MedicalStaffManagementState, PatientsManagementState} from '../../models/local';
/* --- STATE --- */
interface UserManagementState {
  readonly  medicalStaff: MedicalStaffManagementState;
  readonly patients: PatientsManagementState;
  readonly message: string;
  readonly loading: boolean;
}

/* --- ACTIONS --- */
type UserManagementActions = ActionType<typeof actions>;

/* --- EXPORTS --- */
type RootState = ApplicationRootState;
type ContainerState = UserManagementState;
type ContainerActions = UserManagementActions;

export { RootState, ContainerState, ContainerActions };
