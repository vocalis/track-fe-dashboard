/*
 *
 * UserManagement actions
 *
 */

import {action} from 'typesafe-actions';
import {
    UserManagemntparams,
    MedicalStaffManagementState,
    PatientsManagementState,
    MedicalStaff, Patients,
} from '../../models/local';
import {} from './types';

import ActionTypes from './constants';

export const defaultAction = () =>
    action(ActionTypes.DEFAULT_ACTION);

export const loadMedicalStaffData = (loading: boolean, params: UserManagemntparams) =>
    action(ActionTypes.LOAD_MEDICAL_STAFF_DATA_ACTION,
        {loading, params});
export const loadMedicalStaffDataSucess = (
     loading: boolean,
     data: MedicalStaff[],
     selectedPage: number,
     rowsPerPage: number,
     pagesCount: number,
     rows: number,
     resultCount: number) =>
    action(ActionTypes.LOAD_MEDICAL_STAFF_DATA_ACTION_SUCESS,
        {loading, data, selectedPage , rowsPerPage , pagesCount, rows, resultCount});
export const loadMedicalStaffDataFails = (loading: boolean, message: string) =>
    action(ActionTypes.LOAD_MEDICAL_STAFF_DATA_ACTION_FAILS,
        {loading, message});

export const loadPatientsData =
    (loading: boolean, params: UserManagemntparams) =>
    action(ActionTypes.LOAD_PATENTS_DATA,
        {loading, params});
export const loadPatientsDataSucess = (
    loading: boolean,
    data: Patients[],
    selectedPage: number,
    rowsPerPage: number,
    pagesCount: number,
    rows: number,
    resultCount: number) =>
    action(ActionTypes.LOAD_PATENTS_DATA_SUCESS,
        {loading, data, selectedPage , rowsPerPage , pagesCount, rows, resultCount});
export const loadPatientsDataFails = (loading: boolean, message: string) =>
    action(ActionTypes.LOAD_PATENTS_DATA_ACTION_FAILS,
        {loading, message});
export const changePatientTableParams = (loading: boolean, params: UserManagemntparams) =>
    action(ActionTypes.CHANGE_PATIENTS_TABLE_PARAMS, {loading, params});
export const changeMedicalStaffParams = (loading: boolean, params: UserManagemntparams) =>
    action(ActionTypes.CHANGE_MEDICAL_STAFF_PARAMS, {loading, params});

export const setLoadingAction = (loading: boolean) =>
    action(ActionTypes.SET_LOADING_ACTION, {loading});

export const cleanUpUserManagementData = () =>
    action(ActionTypes.CLEAN_UP_USER_MANAGEMENT_DATA, {});
export const cleanMessage = () =>
    action(ActionTypes.CLEAN_MESSAGE);

