/*
 *
 * UserManagement
 *
 */

import React, { memo, useCallback, useEffect, useRef, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectUserManagement, {
  makeSelectIsLoading,
  makeSelectPatientsParams,
  makeSelectPatientsData,
  makeSelectMessage,
  makeSelectMedicalStaffParams,
  makeSelectMedicalStaffData,
} from './selectors';
import reducer from './reducer';
import saga from './saga';
import Layout from '../../components/common/Layout';
import {
  Contanier,
  Title,
  Span,
  TitleWrapper,
  TextWrapper,
  Div,
  AddActionWrapper,
  HeaderWrapper,
  Img,
  ImgWrapper,
  PaginationWrapper,
  SearchWrapper,
  TextFieldWrapper,
  IconWrapper,
  SelecetWrapper,
} from './styles';

import {
  createMedicalStaff,
  createPatient,
  editMedicalStaffWithParam,
  editPatientWithParam,
} from '../../routes/paths';
import { RouteComponentProps } from 'react-router';
import { FormattedMessage } from 'react-intl';
import TextField from '../../components/TextField';
import Button from '../../components/Button';
import {
  changeMedicalStaffParams,
  cleanMessage,
  cleanUpUserManagementData,
  loadMedicalStaffData,
  loadPatientsData,
  setLoadingAction,
} from './actions';
import MaterialTable from '../../components/MaterialTable';
import {
  HeadCell,
  medicalStaffheadCells,
  patientsHeadCells,
} from '../../utils/constants';
import RoundedPagination from '../../components/RoundedPagination';
import { RowsPerPage } from './constants';
import { Select } from '../../components/Select';
import { makeSelectSelectedTabId } from '../SideBar/selectors';
import { mediclStaffManagement } from '../SideBar/constants';
import { Modal } from '@material-ui/core';
import MessageCard from '../../components/MessageCard';
import { toggleSideBar } from '../SideBar/actions';
import _ from 'lodash';
import { StyledDialogContent } from '../FormDetails/styles';
const errorIcon = require('images/errorIcon.svg');
const PatientManagementImage = require('images/PatientManagementImage1.svg');
const MedicalStaffManagementImage = require('images/MedicalStaffManagementImage1.svg');
const searchIcon = require('images/searchIcon.svg');
const deleteTextIcon = require('images/removeTextIcon.svg');
const stateSelector = createStructuredSelector({
  userManagementState: makeSelectUserManagement(),
  isLoading: makeSelectIsLoading(),
  medicalStaffData: makeSelectMedicalStaffData(),
  medicalStaffParams: makeSelectMedicalStaffParams(),
  message: makeSelectMessage(),
  patientsData: makeSelectPatientsData(),
  patientsParams: makeSelectPatientsParams(),
  SelectedTabId: makeSelectSelectedTabId(),
});

const editIcon = require('images/edit.svg');

interface Props extends RouteComponentProps {}

interface Data {}

function UserManagement(props: Props) {
  const { location, history } = props;

  useInjectReducer({ key: 'userManagement', reducer: reducer });
  useInjectSaga({ key: 'userManagement', saga: saga });

  const {
    medicalStaffData,
    medicalStaffParams,
    patientsData,
    patientsParams,
    isLoading,
    SelectedTabId,
    message,
  } = useSelector(stateSelector);
  const dispatch = useDispatch();
  const [data, setData] = useState<Data[]>([]);
  const [value, setValue] = useState('');
  const [searchResultCount, setSearchResultCount] = useState(-1);
  const serverSidepagination = false;
  let medicalStaffPage = false;
  const isInitialMount = useRef(true);
  const [headCells, setHeadCells] = useState<[] | HeadCell[]>([]);

  medicalStaffPage = SelectedTabId === mediclStaffManagement;
  useEffect(() => {
    dispatch(cleanUpUserManagementData());
    setHeadCells([]);
    if (medicalStaffPage) {
      setValue(medicalStaffParams.searchText || '');
      const newParamsObject = {
        ...medicalStaffParams,
      };
      setHeadCells(medicalStaffheadCells);
      dispatch(loadMedicalStaffData(true, newParamsObject));
    } else {
      setValue(patientsParams.searchText || '');
      const newParamsObject = {
        ...patientsParams,
      };
      dispatch(loadPatientsData(true, newParamsObject));
      setHeadCells(patientsHeadCells);
    }
    return () => {
      if (
        medicalStaffPage &&
        !history.location.pathname.includes('medicalStaff')
      ) {
        dispatch(cleanUpUserManagementData());
      }
      if (!medicalStaffPage && !history.location.pathname.includes('patient')) {
        dispatch(cleanUpUserManagementData());
      }
    };
  }, [SelectedTabId]);

  useEffect(() => {
    if (medicalStaffPage) {
      addEditIconToTableRow(medicalStaffData);
    } else {
      addEditIconToTableRow(patientsData);
    }
  }, [medicalStaffData, patientsData]);

  useEffect(() => {
    dispatch(toggleSideBar(false));
    window.scrollTo(0, 0);
  });
  const addEditIconToTableRow = (usersData) => {
    const addIconToDAta = usersData.map((data) => {
      return {
        ...data,
        editIcon: (
          <div id={data.Id} onClick={() => onEdideClick(data.Id)}>
            <img src={editIcon} alt="editIcon" />
          </div>
        ),
      };
    });
    dispatch(setLoadingAction(false));
    setData(addIconToDAta);
  };

  const onEdideClick = useCallback(
    (id) => {
      if (medicalStaffPage) {
        history.push(`${editMedicalStaffWithParam}/${id}`);
      } else {
        history.push(`${editPatientWithParam}/${id}`);
      }
    },
    [history, medicalStaffPage],
  );
  const onClickAdd = useCallback(() => {
    if (medicalStaffPage) {
      history.push(createMedicalStaff);
    } else {
      history.push(createPatient);
    }
  }, [history, medicalStaffPage]);
  const onDoneClick = useCallback(() => {
    dispatch(cleanMessage());
  }, [dispatch]);
  const onSearch = (name, value) => {
    window.scrollTo(0, 0);
    const params = medicalStaffPage ? medicalStaffParams : patientsParams;
    setValue(value);
    const newParamsObject = {
      ...params,
      searchText: value,
      selectedPage: 1,
    };

    if (medicalStaffPage) {
      dispatch(loadMedicalStaffData(true, newParamsObject));
    } else {
      dispatch(loadPatientsData(true, newParamsObject));
    }
  };

  // const onSearch = (name, value) => {
  //   window.scrollTo(0, 0);
  //   const params = medicalStaffPage ? medicalStaffParams : patientsParams;
  //   setValue(value);
  //   const newParamsObject = {
  //     ...params,
  //     searchText: value,
  //     selectedPage: 1,
  //   };
  //   debugger
  //   _.debounce(() => {
  //     debugger
  //     if (medicalStaffPage) {
  //       dispatch(loadMedicalStaffData(true, newParamsObject));
  //     } else {
  //       dispatch(loadPatientsData(true, newParamsObject));
  //     }
  //   }, 300);
  //   debugger
  // };

  const onRowDoubleClick = useCallback(
    (row) => {
      onEdideClick(row.Id);
    },
    [onEdideClick],
  );
  const onSelectRowsPerPage = (id, value) => {
    window.scrollTo(0, 0);
    const params = medicalStaffPage ? medicalStaffParams : patientsParams;
    const newParamsObject = {
      ...params,
      rowsPerPage: parseInt(value, 10),
      selectedPage: 1,
    };
    if (medicalStaffPage) {
      dispatch(loadMedicalStaffData(true, newParamsObject));
    } else {
      dispatch(loadPatientsData(true, newParamsObject));
    }
  };
  const getPagesCount = useCallback(() => {
    if (medicalStaffPage) {
      if (searchResultCount > -1) {
        return Math.ceil(searchResultCount / medicalStaffParams.rowsPerPage);
      }
      return medicalStaffParams.pagesCount;
    } else {
      if (searchResultCount > -1) {
        return Math.ceil(searchResultCount / patientsParams.rowsPerPage);
      }
      return patientsParams.pagesCount;
    }
  }, [patientsParams, medicalStaffParams, searchResultCount]);
  const changePage = useCallback(
    (page) => {
      if (page === undefined) {
        return;
      }
      const lastPageIndex = getPagesCount();
      const firstPageIndex = 1;
      window.scrollTo(0, 0);
      const params = medicalStaffPage ? medicalStaffParams : patientsParams;
      let selectedPage = params.selectedPage;
      if (page === 'next') {
        if (selectedPage + 1 > lastPageIndex) {
          return;
        }
        selectedPage = params.selectedPage + 1;
      } else {
        if (page === 'previous') {
          if (selectedPage - 1 < firstPageIndex) {
            return;
          }
          selectedPage = params.selectedPage - 1;
        } else {
          selectedPage = parseInt(page, 10);
          if (params.selectedPage === selectedPage) {
            return;
          }
        }
      }
      const newParamsObject = {
        ...params,
        selectedPage: selectedPage,
      };
      if (medicalStaffPage) {
        dispatch(loadMedicalStaffData(true, newParamsObject));
      } else {
        dispatch(loadPatientsData(true, newParamsObject));
      }
    },
    [
      dispatch,
      getPagesCount,
      medicalStaffParams,
      patientsParams,
      loadMedicalStaffData,
      loadPatientsData,
      changeMedicalStaffParams,
    ],
  );
  const createSortHandler = (property) => (event) => {
    window.scrollTo(0, 0);
    const params = medicalStaffPage ? medicalStaffParams : patientsParams;
    const isAsc = params.orderBy === property && params.sortDirection === 'asc';
    const sortDirection = isAsc ? 'desc' : 'asc';
    const orderBy = property;
    const newParamsObject = {
      ...params,
      orderBy,
      sortDirection,
    };
    if (medicalStaffPage) {
      dispatch(loadMedicalStaffData(true, newParamsObject));
    } else {
      dispatch(loadPatientsData(true, newParamsObject));
    }
  };
  const getUsersCount = () => {
    if (medicalStaffPage) {
      if (
        medicalStaffParams.resultCount !== medicalStaffParams.rows &&
        value !== ''
      ) {
        return (
          <FormattedMessage
            id={'USERS'}
            values={{
              count: `${medicalStaffParams.resultCount}  of  ${medicalStaffParams.rows}`,
            }}
          />
        );
      } else {
        return (
          <FormattedMessage
            id={'USERS'}
            values={{ count: medicalStaffParams.rows }}
          />
        );
      }
    } else {
      if (patientsParams.resultCount !== patientsParams.rows) {
        return (
          <FormattedMessage
            id={'PATIENTS'}
            values={{
              count: `${patientsParams.resultCount} of ${patientsParams.rows}`,
            }}
          />
        );
      } else {
        return (
          <FormattedMessage
            id={'PATIENTS'}
            values={{ count: patientsParams.rows }}
          />
        );
      }
    }
  };
  return (
    <Layout title="UserManagement">
      <HeaderWrapper>
        <ImgWrapper>
          <Img
            src={
              medicalStaffPage
                ? MedicalStaffManagementImage
                : PatientManagementImage
            }
            alt="manegmentPage"
          />
        </ImgWrapper>
        <TitleWrapper>
          <Span color="#707070" letterSpacing="0.72px">
            Home >
          </Span>
          <Title>
            <FormattedMessage
              id={
                medicalStaffPage
                  ? 'MEDICAL_STAFF_MANAGEMENT'
                  : 'PATIENTS_MANAGEMENT'
              }
            />
          </Title>
        </TitleWrapper>
      </HeaderWrapper>
      <Modal
        open={message !== ''}
        onClose={onDoneClick}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        disableBackdropClick
      >
        <StyledDialogContent>
          <MessageCard
            src={errorIcon}
            messageTitle={'SERVER_ERROR'}
            message=""
            buttonTitle={'Done'}
            messageValue={undefined}
            actionText={undefined}
            onActionClick={() => {}}
            onButtonClick={onDoneClick}
            maxWidth="160px"
            isError={true}
          />
        </StyledDialogContent>
      </Modal>
      <Contanier>
        <TextWrapper>
          <FormattedMessage
            id={
              medicalStaffPage
                ? 'MEDICAL_STAFF_MANAGEMENT_TEXT'
                : 'PATIENTS_MANAGEMENT_TEXT'
            }
          />
        </TextWrapper>
        <Div>
          <SearchWrapper>
            <div
              style={{ width: '398px', height: '42px', marginRight: '24px' }}
            >
              <TextFieldWrapper>
                <IconWrapper direction="left:12px">
                  <Img src={searchIcon} opacity="0.3" alt="search Icon" />
                </IconWrapper>
                <TextField
                    onChange={_.debounce(onSearch, 300)}
                  type="text"
                  value={value}
                  id="searchTitle"
                  placeHolder={
                    medicalStaffPage ? 'SEARCH_USER' : 'SEARCH_PATIENT'
                  }
                  placeHolderPaddingLeft="42px"
                  autoFocus={true}
                />
                <IconWrapper
                  onClick={() => onSearch('name', '')}
                  direction="right:10.77px"
                  style={{ cursor: 'pointer' }}
                >
                  <Img
                    width="18px"
                    height="18px"
                    src={deleteTextIcon}
                    opacity="1"
                    alt="delete"
                  />
                </IconWrapper>
              </TextFieldWrapper>
            </div>
            <Span color="#1C1C1C" letterSpacing="0.72px">
              {getUsersCount()}
            </Span>
          </SearchWrapper>
          <AddActionWrapper>
            <Button
              height="42px"
              width="151px"
              onClick={onClickAdd}
              title={medicalStaffPage ? 'ADD_USER' : 'ADD_PATIENT'}
            />
          </AddActionWrapper>
        </Div>
        <MaterialTable
          searchText={value}
          isLoading={isLoading}
          data={data}
          headCells={headCells}
          orderBy={
            medicalStaffPage
              ? medicalStaffParams.orderBy
              : patientsParams.orderBy
          }
          order={
            medicalStaffPage
              ? medicalStaffParams.sortDirection
              : patientsParams.sortDirection
          }
          onRowDoubleClick={onRowDoubleClick}
          createSortHandler={createSortHandler}
        />
        <PaginationWrapper count={getPagesCount()}>
          <RoundedPagination
            selectedpage={
              medicalStaffPage
                ? medicalStaffParams.selectedPage
                : patientsParams.selectedPage
            }
            count={
              medicalStaffPage
                ? Math.ceil(
                    medicalStaffParams.resultCount /
                      medicalStaffParams.rowsPerPage,
                  )
                : Math.ceil(
                    patientsParams.resultCount / patientsParams.rowsPerPage,
                  )
            }
            onChange={changePage}
          />
          <SelecetWrapper>
            <FormattedMessage
              id={
                medicalStaffPage
                  ? 'NMBER_OF_USERS_PER_PAGE'
                  : 'NUMBER_OF_PATIENTS_PER_PAGE'
              }
            />
            <Select
              onChange={onSelectRowsPerPage}
              id="rowsPerPage"
              name="rowsPerPage"
              value={[
                `${RowsPerPage.OPT1}`,
                `${RowsPerPage.OPT2}`,
                `${RowsPerPage.OPT3}`,
                `${RowsPerPage.OPT4}`,
              ]}
              selectedValue={
                medicalStaffPage
                  ? `${medicalStaffParams.rowsPerPage}`
                  : `${patientsParams.rowsPerPage}`
              }
              withLabel={false}
              width="63px"
              height="38px"
            />
          </SelecetWrapper>
        </PaginationWrapper>
      </Contanier>
    </Layout>
  );
}

export default memo(UserManagement);
