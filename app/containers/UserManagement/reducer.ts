/*
 *
 * UserManagement reducer
 *
 */

import ActionTypes, {RowsPerPage} from './constants';
import {ContainerActions, ContainerState} from './types';

// @ts-ignore
// @ts-ignore
// @ts-ignore
export const initialState: ContainerState = {
    medicalStaff: {
        data: [],
        params: {
            pagesCount: 0,
            selectedPage: 1,
            rowsPerPage: RowsPerPage.OPT1,
            orderBy: 'id',
            sortDirection: 'asc',
            resultCount: 0,
            rows: 0,
            searchText: '',
        },
    },
    patients: {
        data: [],
        params: {
            pagesCount: 0,
            selectedPage: 1,
            rowsPerPage: RowsPerPage.OPT1,
            resultCount: 0,
            rows: 0,
            orderBy: 'id',
            sortDirection: 'asc',
            searchText: '',
        },
    },
    message: '',
    loading: true,
};

function userManagementReducer(
    state: ContainerState = initialState,
    action: ContainerActions,
): ContainerState {
    // @ts-ignore
    switch (action.type) {
        case ActionTypes.DEFAULT_ACTION:

            return state;
        case ActionTypes.LOAD_MEDICAL_STAFF_DATA_ACTION:
            return {
                ...state,
                medicalStaff: {
                    ...state.medicalStaff,
                    params: action.payload.params,
                },
                loading: action.payload.loading,
            };
        case ActionTypes.LOAD_MEDICAL_STAFF_DATA_ACTION_SUCESS:
            return {
                ...state,
                medicalStaff: {
                    ...state.medicalStaff,
                    data: action.payload.data,
                    params: {
                        ...state.medicalStaff.params,
                        pagesCount: action.payload.pagesCount,
                        selectedPage: action.payload.selectedPage,
                        rowsPerPage: action.payload.rowsPerPage,
                        rows: action.payload.rows,
                        resultCount: action.payload.resultCount,
                    },
                },
                loading: action.payload.loading,
            };
        case ActionTypes.LOAD_MEDICAL_STAFF_DATA_ACTION_FAILS:
            return {
                ...state,
                medicalStaff: {
                    ...state.medicalStaff,
                    params: {...state.medicalStaff.params,
                             orderBy: 'id',
                             sortDirection: 'asc'},
                },
                loading: action.payload.loading,
                message: action.payload.message,
            };

        case ActionTypes.LOAD_PATENTS_DATA:
            return {
                ...state,
                patients: {
                    ...state.patients,
                    params: action.payload.params,
                },
                loading: action.payload.loading,
            };
        case ActionTypes.LOAD_PATENTS_DATA_SUCESS:
            return {
                ...state,
                patients: {
                    ...state.patients,
                    data: action.payload.data,
                    params: {
                        ...state.patients.params,
                        pagesCount: action.payload.pagesCount,
                        selectedPage: action.payload.selectedPage,
                        rowsPerPage: action.payload.rowsPerPage,
                        rows: action.payload.rows,
                        resultCount: action.payload.resultCount,

                    },
                },
                loading: action.payload.loading,
            };
        case ActionTypes.LOAD_PATENTS_DATA_ACTION_FAILS:
            return {
                ...state,
                patients: {
                    ...state.patients,
                    params: {...state.patients.params,
                             orderBy: 'id',
                             sortDirection: 'asc'},
                },
                loading: action.payload.loading,
                message: action.payload.message,
            };
            case ActionTypes.CHANGE_PATIENTS_TABLE_PARAMS:
           return  {
            ...state,
            patients: {...state.patients,
                       params: action.payload.params,
            },
            loading: action.payload.loading,
            };
        case ActionTypes.CHANGE_MEDICAL_STAFF_PARAMS:
            return  {
                ...state,
                medicalStaff: {...state.medicalStaff,
                               params: action.payload.params,
                },
                loading: action.payload.loading,
            };
        case ActionTypes.SET_LOADING_ACTION:
            return {
                ...state,
                loading: action.payload.loading,
            };
        case  ActionTypes.CLEAN_UP_USER_MANAGEMENT_DATA:
            return {
                medicalStaff: {
                    data: [],
                    params: {
                        pagesCount: 0,
                        selectedPage: 1,
                        rowsPerPage: RowsPerPage.OPT1,
                        orderBy: 'id',
                        sortDirection: 'asc',
                        resultCount: 0,
                        rows: 0,
                        searchText: '',
                    },
                },
                patients: {
                    data: [],
                    params: {
                        pagesCount: 0,
                        selectedPage: 1,
                        rowsPerPage: RowsPerPage.OPT1,
                        orderBy: 'id',
                        sortDirection: 'asc',
                        resultCount: 0,
                        rows: 0,
                        searchText: '',
                    },
                },
                message: '',
                loading: true,
            };
        case  ActionTypes.CLEAN_MESSAGE:
            return {
                ...state,
                message: '',
            };
        default:
            return state;
    }
}

//  return {...state, recordingBlobData: action.payload};

export default userManagementReducer;
