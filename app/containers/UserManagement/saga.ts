import { call, put, takeLatest } from 'redux-saga/effects';

import { mockMedicalStaffData } from '../../utils/mock';
import { getMedicalStaffTable, getPatientTable } from '../../models/model';
import { getData } from '../../utils/network';
import {
  API_MEDICAL_STAFF,
  API_PATIENTS_ENDPOINT,
} from '../FormDetails/constants';
import ActionTypes from './constants';
import { enableMock } from '../../utils/constants';
import {
  loadMedicalStaffDataFails,
  loadMedicalStaffDataSucess,
  loadPatientsDataFails,
  loadPatientsDataSucess,
} from './actions';
import _ from 'lodash';
import { Simulate } from 'react-dom/test-utils';
export default function* userManagementSaga() {
  // See example in containers/HomePage/saga.tsx
  yield takeLatest(
    ActionTypes.LOAD_MEDICAL_STAFF_DATA_ACTION,
    getMedicalStaffData,
  );
  yield takeLatest(ActionTypes.LOAD_PATENTS_DATA, getPatientsData);
}

function* getMedicalStaffData(action) {
  const params = {
    limit: undefined,
    page: undefined,
    search: undefined,
    sortBy: '',
    sortOrder: '',
  };
  const limit = _.get(action.payload.params, 'rowsPerPage');
  params.limit = limit;
  const page = _.get(action.payload.params, 'selectedPage');
  params.page = page;
  const search = _.get(action.payload.params, 'searchText');
  search === '' ? (params.search = undefined) : (params.search = search);
  const sortBy = _.get(action.payload.params, 'orderBy');
  switch (sortBy) {
    case 'status':
      params.sortBy = 'enabled';
      break;
    case 'created':
      params.sortBy = 'creationDate';
      break;
    default:
      params.sortBy = sortBy;
  }
  const sortOrder = _.get(action.payload.params, 'sortDirection');
  params.sortOrder = sortOrder;
  const response = yield call(getData, API_MEDICAL_STAFF, { ...params });
  if (!response.error) {
    const medicalStaffTable = yield getMedicalStaffTable(response.data);
    if (medicalStaffTable !== null) {
      yield put(
        loadMedicalStaffDataSucess(
          true,
          medicalStaffTable.data,
          medicalStaffTable.params.selectedPage,
          medicalStaffTable.params.rowsPerPage,
          medicalStaffTable.params.pagesCount,
          medicalStaffTable.params.rows,
          medicalStaffTable.params.resultCount,
        ),
      );
    } else {
      yield put(loadMedicalStaffDataFails(false, 'null'));
    }
  } else {
    yield put(loadMedicalStaffDataFails(false, response.error));
  }
}

function* getPatientsData(action) {
  const params = {
    sortOrder: '',
    sortBy: '',
    search: undefined,
    page: undefined,
    limit: undefined,
  };
  const limit = _.get(action.payload.params, 'rowsPerPage');
  params.limit = limit;
  const page = _.get(action.payload.params, 'selectedPage');
  params.page = page;
  const search = _.get(action.payload.params, 'searchText');
  search === '' ? (params.search = undefined) : (params.search = search);
  const sortBy = _.get(action.payload.params, 'orderBy');
  switch (sortBy) {
    case 'status':
      params.sortBy = 'enabled';
      break;
    case 'created':
      params.sortBy = 'creationDate';
      break;
    default:
      params.sortBy = sortBy;
  }
  const sortOrder = _.get(action.payload.params, 'sortDirection');
  params.sortOrder = sortOrder;
  const response = yield call(getData, API_PATIENTS_ENDPOINT, { ...params });
  if (!response.error) {
    const patientsTable = getPatientTable(response.data);
    if (patientsTable !== null) {
      yield put(
        loadPatientsDataSucess(
          true,
          patientsTable.data,
          patientsTable.params.selectedPage,
          patientsTable.params.rowsPerPage,
          patientsTable.params.pagesCount,
          patientsTable.params.rows,
          patientsTable.params.resultCount,
        ),
      );
    } else {
      yield put(loadPatientsDataFails(false, 'null'));
    }
  } else {
    yield put(loadPatientsDataFails(false, response.error));
  }
}
