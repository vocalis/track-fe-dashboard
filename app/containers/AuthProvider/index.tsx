/*
 *
 * AuthProvider
 *
 */

import React, { memo } from 'react';
import { Helmet } from 'react-helmet';
import { useSelector, useDispatch } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectAuthProvider from './selectors';
import reducer from './reducer';
import saga from './saga';

const stateSelector = createStructuredSelector({
  authProvider: makeSelectAuthProvider(),
});

interface Props {
  children?: React.ReactNode[];
}

function AuthProvider(props: Props) {
  // Warning: Add your key to RootState in types/index.d.ts file
  useInjectReducer({key: 'authProvider', reducer: reducer});
  useInjectSaga({key: 'authProvider', saga: saga });
  const {children} = props;
  const { authProvider } = useSelector(stateSelector);
  const dispatch = useDispatch();
  return (
      <>
        {children && children.map(child => {
          return child;
        })}
      </>
  );
}

export default memo(AuthProvider);
