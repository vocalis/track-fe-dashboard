import { createSelector } from 'reselect';
import { ApplicationRootState } from 'types';
import { initialState } from './reducer';

/**
 * Direct selector to the authProvider state domain
 */

const selectAuthProviderDomain = (state: ApplicationRootState) => {
  return state.authProvider || initialState;
};

/**
 * Other specific selectors
 */

/**
 * Default selector used by AuthProvider
 */

const makeSelectAuthProvider = () =>
  createSelector(
    selectAuthProviderDomain,
    substate => {
      return substate;
    },
  );

const makeSelectIsAuthenticated = () =>
    createSelector(
        selectAuthProviderDomain,
        substate => {
            return substate.isAuthenticated;
        },
    );
const makeSelectLoginErrorMessage = () =>
    createSelector(
        selectAuthProviderDomain,
        substate => {
            return substate.error;
        },
    );
const makeSelectGetUserRole = () =>
    createSelector(
        selectAuthProviderDomain,
        substate => {
            return substate.role;
        },
);
const makeSelectIsRefresh = () =>
    createSelector(
        selectAuthProviderDomain,
        substate => {
            return substate.isRefresh;
        },
    );


export default makeSelectAuthProvider;
export { selectAuthProviderDomain,
         makeSelectIsAuthenticated,
             makeSelectLoginErrorMessage,
    makeSelectGetUserRole,
    makeSelectIsRefresh,
        };
