import { call, put, takeLatest } from 'redux-saga/effects';
import ActionTypes, {
  API_LOGIN_ENDPOINT,
  API_REFRESH_TOKEN,
} from './constants';
import { postData } from '../../utils/network';
import { getIsAuthenticated } from '../../utils/commonFunc';
import { authenticateActionFails, authenticateActionSuccess } from './actions';
import { getDataFromStorage, saveDataToStorage } from '../../utils/cookies';
import { enableMock } from '../../utils/constants';
import { getAuthenticatedUser } from '../../models/model';
import { logout } from '../SideBar/actions';

export default function* authProviderSaga() {
  yield takeLatest(ActionTypes.AUTHENTIC_ATEACTION, authenticateUser);
  yield takeLatest(ActionTypes.REFRESH_TOKEN, refreshToken);
}

function* authenticateUser(action) {
  const { email, password } = action.payload.loginUserData;
  if (!enableMock) {
    const response = yield call(postData, API_LOGIN_ENDPOINT, {
      username: email,
      password,
    });
    if (!response.error) {
      const user = getAuthenticatedUser(response.data);
      if (user !== null) {
        const isAuthenticated = getIsAuthenticated(
          user.accessToken,
          user.expiresAt,
        );
        saveDataToStorage(
          user.userId,
          user.accessToken,
          user.refreshToken,
          user.expiresAt,
          user.role,
        );
        yield put(
          authenticateActionSuccess(
            user.userId,
            user.accessToken,
            user.refreshToken,
            `$user.expiresAt}`,
            isAuthenticated,
            false,
            user.role,
            '',
          ),
        );
      } else {
        yield put(authenticateActionFails('BAD_REQUEST', false, false));
      }
    } else {
      yield put(authenticateActionFails('LOGIN_ERROR_MESSAGE', false, false));
    }
  } else {
    if (email && password) {
      const response = yield call(postData, API_LOGIN_ENDPOINT, {
        username: email,
        password,
      });
      if (!response.error) {
        const isAuthenticated = getIsAuthenticated(
          response.data.accessToken,
          response.data.expiresAt,
        );
        saveDataToStorage(
          response.data.user,
          response.data.accessToken,
          response.data.refreshToken,
          response.data.expiresAt,
          response.data.role,
        );
        yield put(
          authenticateActionSuccess(
            response.data.user,
            response.data.accessToken,
            response.data.refreshToken,
            response.data.expiresAt,
            isAuthenticated,
            false,
            response.data.role,
            '',
          ),
        );
      } else {
        yield put(authenticateActionFails('LOGIN_ERROR_MESSAGE', false, false));
      }
    }
  }
}
export function* refreshToken() {
  const oldAccessToken = getDataFromStorage().accessToken;
  const refreshToken = getDataFromStorage().refreshToken;
  const response = yield call(postData, API_REFRESH_TOKEN, {
    oldAccessToken,
    refreshToken,
  });
  if (!response.error) {
    const isAuthenticated = getIsAuthenticated(
      response.data.accessToken,
      response.data.expiresAt,
    );
    saveDataToStorage(
      response.data.user,
      response.data.accessToken,
      response.data.refreshToken,
      response.data.expiresAt,
      response.data.roles[0],
    );
    yield put(
      authenticateActionSuccess(
        response.data.user,
        response.data.accessToken,
        response.data.refreshToken,
        response.data.expiresAt,
        isAuthenticated,
        false,
        response.data.roles[0],
        '',
      ),
    );
  } else {
    yield put(authenticateActionFails(response.error, false, false));
    yield put(logout());
  }
}
