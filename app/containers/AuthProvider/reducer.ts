/*
 *
 * AuthProvider reducer
 *
 */

import ActionTypes from './constants';
import {ContainerActions, ContainerState} from './types';

export const initialState: ContainerState = {
    useId: -1,
    isLoading: false,
    accessToken : '',
    refreshToken: '',
    expiresAt: '',
    isAuthenticated: false,
    error: '',
    role: 'none',
    isRefresh: false ,
};

function authProviderReducer(
  state: ContainerState = initialState,
  action: ContainerActions,
): ContainerState {
  switch (action.type) {
    case ActionTypes.DEFAULT_ACTION:
      return state;
      case ActionTypes.AUTHENTIC_ATEACTION:
          return {...state, isLoading: action.payload.isLoading};
      case ActionTypes.AUTHENTIC_ATEACTION_SUCCESS:
          return {
              ...state,
              ...action.payload,
              isRefresh: false,
              error: '',
  };
      case ActionTypes.AUTHENTIC_ATEACTION_FAILS:
          return {
              ...state,
              useId: -1,
              isLoading: false,
              accessToken : '',
              refreshToken: '',
              expiresAt: '',
              isAuthenticated: false,
              error: action.payload.error,
              role: 'none',
              isRefresh: false,
          };
      case  ActionTypes.CLEAN_UP_USER_DATA:
          return {
              ...state,
              useId: -1,
              isLoading: false,
              accessToken : '',
              refreshToken: '',
              expiresAt: '',
              isAuthenticated: false,
              error: '',
              role: 'none',
              isRefresh: false,
          };
      case ActionTypes.REFRESH_TOKEN:
          return {
              ...state,
              isRefresh: action.payload.isRefresh,
          };
    default:
      return state;
  }
}

export default authProviderReducer;
