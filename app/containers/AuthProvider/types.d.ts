import { ActionType } from "typesafe-actions";
import * as actions from "./actions";
import { ApplicationRootState } from "types";

/* --- STATE --- */
interface AuthProviderState {
  readonly isLoading: boolean;
  readonly accessToken: string;
  readonly expiresAt: string;
  readonly isAuthenticated: boolean;
  readonly refreshToken: string;
  readonly useId: number;
  readonly role: string;
  readonly error: string;
  readonly  isRefresh: boolean;
}
/* --- ACTIONS --- */
type AuthProviderActions = ActionType<typeof actions>;

/* --- EXPORTS --- */
type RootState = ApplicationRootState;
type ContainerState = AuthProviderState;
type ContainerActions = AuthProviderActions;

export { RootState, ContainerState, ContainerActions };
