/*
 *
 * AuthProvider actions
 *
 */

import { action } from 'typesafe-actions';
import {} from './types';

import ActionTypes from './constants';
import {LoginUserData} from '../../models/local';

export const defaultAction = () => action(ActionTypes.DEFAULT_ACTION);
export const authenticateAction = (loginUserData: LoginUserData, isLoading: boolean) =>
    action(ActionTypes.AUTHENTIC_ATEACTION, { loginUserData, isLoading});
export const authenticateActionSuccess = (
    useId: number,
    accessToken: string,
    refreshToken: string,
    expiresAt: string ,
    isAuthenticated: boolean,
    isLoading: boolean,
    role: string,
    error: string) =>
    action(ActionTypes.AUTHENTIC_ATEACTION_SUCCESS,
        {useId, accessToken, refreshToken, expiresAt, isAuthenticated, isLoading, role, error});
export const authenticateActionFails = (error: string, isAuthenticated: boolean, isLoading: boolean) =>
    action(ActionTypes.AUTHENTIC_ATEACTION_FAILS, { error, isAuthenticated, isLoading});
export const cleanUpUserData = () => action(ActionTypes.CLEAN_UP_USER_DATA);
export const refreshTokenAction = (isRefresh: boolean) => action(ActionTypes.REFRESH_TOKEN, {isRefresh});
