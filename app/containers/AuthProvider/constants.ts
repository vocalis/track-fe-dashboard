/*
 *
 * AuthProvider constants
 *
 */

enum ActionTypes {
  DEFAULT_ACTION = 'app/AuthProvider/DEFAULT_ACTION',
  AUTHENTIC_ATEACTION= 'app/AuthProvider/AUTHENTIC_ATEACTION',
  AUTHENTIC_ATEACTION_SUCCESS= 'app/AuthProvider/AUTHENTIC_ATEACTION_SUCCESS',
  AUTHENTIC_ATEACTION_FAILS= 'app/AuthProvider/AUTHENTIC_ATEACTION_FAILS',
  CLEAN_UP_USER_DATA = 'app/AuthProvider/CLEAN_UP_USER_DATA',
  REFRESH_TOKEN = 'app/AuthProvider/REFRESH_TOKEN',

}

export default ActionTypes;

export const API_LOGIN_ENDPOINT = 'login';
export const API_REFRESH_TOKEN = 'refresh';
