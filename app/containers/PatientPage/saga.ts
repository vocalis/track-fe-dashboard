import { call, put, select } from 'redux-saga/effects';
import moment from 'moment';
import {all, takeLatest} from '@redux-saga/core/effects';
import ActionTypes, {API_PATIENT_ENDPOINT, API_QUESTIONNAIRE, API_RECORDING_ENDPOINT} from './constants';
import {downloadBlob, getData, putData} from 'utils/network';
import {
    getPatientRecordingFailureAction,
    getPatientRecordingSuccessAction,
    loadPatientDataFailureAction,
    loadPatientDataSuccessAction,
    setMonitorQuestionnaireData,
    setMonitorSinglePatientReviewAction,
    setPatientLoadingAction,
} from './actions';
import {getPatient} from 'models/model';
import {makeSelectPatientData} from './selectors';
import {API_UPDATE_PATIENT_REVIEW} from '../MonitorPage/constants';

function* actionWatcher() {
    yield takeLatest(ActionTypes.LOAD_PATIENT_DATA_ACTION, loadPatientPageData);
    yield takeLatest(ActionTypes.TOGGLE_MONITOR_SINGLE_PATIENT_REVIEW_ACTION, togglePatientReviewed);
    yield takeLatest(ActionTypes.DETAILED_PATIENT_RECORDING_ACTION, getPatientRecording);
    yield takeLatest(ActionTypes.LOAD_MONITOR_QUESTIONNAIRE_ACTION, loadQuestionnaireData);
}

export default function* monitorSaga() {
    yield all([actionWatcher()]);
}

function* loadPatientPageData(action) {
    yield put(setPatientLoadingAction(true));

    // calculate time range
    const now = moment();
    const to = now.format('YYYY-MM-DD');
    const from = now.subtract(100, 'days').format('YYYY-MM-DD');
    const patientId = action.payload;

    const response = yield call(getData, `${API_PATIENT_ENDPOINT}/${patientId}`, {from, to, patientId});

    if (!response.error) {
        const data = getPatient(response.data);
        yield put(loadPatientDataSuccessAction(data));
    } else {
        yield put(loadPatientDataFailureAction());
    }
    yield put(setPatientLoadingAction(false));
}

function* togglePatientReviewed(action) {
    const patient = yield select(makeSelectPatientData());
    const isReviewed = !patient.reviewed;
    const response = yield call(putData,
        `${API_PATIENT_ENDPOINT}/${API_UPDATE_PATIENT_REVIEW}/${action.payload}?reviewed=${isReviewed}`,
        {});
    if (!response.error) {
        yield put(setMonitorSinglePatientReviewAction(patient.patientId, isReviewed, ''));
    } else {
        yield put(setMonitorSinglePatientReviewAction(patient.patientId, !isReviewed, response.error));

    }
}

function* getPatientRecording(action) {
    const response = yield call(downloadBlob, `${API_RECORDING_ENDPOINT}/${action.payload}`);
    if (!response.error) {
        yield put(getPatientRecordingSuccessAction(response.data));
    } else {
        yield put(getPatientRecordingFailureAction(response.error));
    }
}

function* loadQuestionnaireData(action) {
    const response = yield call(getData, `${API_QUESTIONNAIRE}`, {});
    if (!response.error) {
        yield put(setMonitorQuestionnaireData(response.data));
    }
}
