import React, {useCallback, useEffect, useState} from 'react';
import {withRouter, RouteComponentProps} from 'react-router';
import PatientMonitorDetailed from '../../components/PatientMonitorDetalied';
import {Patient} from 'models/local';
import {createStructuredSelector} from 'reselect';
import {
    makeSelectPatientData,
    makeSelectPatientLoading,
    makeSelectQuestionnaireData,
    makeSelectScreenRecodingBlobData,
    makeSelectErrorMessage,
} from './selectors';
import {useDispatch, useSelector} from 'react-redux';
import {useInjectReducer} from '../../utils/injectReducer';
import reducer from './reducer';
import {useInjectSaga} from '../../utils/injectSaga';
import saga from './saga';
import {
    cleanPatientPageErrorMessage,
    getPatientRecordingAction, getPatientRecordingSuccessAction,
    loadPatientDataAction,
    loadPatientDataSuccessAction, loadQuestionnaireData,
    setPatientLoadingAction,
    toggleMonitorSinglePatientReviewAction,
} from './actions';
import {AudioPlayerModal, Layout} from '../../components/common';
import {StyledLinerProgress} from '../../components/common/LinerProgress';
import ServerErrorMessage from '../../components/common/ServerErrorMessage';
import {toggleSideBar} from '../SideBar/actions';

interface Props extends RouteComponentProps<PathParams> {
    patient: Patient;
}

interface PathParams {
    id: string;
}

const stateSelector = createStructuredSelector({
    data: makeSelectPatientData(),
    loading: makeSelectPatientLoading(),
    recordingBlobData: makeSelectScreenRecodingBlobData(),
    questionnaireData: makeSelectQuestionnaireData(),
    errorMessage: makeSelectErrorMessage(),
});

interface RecordingModalData {
    recordingId: number;
    name?: string | null;
    id?: number | null;
    age?: number | null;
}

const PatientPage = (props: Props) => {
    const {match, history} = props;
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(toggleSideBar(false));
        window.scrollTo(0, 0);
    });
    const {data, loading, recordingBlobData, questionnaireData, errorMessage} = useSelector(stateSelector);
    useInjectReducer({key: 'patient', reducer: reducer});
    useInjectSaga({key: 'patient', saga: saga});
    const patientId = match.params.id;
    const [showAudioPlayerModal, setShowAudioPlayerModal] = useState<RecordingModalData | null>(null);

    useEffect(() => {
        try {
            dispatch(loadQuestionnaireData());
            dispatch(loadPatientDataAction(parseInt(patientId, 10)));
        } catch (e) {
        }
        return () => {
            dispatch(setPatientLoadingAction(true));
            dispatch(loadPatientDataSuccessAction(null));
        };
    }, []);

    const goBack = () => {
        history.goBack();
    };

    const playSample = (recordingId) => {
        const patient: Patient | null = data;
        // const ageDetector = patient && patient.detectors.find(detector => detector.detector === 'age') || '';
        setShowAudioPlayerModal({
            recordingId: recordingId,
            name: patient && patient.name,
            id: patient && patient.patientId,
            age: patient && patient.age || null,
        });
        dispatch(getPatientRecordingAction(recordingId));

    };
    const closeAudioPlayerModal = () => {
        setShowAudioPlayerModal(null);
        dispatch(getPatientRecordingSuccessAction(null));
    };
    const patientReviewed = (patientId: number) => dispatch(toggleMonitorSinglePatientReviewAction(patientId));
    const onDoneClick = useCallback(() => {
        dispatch(cleanPatientPageErrorMessage());
    }, [dispatch]);
    // if (loading || !data) {
    //    return  <StyledLinerProgress color="secondary"  />;
    // }

    return (
        <div>
            <Layout title={'PatientMonitorDetailed'}>
                <ServerErrorMessage
                    onDoneClick={onDoneClick}
                    message={errorMessage}
                />
                {loading || !data ?
                    <StyledLinerProgress color="secondary"/> :
                    <PatientMonitorDetailed
                        playSample={playSample}
                        questionnaireData={questionnaireData}
                        patient={data}
                        back={goBack}

                        onPatientReviewClicked={patientReviewed}/>
                }
                {
                    showAudioPlayerModal !== null &&
                    <AudioPlayerModal
                        {...showAudioPlayerModal}
                        data={recordingBlobData}
                        onClose={closeAudioPlayerModal}
                    />
                }
            </Layout>
        </div>
    );
};

export default withRouter(PatientPage);
