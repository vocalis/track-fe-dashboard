/*
 *
 * PatientPage actions
 *
 */
import { action } from 'typesafe-actions';
import ActionTypes from './constants';
import { Patient } from 'models/local';
import {
    SCREEN_PATEINT_RECORDING_ACTION,
    SCREEN_PATEINT_RECORDING_FAILURE_ACTION,
    SCREEN_PATEINT_RECORDING_SUCCESS_ACTION,
} from '../ScreenPage/constants';

export const defaultAction = () => action(ActionTypes.DEFAULT_ACTION);

export const setPatientLoadingAction = (isLoading: boolean) => action(ActionTypes.SET_PATIENT_LOADING, isLoading);

export const loadPatientDataAction = (patientId: number) => action(ActionTypes.LOAD_PATIENT_DATA_ACTION, patientId);

export const toggleMonitorSinglePatientReviewAction = (patientId: number) =>
    action(ActionTypes.TOGGLE_MONITOR_SINGLE_PATIENT_REVIEW_ACTION, patientId);

export const setMonitorSinglePatientReviewAction = (patientId: number, isReviewed: boolean, errorMessage: string) =>
    action(ActionTypes.SET_PATIENT_SINGLE_REVIEWED_ACTION, {patientId, isReviewed, errorMessage});

export const loadPatientDataSuccessAction = (patient: Patient | null) =>
    action(ActionTypes.LOAD_PATIENT_DATA_SUCCESS_ACTION, patient);

export const loadPatientDataFailureAction = () => action(ActionTypes.LOAD_PATIENT_DATA_FAILURE_ACTION);

export const getPatientRecordingAction = (recordingId) =>
    action(ActionTypes.DETAILED_PATIENT_RECORDING_ACTION, recordingId);

export const getPatientRecordingSuccessAction = (data) =>
    action(ActionTypes.DETAILED_PATIENT_RECORDING_SUCCESS_ACTION, data);

export const getPatientRecordingFailureAction = (error) =>
    action(ActionTypes.DETAILED_PATIENT_RECORDING_FAILURE_ACTION, error);

export const loadQuestionnaireData = () => action(ActionTypes.LOAD_MONITOR_QUESTIONNAIRE_ACTION);

export const setMonitorQuestionnaireData = (data) =>
    action(ActionTypes.LOAD_MONITOR_QUESTIONNAIRE_DATA_SUCCESS_ACTION, data);
export const cleanPatientPageErrorMessage = () =>
    action(ActionTypes.CLEAN_PATIENT_PAGE_ERROR_MESSAGE);
