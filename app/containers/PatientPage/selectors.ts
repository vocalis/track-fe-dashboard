import { createSelector } from 'reselect';
import { ApplicationRootState } from 'types';
import { initialState } from './reducer';

/**
 * Direct selector to the monitor state domain
 */
const selectPatientPageDomain = (state: ApplicationRootState) => {
    return state.patient || initialState;
};

const makeSelectPatientData = () => createSelector(selectPatientPageDomain, substate => substate.data);

const makeSelectPatientLoading = () => createSelector(selectPatientPageDomain, substate => substate.loading);

const makeSelectScreenRecodingBlobData = () =>
    createSelector(selectPatientPageDomain, substate => substate.recordingBlobData);

const makeSelectQuestionnaireData = () =>
    createSelector(selectPatientPageDomain, substate => substate.questionnaireData);

const makeSelectErrorMessage = () =>
    createSelector(selectPatientPageDomain, substate => substate.errorMessage);
export {
    makeSelectPatientData,
    makeSelectPatientLoading,
    makeSelectScreenRecodingBlobData,
    makeSelectQuestionnaireData,
    makeSelectErrorMessage,
};
