/*
 *
 * PatientPage reducer
 *
 */
import {fromJS} from 'immutable';
import ActionTypes from './constants';
import {ContainerActions, ContainerState} from './types';
import {Patient} from '../../models/local';
import {SCREEN_PATEINT_RECORDING_SUCCESS_ACTION} from '../ScreenPage/constants';

export const initialState: ContainerState = {
    data: null,
    loading: false,
    recordingBlobData: null,
    questionnaireData: null,
    errorMessage: '',
};

function patientReducer(
    state: ContainerState = initialState,
    action: ContainerActions,
): ContainerState {
    switch (action.type) {
        case ActionTypes.DEFAULT_ACTION:
            return state;
        case ActionTypes.SET_PATIENT_LOADING:
            return {...state, loading: action.payload};
        case ActionTypes.LOAD_PATIENT_DATA_SUCCESS_ACTION:
            return {...state, data: action.payload};
        case ActionTypes.LOAD_PATIENT_DATA_FAILURE_ACTION:
            return {...state, data: null};
        case ActionTypes.DETAILED_PATIENT_RECORDING_SUCCESS_ACTION:
            return {...state, recordingBlobData: action.payload};
        case ActionTypes.LOAD_MONITOR_QUESTIONNAIRE_DATA_SUCCESS_ACTION:
            return {...state, questionnaireData: action.payload};
        case ActionTypes.SET_PATIENT_SINGLE_REVIEWED_ACTION:
            let newData: Patient | null = null;
            if (state.data) {
                state.data.reviewed = action.payload.isReviewed;
                newData = {...state.data};
            }
            return {...state, data: newData, errorMessage: action.payload.errorMessage};
        case ActionTypes.CLEAN_PATIENT_PAGE_ERROR_MESSAGE:
            return {
                ...state,
                errorMessage: '',
            };
            default:
            return state;
    }
}

export default patientReducer;
