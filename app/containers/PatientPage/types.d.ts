import { ActionType } from 'typesafe-actions';
import * as actions from './actions';
import { ApplicationRootState } from 'types';
import {Patient} from '../../models/local';

/* --- STATE --- */
interface PatientState {
    errorMessage: string;
    readonly loading: boolean;
    readonly data: Patient | null;
    readonly recordingBlobData: Blob | null;
    readonly questionnaireData: {
        daily: QuestionnaireDataInterface[],
        expanded: QuestionnaireDataInterface[],
    } | null;
}

interface QuestionnaireDataInterface {
    id: number;
    question: string;
    answers: QuestionnaireAnswersDataInterface[];
}

interface QuestionnaireAnswersDataInterface {
    id: number;
    answer: string;
}

/* --- ACTIONS --- */
type PatientActions = ActionType<typeof actions>;

/* --- EXPORTS --- */
type RootState = ApplicationRootState;
type ContainerState = PatientState;
type ContainerActions = PatientActions;

export { RootState, ContainerState, ContainerActions };
