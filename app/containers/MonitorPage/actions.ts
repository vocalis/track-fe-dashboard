/*
 *
 * MonitorPage actions
 *
 */
import { action } from 'typesafe-actions';
import ActionTypes from './constants';
import {MonitorSearchParams} from './types';
import {Patient} from 'models/local';

export const defaultAction = () => action(ActionTypes.DEFAULT_ACTION);

export const setMonitorLoadingAction = (isLoading: boolean) => action(ActionTypes.SET_MONITOR_LOADING, isLoading);

export const loadMonitorDataAction = () => action(ActionTypes.LOAD_MONITOR_DATA_ACTION);

export const setMonitorPageAction = (page: number) => action(ActionTypes.SET_PAGE_NUMBER_ACTION, page);

export const setMonitorSearchParamsAction = (searchParams: MonitorSearchParams | null) =>
    action(ActionTypes.SET_SEARCH_PARAMS_ACTION, searchParams);

export const loadMonitorDataSuccessAction = (data: Patient[]) =>
    action(ActionTypes.LOAD_MONITOR_DATA_SUCCESS_ACTION, data);

export const loadMonitorDataFailureAction = (error: string) =>
    action(ActionTypes.LOAD_MONITOR_DATA_FAILURE_ACTION, error);

export const setTotalResultsAction = (total: number) => action(ActionTypes.SET_TOTAL_RESULTS_ACTION, total);

export const toggleMonitorPatientReviewAction = (patientId: number) =>
    action(ActionTypes.TOGGLE_PATIENT_REVIEWED_ACTION, patientId);

export const setMonitorPatientReviewAction = (patientId: number, reviewed: boolean, errorMessage: string) =>
    action(ActionTypes.SET_PATIENT_REVIEWED_ACTION, {patientId, reviewed, errorMessage});
export const setMonitorPatientReviewFailureAction = (patientId: number, reviewed: boolean, errorMessage) =>
    action(ActionTypes.SET_PATIENT_REVIEWED_ACTION, {patientId, reviewed, errorMessage});
export const cleanMessage = () =>
    action(ActionTypes.CLEAN_MONITOR_PAGE_MESSAGE);
