import { ActionType } from 'typesafe-actions';
import * as actions from './actions';
import { ApplicationRootState } from 'types';
import {Patient} from '../../models/local';

/* --- STATE --- */
interface MonitorState {
  readonly page: number;
  readonly searchParams?: MonitorSearchParams | null;
  readonly loading: boolean;
  readonly data: Patient[];
  readonly total: number;
  readonly  errorMessage: string;
  readonly questionnaireData?: {
    daily: QuestionnaireDataInterface[],
    expanded: QuestionnaireDataInterface[],
  };
}

interface QuestionnaireDataInterface {
  id: number;
  question: string;
  answers: QuestionnaireAnswersDataInterface[];
}

interface QuestionnaireAnswersDataInterface {
  id: number;
  answer: string;
}

/* --- ACTIONS --- */
type MonitorActions = ActionType<typeof actions>;

/* --- EXPORTS --- */
type RootState = ApplicationRootState;
type ContainerState = MonitorState;
type ContainerActions = MonitorActions;

interface MonitorSearchParams {
  searchTextValue: string;
  reviewed: string | null;
  isFiltering: boolean;
}

export { RootState, ContainerState, ContainerActions, MonitorSearchParams };
