import { createSelector } from 'reselect';
import { ApplicationRootState } from 'types';
import { initialState } from './reducer';

/**
 * Direct selector to the monitor state domain
 */
const selectMonitorPageDomain = (state: ApplicationRootState) => {
  return state.monitor || initialState;
};

const makeSelectMonitorData = () => createSelector(selectMonitorPageDomain, substate => substate.data);

const makeSelectMonitorLoading = () => createSelector(selectMonitorPageDomain, substate => substate.loading);

const makeSelectMonitorTotal = () => createSelector(selectMonitorPageDomain, substate => substate.total);

const makeSelectMonitorSelectedPage = () => createSelector(selectMonitorPageDomain, substate => substate.page);

const makeSelectMonitorSearchParams = () =>
    createSelector(selectMonitorPageDomain, substate =>
        substate.searchParams);
const makeSelectMonitorErrorMessage = () =>
    createSelector(selectMonitorPageDomain, substate =>
        substate.errorMessage);

export {
  makeSelectMonitorData,
  makeSelectMonitorLoading,
  makeSelectMonitorTotal,
  makeSelectMonitorSelectedPage,
  makeSelectMonitorSearchParams,
  makeSelectMonitorErrorMessage,
};
