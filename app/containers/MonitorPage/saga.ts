import { call, put, select } from 'redux-saga/effects';
import moment from 'moment';
import {all, takeLatest} from '@redux-saga/core/effects';
import ActionTypes, {API_MONITOR_ENDPOINT, API_UPDATE_PATIENT_REVIEW, ROWS_PER_PAGE} from './constants';
import {makeSelectMonitorData, makeSelectMonitorSearchParams, makeSelectMonitorSelectedPage} from './selectors';
import {getData, putData} from '../../utils/network';
import {
    loadMonitorDataFailureAction,
    loadMonitorDataSuccessAction,
    setMonitorLoadingAction, setMonitorPatientReviewAction, setMonitorPatientReviewFailureAction,
    setTotalResultsAction,
} from './actions';
import {getPatients} from '../../models/model';
import {MonitorSearchParams} from './types';

function* actionWatcher() {
    yield takeLatest(ActionTypes.LOAD_MONITOR_DATA_ACTION, loadMonitorPageData);
    yield takeLatest(ActionTypes.TOGGLE_PATIENT_REVIEWED_ACTION, togglePatientReviewed);
}

export default function* monitorSaga() {
    yield all([actionWatcher()]);
}

function* loadMonitorPageData() {
    yield put(setMonitorLoadingAction(true));
    // get current page and items pr page
    const page = yield select(makeSelectMonitorSelectedPage());
    const searchFilters = yield select(makeSelectMonitorSearchParams());
    const limit = ROWS_PER_PAGE;

    // calculate time range
    const now = moment();
    const to = now.format('YYYY-MM-DD');
    const from = now.subtract(100, 'days').format('YYYY-MM-DD');
    const filters = getFiltersObject(searchFilters);

    const response = yield call(getData, API_MONITOR_ENDPOINT, {from, to, page, limit, ...filters});

    if (!response.error) {
        const data = getPatients(response.data.data);
        yield put(setTotalResultsAction(response.data.total));
        yield put(loadMonitorDataSuccessAction(data));
    } else {
        yield put(loadMonitorDataFailureAction(response.error));
    }
    yield put(setMonitorLoadingAction(false));
}

function* togglePatientReviewed(action) {
    const data = yield select(makeSelectMonitorData());
    const patient = data.find(patient => patient.patientId === action.payload);
    const isReviewed = !patient.reviewed;
    yield put(setMonitorPatientReviewAction(patient.patientId, isReviewed, ''));
    const response = yield call(putData,
        `${API_MONITOR_ENDPOINT}/${API_UPDATE_PATIENT_REVIEW}/${action.payload}?reviewed=${isReviewed}`,
        {});
    if (response.error) {
        yield put(setMonitorPatientReviewAction(patient.patientId, !isReviewed, response.error));

    }
}


const getFiltersObject = (searchParams: MonitorSearchParams | null) => {
    const filters: any = {};
    if (searchParams) {
        if (searchParams.searchTextValue) {
            filters['search'] = searchParams.searchTextValue;
        }
        if (searchParams.reviewed || searchParams.reviewed === null) {
            filters['reviewed'] = searchParams.reviewed;
        }
    }
    return filters;
};


