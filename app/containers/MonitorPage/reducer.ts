/*
 *
 * MonitorPage reducer
 *
 */
import {fromJS} from 'immutable';
import ActionTypes from './constants';
import {ContainerActions, ContainerState} from './types';

export const initialState: ContainerState = {
  data: [],
  page: 1,
  loading: false,
  total: 0,
  searchParams: {
    searchTextValue: '',
    reviewed: null,
    isFiltering: false,
  },
  errorMessage: '',
};

function monitorReducer(
  state: ContainerState = initialState,
  action: ContainerActions,
): ContainerState {
  switch (action.type) {
    case ActionTypes.DEFAULT_ACTION:
      return state;
    case ActionTypes.SET_MONITOR_LOADING:
      return {...state, loading: action.payload};
    case ActionTypes.LOAD_MONITOR_DATA_SUCCESS_ACTION:
      return {...state, data: action.payload};
    case ActionTypes.LOAD_MONITOR_DATA_FAILURE_ACTION:
      return {...state, data: []};
    case ActionTypes.SET_TOTAL_RESULTS_ACTION:
      return {...state, total: action.payload};
    case ActionTypes.SET_SEARCH_PARAMS_ACTION:
      return {...state, searchParams: action.payload};
    case ActionTypes.SET_PAGE_NUMBER_ACTION:
      return {...state, page: action.payload};
    case ActionTypes.SET_PATIENT_REVIEWED_ACTION:
      const patient = state.data.find(patient => patient.patientId === action.payload.patientId);
      if (patient) {
        const index = state.data.indexOf(patient);
        state.data[index].reviewed = action.payload.reviewed;
        return ({
          ...state,
          data: [
            ...state.data.slice(0, index),
            patient,
            ...state.data.slice(index + 1),
          ],
          errorMessage: action.payload.errorMessage,
        });
      }
      return state;
    case ActionTypes.CLEAN_MONITOR_PAGE_MESSAGE:
      return {
        ...state,
        errorMessage: '',
      };
    default:
      return state;
  }
}

export default monitorReducer;
