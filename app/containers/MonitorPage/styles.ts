import styled from 'styled-components';
import styles from 'styles/values';

const ButtonWrapper = styled.div`
display: flex;
    flex-direction: row;
    justify-content: center;
    height: 54px;
    color: ${styles.colors.TEXT.WHITE};
    background-color:  #38ABB6;
    font-size: 20px;
    font-family: Roboto-Regular;
    font-weight: normal;
    border-radius: 6px;
    border: 1px solid #38ABB6;
    opacity: 1;
    cursor: pointer;
            @media ${styles.sizes.SMALL_SCREEN}{
  margin-right: ${styles.sizes.APP_SIDE_PADDING_RIGHT_SMALL_SCREEN};
  margin-left: ${styles.sizes.APP_SIDE_PADDING_LEFT_SMALL_SCREEN};
 }
   @media ${styles.sizes.MEDIUM_SCREEN}{
   margin-right: ${styles.sizes.APP_SIDE_PADDING_RIGHT};
  margin-left: ${styles.sizes.APP_SIDE_PADDING_LEFT};
 }
   @media ${styles.sizes.LARGE_SCREEN}{
   margin-right: ${styles.sizes.APP_SIDE_PADDING_RIGHT_LARGE_SCREEN};
  margin-left: ${styles.sizes.APP_SIDE_PADDING_LEFT_LARGE_SCREEN};
  }
      margin-top: 6.25vh;
:hover{
  background-color: #5CBAC3;
}
`;

const Img = styled.img`
`;

const Div = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    padding-left: 10px;
    padding-right: 10px;
`;
export {ButtonWrapper, Img, Div};
