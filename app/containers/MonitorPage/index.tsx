/*
 *
 * MonitorPage
 *
 */

import React, { useCallback, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { FormattedMessage } from 'react-intl';
import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import reducer from './reducer';
import saga from './saga';
import { ButtonWrapper, Img, Div } from './styles';
import {Layout, Pagination, Search} from '../../components/common';
import { Patient } from '../../models/local';
import PatientMonitor from '../../components/PatientMonitor';
import { withRouter, RouteComponentProps } from 'react-router';
import routes from '../../routes';
import {
  loadMonitorDataAction,
  loadMonitorDataSuccessAction,
  setMonitorPageAction,
  setMonitorSearchParamsAction,
  toggleMonitorPatientReviewAction,
  cleanMessage,
} from './actions';
import {
  makeSelectMonitorData,
  makeSelectMonitorLoading,
  makeSelectMonitorSelectedPage,
  makeSelectMonitorTotal,
  makeSelectMonitorErrorMessage,
  makeSelectMonitorSearchParams,
} from './selectors';
import { ROWS_PER_PAGE } from './constants';
import { StyledLinerProgress } from '../../components/common/LinerProgress';
import moment from 'moment';
import ServerErrorMessage from '../../components/common/ServerErrorMessage';
import { toggleSideBar } from '../SideBar/actions';
import Button from '../../components/Button';
import { PatientGraphBody } from '../../components/PatientMonitor/styles';
const arrowUpIcon = require('images/arrowUpIcon.svg');
const stateSelector = createStructuredSelector({
  data: makeSelectMonitorData(),
  page: makeSelectMonitorSelectedPage(),
  total: makeSelectMonitorTotal(),
  loading: makeSelectMonitorLoading(),
  errorMessage: makeSelectMonitorErrorMessage(),
  searchParams: makeSelectMonitorSearchParams(),
});

interface Props extends RouteComponentProps {}

function Monitor(props: Props) {
  const { history } = props;
  const dispatch = useDispatch();
  const { data, page, total, loading, errorMessage, searchParams } = useSelector(
    stateSelector,
  );
  useInjectReducer({ key: 'monitor', reducer: reducer });
  useInjectSaga({ key: 'monitor', saga: saga });
  const loadPage = (pageNumber: number) => {
    dispatch(setMonitorPageAction(pageNumber));
    dispatch(loadMonitorDataAction());
  };
  useEffect(() => {
    dispatch(toggleSideBar(false));
    window.scrollTo(0, 0);
  });
  useEffect(() => {
    loadPage(1);
    return () => {
      // dispatch(setMonitorSearchParamsAction(null));
      dispatch(loadMonitorDataSuccessAction([]));
    };
  }, []);

  const onSearchClicked = (searchTextValue, reviewedPatients) => {
    let reviewed: string | null = null;
    if (reviewedPatients === 'reviewed_today') {
      reviewed = `${moment.utc(new Date()).format('YYYY-MM-DD')}T00:00:00.000Z`;
    } else if (reviewedPatients === 'not_reviewed_today') {
      reviewed = `1970-01-01T00:00:00.000Z`;
    }
    dispatch(
      setMonitorSearchParamsAction({
        searchTextValue,
        reviewed,
        isFiltering: (reviewed !== null || searchTextValue !== ''),
      }),
    );
    loadPage(1);
  };

  const onBackwardClicked = () => {
    if (page > 1) {
      loadPage(page - 1);
    }
  };

  const onFastBackwardClicked = () => {
    if (page > 1) {
      loadPage(1);
    }
  };

  const onForwardClicked = () => {
    if (page < Math.ceil(total / ROWS_PER_PAGE)) {
      loadPage(page + 1);
    }
  };

  const onFastForwardClicked = () => {
    if (page < Math.ceil(total / ROWS_PER_PAGE)) {
      loadPage(Math.ceil(total / ROWS_PER_PAGE));
    }
  };

  const goToPatientInfo = (patientId: number) => {
    const path = routes.patient.path.replace(':id', `${patientId}`);
    history.push(path);
  };

  const getPatients = (): JSX.Element[] => {
    if (!data) {
      return [];
    }
    return data.map((patient: Patient) => (
      <PatientMonitor
        key={`patient_${patient.patientId}`}
        patient={patient}
        onPatientReviewClicked={patientReviewed}
        onPatientSelected={goToPatientInfo}
      />
    ));
  };
  const onDoneClick = useCallback(() => {
    dispatch(cleanMessage());
  }, [dispatch]);
  const patientReviewed = (patientId: number) =>
    dispatch(toggleMonitorPatientReviewAction(patientId));

  return (
    <Layout title={'card_title_monitor'}>
      <ServerErrorMessage message={errorMessage} onDoneClick={onDoneClick} />
      <Search
          onSearchClicked={onSearchClicked}
          reviewedPatients={[]}
          searchParams={searchParams}
      />
      {total && page ? (
        <Pagination
          totalPages={Math.ceil(total / ROWS_PER_PAGE)}
          rawPerPage={ROWS_PER_PAGE}
          pageNumber={total === 0 ? 0 : page}
          onBackwardClicked={onBackwardClicked}
          onFastBackwardClicked={onFastBackwardClicked}
          onFastForwardClicked={onFastForwardClicked}
          onForwardClicked={onForwardClicked}
        />
      ) : (
        ''
      )}
      {loading ? (
        <StyledLinerProgress color="secondary" />
      ) : (
        <>
          {getPatients()}
          <ButtonWrapper
            onClick={() => {
              window.scrollTo(0, 0);
            }}
          >
            <Img src={arrowUpIcon} />
            <Div>
              <FormattedMessage id="GOT_TO_TOP" />
            </Div>
            <Img src={arrowUpIcon} />
          </ButtonWrapper>
        </>
      )}
    </Layout>
  );
}

export default withRouter(Monitor);
