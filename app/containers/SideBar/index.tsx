/*
 *
 * SideBar
 *
 */

import React, {memo, useState, Fragment} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectSideBar, {makeSelectOpenMenu, makeSelectSelectedTabId} from './selectors';
import reducer from './reducer';
import saga from './saga';
import {
  Img, Span,
  StyledDivider,
  StyledDrawer,
  StyledListItem,
  StyledListItemIcon,
  StyledListItemText,
  useStyles,
  StyledSelectedListItemText,
  StyledList,
    StyledDialogContent,
  VersionWrapper,
} from './styles';
import {DialogContent, ListItem, ListItemIcon, Modal} from '@material-ui/core';
import {
  aboutUs,
  closeMenu,
  contactUs,
  helpId, logOut,
  mediclStaffManagement,
  patientManagement,
  privacy,
  sideBarlinksArr,
  sideBarsecondlinksArr, sideBarSignIn, signIn,
  terms,
} from './constants';
import {FormattedMessage} from 'react-intl';
import {RouteComponentProps, withRouter} from 'react-router';
import {about, contact, help, login, medicalStaff, patients, privacyNotice, termsOfService} from '../../routes/paths';
import {logout, selectTabAction, toggleSideBar} from './actions';
import MessageCard from '../../components/MessageCard';
import {makeSelectGetUserRole} from '../AuthProvider/selectors';
import {stringToNumber} from '../../utils/commonFunc';
import {makeSelectStatusAll} from '../App/selectors';
const exitIcon = require('images/exitIcon.svg');
const packageJson = require('../../../package.json');

export const stateSelector = createStructuredSelector({
  sideBar: makeSelectSideBar(),
  selectedTabId: makeSelectSelectedTabId(),
  openMenu: makeSelectOpenMenu(),
  role: makeSelectGetUserRole(),
  statusAll: makeSelectStatusAll(),
});

interface Props extends RouteComponentProps {
}

function SideBar(props: Props) {
  // Warning: Add your key to RootState in types/index.d.ts file
  useInjectReducer({ key: 'sideBar', reducer: reducer });
  useInjectSaga({ key: 'sideBar', saga: saga });
  const { selectedTabId, openMenu, role, statusAll  } = useSelector(stateSelector);
  const dispatch = useDispatch();
  const classes = useStyles();
  const { history} = props;
  const env = window['_env_'];
  const [isLogOut, setIsLogOut] = useState(false);
  const handleDrawerClose = () => {
    dispatch(toggleSideBar(false));
  };
  const onConfirmLogOut = () => {
    dispatch(logout());

  };
  const onCancelLogOut = () => {
    setIsLogOut(false);
  };
  const getVersion = () => {
    const env = window['_env_'];
    if (statusAll && statusAll.length > 0) {
      statusAll.join('_');
      return(`version ${packageJson && packageJson.version}.${env.BUILD}_${statusAll.join('_')}`);
    } else {
      return(`version ${packageJson && packageJson.version}`);
    }
  };
  const handleClick = (id: string) => {
    dispatch(selectTabAction(id));
    handleDrawerClose();
    switch (id) {
      case patientManagement:
        history.push(`${patients}`);
        break;
      case mediclStaffManagement:
        history.push(`${medicalStaff}`);
        break;
      case helpId:
        history.push(`${help}`);
        break;
      case contactUs:
        history.push(`${contact}`);
        break;
      case aboutUs:
        history.push(`${about}`);
        break;
      case privacy:
        history.push(`${privacyNotice}`);
        break;
      case terms:
        history.push(`${termsOfService}`);
        break;
      case logOut:
          setIsLogOut(true);
          break;
      case signIn:
        history.push(`${login}`);
    }
  };

  return (
      <StyledDrawer
          className={classes.drawer}
          variant="persistent"
          anchor="right"
          open={openMenu}

      >
        <div className={classes.drawerHeader} onClick={handleDrawerClose}>

          <ListItem>
            <ListItemIcon>
              <Img width="38.7px" height="38.7px" src={closeMenu}/>
            </ListItemIcon>
          </ListItem>
        </div>
        <StyledDivider/>
        <StyledList>
          {sideBarlinksArr.map((item, index) => (
              stringToNumber(item.role) <= stringToNumber(role) ?  <div key={item.id}>
                <StyledListItem
                    className={ item.id === selectedTabId ?
                        classes.SelectedListItem :
                        classes.ListItem
                    }
                    onClick={() => handleClick(item.id)}
                    button key={item.id}>
                  <StyledListItemIcon>
                    <Img
                         width="20px"
                         height="20px" src={item.src} opacity={
                      item.id === selectedTabId ? '1' : '0.58'}/>
                  </StyledListItemIcon>
                  {item.id === selectedTabId ?
                      < StyledSelectedListItemText
                          primary={<FormattedMessage id={item.text}/>}
                      />
                      :
                      <StyledListItemText
                          primary={<FormattedMessage id={item.text}/>}
                      />
                  }
                </StyledListItem>
                <StyledDivider/>
              </div> :
              <Fragment key={item.id}/>
          ))}
        </StyledList>

        <StyledList>
          {sideBarSignIn.map((item, index) => (
              stringToNumber(item.role) === stringToNumber(role) ?  <div key={item.id}>
                    <StyledListItem
                        className={ item.id === selectedTabId ?
                            classes.SelectedListItem :
                            classes.ListItem
                        }
                        onClick={() => handleClick(item.id)}
                        button
                        key={item.id}>
                      <StyledListItemIcon>
                        <Img  transform="rotateY(180deg)" width="20px" height="20px" src={item.src} opacity={
                          item.id === selectedTabId ? '1' : '0.58'}/>
                      </StyledListItemIcon>
                      {item.id === selectedTabId ?
                          < StyledSelectedListItemText
                              primary={<FormattedMessage id={item.text}/>}
                          />
                          :
                          <StyledListItemText
                              primary={<FormattedMessage id={item.text}/>}
                          />
                      }
                    </StyledListItem>
                    <StyledDivider/>
                  </div> :
                  <  Fragment key={index}/>
          ))}
        </StyledList>


        <Span>
          <StyledDivider/>
          {sideBarsecondlinksArr.map((item, index) => (
              <div key={item.id}>
                <StyledListItem
                    button key={item.id}
                    onClick={() => handleClick(item.id)}
                    className={ item.id === selectedTabId ?
                        classes.SelectedListItem :
                        classes.ListItem
                    }
                >
                  {item.id === selectedTabId ?

                      <StyledSelectedListItemText
                          primary={<FormattedMessage id={item.text}/>}
                      />
                      :
                      <StyledListItemText
                          primary={<FormattedMessage id={item.text}/>}
                      />
                  }
                </StyledListItem>
                <StyledDivider/>
              </div>
          ))}
        </Span>

        <Modal
            open={isLogOut}
            onClose={onCancelLogOut}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            disableBackdropClick
        >
          <StyledDialogContent>
          <MessageCard
              src={exitIcon}
              messageTitle={'ARE_YOU_SURE_YOU_WANT_TO_LOG_OUT'}
              message={undefined}
              buttonTitle={'LOG_OUT'}
              messageValue={undefined}
              actionText={'CANCEL'}
              onActionClick={onCancelLogOut}
              onButtonClick={onConfirmLogOut}
              maxWidth="368px"
              isError={false}
          />
          </StyledDialogContent>
        </Modal>
        <VersionWrapper>
          {getVersion()}
        </VersionWrapper>

      </StyledDrawer>
  );
}

export default memo (withRouter(SideBar));
