import styled from 'styled-components';
import {
  createStyles, DialogContent,
  Divider,
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Theme,
  withStyles,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { drawerWidth } from './constants';
import styles from 'styles/values';
const Span = styled.span`
  position: absolute;
  bottom: 6%;
  width: 100%;
`;
const VersionWrapper = styled.span`

  position: absolute;
  bottom: 5px;
  width: 100%;
      font-size: 13px;
       padding-left: 29.35px;
       color: #FFFFFF;
`;
const Img = styled.img<{
  opacity?: string;
  transform?: string;
  width: string;
  height: string;
}>`
  opacity: ${(props) => (props.opacity ? (props) => props.opacity : 1)};
  cursor: pointer;
  width: ${(props) => props.width};
  height: ${(props) => props.height};
  transform: ${(props) =>
    props.transform ? props.transform : 'rotateY(0deg)'};
`;
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
    },
    drawerHeader: {
      display: 'flex',
      alignItems: 'right',
      justifyContent: 'flex-start',
      height: styles.sizes.HEADER_HEIGHT,
    },
    ListItem: {
      'backgroundColor': 'rgba(56, 171, 182, 1)',
      '&:hover': {
        backgroundColor: '#5CBAC3',
      },
    },
    SelectedListItem: {
      'backgroundColor': '#30A1AD',
      '&:hover': {
        backgroundColor: '#5CBAC3',
      },
    },
    ListItemText: {
      fontWeight: 'normal',
      fontFamily: ' Roboto-Regular',
      fontSize: '18px',
      letterSpacing: '0.72px',
    },
    SelectedListItemText: {},
  }),
);

const StyledDivider = withStyles(() => ({
  root: {
    backgroundColor: '#FDFDFD',
  },
}))(Divider);

// @ts-ignore
const StyledDrawer = withStyles(() => ({
  paper: {
    background: '#38ABB6 0% 0% no-repeat padding-box',
    boxShadow: '0px 3px 6px #00000029',
    border: '1px solid #EAEAEA',
    opacity: '1',
    width: drawerWidth,
  },
}))(Drawer);

const StyledList = withStyles(() => ({
  root: {
    paddingTop: '0px',
    paddingBottom: '0px',
  },
}))(List);

const StyledListItemIcon = withStyles(() => ({
  root: {
    width: '20px',
    height: '20px',
    minWidth: '20px',
    marginRight: '43.15px',
  },
}))(ListItemIcon);
const StyledListItem = withStyles(() => ({
  root: {
    paddingTop: '0px',
    paddingBottom: '0px',
    height: '55px',
    paddingLeft: '29.35px',
    backgroundColor: 'rgba(56, 171, 182, 1,)',
    width: '100%',
  },
}))(ListItem);
const StyledListItemText = withStyles(() => ({
  primary: {
    color: '#FFFFFF',
    fontWeight: 'normal',
    fontFamily: ' Roboto-Regular',
    fontSize: '18px',
    letterSpacing: '0.72px',
  },
}))(ListItemText);
const StyledSelectedListItemText = withStyles(() => ({
  primary: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontFamily: ' Roboto-Regular',
    fontSize: '18px',
    letterSpacing: '0.72px',
  },
}))(ListItemText);

const StyledDialogContent = withStyles(() => ({
  root: {
    height: '100%',
  },
}))(DialogContent);

export {
  StyledListItemText,
  Span,
  StyledListItemIcon,
  Img,
  StyledListItem,
  StyledDivider,
  StyledDrawer,
  StyledList,
  StyledSelectedListItemText,
  StyledDialogContent,
  VersionWrapper,
};
