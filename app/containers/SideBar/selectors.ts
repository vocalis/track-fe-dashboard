import { createSelector } from 'reselect';
import { ApplicationRootState } from 'types';
import { initialState } from './reducer';

/**
 * Direct selector to the sideBar state domain
 */

const selectSideBarDomain = (state: ApplicationRootState) => {
  return state.sideBar || initialState;
};

/**
 * Other specific selectors
 */

/**
 * Default selector used by SideBar
 */

const makeSelectSideBar = () =>
  createSelector(
    selectSideBarDomain,
    substate => {
      return substate;
    },
  );
const makeSelectSelectedTabId = () =>
    createSelector(
        selectSideBarDomain,
        substate => {
            return substate.selectedTabId;
        },
    );
const makeSelectOpenMenu = () =>
    createSelector(
        selectSideBarDomain,
        substate => {
            return substate.openMenu;
        },
    );
export default makeSelectSideBar;
export { selectSideBarDomain, makeSelectSelectedTabId, makeSelectOpenMenu };
