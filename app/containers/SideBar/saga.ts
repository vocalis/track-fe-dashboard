import { call, put, takeLatest, all } from 'redux-saga/effects';

import { postData } from '../../utils/network';
import ActionTypes, { API_LOGOUT_ENDPOINT } from './constants';
import { cleanUpUserData } from '../AuthProvider/actions';
import { clearDataFromStorage } from '../../utils/cookies';

function* actionWatcher() {
  yield takeLatest(ActionTypes.LOG_OUT, logOut);
}
function* logOut(action) {
  yield call(postData, API_LOGOUT_ENDPOINT, {});
  yield put(cleanUpUserData());
  clearDataFromStorage();
  window.location.replace('/login');
}

export default function* sideBarSaga() {
  yield all([actionWatcher()]);
}
