import sideBarReducer from '../reducer';
// import { someAction } from '../actions';
import { ContainerState } from '../types';

describe('sideBarReducer', () => {
  let state: ContainerState;
  beforeEach(() => {
    state = {
      selectedTabId: '',
      openMenu: false,
    };
  });

  it('returns the initial state', () => {
    const expectedResult = state;
    expect(sideBarReducer(undefined, {} as any)).toEqual(expectedResult);
  });

  /**
   * Example state change comparison
   *
   * it('should handle the someAction action correctly', () => {
   *   const expectedResult = {
   *     loading = true;
   *   );
   *
   *   expect(appReducer(state, someAction())).toEqual(expectedResult);
   * });
   */
});
