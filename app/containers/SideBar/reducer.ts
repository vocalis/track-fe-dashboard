/*
 *
 * SideBar reducer
 *
 */

import ActionTypes from './constants';
import {ContainerActions, ContainerState} from './types';

export const initialState: ContainerState = {
  selectedTabId: '',
  openMenu: false,
};

function sideBarReducer(
  state: ContainerState = initialState,
  action: ContainerActions,
): ContainerState {
  switch (action.type) {
    case ActionTypes.DEFAULT_ACTION:
      return state;
    case ActionTypes.SELECT_TAB_ACTION:
      return {...state, selectedTabId: action.payload.selectedTabId};
    case ActionTypes.TOGGLE_SIDE_BAR:
      return {...state, openMenu: action.payload.open};
    default:
      return state;
  }
}

export default sideBarReducer;
