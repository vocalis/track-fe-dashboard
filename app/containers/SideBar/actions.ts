/*
 *
 * SideBar actions
 *
 */

import { action } from 'typesafe-actions';
import {} from './types';

import ActionTypes from './constants';

export const defaultAction = () => action(ActionTypes.DEFAULT_ACTION);
export const selectTabAction = (selectedTabId: string) => action(ActionTypes.SELECT_TAB_ACTION, {selectedTabId});
export const toggleSideBar = (open: boolean) => action(ActionTypes.TOGGLE_SIDE_BAR, {open});
export const logout = () => action(ActionTypes.LOG_OUT);
