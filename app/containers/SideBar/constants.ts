/*
 *
 * SideBar constants
 *
 */

import {userRole} from '../../utils/constants';

enum ActionTypes {
  DEFAULT_ACTION = 'app/SideBar/DEFAULT_ACTION',
  SELECT_TAB_ACTION = 'app/SideBar/SELECT_TAB_ACTION',
  TOGGLE_SIDE_BAR = 'app/SideBar/TOGGLE_SIDE_BAR',
  LOG_OUT = 'app/SideBar/LOG_OUT',
}
export const API_LOGOUT_ENDPOINT = 'logout';
export const patientManagement = 'patient_Management';
export const mediclStaffManagement = 'medical_Staff_Management';
export const helpId = 'help';
export const contactUs = 'contact';
export const aboutUs = 'about';
export const logOut = 'logOut';
export const signIn = 'signIn';
export const privacy = 'privacy_Notice';
export const terms = 'terms_of_Service';

export const closeMenu = require('images/closeMenu.svg');
const help = require('images/small help icon.svg');
const about = require('images/about icon.svg');
const contact = require('images/main_ icon.svg');


const mStaffSideBar = require('images/Medical_staff_small_sideBar.svg');
const helpSideBar = require('images/Help_sideBar.svg');
const aboutSideBar = require('images/about_us_sideBar.svg');
const contactSideBar = require('images/Contact_Us_sideBar.svg');
const pManagementSideBar = require('images/Patient_Management_small.svg');
const signOutSideBar = require('images/sign_out_small_Icon.svg');
export const drawerWidth = 326;

export const appHeaderLinksArr = [
  {
    id: aboutUs,
    src: about,
    text: 'ABOUT_US_HEADER',
  },
  {
    id: contactUs,
    src: contact,
    text: 'CONTACT_US_HEADER',
  },
  {
    id: helpId,
    src: help,
    text: 'HELP_HEADER',
  },

];
export const sideBarlinksArr = [
  {
    id: patientManagement,
    src: pManagementSideBar,
    text: 'PATIENT_MANAGEMENT',
    needAuthenticated: true,
    role: userRole.staff,
  },
  {
    id: mediclStaffManagement,
    src: mStaffSideBar,
    text: 'MEDICAL_STAFF',
    role: userRole.admin,
  },
  {
    id: helpId,
    src: helpSideBar,
    text: 'HELP_HEADER',
    role: userRole.none,
  },
  {
    id: contactUs,
    src: contactSideBar,
    text: 'CONTACT_US_HEADER',
    needAuthenticated: false,
    role: userRole.none,
  },
  {
    id: aboutUs,
    src: aboutSideBar,
    text: 'ABOUT_US_HEADER',
    role: userRole.none,
  },
  {
    id: logOut,
    src: signOutSideBar,
    text: 'LOG_OUT',
    role: userRole.patient,
  },
];


export const sideBarsecondlinksArr = [
  {
    id: privacy,
    text: 'PRIVACY_NOTICE_2',
    role: userRole.none,
  },
  {
    id: terms,
    text: 'TERMS_OF_SERVICE_2',
    role: userRole.none,
  },
];

export const sideBarSignIn = [
  {
    id: signIn,
    text: 'SIGN_IN',
    role: userRole.none,
    src: signOutSideBar,
  },
];

export const appFooterLinksArr = [
  {
    id: helpId,
    text: 'HELP',
  },
  {
    id: contactUs,
    text: 'CONTACT_US',
  },
  {
    id: privacy,
    text: 'PRIVACY_NOTICE',
  },
  {
    id: terms,
    text: 'TERMS_OF_SERVICE',
  },
  {
    id: aboutUs,
    text: 'ABOUT_US_HEADER',
  },


];


export default ActionTypes;
