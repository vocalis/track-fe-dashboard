import { ActionType } from 'typesafe-actions';
import * as actions from './actions';
import { ApplicationRootState } from 'types';

/* --- STATE --- */
interface SideBarState {
  readonly  selectedTabId: string;
  readonly  openMenu: boolean ;
}

/* --- ACTIONS --- */
type SideBarActions = ActionType<typeof actions>;

/* --- EXPORTS --- */
type RootState = ApplicationRootState;
type ContainerState = SideBarState;
type ContainerActions = SideBarActions;

export { RootState, ContainerState, ContainerActions };
