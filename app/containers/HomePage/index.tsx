/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import * as React from 'react';
import {HomePageWrapper, VocalisLogo, CardsWrapper, HomePageBottomLines} from './styles';
import routes from '../../routes';
import HomeCard from '../../components/HomeCard';
import {Link} from 'react-router-dom';
import FotterLinks from '../../components/FotterLinks';
const logo = require('images/app_logo.svg');

const triageIcon = require('images/ic_triage.svg');
const monitorIcon = require('images/ic_monitor.svg');
const screenIcon = require('images/ic_screen.svg');
const lines = require('images/home_lines.svg');

interface CardData {
    path: string;
    icon: any;
    title: string;
    subtitle: string;
}

const cards = [
    {
        path: routes.triage,
        icon: triageIcon,
        title: 'card_title_triage',
        subtitle: 'card_subtitle_triage',
    },
    {
        path: routes.monitor,
        icon: monitorIcon,
        title: 'card_title_monitor',
        subtitle: 'card_subtitle_monitor',
    },
    {
        path: routes.screen,
        icon: screenIcon,
        title: 'card_title_screen',
        subtitle: 'card_subtitle_screen',
    },
];


const HomePage = () => {
  return (
    <HomePageWrapper>
        <VocalisLogo src={logo}/>
        <CardsWrapper>
            {cards.map(card => (
                <Link
                    key={card.title}
                    to={card.path.path}
                    style={{ textDecoration: 'none' }}>
                    <HomeCard
                        icon={card.icon}
                        title={card.title}
                        subtitle={card.subtitle}
                    />
                </Link>))}
        </CardsWrapper>

        <HomePageBottomLines src={lines} />
    </HomePageWrapper>
  );
};

export default HomePage;
