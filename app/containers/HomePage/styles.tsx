import styled from 'styled-components';
import styles from 'styles/values';

const HomePageWrapper = styled.div`
  width: 100%;
  height: 100%;
  max-height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  background-image:
  linear-gradient(to bottom right, ${styles.colors.GRADIENT.HOME.START}, ${styles.colors.GRADIENT.HOME.END});
`;

const VocalisLogo = styled.img`
  width: 13.385vw;
  height: 50px;
  margin-top: 82.5px;
  margin-bottom: 76.5px;
`;

const CardsWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const HomePageBottomLines = styled.img`
    width: 100%;
    position: absolute;
    bottom: 0;
    left: 0;
    height: 108px;
`;

export {HomePageWrapper, VocalisLogo, CardsWrapper, HomePageBottomLines};
