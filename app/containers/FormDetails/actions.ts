/*
 *
 * FormDetails actions
 *
 */

import { action } from 'typesafe-actions';
import {} from './types';

import ActionTypes, {emptyUserForm} from './constants';
import {MedicalStaffDetails, PatientDetails} from '../../models/local';

export const defaultAction = () => action(ActionTypes.DEFAULT_ACTION);
export const editPatientAction = (values: object , isLoading: boolean, editPage: boolean, id: string) =>
    action(ActionTypes.EDIT_ACTION, {values, isLoading, editPage, id});
export const editPatientActionSucess = (isLoading: boolean, id: number) =>
    action(ActionTypes.EDIT_ACTION_SUCESS, {isLoading, id});
export const updatePatientActionSucess = (isLoading: boolean, id: number, message: string) =>
    action(ActionTypes.UPDATE_ACTION_SUCESS, {isLoading, id, message});
export const editPatientActionFails = (isLoading: boolean, error: string) =>
    action(ActionTypes.EDIT_ACTION_FAILS, {isLoading, error});

export const getPatientDetailsAction = (isLoading: boolean, patientId: string) =>
    action(ActionTypes.GET_PATIENT_DETAILS_ACTION, {isLoading, patientId});
export const getPatientDetailsActionSucess = (patientDetails: PatientDetails, isLoading: boolean) =>
    action(ActionTypes.GET_PATIENT_DETAILS_ACTION_SUCESS, {patientDetails, isLoading});
export const getPatientDetailsActionFails = (isLoading: boolean) =>
    action(ActionTypes.GET_PATIENT_DETAILS_ACTION_FAILS, {isLoading});

export const getMedicalStaffDetailsAction = (isLoading: boolean, medicalStaffId: string) =>
    action(ActionTypes.GET_MEDICAL_STAFF_DETAILS_ACTION, {isLoading, medicalStaffId});
export const getMedicalStaffDetailsActionSucess = (medicalStaffDetails: MedicalStaffDetails, isLoading: boolean) =>
    action(ActionTypes.GET_MEDICAL_STAFF_DETAILS_ACTION_SUCESS, {medicalStaffDetails, isLoading});
export const getMedicalStaffDetailsActionFails = (isLoading: boolean) =>
    action(ActionTypes.GET_MEDICAL_STAFF_DETAILS_ACTION_FAILS, {isLoading});

export const editMedicalStaffAction = (values: object , isLoading: boolean, editPage: boolean, id: string) =>
    action(ActionTypes.EDIT_MEDICAL_STAFF_ACTION, {values, isLoading, editPage, id});
export const editMedicalStaffActionSucess = (isLoading: boolean, id: number) =>
    action(ActionTypes.EDIT_MEDICAL_STAFF_ACTION_SUCESS, {isLoading, id});
export const updateMedicalStaffActionSucess = (isLoading: boolean, id: number , message: string) =>
    action(ActionTypes.UPDATE_MEDICAL_STAFF_ACTION_SUCESS, {isLoading, id, message});
export const editMedicalStaffActionFAILS = (isLoading: boolean, error: string) =>
    action(ActionTypes.EDIT_MEDICAL_STAFF_ACTION_FAILS, {isLoading, error});
export const cleanUserManagementError = () => action(ActionTypes.CLEAN_USER_MANAGEMENT_ERROR);
export const cleanUserManagementMessage = () => action(ActionTypes.CLEAN_USER_MANAGEMENT_MESSAGE);
export const cleanUpAction = (userData: PatientDetails | {}) =>
    action(ActionTypes.CLEANUP_ACTION, {userData});

export const changePasswordAction = (password: string, id: string, isMedicalStaffPage: boolean, isLoading) =>
    action(ActionTypes.CHANGE_PASSWORD_ACTION, {password, id, isMedicalStaffPage, isLoading});
export const changePasswordActionSucess = (isLoading: boolean, message: string) =>
    action(ActionTypes.CHANGE_PASSWORD_ACTION_SUCESS, {isLoading, message});
export const changePasswordActionFails = (isLoading: boolean, error) =>
    action(ActionTypes.CHANGE_PASSWORD_ACTION_FAILS, {isLoading, error});
