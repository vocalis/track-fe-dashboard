/*
 *
 * FormDetails
 *
 */

import React, { memo, useEffect, useRef, useState, Fragment } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  FormikWrapper,
  Contanier,
  Img,
  TitleWrapper,
  ActionWrapper,
  ButtonWrapper,
  ImgWrapper,
  Span,
  HeaderWrapper,
  Title,
  CancelWrapper,
  StyledDialogContent,
} from './styles';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectFormDetails, {
  makeSelectCreatedUserId,
  makeSelectError,
  makeSelectisLoading,
  makeSelectMessage,
  makeSelectPatientDetails,
  makeSelectMedicalStaffDetails,
} from './selectors';
import reducer from './reducer';
import saga from './saga';
import FormLayout from '../../components/FormLayout';
import { Formik, FormikProps, Form } from 'formik';
import { FormattedMessage } from 'react-intl';
import Button from '../../components/Button';
import {
  changePasswordAction,
  cleanUpAction,
  cleanUserManagementError,
  cleanUserManagementMessage,
  editMedicalStaffAction,
  editMedicalStaffActionFAILS,
  editPatientAction,
  editPatientActionFails,
  getMedicalStaffDetailsAction,
  getPatientDetailsAction,
} from './actions';
import _ from 'lodash';
import { RouteComponentProps, withRouter } from 'react-router';
import Layout from '../../components/common/Layout';
import { ValuesProps } from '../../models/local';
import {
  createMedicalStaff,
  createPatient,
  editMedicalStaff,
  editPatient,
  medicalStaff,
} from '../../routes/paths';
import { StyledLinerProgress } from '../../components/common/LinerProgress';
import MessageCard from '../../components/MessageCard';
import { Modal} from '@material-ui/core';
import {
  ContactTextFieldPropsArrayForMedicalStaff,
  ContactTextFieldPropsArrayForPatients,
  emptyUserForm,
  formValidation,
  PersonalTextFieldPropsArrayForMedicalStaff,
  PersonalTextFieldPropsArrayForPatients,
  StatusProps,
} from './constants';
import { toggleSideBar } from '../SideBar/actions';
const PatientManagementImage = require('images/PatientManagementImage1.svg');
const MedicalStaffManagementImage = require('images/MedicalStaffManagementImage1.svg');
const user = require('images/ic_triage.svg');
const exitIcon = require('images/exitIcon.svg');
const userUpdatedIcon = require('images/userUpdatedIcon.svg');
const notUpdatedIcon = require('images/notUpdatedIcon.svg');
const createUserIcon = require('images/userCreatedIcon.svg');
const notCreatedIcon = require('images/oopsIcon.svg');
const patientCreatedIcon = require('images/patientCreatedIcon.svg');
const stateSelector = createStructuredSelector({
  formDetails: makeSelectFormDetails(),
  patientDetails: makeSelectPatientDetails(),
  medicalStaffDetails: makeSelectMedicalStaffDetails(),
  isLoading: makeSelectisLoading(),
  message: makeSelectMessage(),
  error: makeSelectError(),
  createdId: makeSelectCreatedUserId(),
});

export let medicalStaffPage = false;
export let editPage = false;

interface Props extends RouteComponentProps {}

function FormDetails(props: Props) {
  const { match } = props;
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  switch (match.path) {
    case editMedicalStaff:
      medicalStaffPage = true;
      editPage = true;
      break;
    case editPatient:
      medicalStaffPage = false;
      editPage = true;
      break;
    case createPatient:
      medicalStaffPage = false;
      editPage = false;
      break;
    case createMedicalStaff:
      medicalStaffPage = true;
      editPage = false;
  }
  // Warning: Add your key to RootState in types/index.d.ts file
  useInjectReducer({ key: 'formDetails', reducer: reducer });
  useInjectSaga({ key: 'formDetails', saga: saga });
  const {
    patientDetails,
    medicalStaffDetails,
    isLoading,
    message,
    error,
    createdId,
  } = useSelector(stateSelector);
  const dispatch = useDispatch();
  const [userData, setUserData] = useState<{}>(emptyUserForm);
  const formRef = useRef(null) as any;
  const [isSubmitted, setIsSubmitted] = useState(false);
  const [disableButton, setDisableButton] = useState(true);
  const [isCanceled, setIsCanceled] = useState(false);

  // const passwordReExp = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{10,}$/
  // Minimum eight characters, at least one letter, one number and one special character:
  useEffect(() => {
    if (
      match &&
      match.params &&
      match.params['id'] &&
      editPage &&
      !medicalStaffPage
    ) {
      dispatch(getPatientDetailsAction(true, match.params['id']));
    }
    if (
      match &&
      match.params &&
      match.params['id'] &&
      editPage &&
      medicalStaffPage
    ) {
      dispatch(getMedicalStaffDetailsAction(true, match.params['id']));
    }
    return function cleanup() {
      dispatch(cleanUpAction({}));
    };
  }, []);
  useEffect(() => {
    dispatch(toggleSideBar(false));
  }, []);
  useEffect(() => {
    if (medicalStaffPage && Object.keys(medicalStaffDetails).length) {
      setUserData({ ...medicalStaffDetails });
    }
    if (!medicalStaffPage && Object.keys(patientDetails).length) {
      setUserData({ ...patientDetails });
    }
  }, [patientDetails, medicalStaffDetails]);
  const submitForm = () => {
    if (formRef && formRef.current && formRef.current.handleSubmit) {
      setIsSubmitted(true);
      formRef.current.handleSubmit();
    }
  };
  const onCancelClick = () => {
    if (isCanceled) {
      dispatch(cleanUpAction({}));
      history.back();
    } else {
      setIsCanceled(true);
    }
  };
  const onTryAgainClick = () => {
    dispatch(cleanUpAction(userData));
  };
  const onClickAddAnother = () => {
    setUserData(emptyUserForm);
    setIsSubmitted(false);
    dispatch(cleanUpAction({}));
  };
  const onClickDone = () => {
    dispatch(cleanUpAction({}));
    history.back();
  };
  const onClickContinue = () => {
    setIsCanceled(false);
  };
  const onClickContiueUpdating = () => {
    dispatch(cleanUserManagementError());
  };
  const onUpdatePasswordFinished = () => {
    dispatch(cleanUserManagementMessage());
  };
  const onChangePasswordClicked = (password) => {
    dispatch(
      changePasswordAction(password, match.params['id'], medicalStaffPage, true),
    );
  };
  const getMessageCard = () => {
    let src;
    let messageTitle;
    let messageText;
    let buttonTitle;
    let actionText;
    let onActionClick;
    let onButtonClick;
    let messageValue;
    let maxWidth = '100%';
    let isError = false;
    if (isCanceled) {
      if (editPage) {
        src = exitIcon;
        messageTitle = 'ARE_YOU_SURE_YOU_WANT_TO_LEAVE_THIS_PAGE';
        messageText = undefined;
        messageValue = undefined;
        buttonTitle = 'CONTINUE_EDITING';
        actionText = 'LEAVE_PAGE';
        onActionClick = onCancelClick;
        onButtonClick = onClickContinue;
        maxWidth = '368px';
      } else {
        src = exitIcon;
        messageTitle = 'ARE_YOU_SURE_YOU_WANT_TO_LEAVE_THIS_PAGE';
        messageText = undefined;
        messageValue = undefined;
        buttonTitle = 'CONTINUE_CREATING';
        actionText = 'LEAVE_PAGE';
        onActionClick = onCancelClick;
        onButtonClick = onClickContinue;
        maxWidth = '368px';
      }
    } else {
      if (medicalStaffPage) {
        if (editPage) {
          if (!error) {
            if (message) {
              if (message === 'Password Changed') {
                src = userUpdatedIcon;
                messageTitle = 'PASSWORD_UPDATED';
                messageText = 'YOU_CAN_KEEP_UPDATING_USER';
                buttonTitle = 'DONE';
                actionText = undefined;
                onActionClick = undefined;
                onButtonClick = onUpdatePasswordFinished;
              } else {
                src = userUpdatedIcon;
                messageTitle = 'USER_UPDATED';
                messageText = 'YOU_CAN_KEEP_UPDATING_USERS';
                messageValue = createdId;
                buttonTitle = 'DONE';
                actionText = undefined;
                onActionClick = undefined;
                onButtonClick = onClickDone;
              }
            }
          } else {
            if (error === 'updateWithoutChanges') {
              src = notUpdatedIcon;
              messageTitle = 'NONE_OF_THE_FIELDS_WERE_UPDATED';
              messageText = undefined;
              buttonTitle = 'DONE';
              actionText = 'TRY_AGAIN';
              onActionClick = onClickContiueUpdating;
              onButtonClick = onClickDone;
              maxWidth = '368px';
            } else {
              src = notCreatedIcon;
              messageTitle = 'OOOPS';
              messageText = 'SOMETHING_WENT_WRONG_USER_WAS_NOT_UPDATED';
              buttonTitle = 'TRY_AGAIN';
              actionText = 'BACK';
              onActionClick = onClickDone;
              onButtonClick = onTryAgainClick;
            }
          }
        } else {
          if (!error) {
            src = createUserIcon;
            messageTitle = 'USER_CREATED';
            messageText = 'YOU_CAN_CREATE_ANOTHER_USER';
            buttonTitle = 'DONE';
            messageValue = createdId;
            actionText = 'ADD_ANOTHER_USER';
            onActionClick = onClickAddAnother;
            onButtonClick = onClickDone;
          } else {
            src = notCreatedIcon;
            messageTitle = 'OOOPS';
            messageText = 'SOMETHING_WENT_WRONG_USER_WAS_NOT_CREATED';
            buttonTitle = 'TRY_AGAIN';
            actionText = 'BACK';
            onActionClick = onClickDone;
            onButtonClick = onTryAgainClick;
          }
        }
      } else {
        if (editPage) {
          if (!error) {
            if (message) {
              if (message === 'Password Changed') {
                src = userUpdatedIcon;
                messageTitle = 'PASSWORD_UPDATED';
                messageText = 'YOU_CAN_KEEP_UPDATING_PATIENT';
                buttonTitle = 'DONE';
                actionText = undefined;
                onActionClick = undefined;
                onButtonClick = onUpdatePasswordFinished;
              } else {
                src = userUpdatedIcon;
                messageTitle = 'PATIENT_UPDATED';
                messageText = 'YOU_CAN_KEEP_UPDATING_PATIENTS';
                messageValue = createdId;
                buttonTitle = 'DONE';
                actionText = undefined;
                onActionClick = undefined;
                onButtonClick = onClickDone;
              }
            }
          } else {
            if (error === 'updateWithoutChanges') {
              src = notUpdatedIcon;
              messageTitle = 'NONE_OF_THE_FIELDS_WERE_UPDATED';
              messageText = undefined;
              buttonTitle = 'DONE';
              actionText = 'TRY_AGAIN';
              onActionClick = onClickContiueUpdating;
              onButtonClick = onClickDone;
              maxWidth = '368px';
            } else {
              src = notCreatedIcon;
              messageTitle = 'OOOPS';
              messageText = 'SOMETHING_WENT_WRONG_PATIENT_WAS_NOT_UPDATED';
              buttonTitle = 'TRY_AGAIN';
              actionText = 'BACK';
              onActionClick = onClickDone;
              onButtonClick = onTryAgainClick;
            }
          }
        } else {
          if (!error) {
            src = patientCreatedIcon;
            messageTitle = 'PATIENT_CREATED';
            messageValue = createdId;
            messageText = 'YOU_CAN_CREATE_ANOTHER_PATIENT';
            buttonTitle = 'DONE';
            actionText = 'ADD_ANOTHER_PATIENT';
            onActionClick = onClickAddAnother;
            onButtonClick = onClickDone;
          } else {
            src = notCreatedIcon;
            messageTitle = 'OOOPS';
            messageText = 'SOMETHING_WENT_WRONG_PATIENT_WAS_NOT_CREATED';
            buttonTitle = 'TRY_AGAIN';
            actionText = 'BACK';
            onActionClick = onClickDone;
            onButtonClick = onTryAgainClick;
            isError = true;
          }
        }
      }
    }

    return (
        <Modal

            open={isCanceled || message !== '' || createdId > -1 || error !== ''}
            onClose={onCancelClick}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            disableBackdropClick
        >
          <StyledDialogContent>
          <MessageCard
              src={src}
              messageTitle={messageTitle}
              message={messageText}
              messageValue={messageValue}
              buttonTitle={buttonTitle}
              actionText={actionText}
              onActionClick={onActionClick}
              onButtonClick={onButtonClick}
              maxWidth={maxWidth}
              isError={isError}
          />
          </StyledDialogContent>
        </Modal>
    );
  };
  return (
    <Layout title="Form">

        {getMessageCard()}
      <HeaderWrapper>
        <ImgWrapper>
          <Img
            src={
              medicalStaffPage
                ? MedicalStaffManagementImage
                : PatientManagementImage
            }
            alt="managementPage"
          />
        </ImgWrapper>
        <TitleWrapper>
          <Span fontSize="18px" color="#707070" letterSpacing="0.72px">
            Home > &nbsp;
            <FormattedMessage
              id={
                medicalStaffPage
                  ? 'MEDICAL_STAFF_MANAGEMENT'
                  : 'PATIENTS_MANAGEMENT'
              }
            />
          </Span>
          <Title>
            {!medicalStaffPage && !editPage && (
              <FormattedMessage id="CREATE_PATIENT" />
            )}
            {!medicalStaffPage && editPage && (
              <FormattedMessage id="EDIT_PATIENT" />
            )}
            {medicalStaffPage && editPage && (
              <FormattedMessage id="EDIT_MEDICAL_STAFF" />
            )}
            {medicalStaffPage && !editPage && (
              <FormattedMessage id="CREATE_MEDICAL_STAFF" />
            )}
          </Title>
        </TitleWrapper>
        <ActionWrapper>
          <CancelWrapper onClick={onCancelClick}>
            <FormattedMessage id="CANCEL" />
          </CancelWrapper>
          <ButtonWrapper>
            <Button
              title={editPage ? 'UPDATE' : 'CREATE'}
              disableButton={disableButton}
              height="42px"
              width="216px"
              onClick={submitForm}
            />
          </ButtonWrapper>
        </ActionWrapper>
      </HeaderWrapper>
      {isLoading ? (
        <StyledLinerProgress color="secondary" />
      ) : (
        <Contanier>
          <FormikWrapper>
            <Formik
              innerRef={formRef}
              initialValues={userData}
              enableReinitialize={true}
              validationSchema={formValidation(editPage, medicalStaffPage)}
              validateOnChange={true}
              onSubmit={(values, element) => {
                if (!_.isEqual(userData, values)) {
                  if (medicalStaffPage) {
                    dispatch(
                      editMedicalStaffAction(
                        values,
                        true,
                        editPage,
                        match.params['id'],
                      ),
                    );
                  } else {
                    dispatch(
                      editPatientAction(
                        values,
                        true,
                        editPage,
                        match.params['id'],
                      ),
                    );
                  }
                  setUserData({ ...values });
                } else {
                  if (medicalStaffPage) {
                    dispatch(
                      editMedicalStaffActionFAILS(false, 'updateWithoutChanges'),
                    );
                  } else {
                    dispatch(
                      editPatientActionFails(false, 'updateWithoutChanges'),
                    );
                  }
                }
              }}
            >
              {(renderProps: FormikProps<ValuesProps>): JSX.Element => {
                const {
                  values,
                  setFieldValue,
                  errors,
                  dirty,
                  setValues,
                } = renderProps;

                setDisableButton(
                  Object.keys(errors).length !== 0 && isSubmitted,
                );
                return (
                  <Form>
                    <FormLayout
                      values={values}
                      setFieldValue={setFieldValue}
                      setValues={setValues}
                      errors={errors}
                      editPage={editPage}
                      onChangePasswordRequest={onChangePasswordClicked}
                      PersonalTextFieldPropsArray={
                        !medicalStaffPage
                          ? PersonalTextFieldPropsArrayForPatients
                          : PersonalTextFieldPropsArrayForMedicalStaff
                      }
                      ContactTextFieldPropsArray={
                        !medicalStaffPage
                          ? ContactTextFieldPropsArrayForPatients
                          : ContactTextFieldPropsArrayForMedicalStaff
                      }
                      isSubmitted={isSubmitted}
                      statusProps={StatusProps}
                    />
                  </Form>
                );
              }}
            </Formik>
          </FormikWrapper>
        </Contanier>
      )}
    </Layout>
  );
}

export default memo(withRouter(FormDetails));
