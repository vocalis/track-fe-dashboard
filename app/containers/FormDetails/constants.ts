/*
 *
 * FormDetails constants
 *
 */
import * as Yup from 'yup';

enum ActionTypes {
  DEFAULT_ACTION = 'app/FormDetails/DEFAULT_ACTION',
  EDIT_ACTION = 'app/FormDetails/EDIT_ACTION',
  EDIT_ACTION_SUCESS = 'app/FormDetails/EDIT_ACTION_SUCESS',
  UPDATE_ACTION_SUCESS = 'app/FormDetails/UPDATE_ACTION_SUCESS',
  EDIT_ACTION_FAILS = 'app/FormDetails/EDIT_ACTION_FAILS',
  GET_PATIENT_DETAILS_ACTION = 'app/FormDetails/GET_PATIENT_DETAILS_ACTION',
  GET_PATIENT_DETAILS_ACTION_SUCESS = 'app/FormDetails/GET_PATIENT_DETAILS_ACTION_SUCESS',
  GET_PATIENT_DETAILS_ACTION_FAILS = 'app/FormDetails/GET_PATIENT_DETAILS_ACTION_FAILS',

  EDIT_MEDICAL_STAFF_ACTION = 'app/FormDetails/EDIT_MEDICAL_STAFF_ACTION',
  EDIT_MEDICAL_STAFF_ACTION_SUCESS = 'app/FormDetails/EDIT_MEDICAL_STAFF_ACTION_SUCESS',
  UPDATE_MEDICAL_STAFF_ACTION_SUCESS = 'app/FormDetails/UPDATE_MEDICAL_STAFF_ACTION_SUCESS',
  EDIT_MEDICAL_STAFF_ACTION_FAILS = 'app/FormDetails/EDIT_MEDICAL_STAFF_ACTION_FAILS',
  GET_MEDICAL_STAFF_DETAILS_ACTION = 'app/FormDetails/GET_MEDICAL_STAFF_DETAILS_ACTION',
  GET_MEDICAL_STAFF_DETAILS_ACTION_SUCESS = 'app/FormDetails/GET_MEDICAL_STAFF_DETAILS_ACTION_SUCESS',
  GET_MEDICAL_STAFF_DETAILS_ACTION_FAILS = 'app/FormDetails/GET_MEDICAL_STAFF_DETAILS_ACTION_FAILS',
  CLEAN_USER_MANAGEMENT_MESSAGE = 'app/FormDetails/CLEAN_USER_MANAGEMENT_MESSAGE',
  CLEAN_USER_MANAGEMENT_ERROR = 'app/FormDetails/CLEAN_USER_MANAGEMENT_ERROR',
  CLEANUP_ACTION = 'app/FormDetails/CLEANUP_ACTION',
  CHANGE_PASSWORD_ACTION = 'app/FormDetails/CHANGE_PASSWORD_ACTION',
  CHANGE_PASSWORD_ACTION_SUCESS = 'app/FormDetails/CHANGE_PASSWORD_ACTION_SUCESS',
  CHANGE_PASSWORD_ACTION_FAILS = 'app/FormDetails/CHANGE_PASSWORD_ACTION_FAILS',
}

// moment().format('YYYY-MM-DD')
// const phoneReExp = /^\+?(972|0)(\-)?0?(([23489]{1}\d{7})|[5]{1}\d{8})$/;
const  phoneRegEXPMIN = /^[0-9]{10,}$/;
const  phoneRegEXPMAx = /^\d{0,20}$/;
const passwordRegExp = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
export const emptyUserForm = {
  firstName: '',
  username: '',
  lastName: '',
  email: '',
  birthDate: null,
  password: '',
  phone: '',
  status: 'Active',
};
export const API_PATIENTS_ENDPOINT = 'patients';
export const API_PATIENT_ENDPOINT = 'patient';
export const API_MEDICAL_STAFF = 'users';
export const API_CHANGE_USER_PASSWORD = 'users/changePassword';
export const API_CHANGE_PATIENT_PASSWORD = 'patients/changePassword';
export default ActionTypes;

export const PersonalTextFieldPropsArrayForPatients = [
  {
    id: 'username',
    label: 'USERNAME_*',
    placeholder: 'USERNAME',
    type: 'string',
    editable: false,
  },
  {
    id: 'firstName',
    label: 'FIRST_NAME_*',
    placeholder: 'FIRST_NAME_PLACEHOLDER',
    type: 'string',
    editable: true,
  },
  {
    id: 'lastName',
    label: 'LAST_NAME_*',
    placeholder: 'LAST_NAME_PLACEHOLDER',
    type: 'string',
    editable: true,
  },
  {
    id: 'birthDate',
    label: 'BIRTH_DATE',
    placeholder: 'BIRTH_DATE_PLACEHOLDER',
    type: 'date',
    editable: true,
  },
];
export const PersonalTextFieldPropsArrayForMedicalStaff = [
  {
    id: 'firstName',
    label: 'FIRST_NAME_*',
    placeholder: 'FIRST_NAME_PLACEHOLDER',
    type: 'string',
    editable: true,
  },
  {
    id: 'lastName',
    label: 'LAST_NAME_*',
    placeholder: 'LAST_NAME_PLACEHOLDER',
    type: 'string',
    editable: true,
  },
];

export const ContactTextFieldPropsArrayForPatients = [
  {
    id: 'email',
    label: 'EMAIL',
    placeholder: 'EMAIL_PLACEHOLDER',
    type: 'string',
    editable: true,
  },
  {
    id: 'phone',
    label: 'PHONE',
    placeholder: 'PHONE_PLACEHOLDER',
    type: 'string',
    editable: true,
  },
  {
    id: 'password',
    label: 'PASSWORD_*',
    placeholder: 'PASSWORD_PLACEHOLDER',
    type: 'string',
    editable: false,
  },
];
export const ContactTextFieldPropsArrayForMedicalStaff = [
  {
    id: 'email',
    label: 'EMAIL_*',
    placeholder: 'EMAIL_PLACEHOLDER',
    type: 'string',
    editable: false,
  },
  {
    id: 'password',
    label: 'PASSWORD_*',
    placeholder: 'PASSWORD_PLACEHOLDER',
    type: 'string',
    editable: false,
  },
];
export const StatusProps = {
  id: 'status',
  name: 'STATUS',
  values: ['Active', 'Not Active'],
};

// Yup.addMethod(Date, 'isDate', (args)=>{
//     debugger;
//         console.log(args)
// })
// matches(phoneReExp, 'Phone number is invalid')
const getYupShape = (editPage: boolean, medicalStaffPage: boolean) => {
  let username = Yup.string().required('USERNAME_VALIDATION');
  const firstName = Yup.string().required('FIRSTNAME_VALIDATION');
  const lastName = Yup.string().required('LASTNAME_VALIDATION');
  let email = Yup.string()
    .email('EMAIL_INVALID_VALIDATION')
    .required('EMAIL_VALIDATION');
  const phone = Yup.string()
      .matches(phoneRegEXPMIN, 'PHONE_MIN_VALIDATION')
        .matches(phoneRegEXPMAx, 'PHONE_MAX_VALIDATION');
  const birthDate = Yup.string()
    .nullable()
    .test('isDate', 'invalid Date', (value) => {
      if (value === 'Invalid date') {
        return false;
      } else {
        return true;
      }
    });

  let password = Yup.string()
    .matches(passwordRegExp, 'PASSWORD_INCLUDES_VALIDATION')
    .required('PASSWORD_VALIDATION')
    .min(8, 'PASSWORD_SHORT_VALIDATION');
  if (editPage) {
    password = Yup.string()
      .matches(passwordRegExp, 'PASSWORD_INCLUDES_VALIDATION')
      .min(8, 'PASSWORD_SHORT_VALIDATION');
    username = Yup.string();
  }
  if (!medicalStaffPage) {
    email = Yup.string().email('EMAIL_INVALID_VALIDATION');
  } else {
    username = Yup.string();
  }
  return {
    firstName,
    lastName,
    email,
    phone,
    password,
    birthDate,
    username,
  };
};

export const formValidation = (editPage: boolean, medicalStaffPage: boolean) =>
  Yup.object().shape(getYupShape(editPage, medicalStaffPage));
