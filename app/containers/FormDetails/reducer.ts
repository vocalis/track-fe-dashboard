/*
 *
 * FormDetails reducer
 *
 */

import ActionTypes from './constants';
import {ContainerActions, ContainerState} from './types';

export const initialState: ContainerState = {
  isLoading: false,
  message: '',
  error: '',
  patientDetails: {},
  medicalStaffDetails: {},
  id: -1,
};

function formDetailsReducer(
  state: ContainerState = initialState,
  action: ContainerActions,
): ContainerState {
  switch (action.type) {
    case ActionTypes.DEFAULT_ACTION:
      return state;
    case ActionTypes.EDIT_ACTION:
      return {...state, isLoading: action.payload.isLoading};
    case ActionTypes.EDIT_ACTION_SUCESS:
      return {...state,
              isLoading: action.payload.isLoading,
              id: action.payload.id,
              };
      case ActionTypes.UPDATE_ACTION_SUCESS:
          return {...state,
                  isLoading: action.payload.isLoading,
                  message: action.payload.message,
                  id: action.payload.id,
          };
    case ActionTypes.EDIT_ACTION_FAILS:
      return {...state,
              isLoading: action.payload.isLoading,
              error: action.payload.error,
          };
    case ActionTypes.GET_PATIENT_DETAILS_ACTION:
      return {...state, isLoading: action.payload.isLoading};
    case ActionTypes.GET_PATIENT_DETAILS_ACTION_SUCESS:
      return {...state, isLoading: action.payload.isLoading, patientDetails: action.payload.patientDetails};
    case ActionTypes.GET_PATIENT_DETAILS_ACTION_FAILS:
      return {...state, isLoading: action.payload.isLoading};

      case ActionTypes.EDIT_MEDICAL_STAFF_ACTION:
          return {...state, isLoading: action.payload.isLoading};
      case ActionTypes.EDIT_MEDICAL_STAFF_ACTION_SUCESS:
          return {...state,
                  isLoading: action.payload.isLoading,
                  id: action.payload.id,
          };
      case ActionTypes.UPDATE_MEDICAL_STAFF_ACTION_SUCESS:
          return {...state,
                  isLoading: action.payload.isLoading,
                  message: action.payload.message,
                  id: action.payload.id,
          };
      case ActionTypes.EDIT_MEDICAL_STAFF_ACTION_FAILS:
          return {...state,
                  isLoading: action.payload.isLoading,
                  error: action.payload.error,
          };

      case ActionTypes.GET_MEDICAL_STAFF_DETAILS_ACTION:
          return {...state, isLoading: action.payload.isLoading};
      case ActionTypes.GET_MEDICAL_STAFF_DETAILS_ACTION_SUCESS:
          return {
              ...state,
              isLoading: action.payload.isLoading,
              medicalStaffDetails: action.payload.medicalStaffDetails};
      case ActionTypes.GET_MEDICAL_STAFF_DETAILS_ACTION_FAILS:
          return {...state, isLoading: action.payload.isLoading};
      case ActionTypes.CLEANUP_ACTION:
          return {...state,
                  isLoading: false,
                  id: -1,
                  error: '',
                  patientDetails: action.payload.userData,
                  medicalStaffDetails: action.payload.userData,
                  message: '',
          };
      case ActionTypes.CLEAN_USER_MANAGEMENT_MESSAGE:
          return {
              ...state,
              message: '',
          };
      case ActionTypes.CLEAN_USER_MANAGEMENT_ERROR:
          return {
              ...state,
              error: '',
          };
      case ActionTypes.CHANGE_PASSWORD_ACTION: {
          return {
              ...state,
              isLoading: action.payload.isLoading,
          };
      }
      case ActionTypes.CHANGE_PASSWORD_ACTION_FAILS:
              return {
                  ...state,
                  isLoading: action.payload.isLoading,
                  error: action.payload.error,
              };
      case ActionTypes.CHANGE_PASSWORD_ACTION_SUCESS:
          return {
              ...state,
              isLoading: action.payload.isLoading,
              message: action.payload.message,
          };
      default:
      return state;
  }
}

export default formDetailsReducer;
