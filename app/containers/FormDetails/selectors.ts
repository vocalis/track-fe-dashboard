import { createSelector } from 'reselect';
import { ApplicationRootState } from 'types';
import { initialState } from './reducer';

/**
 * Direct selector to the formDetails state domain
 */

const selectFormDetailsDomain = (state: ApplicationRootState) => {
  return state.formDetails || initialState;
};

/**
 * Other specific selectors
 */

/**
 * Default selector used by FormDetails
 */

const makeSelectFormDetails = () =>
  createSelector(
    selectFormDetailsDomain,
    substate => {
      return substate;
    },
  );

const makeSelectPatientDetails = () =>
    createSelector(selectFormDetailsDomain, substate =>
        substate.patientDetails);
const makeSelectMedicalStaffDetails = () =>
    createSelector(selectFormDetailsDomain, substate =>
        substate.medicalStaffDetails);
const makeSelectisLoading = () =>
    createSelector(selectFormDetailsDomain, substate =>
        substate.isLoading);
const makeSelectError = () =>
    createSelector(selectFormDetailsDomain, substate =>
        substate.error);
const makeSelectMessage = () =>
    createSelector(selectFormDetailsDomain, substate =>
        substate.message);
const makeSelectCreatedUserId = () =>
    createSelector(selectFormDetailsDomain, substate =>
        substate.id);
export default makeSelectFormDetails;
export {
    selectFormDetailsDomain,
    makeSelectPatientDetails,
    makeSelectisLoading,
    makeSelectError,
    makeSelectMessage,
    makeSelectCreatedUserId,
    makeSelectMedicalStaffDetails,
};
