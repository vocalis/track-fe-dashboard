import {call, put, takeLatest} from 'redux-saga/effects';
import ActionTypes, {
    API_CHANGE_PATIENT_PASSWORD,
    API_CHANGE_USER_PASSWORD,
    API_MEDICAL_STAFF,
    API_PATIENT_ENDPOINT,
    API_PATIENTS_ENDPOINT,
    StatusProps,
} from './constants';
import {getData, postData, putData} from '../../utils/network';
import {
    changePasswordActionFails,
    changePasswordActionSucess,
    editMedicalStaffActionFAILS,
    editMedicalStaffActionSucess,
    editPatientActionFails,
    editPatientActionSucess, getMedicalStaffDetailsActionFails, getMedicalStaffDetailsActionSucess,
    getPatientDetailsActionFails,
    getPatientDetailsActionSucess, updateMedicalStaffActionSucess, updatePatientActionSucess,
} from './actions';
import {getMedicalStaffDetails, getPatientDetails} from '../../models/model';
import {mockPatientData, MockpostData, MockputData} from '../../utils/mock';
import {MedicalStaffDetails, PatientDetails} from '../../models/local';
import moment from 'moment';
import React from 'react';

// Individual exports for testing

const enableMock = false;
export default function* formDetailsSaga() {
    yield takeLatest(ActionTypes.EDIT_ACTION, editPatient);
    yield takeLatest(ActionTypes.EDIT_MEDICAL_STAFF_ACTION, editMedicalStaff);
    yield takeLatest(ActionTypes.GET_PATIENT_DETAILS_ACTION, getPatient);
    yield takeLatest(ActionTypes.GET_MEDICAL_STAFF_DETAILS_ACTION, getMedicalStaff);
    yield takeLatest(ActionTypes.CHANGE_PASSWORD_ACTION, changePassword);
}

function* editMedicalStaff(action) {
    const values: MedicalStaffDetails = action.payload.values;
    const editPage: boolean = action.payload.editPage;
    let medicalStaffDetails = {};
    if (!editPage) {
        medicalStaffDetails = {
            username: values.email,
            password: values.password,
            firstName: values.firstName,
            lastName: values.lastName,
            email: values.email,
            phoneNumber: values.phone ? values.phone : null,
            birthDate: values.birthDate ? moment.utc(values.birthDate).format() : null,
            // enabled: values.status === StatusProps.values[0],
        };
    } else {
        medicalStaffDetails = {
            id: Number(action.payload.id),
            username: values.email,
            firstName: values.firstName,
            lastName: values.lastName,
            email: values.email,
            phoneNumber: values.phone ? values.phone : null,
            birthDate: values.birthDate ? moment.utc(values.birthDate).format() : null,
            enabled: values.status === StatusProps.values[0],
        };
    }
    Object.keys(medicalStaffDetails)
        .forEach(key => medicalStaffDetails[key] === undefined ?
            delete medicalStaffDetails[key] :
            {});
    const response = !editPage ?
        yield call(
            postData,
            API_MEDICAL_STAFF,
            {...medicalStaffDetails})
        :
        yield call(
            putData,
            API_MEDICAL_STAFF,
            {...medicalStaffDetails});
    if (!editPage) {
        if (!response.error) {
            yield put(editMedicalStaffActionSucess(false, response.data));
        } else {
            yield put(editMedicalStaffActionFAILS(false, response.error));
        }
    } else {
        if (!response.error) {
            yield put(updateMedicalStaffActionSucess(false, Number(action.payload.id), 'Updated'));
        } else {
            yield put(editMedicalStaffActionFAILS(false, response.error));
        }
    }

}

function* editPatient(action) {
    const values: PatientDetails = action.payload.values;
    const editPage: boolean = action.payload.editPage;
    let pateintDetails = {};
    const hostname = window.location.host.split('.')[0];
    if (!editPage) {
        pateintDetails = {
            username: `${hostname}_${values.username}`,
            password: values.password,
            firstName: values.firstName,
            lastName: values.lastName,
            email: values.email ? values.email : null,
            phoneNumber: values.phone ? values.phone : null,
            birthDate: values.birthDate ? moment.utc(values.birthDate).format() : null,
            // enabled: values.status === StatusProps.values[0],
        };
    } else {
        pateintDetails = {
            id: Number(action.payload.id),
            username: values.username,
            firstName: values.firstName,
            lastName: values.lastName,
            email: values.email ? values.email : null,
            phoneNumber: values.phone ? values.phone : null,
            birthDate: values.birthDate ? moment.utc(values.birthDate).format() : null,
            enabled: values.status === StatusProps.values[0],
        };
    }
    // Object.keys(pateintDetails).forEach(key => pateintDetails[key] === undefined ? delete pateintDetails[key] : {});
    const response = !editPage ? yield call(
        postData,
        API_PATIENTS_ENDPOINT,
        {...pateintDetails}) :
        yield call(
            putData,
            API_PATIENTS_ENDPOINT,
            {...pateintDetails});
    if (!editPage) {
        if (!response.error) {
            yield put(editPatientActionSucess(false, response.data));
        } else {
            yield put(editPatientActionFails(false, response.error));
        }
    } else {
        if (!response.error) {
            yield put(updatePatientActionSucess(false, Number(action.payload.id), 'updated'));
        } else {
            yield put(editPatientActionFails(false, response.error));
        }
    }

}

function* getPatient(action) {
    if (enableMock) {
        const data = yield  call(mockPatientData);
        const patientDetails = yield getPatientDetails(data);
        yield put(getPatientDetailsActionSucess(patientDetails, false));
    } else {
        const response = yield call(getData, `${API_PATIENTS_ENDPOINT}/${action.payload.patientId}`, {});
        if (!response.error) {
            const pateintDetails = getPatientDetails(response.data);
            if (pateintDetails !== null) {
                yield put(getPatientDetailsActionSucess(pateintDetails, false));
            } else {
                yield put(getPatientDetailsActionFails(false));
            }
        } else {
            yield put(getPatientDetailsActionFails(false));
        }
    }
}

function* getMedicalStaff(action) {
    if (enableMock) {
        const data = yield  call(mockPatientData);
        const MedicalStaffDetails = yield getMedicalStaffDetails(data);
        yield put(getPatientDetailsActionSucess(MedicalStaffDetails, false));
    } else {
        const response = yield call(getData, `${API_MEDICAL_STAFF}/${action.payload.medicalStaffId}`, {});
        const medicalStaffIdDetails = getMedicalStaffDetails(response.data);
        if (!response.error && medicalStaffIdDetails !== null) {
            yield put(getMedicalStaffDetailsActionSucess(medicalStaffIdDetails, false));
        } else {
            yield put(getMedicalStaffDetailsActionFails(false));
        }
    }
}

function* changePassword(action) {
    const password = action.payload.password;
    const id = Number(action.payload.id);
    const isMedicalStaffPage = action.payload.isMedicalStaffPage;
    let response: any;
    if (isMedicalStaffPage) {
         response = yield call(
            putData,
             API_CHANGE_USER_PASSWORD,
            {id, password});
    } else {
         response = yield call(
             putData,
            API_CHANGE_PATIENT_PASSWORD,
             {id, password});
    }
    if (!response.error) {
            yield put(changePasswordActionSucess(false, 'Password Changed'));
        } else {
        yield put(changePasswordActionFails(false, 'cann\'t change password\''));
        }
}
