import { ActionType } from 'typesafe-actions';
import * as actions from './actions';
import { ApplicationRootState } from 'types';

/* --- STATE --- */
interface FormDetailsState {
  readonly isLoading: boolean;
  readonly patientDetails: object;
  readonly message: string;
  readonly  error: string;
  readonly  id: number;
  readonly  medicalStaffDetails: object;
}

/* --- ACTIONS --- */
type FormDetailsActions = ActionType<typeof actions>;

/* --- EXPORTS --- */
type RootState = ApplicationRootState;
type ContainerState = FormDetailsState;
type ContainerActions = FormDetailsActions;

export { RootState, ContainerState, ContainerActions };
