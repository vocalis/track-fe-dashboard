import styled from 'styled-components';
import styles from 'styles/values';
import {DialogContent, withStyles} from '@material-ui/core';


const FormikWrapper = styled.div`
  width: 100%;
  height: calc(100% - 120px);
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
   background: #FFFFFF;
`;
const FormikHeader = styled.div`
  width: 100%;
  height: 120px;
padding-left: ${styles.sizes.APP_SIDE_PADDING};
padding-right: ${styles.sizes.APP_SIDE_PADDING};
display: flex;
flex-direction: row;
    align-items: center;
 background: #FDFDFD;
opacity: 1;
`;
const HeaderWrapper = styled.div`
  width: 100%;
  height: 120px;
display: flex;
flex-direction: row;
    align-items: center;
 background: #FDFDFD;
opacity: 1;
border-bottom: 2px solid #00000007;

    margin-right: 14px;
        @media ${styles.sizes.SMALL_SCREEN}{
  padding-right: ${styles.sizes.APP_SIDE_PADDING_RIGHT_SMALL_SCREEN};
  padding-left: ${styles.sizes.APP_SIDE_PADDING_LEFT_SMALL_SCREEN};
 }
   @media ${styles.sizes.MEDIUM_SCREEN}{
   padding-right: ${styles.sizes.APP_SIDE_PADDING_RIGHT};
  padding-left: ${styles.sizes.APP_SIDE_PADDING_LEFT};
 }
   @media ${styles.sizes.LARGE_SCREEN}{
   padding-right: ${styles.sizes.APP_SIDE_PADDING_RIGHT_LARGE_SCREEN};
  padding-left: ${styles.sizes.APP_SIDE_PADDING_LEFT_LARGE_SCREEN};
 }
`;
const Contanier = styled.div`
width:100%;
height: calc( 100% - 120px);
        @media ${styles.sizes.SMALL_SCREEN}{
  padding-right: ${styles.sizes.APP_SIDE_PADDING_RIGHT_SMALL_SCREEN};
  padding-left: ${styles.sizes.APP_SIDE_PADDING_LEFT_SMALL_SCREEN};
 }
   @media ${styles.sizes.MEDIUM_SCREEN}{
   padding-right: ${styles.sizes.APP_SIDE_PADDING_RIGHT};
  padding-left: ${styles.sizes.APP_SIDE_PADDING_LEFT};
 }
   @media ${styles.sizes.LARGE_SCREEN}{
   padding-right: ${styles.sizes.APP_SIDE_PADDING_RIGHT_LARGE_SCREEN};
  padding-left: ${styles.sizes.APP_SIDE_PADDING_LEFT_LARGE_SCREEN};
 }
`;
const Img = styled.img<{opacity?: string}>`
opacity: ${props => props.opacity ? props.opacity : '1'};
`;
const ImgWrapper = styled.div`
width:88px;
height: 70px;
margin-right: 22px;
`;
const TitleWrapper = styled.div`
width: calc(100% - 100px - 300px);
font-size: 35px;
  font-weight: bold;
`;
const ActionWrapper = styled.div`
width: 326px;
    display: flex;
    flex-direction: row;
        align-items: center;
        justify-content: space-between;
`;
const ButtonWrapper = styled.div`
margin-left: 23px;
`;
const Span = styled.div<{color?: string, letterSpacing: string, fontSize: string }>`
  color: ${props => props.color};
  letter-spacing: ${props => props.letterSpacing};
  font-family: Roboto-Regular;
  font-weight: normal;
  font-size: ${props => props.fontSize};
  display: flex;
     align-items: center;
`;
const Title = styled.div`
  color:#707070;
  font-weight: bold;
  font-family: Roboto-Bold;
  font-size:42px;
`;
const CancelWrapper = styled.div`
 fontSize:20px;
 color:#38ABB6;
  letterSpacing:0px;
  cursor: pointer;
  font-family: Roboto-Regular;
  font-weight: normal;
   display: flex;
     align-items: center;
     justify-content: center;
 width: 260px;
height: 42px;
     border: 1px solid transparent;
&:hover{
background: #FCFCFC;
border: 1px solid #DBDBDB;
border-radius: 6px;
opacity: 1;
width: 260px;
height: 42px;
}
`;
const StyledDialogContent = withStyles(() => ({
    root: {
        height: '100%',
    },
}))(DialogContent);
export {
    FormikWrapper,
    FormikHeader,
    Contanier,
    Img,
    TitleWrapper,
    ActionWrapper,
    ButtonWrapper,
    ImgWrapper,
    Span,
    HeaderWrapper,
    Title,
    CancelWrapper,
    StyledDialogContent,
};
