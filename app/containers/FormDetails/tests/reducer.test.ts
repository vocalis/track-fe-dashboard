import formDetailsReducer from '../reducer';
// import { someAction } from '../actions';
import { ContainerState } from '../types';

describe('formDetailsReducer', () => {
  let state: ContainerState;
  beforeEach(() => {
    // @ts-ignore
    state = {
      isLoading: false,
      patientDetails: {},
    };
  });

  it('returns the initial state', () => {
    const expectedResult = state;
    expect(formDetailsReducer(undefined, {} as any)).toEqual(expectedResult);
  });

  /**
   * Example state change comparison
   *
   * it('should handle the someAction action correctly', () => {
   *   const expectedResult = {
   *     loading = true;
   *   );
   *
   *   expect(appReducer(state, someAction())).toEqual(expectedResult);
   * });
   */
});
