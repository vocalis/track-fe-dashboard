import { createSelector } from 'reselect';
import { ApplicationRootState } from 'types';
import { initialState } from './reducer';


const selectScreenPageDomain = (state: ApplicationRootState) => {
    return state.screen || initialState;
};


const makeSelectScreenSelectedPage = () => createSelector(selectScreenPageDomain, substate => substate.page);

const makeSelectScreenData = () => createSelector(selectScreenPageDomain, substate => substate.data);

const makeSelectScreenLoading = () => createSelector(selectScreenPageDomain, substate => substate.loading);

const makeSelectScreenTotal = () => createSelector(selectScreenPageDomain, substate => substate.total);

const makeSelectScreenSearchParams = () => createSelector(selectScreenPageDomain, substate => substate.searchParams);

const makeSelectScreenRecodingBlobData = () =>
    createSelector(selectScreenPageDomain, substate => substate.recordingBlobData);

const makeSelectScreenSearchData = () => createSelector(selectScreenPageDomain, substate => substate.searchData);

export {
    makeSelectScreenSelectedPage,
    makeSelectScreenData,
    makeSelectScreenLoading,
    makeSelectScreenTotal,
    makeSelectScreenSearchParams,
    makeSelectScreenRecodingBlobData,
    makeSelectScreenSearchData,
};
