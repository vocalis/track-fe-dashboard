/*
 *
 * FilterDashboard reducer
 *
 */
import { fromJS } from 'immutable';
import produce from 'immer';
import {
    DEFAULT_ACTION,
    LOAD_SCREEN_DATA_FAILURE_ACTION,
    LOAD_SCREEN_DATA_SUCCESS_ACTION, LOAD_SCREEN_PATIENT_DATA_SUCCESS_ACTION,
    SCREEN_PATEINT_RECORDING_ACTION,
    SCREEN_PATEINT_RECORDING_SUCCESS_ACTION, SEARCH_PATIENTS_ACTION,
    SET_PAGE_SCREEN_NUMBER,
    SET_SCREEN_LOADING_ACTION, SET_SCREEN_PATIENT_LOADING, SET_SCREEN_PATIENT_REVIEW_ACTION,
    SET_TOTAL_RESULTS_ACTION,
} from './constants';

export const initialState = fromJS({
    data: [],
    page: 1,
    loading: false,
    total: 0,
    searchParams: undefined,
    recordingBlobData: null,
});

/* eslint-disable default-case, no-param-reassign */
const ScreenPageReducer = (state = initialState, action) =>
  produce(state, (/* draft */) => {
    switch (action.type) {
        case SET_SCREEN_LOADING_ACTION:
            return {...state, loading: action.payload};
        case SET_TOTAL_RESULTS_ACTION:
            return {...state, total: action.payload};
        case LOAD_SCREEN_DATA_SUCCESS_ACTION:
            return {...state, data: action.payload};
        case LOAD_SCREEN_DATA_FAILURE_ACTION:
            return {...state, data: []};
        case SCREEN_PATEINT_RECORDING_SUCCESS_ACTION:
            return {...state, recordingBlobData: action.payload};
        case SCREEN_PATEINT_RECORDING_ACTION:
            return {...state, recordingBlobData: null};
        case SET_PAGE_SCREEN_NUMBER:
            return {...state, page: action.payload};
        case SEARCH_PATIENTS_ACTION:
            return {...state, searchData: action.payload};
        case SET_SCREEN_PATIENT_LOADING:
            const patientForLoading = state.data.find(patient => patient.patientId === action.payload.patientId);
            if (patientForLoading) {
                const index = state.data.indexOf(patientForLoading);
                state.data[index].loading = action.payload.isLoading;
                return ({
                    ...state,
                    data: [
                        ...state.data.slice(0, index),
                        patientForLoading,
                        ...state.data.slice(index + 1),
                    ],
                });
            }
            return state;
        case SET_SCREEN_PATIENT_REVIEW_ACTION:
            const patientForReview = state.data.find(patient => patient.patientId === action.payload.patientId);
            if (patientForReview) {
                const index = state.data.indexOf(patientForReview);
                state.data[index].reviewed = action.payload.isReviewed;
                return ({
                    ...state,
                    data: [
                        ...state.data.slice(0, index),
                        patientForReview,
                        ...state.data.slice(index + 1),
                    ],
                });
            }
            return state;
        case LOAD_SCREEN_PATIENT_DATA_SUCCESS_ACTION:
            const patientForUpdate = state.data.find(patient => patient.patientId === action.payload.patientId);
            if (patientForUpdate) {
                const index = state.data.indexOf(patientForUpdate);
                state.data[index] = action.payload;
                return ({
                    ...state,
                    data: [
                        ...state.data.slice(0, index),
                        patientForUpdate,
                        ...state.data.slice(index + 1),
                    ],
                });
            }
            return state;
        case DEFAULT_ACTION:
        default:
        return state;
    }
  });

export default ScreenPageReducer;
