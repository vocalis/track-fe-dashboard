import React, {useEffect, useState} from 'react';
import {AudioPlayer, AudioPlayerModal, Layout, Pagination, Search} from '../../components/common';
import PatientScreen from 'components/PatientScreen';
import {Patient, PatientAnalysis} from '../../models/local';
import {
    getPatientRecordingAction,
    loadScreenDataAction, loadScreenDataSuccessAction, resetPatientDataAction,
    reviewPatientAction,
    searchPatientsAction,
    setScreenPageAction,
} from './actions';
import {compose} from 'redux';
import {connect, useDispatch, useSelector} from 'react-redux';
import {useInjectReducer} from '../../utils/injectReducer';
import {useInjectSaga} from '../../utils/injectSaga';

import reducer from './reducer';
import saga from './saga';
import {createStructuredSelector} from 'reselect';
import {
    makeSelectScreenData,
    makeSelectScreenLoading, makeSelectScreenRecodingBlobData,
    makeSelectScreenSelectedPage,
    makeSelectScreenTotal,
} from './selectors';
import {ROWS_PER_PAGE} from './constants';
import {StyledLinerProgress} from '../../components/common/LinerProgress';

function sameDay(d1, d2) {
    return d1 && d2 &&
        d1.getFullYear() === d2.getFullYear() &&
        d1.getMonth() === d2.getMonth() &&
        d1.getDate() === d2.getDate();
}

const stateSelector = createStructuredSelector({
    data: makeSelectScreenData(),
    recordingBlobData: makeSelectScreenRecodingBlobData(),
    page: makeSelectScreenSelectedPage(),
    total: makeSelectScreenTotal(),
    loading: makeSelectScreenLoading(),
});

interface RecordingModalData {
    recordingId: number;
    name: string;
    id: number;
    age?: string;
}

export function ScreenPage(props) {
    const dispatch = useDispatch();
    const {data, page, total, loading, recordingBlobData} = useSelector(stateSelector);

    useInjectReducer({ key: 'screen', reducer });
    useInjectSaga({ key: 'screen', saga });
    const [showAudioPlayerModal, setShowAudioPlayerModal] = useState<RecordingModalData | null>(null);

    const loadPage = (pageNumber: number) => {
        dispatch(setScreenPageAction(pageNumber));
        dispatch(loadScreenDataAction());
    };

    useEffect(() => {
        loadPage(1);
        return () => {
            dispatch(searchPatientsAction(null));
            dispatch(loadScreenDataSuccessAction([]));
        };
    }, []);


    const getPatients = (): JSX.Element[] => {
        if (!data) {
            return [];
        }
        return data.map((patient: Patient, index: number) => {
            return (
                <PatientScreen
                    key={`patient_${index}`}
                    patient={patient}
                    onPatientReviewClicked={patientReviewed}
                    onResetDataClicked={resetData}
                    onPlaySampleClicked={playSample}
                />
            );
        });
    };

    const getPatient = (patientId: number): Patient | undefined => {
        return data.find(patient => patient.patientId === patientId);
    };

    const patientReviewed = (patientId) => {
        dispatch(reviewPatientAction(patientId));
    };

    const resetData = (patientId) => {
        dispatch(resetPatientDataAction(patientId));
    };

    const playSample = (patientId) => {
        const patient: Patient | undefined = getPatient(patientId);
        if (patient) {
            if (patient.detectors && patient.detectors.length > 0) {
                const firstDetector = patient.detectors[0];
                if (firstDetector && firstDetector.recordingId) {
                    const ageDetector = patient.detectors.find(detector => detector.detector === 'age');
                    setShowAudioPlayerModal({
                        recordingId: firstDetector.recordingId,
                        name: patient.name,
                        id: patient.patientId,
                        age: ageDetector && ageDetector.value,
                    });

                    dispatch(getPatientRecordingAction(firstDetector.recordingId));
                }
            }
        }
    };

    const onSearchClicked = (
        patientID: number,
        patientFirstName: string,
        patientLastName: string,
        reviewedPatients: string) => {
        const searchObj = {
            patientId: patientID,
            patientName: patientFirstName,
            lastName: patientLastName,
            isReviewedToday: reviewedPatients,
        };
        dispatch(searchPatientsAction(searchObj));
        loadPage(1);
    };

    const onBackwardClicked = () => {
        if (page > 1) {
            loadPage(page - 1);
        }
    };

    const onFastBackwardClicked = () => {
        if (page > 1) {
            loadPage(1);
        }
    };

    const onForwardClicked = () => {
        if (page < Math.ceil(total / ROWS_PER_PAGE)) {
            loadPage(page + 1);
        }
    };

    const onFastForwardClicked = () => {
        if (page < Math.ceil(total / ROWS_PER_PAGE)) {
            loadPage(Math.ceil(total / ROWS_PER_PAGE));
        }
    };
    return (
        <Layout title={'card_title_screen'}>
            <Search onSearchClicked={onSearchClicked} reviewedPatients={[]}/>
            { total && page ?
                <Pagination
                    totalPages={Math.ceil(total / ROWS_PER_PAGE)}
                    rawPerPage={ROWS_PER_PAGE}
                    pageNumber={page}
                    onBackwardClicked={onBackwardClicked}
                    onFastBackwardClicked={onFastBackwardClicked}
                    onFastForwardClicked={onFastForwardClicked}
                    onForwardClicked={onForwardClicked}
                />
                :
                ''
            }
            {
                loading ?
                    <StyledLinerProgress color="secondary"  />
                    :
                    getPatients()
            }
            {
                showAudioPlayerModal !== null &&
                <AudioPlayerModal
                    {...showAudioPlayerModal}
                    data={recordingBlobData}
                    onClose={() => setShowAudioPlayerModal(null)}
                />
            }
        </Layout>
    );
}


export default ScreenPage;
