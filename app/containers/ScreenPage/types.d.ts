import { ActionType } from 'typesafe-actions';
import * as actions from './actions';
import { ApplicationRootState } from 'types';
import {Patient} from '../../models/local';

interface SearchData {
    patientId?: number;
    patientName?: string;
    lastName?: string;
    isReviewedToday?: string;
}

/* --- STATE --- */
interface ScreenState {
    readonly page: number;
    readonly recordingBlobData: Blob | null;
    readonly searchParams?: ScreenSearchParams;
    readonly loading: boolean;
    readonly data: Patient[];
    readonly total: number;
    readonly searchData: SearchData;
}

/* --- ACTIONS --- */
type ScreenActions = ActionType<typeof actions>;

/* --- EXPORTS --- */
type RootState = ApplicationRootState;
type ContainerState = ScreenState;
type ContainerActions = ScreenActions;

interface ScreenSearchParams {
    patientId: number;
    patientName: string;
    lastName: string;
    isReviewedToday: string;
}

export { RootState, ContainerState, ContainerActions, ScreenSearchParams };
