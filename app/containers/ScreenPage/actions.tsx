/*
 *
 * Screen Page actions
 *
 */

import {
  LOAD_SCREEN_DATA_ACTION,
  LOAD_SCREEN_DATA_FAILURE_ACTION,
  LOAD_SCREEN_DATA_SUCCESS_ACTION, LOAD_SCREEN_PATIENT_DATA_SUCCESS_ACTION, RESET_PATIENT_DATA_ACTION,
  REVIEW_PATIENT_ACTION,
  SCREEN_PATEINT_RECORDING_ACTION,
  SCREEN_PATEINT_RECORDING_FAILURE_ACTION,
  SCREEN_PATEINT_RECORDING_SUCCESS_ACTION,
  SEARCH_PATIENTS_ACTION, SET_PAGE_SCREEN_NUMBER,
  SET_SCREEN_LOADING_ACTION, SET_SCREEN_PATIENT_LOADING, SET_SCREEN_PATIENT_REVIEW_ACTION,
  SET_TOTAL_RESULTS_ACTION,
} from './constants';
import {Patient} from '../../models/local';
import {ScreenSearchParams} from './types';
import {action} from 'typesafe-actions';

export function resetPatientDataAction(patientId: number) {
  return action(RESET_PATIENT_DATA_ACTION, patientId);
}
export function reviewPatientAction(patientId: number) {
  return action(REVIEW_PATIENT_ACTION, patientId);
}

export function searchPatientsAction(searchData: ScreenSearchParams | null) {
  return action(SEARCH_PATIENTS_ACTION, searchData);
}

export function setScreenPatientLoading(patientId: number, isLoading: boolean) {
  return action(SET_SCREEN_PATIENT_LOADING, {patientId, isLoading});
}

export function setScreenPatientReviewAction(patientId: number, isReviewed: boolean) {
  return action(SET_SCREEN_PATIENT_REVIEW_ACTION, {patientId, isReviewed});
}

export function setScreenPageAction(page: number) {
  return action(SET_PAGE_SCREEN_NUMBER, page);
}

export function loadScreenDataAction() {
  return action(LOAD_SCREEN_DATA_ACTION, null);
}

export function setScreenLoadingAction(isLoading) {
  return action(SET_SCREEN_LOADING_ACTION, isLoading);
}

export function setTotalResultsAction(total: number) {
  return action(SET_TOTAL_RESULTS_ACTION, total);
}

export function loadScreenDataSuccessAction(data: Patient[]) {
  return action(LOAD_SCREEN_DATA_SUCCESS_ACTION, data);
}

export function loadScreenDataFailureAction(error: string) {
  return action(LOAD_SCREEN_DATA_FAILURE_ACTION, error);
}

export function getPatientRecordingAction(recordingId) {
  return action(SCREEN_PATEINT_RECORDING_ACTION, recordingId);
}

export function getPatientRecordingSuccessAction(data) {
  return action(SCREEN_PATEINT_RECORDING_SUCCESS_ACTION, data);
}

export function getPatientRecordingFailureAction(error) {
  return action(SCREEN_PATEINT_RECORDING_FAILURE_ACTION, error);
}

export function loadScreenPatientDataSuccessAction(data: Patient) {
  return action(LOAD_SCREEN_PATIENT_DATA_SUCCESS_ACTION, data);
}
