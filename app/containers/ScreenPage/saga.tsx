import { put, call, select, takeLatest, all } from 'redux-saga/effects';
import moment from 'moment';
import {
    API_RECORDING_ENDPOINT,
    API_SCREEN_ENDPOINT, LOAD_SCREEN_DATA_ACTION, RESET_PATIENT_DATA_ACTION,
    REVIEW_PATIENT_ACTION,
    ROWS_PER_PAGE,
    SCREEN_PATEINT_RECORDING_ACTION,
    SEARCH_PATIENTS_ACTION,
} from './constants';
import {downloadBlob, getData, putData} from '../../utils/network';
import {getPatients} from '../../models/model';
import {
    getPatientRecordingFailureAction,
    getPatientRecordingSuccessAction, loadScreenDataFailureAction,
    loadScreenDataSuccessAction, loadScreenPatientDataSuccessAction,
    setScreenLoadingAction, setScreenPatientLoading, setScreenPatientReviewAction,
    setTotalResultsAction,
} from './actions';
import {makeSelectScreenData, makeSelectScreenSearchData, makeSelectScreenSelectedPage} from './selectors';

export default function* ScreenPageSaga() {
    yield all([actionWatcher()]);
}

function* actionWatcher() {
    yield takeLatest(LOAD_SCREEN_DATA_ACTION, searchPatients);
    yield takeLatest(SCREEN_PATEINT_RECORDING_ACTION, getPatientRecording);
    yield takeLatest(REVIEW_PATIENT_ACTION, togglePatientReviewed);
    yield takeLatest(RESET_PATIENT_DATA_ACTION, resetPatientData);
}

function* searchPatients(action) {

    yield put(setScreenLoadingAction(true));
    // get current page and items pr page
    const page = yield select(makeSelectScreenSelectedPage());
    const limit = ROWS_PER_PAGE;

    // calculate time range
    const now = moment();
    const to = now.format('YYYY-MM-DD');
    const from = now.subtract(1, 'year').format('YYYY-MM-DD');
    const params = {
        from,
        to,
        page,
        limit,
    };

    const searchData = yield select(makeSelectScreenSearchData());

    if (searchData) {
        params['filter[type]'] = 'patient';
        if (searchData && searchData.isReviewedToday !== 'all') {
            params['filter[reviewed]'] = searchData.isReviewedToday === 'reviewed_today';
        }
        if (searchData && searchData.patientId) {
            params['filter[id]'] = searchData.patientId;
        }
        if (searchData && searchData.patientName) {
            params['filter[firstName]'] = searchData.patientName;
        }
        if (searchData && searchData.lastName) {
            params['filter[lastName]'] = searchData.lastName;
        }
    }

    const response = yield call(getData, API_SCREEN_ENDPOINT, params);

    if (!response.error) {
        const data = getPatients(response.data.data);
        yield put(setTotalResultsAction(response.data.total));
        yield put(loadScreenDataSuccessAction(data));
    } else {
        yield put(loadScreenDataFailureAction(response.error));
    }
    yield put(setScreenLoadingAction(false));
}

function* getPatientRecording(action) {

    const response = yield call(downloadBlob, `${API_RECORDING_ENDPOINT}/${action.payload}`);
    if (!response.error) {
        yield put(getPatientRecordingSuccessAction(response.data));
    } else {
        yield put(getPatientRecordingFailureAction(response.error));
    }
}

function* togglePatientReviewed(action) {
    const data = yield select(makeSelectScreenData());
    const patient = data.find(patient => patient.patientId === action.payload);
    const isReviewed = !patient.reviewed;

    yield put(setScreenPatientLoading(patient.patientId, true));
    const response = yield call(putData, `${API_SCREEN_ENDPOINT}/${action.payload}`,
        {reset: false, reviewed: isReviewed});
    if (response.error) {
        yield put(setScreenPatientReviewAction(patient.patientId, !isReviewed));
    } else {
        yield put(setScreenPatientReviewAction(patient.patientId, isReviewed));
    }
    yield put(setScreenPatientLoading(patient.patientId, false));
}

function* resetPatientData(action) {
    const data = yield select(makeSelectScreenData());
    const patient = data.find(patient => patient.patientId === action.payload);

    yield put(setScreenPatientLoading(patient.patientId, true));
    const response = yield call(putData, `${API_SCREEN_ENDPOINT}/${action.payload}`,
        {reset: true, reviewed: patient.reviewed});

    if (response.error) {
        // TODO should show error message.
    } else {
        yield getPatientData(patient.patientId);
    }
    yield put(setScreenPatientLoading(patient.patientId, false));
}

function* getPatientData(patientId) {
    const now = moment();
    const to = now.format('YYYY-MM-DD');
    const from = now.format('YYYY-MM-DD');
    const params = {
        from,
        to,
    };
    const response = yield call(getData, `${API_SCREEN_ENDPOINT}/${patientId}`, params);
    if (response.error) {
        // TODO should show error message.
    } else {
        const data = getPatients([response.data]);
        yield put(loadScreenPatientDataSuccessAction(data[0]));
    }
}

