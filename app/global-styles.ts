import { createGlobalStyle } from 'styles/styled-components';
import './index.css';
const GlobalStyle = createGlobalStyle`

  html,
  body {
    height: 100%;
    width: 100%;
    line-height: 1.5;
  }


  body {
    font-family: Roboto-Light;
    font-weight: normal;
    font-size: 18px;
  }

  .recharts-brush-slide{
    fill-opacity: 1;
  }

  .recharts-brush-traveller {
    display: none;
  }
  .recharts-brush-texts {
    display: none;
  }


  .recharts-layer.recharts-brush > rect {
    stroke: none;
  }
  #app {
    background-color: #FDFDFD;
    min-height: 100%;
    min-width: 100%;
  }

  .tippy-content {
    padding: 0 !important;
  }

  p,
  label {
    font-family: Georgia, Times, 'Times New Roman', serif;
    line-height: 1.5em;
  }
`;

export default GlobalStyle;
