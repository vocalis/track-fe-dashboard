
import {Patients} from '../models/local';
import moment from 'moment';

export function mockPatientData() {
    return new Promise(res => {
        setTimeout(() => {
            // @ts-ignore
            res({
                id: 1,
                email: 'saeednamih@gmail.com',
                firstName: 'saeed',
                lastName: 'nameih',
                birthDate: moment.utc(new Date()).format(),
                phone: '0524560667',
                enabled: true,
                creationDate: moment.utc(new Date()).format(),
                createdBy: 5,
                lastLoginDate: moment.utc(new Date()).format(),
                lastActivity: moment.utc(new Date()).format(),
                reviewDate: moment.utc(new Date()).format(),
            });
        }, 2000);
    });
}
function generatePatientData() {
    const data: Patients[] = [];
    for (let i: number = 0; i < 20000; i++) {
        data.push({
            Id: i,
            firstName: `saeed${i}`,
            lastName: `namih${i}`,
            status: 'active',
            lastActivity: '10/7/2015',

        });
    }
    return data;
}

export function mockPatientsData() {
    return new Promise(res => {
        setTimeout(() => {
            res(generatePatientData());
        }, 3000);
    });


}
export function MockpostData() {
    return new Promise(res => {
        setTimeout(() => {
            res({data: {data: {id: 369852}}});
        }, 3000);
    });


}
export function MockputData() {
    return new Promise(res => {
        setTimeout(() => {
            res({data: {data: {message: ' patient updated'}}});
        }, 3000);
    });


}


export function mockMedicalStaffData() {
    return new Promise(res => {
        setTimeout(() => {
            res([
                    {
                        Id: 12,
                        firstName: 'a1',
                        lastName: 'b1',
                        status: 'active',
                        created: '10/7/2015',
                        email: 'saeed@mail.com',
                    },
                    {
                        Id: 22,
                        firstName: 'a2',
                        lastName: 'bmgdf',
                        status: 'active',
                        created: '10/7/2015',
                        email: 'saeed@mail.com',
                    },
                    {
                        Id: 32,
                        firstName: 'mmma',
                        lastName: 'dsaa',
                        status: 'active',
                        created: '10/7/2015',
                        email: 'saeed@mail.com',

                    },
                    {
                        Id: 42,
                        firstName: 'read',
                        lastName: 'read',
                        status: 'active',
                        created: '10/7/2015',
                        email: 'saeed@mail.com',
                    },
                ],
            );
        }, 3000);
    });


}

// editIcon: <div id={`3`} onClick={onEdideClick}><img src={editIcon}/></div>,
