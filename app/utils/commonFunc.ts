import {userRole} from './constants';
import moment from 'moment';

export const getIsAuthenticated = (token: string, tokenExpiryTime: Date) => {
    if (token && tokenExpiryTime) {
      const  expiryTime =  moment.utc(tokenExpiryTime).unix();
      const nowTime = moment.utc((new Date().toUTCString())).unix();
      return expiryTime > nowTime;
    }
    return false;
};
export const  stringToNumber = (role: string) => {
    switch (role) {
        case userRole.admin:
            return 4;
        case  userRole.staff:
            return 3;
        case userRole.patient:
            return 2;
        case userRole.none:
            return 1;
    }
    return 0;
};
