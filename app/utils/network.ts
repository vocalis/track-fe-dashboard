import axios, { AxiosRequestConfig } from 'axios';
import { call } from 'redux-saga/effects';
import {
  clearDataFromStorage,
  getDataFromStorage,
  saveDataToStorage,
} from './cookies';
import { Simulate } from 'react-dom/test-utils';
import error = Simulate.error;
import { API_REFRESH_TOKEN } from '../containers/AuthProvider/constants';
import { login } from '../routes/paths';

const API_URL = window['_env_'].BASE_API_URL;

function getHeaders() {
  const token = getDataFromStorage().accessToken;
  return { Authorization: `Bearer ${token}` };
}

export function* postData(uri: string, payload: object) {
  const url = API_URL + uri;
  const conf = {
    headers: getHeaders(),
    url,
  };
  return yield call(useAxios, 'post', url, payload, conf);
}

export function* getData(uri, params) {
  const url = API_URL + uri;
  const config = { headers: getHeaders(), url, params };
  return yield call(useAxios, 'get', url, params, config);
}
export function* getStatusAllData(uri, params) {
  const newApiUrl = API_URL.split('backoffice/').join('');
  const url = newApiUrl + uri;
  const config = {};
  return yield call(useAxios, 'get', url, params, config);
}

export function* downloadBlob(uri) {
  const url = API_URL + uri;
  const config: AxiosRequestConfig = {
    headers: getHeaders(),
    url,
    responseType: 'blob',
  };
  return yield call(useAxios, 'downloadBlob', url, {}, config);
}

export function* putData(uri, data) {
  const url = API_URL + uri;
  const config = { headers: getHeaders(), url };
  return yield call(useAxios, 'put', url, data, config);
}

export function* useAxios(
  type: string,
  url: string,
  payload: object,
  config: object,
) {
  try {
    switch (type) {
      case 'post':
        return yield axios.post(url, payload, config);
      case 'get':
        return yield axios.get(url, config);
      case 'put':
        return yield axios.put(url, payload, config);
      case 'downloadBlob':
        return yield axios.get(url, config);
    }
  } catch (error) {
    if (error.response && error.response.data && error.response.data.error) {
      return { error: error.response.data.error };
    }
    return { error: error && error.message ? error.message : 'Error' };
  }
}

axios.interceptors.request.use(
  (config) => {
    const token = getDataFromStorage().accessToken;
    if (token && config.headers['Authorization']) {
      config.headers['Authorization'] = 'Bearer ' + token;
    }
    config.headers['Content-Type'] = 'application/json';
    return config;
  },
  (error) => {
    Promise.reject(error);
  },
);

axios.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    const originalRequest = error.config;
    if (
      (error.response.status === 401 || error.response.status === 500) &&
      originalRequest.url.includes(API_REFRESH_TOKEN)
    ) {
      clearDataFromStorage();
      window.location.replace(login);
      return Promise.reject(error);
    }
    if (error.response.status === 401 && !originalRequest._retry) {
      originalRequest._retry = true;
      const oldAccessToken = getDataFromStorage().accessToken;
      const refreshToken = getDataFromStorage().refreshToken;
      return axios
        .post(`${API_URL}${API_REFRESH_TOKEN}`, {
          oldAccessToken,
          refreshToken,
        })
        .then((res) => {
          if (res.status === 201 || res.status === 200) {
            saveDataToStorage(
              res.data.user,
              res.data.accessToken,
              res.data.refreshToken,
              res.data.expiresAt,
              res.data.roles[0],
            );
            axios.defaults.headers['Authorization'] =
              'Bearer ' + res.data.accessToken;
            return axios(originalRequest);
          } else {
            return Promise.reject(error);
          }
        })
        .catch((err) => {
          return Promise.reject(error);
        });
    }
    return Promise.reject(error);
  },
);
