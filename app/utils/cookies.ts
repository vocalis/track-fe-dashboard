import Cookies from 'js-cookie';

const domain = window.location.hostname;
const path = '';
export const saveDataToStorage = (useId, accessToken, refreshToken, expiresAt, role) => {
    Cookies.set('useId', useId, {domain, path});
    Cookies.set('accessToken', accessToken, {domain, path});
    Cookies.set('refreshToken', refreshToken, {domain, path});
    Cookies.set('expiresAt', expiresAt, {domain, path});
    Cookies.set('role', role , {domain, path});
};
export const getDataFromStorage = () => ({
    accessToken: Cookies.get('accessToken', { domain, path }),
    refreshToken: Cookies.get('refreshToken', { domain, path }),
    useId: Cookies.get('useId', { domain, path }),
    expiresAt: Cookies.get('expiresAt', { domain, path }),
    role: Cookies.get('role', { domain, path }),
});

export const clearDataFromStorage = () => {
    Cookies.remove('accessToken', { domain, path });
    Cookies.remove('refreshToken', { domain, path });
    Cookies.remove('useId', { domain, path });
    Cookies.remove('expiresAt', { domain, path });
    Cookies.remove('role', { domain, path });
};
