
export const RESTART_ON_REMOUNT = '@@saga-injector/restart-on-remount';
export const DAEMON = '@@saga-injector/daemon';
export const ONCE_TILL_UNMOUNT = '@@saga-injector/once-till-unmount';
export const enableMock = false;
export interface HeadCell {
    disablePadding: boolean;
    id: string;
    label: string;
    numeric: boolean;
}
export const medicalStaffheadCells: HeadCell[] = [
    {
        id: 'id',
        numeric: false,
        disablePadding: true,
        label: 'ID',
    },
    { id: 'email', numeric: false, disablePadding: false, label: 'Email' },
    { id: 'firstName', numeric: false, disablePadding: false, label: 'First Name' },
    { id: 'lastName', numeric: false, disablePadding: false, label: 'Last Name' },
    { id: 'status', numeric: false, disablePadding: false, label: 'Status' },
    { id: 'created', numeric: false, disablePadding: false, label: 'Created' },
    { id: 'editIcon', numeric: false, disablePadding: false, label: ''  },
];

export const patientsHeadCells: HeadCell[] = [
    {
        id: 'id',
        numeric: false,
        disablePadding: true,
        label: 'ID',
    },
    { id: 'firstName', numeric: false, disablePadding: false, label: 'First Name' },
    { id: 'lastName', numeric: false, disablePadding: false, label: 'Last Name' },
    { id: 'status', numeric: false, disablePadding: false, label: 'Status' },
    { id: 'lastActivity', numeric: false, disablePadding: false, label: 'Last Activity' },
    { id: 'editIcon', numeric: false, disablePadding: false, label: '' },
];
export const userRole = {
    admin: 'admin',
    staff: 'staff',
    patient: 'patient',
    none: 'none',
};
