/**
 *
 * Warning
 *
 */
import React, { memo } from 'react';
import {
    Img,
    TextWrapper,
    ImgWrapper,
    RectangleWrapper,
    WarningWrapper,
} from './styles';
import {FormattedMessage} from 'react-intl';
const warningIcon = require('images/warning.svg');
interface Props {
  text: string;
}

function Warning(props: Props) {
  const {text} = props;
  return(
      <RectangleWrapper>
        <WarningWrapper>
          <FormattedMessage id={'WARNING'}/>
        </WarningWrapper>
        <TextWrapper>
          {text}
        </TextWrapper>
        <ImgWrapper>
          <Img src={warningIcon}/>
        </ImgWrapper>
      </RectangleWrapper>
  );
}

export default memo(Warning);
