import styled from 'styled-components';
import styles from 'styles/values';

const RectangleWrapper = styled.div`
display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    font-family: Roboto-Light;
    font-size: 18px;
    color: #353535;
    margin-bottom: 10px;
        border: 2px solid rgb(175, 0, 0);
`;
const Img = styled.img`
`;
const ImgWrapper = styled.div`
        padding-top: 20px;
    padding-bottom: 10px;
`;
const WarningWrapper = styled.div`
 font-family: Roboto-Bold;
    font-size: 24px;
   padding-top: 5px;
    padding-bottom: 10px;
        color: red;
`;
const TextWrapper = styled.div`
padding-left: 10px;
    padding-right: 10px;
`;
export {
    RectangleWrapper,
    ImgWrapper,
    Img,
    TextWrapper,
    WarningWrapper,
};
