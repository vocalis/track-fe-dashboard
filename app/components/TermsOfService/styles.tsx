import styled from 'styled-components';
import React from 'react';
import styles from '../../styles/values';

const Contanier = styled.div`
width:100%;
           @media ${styles.sizes.MEDIUM_SCREEN}{
        padding-left: ${styles.sizes.HELP_SIDE_PADDINGE_LEFT};
        padding-right: ${styles.sizes.HELP_SIDE_PADDINGE_ROGHT};
         }
   @media ${styles.sizes.SMALL_SCREEN}{
   padding-right: ${styles.sizes.HELP_SIDE_PADDINGE_ROGHT_SMALL_SCREEN};
  padding-left: ${styles.sizes.HELP_SIDE_PADDINGE_LEFT_SMALL_SCREEN};
  }
     @media ${styles.sizes.LARGE_SCREEN}{
   padding-right: ${styles.sizes.APP_SIDE_PADDING_RIGHT_LARGE_SCREEN};
  padding-left: ${styles.sizes.APP_SIDE_PADDING_LEFT_LARGE_SCREEN};
  }
background-color: #FFFFFF;
display: flex;
flex-direction: column;
padding-top: 73px;
`;
const TextWrapper = styled.div`
  font-family: Roboto-Light;
  margin-top: 10px;
  margin-bottom: 20px;
      line-height: 1.5;
      width: 100%;
`;
const TextTitle = styled.div<{index?: number}>`
height: fit-content;
font-size: 28px;
color: #38ABB6;
  font-family: Roboto-Regular;
  font-weight: normal;
  text-align: ${(props) => props.index === 0 ? 'center' : 'left'}
`;
export {
    Contanier,
    TextWrapper,
    TextTitle,
};
