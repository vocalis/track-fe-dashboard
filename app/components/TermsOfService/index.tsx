/**
 *
 * TermsOfService
 *
 */
import React, {memo, useEffect} from 'react';
import Layout from '../common/Layout';
import {
    Contanier,
} from './styles';
import TermsOfUseContainer from './TermsOfUseContainer';
import {useDispatch} from 'react-redux';
import {toggleSideBar} from '../../containers/SideBar/actions';

interface Props {}

function TermsOfService(props: Props) {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(toggleSideBar(false));
        window.scrollTo(0, 0);
    });
    return (
        <Layout title="PrivacyNotice">
            <Contanier>
          <TermsOfUseContainer/>
            </Contanier>
        </Layout>
    );
}

export default memo(TermsOfService);
