
import React, {memo, Fragment} from 'react';
import {TextTitle, TextWrapper} from './styles';
import './style.css';
const termsOfUser = require('./termsOfUser.json');

interface Props {
}

interface Paragraph {
    subTitle: string;
    type: string;
    content: string[];
}

interface Section {
    section: {
        title: string;
        paragraph: Paragraph[];
    };
}

const getParagraph = (content: string[]) => {
    return (

        content.map((p, index) => (
            <TextWrapper key={index} dangerouslySetInnerHTML={{__html: p}}/>))
    );
};

const getOrderList = (content: string[], index: number) => {
    return (
            <ol key={`ol${index}`}>
               { content.map((item, index) => (
                   <li key={index}>
                       <span  dangerouslySetInnerHTML={{__html: item}}/>
                   </li>
               ))
               }
            </ol>

    );
};

function TermsOfUseContainer(props: Props) {
    return (
        <div style={{
            display: 'flex',
            flexDirection: 'column',
        }}>
            {termsOfUser.map((section: Section, index: number) => {
                return (
                    <Fragment key={index}>
                        <TextTitle index={index}>
                            {section.section.title}
                        </TextTitle>
                        <TextWrapper>
                            {section.section.paragraph.map((paragraph: Paragraph, index: number) => {
                                switch (paragraph.type) {
                                    case 'p':
                                        return getParagraph(paragraph.content);
                                    case  'ol':
                                        return getOrderList(paragraph.content, index);
                                    default :
                                        return  <Fragment/>;
                                }
                            })}
                        </TextWrapper>
                    </Fragment>
                );
            })}
        </div>
    );
}

export default memo(TermsOfUseContainer);
