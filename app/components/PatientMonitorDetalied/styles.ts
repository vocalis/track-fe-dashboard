import styled from 'styled-components';
import styles from 'styles/values';

const PatientMonitorDetailedWrapper = styled.div`
    padding-left: 11.042vw;
    padding-right: 11.042vw;
    padding-top: 37.5px;
`;

const PatientMonitorDetailedHeader = styled.div`
    padding-top: 37.5px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`;

const PatientMonitorDetailedBody = styled.div`
    // display: flex;
    // flex-direction: row;
    // justify-content: space-between;
    // align-items: center;
`;

const BackIcon = styled.img`
    transform: rotate(180deg);
    padding-left: 1.302vw;
`;

const PatientName = styled.div`
    font-size: 18px;
    letter-spacing: 0;
    color: ${styles.colors.SHADE.GRAY.TABLE_TEXT};
`;

const PatientID = styled.div`

`;

const PatientData = styled.div`
    flex: 3 1;
    font-size: 18px;
    color : ${styles.colors.SHADE.GRAY.TABLE_TEXT};
    display: flex;
`;

const PatientActions = styled.div`
    flex: 1 1;
    font-size: 18px;
    color : ${styles.colors.SHADE.GRAY.TABLE_TEXT};
    margin-left: 12.656vw;
    display: flex;
    justify-content: flex-end;
`;

const Separator = styled.div`
  color: ${styles.colors.SHADE.GRAY.TABLE_TEXT};
  margin-left: 1.042vw;
  margin-right: 1.042vw;
`;

const PatientTitleText = styled.div`

`;

const PatientMonitorDetailedGraphsWrapper = styled.div`
    display: flex;
    justify-content: space-between;
    margin-top: 48px;
`;

const PatientMonitorDetailedGraph = styled.div`
    width: calc(50% - 6px);
`;

const PatientMonitorDetailedGraphTitle = styled.div`
    font-size: 18px;
    font-weight: bold;
    letter-spacing: 0.6px;
    color: ${styles.colors.SHADE.GRAY.TABLE_TEXT};
    display: flex;
`;

const PatientMonitorDetailedGraphBody = styled.div`
    margin-top: 16.5px;
    background: ${styles.colors.SHADE.WHITE.WHITE} 0% 0% no-repeat padding-box;
    border: 1px solid ${styles.colors.SHADE.GRAY.TABLE_BORDER};
    border-radius: 8px;
    height: 250px;
`;

const PatientMonitorDetailedGraphTitleUnits = styled.div`
    display: flex;
    margin-left: 15px;
    color : ${styles.colors.TEXT.LIGHTGRAY};
    font-size: 18px;
    font-weight: bold;
`;

const PatientMonitorDetailedGraphTitleUnit = styled.div<{chosenUnit: boolean}>`
    margin: 0px 5px;
    cursor: pointer;
    color : ${props => props.chosenUnit ? styles.colors.TEXT.GREEN : styles.colors.TEXT.LIGHTGRAY}
`;

export {
    PatientMonitorDetailedWrapper,
    PatientMonitorDetailedHeader,
    PatientMonitorDetailedBody,
    BackIcon,
    PatientName,
    PatientID,
    PatientData,
    PatientActions,
    Separator,
    PatientTitleText,
    PatientMonitorDetailedGraphsWrapper,
    PatientMonitorDetailedGraph,
    PatientMonitorDetailedGraphTitle,
    PatientMonitorDetailedGraphBody,
    PatientMonitorDetailedGraphTitleUnits,
    PatientMonitorDetailedGraphTitleUnit,
};
