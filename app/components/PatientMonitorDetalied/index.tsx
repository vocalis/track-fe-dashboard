import React, { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import {
  BackIcon,
  PatientMonitorDetailedHeader,
  PatientMonitorDetailedWrapper,
  PatientName,
  PatientActions,
  PatientData,
  PatientID,
  Separator,
  PatientTitleText,
  PatientMonitorDetailedBody,
  PatientMonitorDetailedGraphsWrapper,
  PatientMonitorDetailedGraph,
  PatientMonitorDetailedGraphTitle,
  PatientMonitorDetailedGraphBody,
  PatientMonitorDetailedGraphTitleUnits,
  PatientMonitorDetailedGraphTitleUnit,
} from './styles';
import { Patient } from '../../models/local';
import { HorizontalCheckBox, ShortnessOfBreathLevel } from '../common';
import { Modal } from '@material-ui/core';
import ConfirmReviewPopup from '../PatientMonitor/ConfirmReviewPopup';
import {StyledDialogContent} from '../../containers/FormDetails/styles';
// import {BloodPressureGraph, O2SatGraph, TemperatureGraph, WeightGraph} from '../Graphs';

interface PatientScreenProp {
  patient: Patient;
  back: () => void;
  onPatientReviewClicked: (patientId: number) => void;
  playSample: (recordingId: number) => void;
  questionnaireData: {
    daily: QuestionnaireDataInterface[];
    expanded: QuestionnaireDataInterface[];
  } | null;
}

interface QuestionnaireDataInterface {
  id: number;
  question: string;
  answers: QuestionnaireAnswersDataInterface[];
}

interface QuestionnaireAnswersDataInterface {
  id: number;
  answer: string;
}

const backButton = require('images/arrow.svg');

const PatientMonitorDetailed: React.FC<PatientScreenProp> = (
  props: PatientScreenProp,
) => {
  const {
    patient,
    back,
    onPatientReviewClicked,
    playSample,
    questionnaireData,
  } = props;
  const [tempUnit, setTempUnit] = useState<string>('C');
  const [showPatientReviewPopup, setShowPatientReviewPopup] = useState(false);
  const onConfirmPatientReview = () => {
    setShowPatientReviewPopup(false);
    onPatientReviewClicked(patient.patientId);
  };
  const reviewPatient = () => {
    setShowPatientReviewPopup(true);
  };
  const closePopupModal = () => {
    setShowPatientReviewPopup(false);
  };

  return (
    <PatientMonitorDetailedWrapper>
      <Modal
        open={showPatientReviewPopup}
        onClose={closePopupModal}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        disableBackdropClick
      ><StyledDialogContent>
        <ConfirmReviewPopup
          message={patient.reviewed ? 'CONFIRM_NOT_REVIEW' : 'CONFIRM_REVIEW'}
          onActionClick={closePopupModal}
          onButtonClick={onConfirmPatientReview}
          messageWidth={patient.reviewed ? '300px' : '100%'}
        />
      </StyledDialogContent>
      </Modal>
      <PatientMonitorDetailedHeader>
        <PatientData>
          <BackIcon onClick={back} src={backButton} />
          <PatientName>{patient.name}</PatientName>
          <Separator>|</Separator>
          <PatientTitleText>
            <FormattedMessage
              id="ID_NUMBER"
              values={{ number: patient.patientId }}
            />
          </PatientTitleText>
          <Separator>|</Separator>
          <PatientTitleText>
            <FormattedMessage id="AGE" />: {patient.age || '-'}
          </PatientTitleText>
        </PatientData>
        <PatientActions>
          <HorizontalCheckBox
            action={reviewPatient}
            value={patient.reviewed}
            title="REVIEWED_TODAY"
          />
        </PatientActions>
      </PatientMonitorDetailedHeader>
      <PatientMonitorDetailedBody>
        <ShortnessOfBreathLevel
          patient={patient}
          playSample={playSample}
          questionnaireData={questionnaireData}
        />
        {/*<PatientMonitorDetailedGraphsWrapper>*/}
        {/*    <PatientMonitorDetailedGraph>*/}
        {/*        <PatientMonitorDetailedGraphTitle>*/}
        {/*            <FormattedMessage id={'O2_SAT'} />*/}
        {/*        </PatientMonitorDetailedGraphTitle>*/}
        {/*        <PatientMonitorDetailedGraphBody>*/}
        {/*            <O2SatGraph data={patient.vitals} days={20}/>*/}
        {/*        </PatientMonitorDetailedGraphBody>*/}
        {/*    </PatientMonitorDetailedGraph>*/}
        {/*    <PatientMonitorDetailedGraph>*/}
        {/*        <PatientMonitorDetailedGraphTitle>*/}
        {/*            <FormattedMessage id={'TEMPERATURE'} />*/}
        {/*            <PatientMonitorDetailedGraphTitleUnits>*/}
        {/*                <PatientMonitorDetailedGraphTitleUnit*/}
        {/*                    chosenUnit={tempUnit === 'C'}*/}
        {/*                    onClick={() => setTempUnit('C')}>*/}
        {/*                    C*/}
        {/*                </PatientMonitorDetailedGraphTitleUnit>*/}
        {/*                 /*/}
        {/*                <PatientMonitorDetailedGraphTitleUnit*/}
        {/*                    chosenUnit={tempUnit === 'F'}*/}
        {/*                    onClick={() => setTempUnit('F')}>*/}
        {/*                    F*/}
        {/*                </PatientMonitorDetailedGraphTitleUnit>*/}
        {/*            </PatientMonitorDetailedGraphTitleUnits>*/}
        {/*        </PatientMonitorDetailedGraphTitle>*/}
        {/*        <PatientMonitorDetailedGraphBody>*/}
        {/*            <TemperatureGraph data={patient.vitals} days={20} unit={tempUnit}/>*/}
        {/*        </PatientMonitorDetailedGraphBody>*/}
        {/*    </PatientMonitorDetailedGraph>*/}
        {/*</PatientMonitorDetailedGraphsWrapper>*/}
        {/*<PatientMonitorDetailedGraphsWrapper>*/}
        {/*    <PatientMonitorDetailedGraph>*/}
        {/*        <PatientMonitorDetailedGraphTitle>*/}
        {/*            <FormattedMessage id={'PULSE'} />*/}
        {/*        </PatientMonitorDetailedGraphTitle>*/}
        {/*        <PatientMonitorDetailedGraphBody>*/}
        {/*            <WeightGraph data={patient.vitals} days={20}/>*/}
        {/*        </PatientMonitorDetailedGraphBody>*/}
        {/*    </PatientMonitorDetailedGraph>*/}
        {/*    <PatientMonitorDetailedGraph>*/}
        {/*        <PatientMonitorDetailedGraphTitle>*/}
        {/*            <FormattedMessage id={'BLOOD_PRESSURE'} />*/}
        {/*        </PatientMonitorDetailedGraphTitle>*/}
        {/*        <PatientMonitorDetailedGraphBody>*/}
        {/*            <BloodPressureGraph data={patient.vitals} days={20}/>*/}
        {/*        </PatientMonitorDetailedGraphBody>*/}
        {/*    </PatientMonitorDetailedGraph>*/}
        {/*</PatientMonitorDetailedGraphsWrapper>*/}
      </PatientMonitorDetailedBody>
    </PatientMonitorDetailedWrapper>
  );
};

export default PatientMonitorDetailed;
