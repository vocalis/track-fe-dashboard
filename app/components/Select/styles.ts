import styled from 'styled-components';

const StyledSelect = styled.select<{fontFamily?: string}>`
  width: 100%;
  height: 100%;
  padding-top: 7px;
  padding-bottom: 10px;
  padding-left: 9px;
  border: 1px solid #dfdfdf;
  box-sizing: border-box;
      border-radius: 8px;
      font-size: 18px;
      font-family: ${props => props.fontFamily ? props.fontFamily : 'Roboto-Light'}
      color:#353535;
      cursor: pointer;
  //&::-webkit-input-placeholder {
  //  font-size: 18px;
  //
  //  color: #020202;
  //  opacity: 40%;
  //  font-family: OpenSans;
  //  font-weight: 600;
  //}
`;
const SelectWrapper = styled.div<{marginTop?: string , height?: string , width?: string}>`
//margin-top:30px;
margin-top:${props => props.marginTop ? props.marginTop : '0px'};
height:${props => props.height ? props.height : '10px'};
width:${props => props.width ? props.width : '10px'};
position: relative;
`;
const TitleWrapper = styled.div`
color:#38ABB6;
font-size:18px;
font-family: Roboto-Regular;
padding-bottom: 11px;
`;
export {StyledSelect, SelectWrapper, TitleWrapper};
