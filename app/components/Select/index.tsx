/**
 *
 * Select
 *
 */
import React, {memo, ReactText} from 'react';

import {
    TitleWrapper,
    SelectWrapper,
    StyledSelect,
} from './styles';
import {FormattedMessage} from 'react-intl';
import TextField from '../TextField';

interface Props {
    value: string[];
    id: string;
    name: string;
    onChange: (name: string , value: ReactText) => void;
    selectedValue: string;
    withLabel: boolean;
    width?: string;
    height?: string;
    fontFamily?: string;
    classes?: object;

}

export function Select(props: Props) {
    const {value, name, id, onChange, selectedValue, withLabel, width, height, fontFamily, classes} = props;
    const onselectItem = (e) => {
        onChange(e.target.id, e.target.value);
    };
    return (
        <SelectWrapper    width={width}
                          height={height}>
            {withLabel && <TitleWrapper>
                <FormattedMessage id={name}/>
            </TitleWrapper>}
            <StyledSelect
                value={selectedValue}
                onChange={onselectItem}
                id={id}
                name={name}
                fontFamily={fontFamily}
                style={classes}
            >
                {value.map((item: any) => (
                    <option key={item} value={item}>{item}</option>
                ))}
            </StyledSelect>
        </SelectWrapper>
    );
}
