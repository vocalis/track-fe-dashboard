import React from 'react';
import {
    HeaderWrapper,
    VocalisLogoWrapper,
    LinksWrapper,
    LinkWrapper,
    Icon,
    AppLogo,
    Span,
    MenuIcon,
    useStyles,
} from './styles';
import {RouteComponentProps, withRouter} from 'react-router-dom';
import {FormattedMessage} from 'react-intl';
import IconButton from '@material-ui/core/IconButton';
import {aboutUs, appHeaderLinksArr, contactUs, helpId} from '../../containers/SideBar/constants';
import {help, contact, about} from '../../routes/paths';
import {useDispatch, useSelector} from 'react-redux';
import {selectTabAction, toggleSideBar} from '../../containers/SideBar/actions';
import {stateSelector} from '../../containers/SideBar';
const menu = require('images/menu.svg');
const logo = require('images/VocalisTrack-Logo-WHITE.png');

export interface AppHeaderProps extends RouteComponentProps {
    title: string;
    onLogoClicked: () => void;
}

const AppHeader: React.FC<AppHeaderProps> = (props: AppHeaderProps) => {
    const {onLogoClicked, history } = props;
    const {openMenu } = useSelector(stateSelector);
    const dispatch = useDispatch();
    const classes  = useStyles();
    const handleDrawerOpen = () => {
        dispatch(toggleSideBar(true));
    };
    const onLinkClick = (id: string) => {
        dispatch(selectTabAction(id));
        switch (id) {
            case helpId:
                history.push(`${help}`);
                break;
            case contactUs:
                history.push(`${contact}`);
                break;
            case aboutUs:
                history.push(`${about}`);
                break;
            case '':
                history.push('/');
        }
    };
    return (
        <HeaderWrapper>
            <VocalisLogoWrapper>
                <AppLogo src={logo} onClick={() => onLinkClick('')}/>
            </VocalisLogoWrapper>
            <LinksWrapper links={true}>
                {appHeaderLinksArr.map(item => (
                    <LinkWrapper marginRight="8.177vw" key={item.id}>
                        <Span onClick={() => onLinkClick(item.id)}>
                            <Icon src={item.src}/>
                            {<FormattedMessage id={item.text}/>}
                        </Span>
                    </LinkWrapper>
                ))}
            </LinksWrapper>
            <LinksWrapper>
                <LinkWrapper  className={openMenu ? classes.hide : classes.show}>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        disableRipple
                        disableTouchRipple
                        disableFocusRipple
                        className={classes.root}
                    >
                        <MenuIcon src={menu}/>
                    </IconButton>
                </LinkWrapper>
            </LinksWrapper>
        </HeaderWrapper>
    );
};
export default withRouter(AppHeader);
