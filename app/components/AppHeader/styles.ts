import styled from 'styled-components';
import styles from 'styles/values';
import {createStyles, makeStyles} from '@material-ui/core/styles';
const HeaderWrapper = styled.div`
  width: 100%;
  height: ${styles.sizes.HEADER_HEIGHT};
  display: flex;
  flex-direction: row;
  align-items: center;
  background-image:
  linear-gradient(270deg , ${styles.colors.GRADIENT.HEADER.START}, ${styles.colors.GRADIENT.HEADER.END});
  position: fixed;
  z-index: 100;
  top: 0;
  left: 0;
  justify-content: space-between;
  @media ${styles.sizes.SMALL_SCREEN}{
   padding-right: ${styles.sizes.APP_SIDE_PADDING_RIGHT_SMALL_SCREEN};
  padding-left: ${styles.sizes.APP_SIDE_PADDING_LEFT_SMALL_SCREEN};
 }
   @media ${styles.sizes.MEDIUM_SCREEN}{
   padding-right: ${styles.sizes.APP_SIDE_PADDING_RIGHT};
  padding-left: ${styles.sizes.APP_SIDE_PADDING_LEFT};
 }
   @media ${styles.sizes.LARGE_SCREEN}{
   padding-right: ${styles.sizes.APP_SIDE_PADDING_RIGHT_LARGE_SCREEN};
  padding-left: ${styles.sizes.APP_SIDE_PADDING_LEFT_LARGE_SCREEN};
 }

`;
const AppLogo = styled.img`
  cursor: pointer;
      height: 38px;
    width: 191.86px;
`;

const MenuIcon = styled.img`
  height: 22px;
  width: 30px;
  cursor: pointer;
  margin-right: 10px;
    margin-top: -2px;
`;

const Icon = styled.img`
  height: 11px;
  width: 21px;
  cursor: pointer;
  margin-right: 10px;
    margin-top: -2px;
`;

const Separator = styled.div`
  font-size: 20px;
  color: ${styles.colors.SHADE.WHITE.WHITE};
  font-weight: bold;
  margin-left: 1.563vw;
  margin-right: 1.094vw;
`;
const LinksWrapper = styled.div<{links?: boolean}>`
display: flex;
flex-direction: row;
    height: 100%;
    padding-right: ${props => props.links ? '8.177vw' : '0px'};
     padding-left: ${props => props.links ? '18.490vw' : '0px'};
      width: ${props => props.links ? 'calc(100% - 244px)' : 'auto'};
    justify-content: space-between;
`;
const VocalisLogoWrapper = styled.div`
height: 100%;
display: flex;
align-items: center;
`;
const LinkWrapper = styled.div<{marginRight?: string}>`
   // margin-right: ${props => props.marginRight ? props.marginRight : '0px'};
    width: auto;
    height: 100%;
    font-size: 18px;
    font-family: Roboto-Regular;
    letter-spacing: 0.72px;
    display: flex;
    flex-direction: row;
`;
const Span = styled.span`
display: flex;
    color: #FFFFFF;
    cursor:pointer;
    align-items: center;
    width: auto;
`;
const useStyles = makeStyles(() =>
    createStyles({
        hide: {
            visibility: 'hidden',
        },
        show : {
            visibility: 'visible',
        },
        root: {
           '&:hover': {
               backgroundColor: 'transparent !important',
            },
        },
    }),
);
export {
    HeaderWrapper,
    AppLogo,
    Separator,
    VocalisLogoWrapper,
    LinksWrapper,
    LinkWrapper,
    Icon,
    Span,
    MenuIcon,
    useStyles,
};
