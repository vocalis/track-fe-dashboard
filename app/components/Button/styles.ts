import styled from 'styled-components';
import styles from '../../styles/values';

const StyledButton = styled.button<{width?: string; height?: string, disabled?: boolean}>`
    width: ${props => props.width ? props.width : '246px'};
    height: ${props => props.height ? props.height : '54px'};
    color: ${styles.colors.TEXT.WHITE};
    background-color:  #38ABB6;
    font-size: 20px;
    font-family: Roboto-Regular;
    font-weight: normal;
    border-radius: 6px;
    border: 1px solid #38ABB6;
    opacity: 1;
    cursor: pointer;
:disabled{
  color:#fff;
  border-color: #EAEAEA;
  background-color: #EAEAEA;
}
:hover{
  background-color: ${props => props.disabled ? '#EAEAEA' : '#5CBAC3'} ;
}
 `;
export {
    StyledButton,
};
