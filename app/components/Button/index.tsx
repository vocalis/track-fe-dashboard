import React, {RefObject} from 'react';

import {StyledButton} from './styles';
import {FormattedMessage} from 'react-intl';
interface Props {
    onClick: (e: any) => void;
    title: string;
    width?: string;
    height?: string;
    disableButton?: boolean;
    className?: string;
}

const Button = React.forwardRef((props: Props, ref: RefObject<HTMLButtonElement>) => {
    const {onClick, width, title, height, disableButton, className} = props;
    return (
        <StyledButton
            width={width}
            height={height}
            onClick={onClick}
            disabled={disableButton}
            className={className}
            ref={ref}
        >
            <FormattedMessage id={title}/>
        </StyledButton>
        );
},
);
export default Button;
