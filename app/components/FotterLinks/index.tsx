/**
 *
 * FotterLinks
 *
 */
import React, {memo, Fragment} from 'react';
import {Link, Links} from './styles';
// import styled from 'styles/styled-components';
import {FormattedMessage} from 'react-intl';
import {RouteComponentProps, withRouter} from 'react-router';
import {
    aboutUs,
    appFooterLinksArr,
    contactUs,
    helpId,
    privacy,
    terms,
} from '../../containers/SideBar/constants';
import {about, contact, help,  privacyNotice, termsOfService} from '../../routes/paths';
import {selectTabAction} from '../../containers/SideBar/actions';
import {useDispatch} from 'react-redux';
interface Props extends  RouteComponentProps {
}


function FotterLinks(props: Props) {
    const {history } = props;
    const dispatch = useDispatch();
    const onLinkClick = (id) => {
        dispatch(selectTabAction(id));
        switch (id) {
            case helpId:
                history.push(`${help}`);
                break;
            case contactUs:
                history.push(`${contact}`);
                break;
            case aboutUs:
                history.push(`${about}`);
                break;
            case privacy:
                history.push(`${privacyNotice}`);
                break;
            case terms:
                history.push(`${termsOfService}`);
                break;
        }
    };
    return (
        <Links>
                {appFooterLinksArr.map(item =>
                    <Fragment  key={item.id}>
                    <div>{item.text !== 'HELP' ? '|' : ''}</div>
                    <Link key={item.id} onClick={() => {onLinkClick(item.id); }}>
                    <FormattedMessage  id={item.text}/>
                    </Link>
                    </Fragment >,
                )}
        </Links>
    );
}

export default memo(withRouter(FotterLinks));
