import styled from 'styled-components';
import styles from 'styles/values';

const Link = styled.div`
text-align: left;
    font-size: 18px;
    letter-spacing: 0.72px;
    color: #248AC5;
    opacity: 1;
    font-family: Roboto-Regular;
    cursor: pointer;
      border: 1px solid transparent;
    &:hover{
        background: #FCFCFC;
        border: 1px solid #DBDBDB;
        border-radius: 6px;
        opacity: 1;
}
`;
const Links = styled.div`
display: flex;
 text-align: center;
flex-direction: row;
width: 587px;
    justify-content: space-between;
`;

export {
    Link,
    Links,
};
