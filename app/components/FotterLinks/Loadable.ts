/**
 *
 * Asynchronously loads the component for FotterLinks
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
