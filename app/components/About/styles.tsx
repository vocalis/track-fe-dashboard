import styled from 'styled-components';
import React from 'react';
import styles from 'styles/values';
const Img = styled.img <{ opacity?: string }>`
    opacity: ${props => props.opacity ? props.opacity : 1};
`;
const ImgWrapper = styled.div<{ marginRight: string }>`
margin-right: ${props => props.marginRight};
 align-self: center;
`;
const ContentTitle = styled.div`
font-family: Roboto-Bold;
font-size: 35px;
padding-bottom: 30px;
`;
const Text = styled.div<{ width?: string }>`
font-family: Roboto-Light;
font-weight: bold;
font-size: 18px;
padding-bottom: 30px;
text-align: left;
color: #353535;
opacity: 1;
width: ${props => props.width ? props.width : '100%'};
`;
const SubTitle = styled.div`
font-family: Roboto-Bold;
    font-size: 28px;
font-weight:bold ;
height: 56px;
`;
const Contanier = styled.div`
width:100%;
           @media ${styles.sizes.MEDIUM_SCREEN}{
        padding-left: ${styles.sizes.HELP_SIDE_PADDINGE_LEFT};
        padding-right: ${styles.sizes.HELP_SIDE_PADDINGE_ROGHT};
         }
   @media ${styles.sizes.SMALL_SCREEN}{
   padding-right: ${styles.sizes.HELP_SIDE_PADDINGE_ROGHT_SMALL_SCREEN};
  padding-left: ${styles.sizes.HELP_SIDE_PADDINGE_LEFT_SMALL_SCREEN};
  }
     @media ${styles.sizes.LARGE_SCREEN}{
   padding-right: ${styles.sizes.APP_SIDE_PADDING_RIGHT_LARGE_SCREEN};
  padding-left: ${styles.sizes.APP_SIDE_PADDING_LEFT_LARGE_SCREEN};
  }
background-color: #FFFFFF;
display: flex;
flex-direction: column;
padding-top: 73px;
`;
export {
    Img,
    ImgWrapper,
    SubTitle,
    Text,
    ContentTitle,
    Contanier,
};
