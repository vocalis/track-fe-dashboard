/**
 *
 * About
 *
 */
import React, {memo, useEffect} from 'react';
import Layout from '../common/Layout';
import {
  Contanier,
  Img,
  ImgWrapper,
  Text,
  SubTitle,
  ContentTitle,
} from './styles';
import { FormattedMessage } from 'react-intl';
import {toggleSideBar} from '../../containers/SideBar/actions';
import {useDispatch} from 'react-redux';
const ce = require('images/ce.png');
interface Props {}
function About(props: Props) {
    const dispatch = useDispatch();
    useEffect(() => {
        window.scrollTo(0, 0);
        dispatch(toggleSideBar(false));
    });
    return (
    <Layout title="About">
      <Contanier>
        <ContentTitle>
          <FormattedMessage id={'VOCALISTRACK_VER'} />
        </ContentTitle>
        <SubTitle>
          <FormattedMessage id={'DASHBOARD_VER'} />
        </SubTitle>
        <Text>
          <ImgWrapper marginRight="45px">
            <Img width="100px" height="60px" src={ce} alt="About" />
          </ImgWrapper>
        </Text>
        <Text width="335px">
      <FormattedMessage id={'ABOUT_PARAGRAPH_1'}
                        values={{
                          lineBreaking: <br/>,
                          email:
                            <a style={{textDecoration: 'none', color: '#1e70bf'}}
                               href="mailto:info@vocalishealth.com"
                               target="_blank"
                            >
                            info@vocalishealth.com
                            </a>,
                          website:
                            <a style={{textDecoration: 'none', color: '#1e70bf'}}
                               href="https://www.vocalishealth.com/"
                               target="_blank" >
                              www.vocalishealth.com
                            </a>}}/>

        </Text>
        <Text width="400px">
          <FormattedMessage id={'ABOUT_PARAGRAPH_2'}
                            values={{
                              lineBreaking: <br/>,
                              email:
                                  <a style={{textDecoration: 'none', color: '#1e70bf'}}
                                     href="mailto:mail@obelis.net"
                                     target="_blank"
                                  >
                                    mail@obelis.net
                                  </a>}}
          />
        </Text>
        <SubTitle>
          <FormattedMessage id={'ABOUT_VOCALIS'}/>
        </SubTitle>
        <Text width="100%">
          <FormattedMessage id={'ABOUT_PARAGRAPH_3'}
                            values={{lineBreaking: <br/>}}
   />
        </Text>
      </Contanier>
    </Layout>
  );
}

export default memo(About);
