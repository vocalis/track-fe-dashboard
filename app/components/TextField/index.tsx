import React, {useEffect, useState, Fragment} from 'react';
import {
    Input,
    TitleWrapper,
    TextFieldWrapper,
    ErrorWrapper,
    HelpText,
} from './styles';
import {FormattedMessage, injectIntl} from 'react-intl';
import './style.css';

interface InputProps {
    onChange: (id: string, value: string | null) => void;
    type: string;
    intl: any;
    id?: string;
    placeHolder?: string;
    isSubmitted: boolean;
    error?: string;
    value?: string;
    dirty?: boolean;
    label?: JSX.Element;
    placeHolderPaddingLeft?: string;
    autoFocus?: boolean;
    style?: object;
    helpText?: string;
    fontFamily?: string;
    errorPossition?: string;
    hideErrorMessage?: boolean;
    disabled?: boolean;
    onChangePasswordClicked?: () => {};
    showChangePssswordModal?: boolean;
    editPage?: boolean;
}

const TextField: React.FC<InputProps> = (props: InputProps) => {
    const {
        onChange,
        label,
        id,
        placeHolder,
        type,
        error,
        isSubmitted,
        value,
        placeHolderPaddingLeft,
        autoFocus,
        style,
        helpText,
        fontFamily,
        intl,
        errorPossition,
        hideErrorMessage,
        disabled,
        onChangePasswordClicked,
        showChangePssswordModal,
        editPage,
    } = props;
    const [inputValue, setInputValue] = useState(value || '');
    const [prefixWidth, setPrefixWidth] = useState(20);
    const onInputChange = (event) => {
        if (event.currentTarget.value !== ' ') {
            setInputValue(event.currentTarget.value);
            onChange(event.target.id, event.currentTarget.value);
        }

    };
    useEffect(() => {
        setInputValue(value || '');
    }, [value]);
    const getHelpText = () => {
        if (id === 'password' && helpText) {
            if (!editPage) {
                return (
                    <HelpText color={getHelpTextColor()}>
                        {' '}
                        <FormattedMessage id={helpText}/>
                    </HelpText>
                );
            } else {
                if (editPage && showChangePssswordModal) {
                    return (
                        <HelpText color={getHelpTextColor()}>
                            {' '}
                            <FormattedMessage id={helpText}/>
                        </HelpText>
                    );
                } else {
                    return (
                        <HelpText
                            onClick={onChangePasswordClicked}
                            color={'#707070'}
                            cursor="pointer"
                        >
                            <FormattedMessage id={'CHANGE_PASSWORD'}/>
                        </HelpText>
                    );
                }
            }
        }
        return <Fragment/>;
    };
    const getHelpTextColor = () => {
        if (error && isSubmitted && !hideErrorMessage) {
            return '#D22242';
        } else {
            return '#707070';
        }
    };
    useEffect(() => {
        let offsetWidth = 9;
        if (document && document.getElementById('prefix')) {
            // @ts-ignore
            offsetWidth = document.getElementById('prefix').offsetWidth + 2;
        }
        setPrefixWidth(offsetWidth);
    });
    return (
        <TextFieldWrapper style={style}>
            <TitleWrapper>{label}</TitleWrapper>
            {id === 'username' && !editPage ? (
                <div style={{position: 'relative'}}>
                    <div
                        id="prefix"
                        style={{
                            position: 'absolute',
                            color: 'rgb(120, 120, 120)',
                            top: '18px',
                            left: '4px',
                        }}
                    >
                        {' '}
                        {window.location.host.split('.')[0]}_
                    </div>
                    <Input
                        type={type}
                        placeholder={
                            placeHolder ? intl.formatMessage({id: `${placeHolder}`}) : ''
                        }
                        id={id}
                        onChange={onInputChange}
                        value={inputValue === null ? '' : inputValue}
                        label={label}
                        style={{
                            paddingLeft: prefixWidth,
                        }}
                        placeHolderPaddingLeft={placeHolderPaddingLeft}
                        autoFocus={autoFocus}
                        fontFamily={fontFamily}
                        error={isSubmitted ? error : ''}
                        disabled={disabled}
                    />
                </div>
            ) : (
                <Input
                    type={type}
                    placeholder={
                        placeHolder ? intl.formatMessage({id: `${placeHolder}`}) : ''
                    }
                    id={id}
                    onChange={onInputChange}
                    value={inputValue === null ? '' : inputValue}
                    label={label}
                    placeHolderPaddingLeft={placeHolderPaddingLeft}
                    autoFocus={autoFocus}
                    fontFamily={fontFamily}
                    error={isSubmitted ? error : ''}
                    disabled={disabled}
                />
            )}
            <ErrorWrapper
                isPassword={id === 'password'}
                errorPossition={errorPossition}
            >
                {isSubmitted &&
                !hideErrorMessage &&
                !helpText &&
                error &&
                intl.formatMessage({id: `${error}`})}
            </ErrorWrapper>
            {getHelpText()}
        </TextFieldWrapper>
    );
};

export default injectIntl(TextField);
