
import styled from 'styled-components';

const Input = styled.input<{
    type: string,
    error?: string,
    placeHolderPaddingLeft?: string,
    label ?: JSX.Element
    fontFamily?: string; }>`
  width: 100%;
  height: 41px;
  padding-top: 10px;
  padding-bottom: 10px;
  border: 1px solid ${props => props.error ? '#D22242' : '#DBDBDB'}
  box-sizing: border-box;
   padding-left: ${props => props.placeHolderPaddingLeft ? props.placeHolderPaddingLeft : '9px'};
  font-size: 18px;
  margin-top: ${props => props.label ? '11px' : '0px'}
      border-radius: 8px;
  color: #353535;
  font-family:${props => props.fontFamily ? props.fontFamily : 'Roboto-Light'} ;
  &::-webkit-input-placeholder {
    font-size: 18px;
    color: #353535;
    opacity: 0.70;
    font-family:${props => props.fontFamily ? props.fontFamily : 'Roboto-Light'} ;
    // padding-left: ${props => props.placeHolderPaddingLeft ? props.placeHolderPaddingLeft : '0px'};
  }
  &:focus {
  border-radius: 8px;
    outline: none ;
}
::-webkit-clear-button
{
    display: none; /* Hide the button */
    -webkit-appearance: none; /* turn off default browser styling */
}
`;

const ErrorWrapper = styled.div<{isPassword?: boolean, errorPossition?: string}>`
position: absolute;
align-self: end;

   bottom: ${props => props.isPassword ? '-35px' : '-27px'};
   bottom: ${props => props.isPassword && props.errorPossition  ? props.errorPossition : '-35px'};
    font-size: 16px;
    color: #D22242;
    font-family: Roboto-Regular;
        font-weight: normal;
`;
const TextFieldWrapper = styled.div`
position: relative;
width: 100%;
`;
const TitleWrapper = styled.div`

`;
const HelpText = styled.div<{color: string, cursor?: string}>`
position: absolute;
align-self: end;
font-family: Roboto-Regular;
    font-size: 16px;
        bottom: -53px;
            cursor: ${props => props.cursor };
    color: ${props => props.color }

`;
const PasswordWrapper = styled.div`
  margin-left: 5px;
  cursor: pointer;
  font-size: 18px;
`;
const InputWithLink = styled.div`
  display: flex;
    flex-direction: row;
`;
export {
    Input,
    HelpText,
    TextFieldWrapper,
    TitleWrapper,
    ErrorWrapper,
    PasswordWrapper,
    InputWithLink};

