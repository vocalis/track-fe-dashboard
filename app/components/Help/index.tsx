/**
 *
 * Help
 *
 */
import React, {memo, Fragment, useState, useEffect} from 'react';
import Layout from '../common/Layout';
import {FormattedMessage} from 'react-intl';
import {
    Titles,
    HeadLines,
    headLinesArray,
} from './constants';
import {
    IconWithTextWrapper,
    Link,
    Img,
    ImgWrapper,
    Content,
    PanelWrapper,
    Span,
    ContentTitle,
    SubTitle,
    StyledExpansionPanelSummary,
    Text,
    StyledExpansionPanel,
    StyledExpansionPanelDetails,
    Ul,
    Li,
    LinkText,
    TextFieldWrapper,
    IconWrapper,
    Contanier,
    Div,
    Title,
    TitleWrapper,
    HeaderWrapper,
    Ol,
    ViewImg,
    ContentLi,
    ContentLo,
    Stam,
} from './styles';
import TextField from '../TextField';
import Warning from '../Warning';
import {useDispatch} from 'react-redux';
import {toggleSideBar} from '../../containers/SideBar/actions';
import './style.css';

const helpIcon = require('images/helpIcon.svg');
const downloadUserManual = require('images/pdfIcon.svg');
const contactSupport = require('images/contactSupportIcon.svg');
const downArrow = require('images/downOpen.svg');
const searchIcon = require('images/searchIcon.svg');
const dashboardUserManual = require('components/Help/dashboardUserManual.json');

interface Props {

}

interface Paragraph {
    subTitle: string;
    type: string;
    content: string[];
    collection: Paragraph[];
    styleProp?: object;
}

interface Section1 {
    title: string;
    content: Paragraph[];
}

interface Collection {
    subTitle: string;
    type: string;
    content: string[];
    styleProp?: object;
}

interface Paragraph {
    subTitle: string;
    type: string;
    content: string[];
}

interface Obj {
    collection: Collection[];
}

interface Paragraph5 {
    subTitle: string;
    type: string;
    content: Obj[] | string[];
    styleProp?: object;
}

interface Section5 {
    title: string;
    content: Paragraph5[];
}

const loginView = require('../../images/loginView.png');
const dashboardView = require('../../images/dashboardView.svg');
const dasboardFilterView = require('../../images/dasboardFilterView.svg');
const dashboardPaginationView = require('../../images/dashboardPaginationView.svg');
const dashboardMonitorTableView = require('../../images/dashboardMonitorTableView.svg');
const pateintDeatailsView = require('../../images/pateintDeatailsView.svg');
const scrollingAnalysisView = require('../../images/scrollingAnalysisView.svg');
const menuView = require('../../images/menuView.svg');
const patientManagementView = require('../../images/patientManagementView.svg');
const patientFormView = require('../../images/pateintFormView.svg');
const editPatientFormView = require('../../images/editPatientFormView.svg');
const medicalStaffMenuView = require('../../images/medicalStaffMenuView.svg');
const medicalStaffManagementView = require('../../images/medicalStaffManagementView.svg');
const medicalStaffFormView = require('../../images/medicalStaffFormView.svg');
const editMedicalStaffFormView = require('../../images/editMedicalStaffFormView.svg');
const logoutView = require('../../images/logoutView.svg');
const shortnessOfBreathLevelView = require('../../images/shortnessOfBreathLevelView.svg');
const dayToDayComparisonView = require('../../images/dayToDayComparisonView.svg');
const ptientRecordingView = require('../../images/ptientRecordingView.svg');
const manufacturer = require('../../images/manufacturer.svg');
const authorizedRepresentative = require('../../images/authorizedRepresentative.svg');
const ce = require('../../images/ce.png');
const dailyCOPD = require('../../images/dailyCOPD.svg');

function Help(props: Props) {
    const dispatch = useDispatch();
    // useEffect(() => {
    //     if (document &&
    //         document.getElementsByTagName('body') &&
    //         document.getElementsByTagName('body')[0]) {
    //         document.getElementsByTagName('body');
    //         document.getElementsByTagName('body')[0].style['overflow-y'] = 'auto';
    //     }
    //
    // });
    useEffect(() => {
        dispatch(toggleSideBar(false));
        window.scrollTo(0, 0);
    });
    const [value, setValue] = useState('');
    const [headLines, setheadLines] = useState(headLinesArray);
    const [expanded, setExpanded] = useState([...Array(10).fill(undefined)]);
    const handleExpandedChange = (index) => {
        const expandedArray: any[] = expanded;
        expandedArray[index] === undefined ? expandedArray[index] = index : expandedArray[index] = undefined;
        setExpanded([...expandedArray]);
    };
    const goToSubTitle = (id: string) => {
        if (document && document.getElementById(id)) {
            const helpContent = document.getElementById('helpContent');
            const element = document && document.getElementById(id);
            const offsetTop = element && element.offsetTop;
            if (helpContent && offsetTop !== null) {
                helpContent.scrollTo(window.scrollX, offsetTop);
                dispatch(toggleSideBar(false));
                window.scrollTo(0, 0);
            }
        }
    };

    const getSecondSubTitle = (texts) => (
        texts.map((text: Titles): JSX.Element => (
            <Ul key={text.id} paddingInlineStart="15px">
                <Li ListStyleType="none" onClick={() => goToSubTitle(text.titleText)}>
                    <span style={{paddingRight: '20px'}}>-</span>
                    < LinkText>{text.titleText}</LinkText>
                </Li>
            </Ul>
        ))
    );
    const getSubTitle = (subTitles) => {
        if (subTitles.length > 0) {
            return (
                <StyledExpansionPanelDetails>
                    <Ul paddingInlineStart="30px">
                        {subTitles.map((item: Titles) => (
                            <Fragment key={item.id}>
                                <Li onClick={() => goToSubTitle(item.titleText)} key={item.id} ListStyleType="initial">
                                    {item.titleText}
                                </Li>
                                {item.texts && getSecondSubTitle(item.texts)}
                            </Fragment>
                        ))}
                    </Ul>
                </StyledExpansionPanelDetails>
            );
        } else {
            return <Fragment/>;
        }
    };
    const getHeadLine = (title: Titles | null, subTitle: Titles[] | []) => (
        title ? (
            <StyledExpansionPanelSummary
                expandIcon={subTitle.length > 0 && <Img src={downArrow} alt="openIcon"/>}
                aria-controls="panel1a-content"
                id={title.id}
                onClick={() => goToSubTitle(title.titleText)}
            >
                {title.titleText}
            </StyledExpansionPanelSummary>
        ) : <Fragment/>
    );
    const onSearch = (name, value) => {
        setValue(value);
        setExpanded([]);
    };
    const checkSecondSubStitle = (title: Titles[]): Titles[] | [] => {
        if (title.length > 0) {
            return title.filter(item => (item.titleText.toUpperCase().includes(value.toUpperCase())));
        } else {
            return [];
        }

    };
    const checkSubTitle = (title: Titles[]) => {
        let newSecondSubTitles: Titles[] | [] = [];
        const newSubTitles = title.map(item => {
            if (item.titleText.toUpperCase().includes(value.toUpperCase())) {
                return item;
            } else {
                if (item.texts) {
                    newSecondSubTitles = checkSecondSubStitle(item.texts);
                    if (newSecondSubTitles.length > 0) {
                        return {...item, texts: newSecondSubTitles};
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            }
        });
        return newSubTitles.filter(item => item !== null);
    };
    const getParagraph = (content: string[]) => (content.map((item, index) => {
        return (
            <Text key={index}>
                {<div>
                    <div dangerouslySetInnerHTML={{__html: item}}/>
                </div>}
            </Text>
        );
    }));
    const getParagraphs = (subTitle: string, content: string[], index: number) => {
        return (
            <Fragment key={index}>
                {subTitle && <SubTitle id={subTitle}>
                    {subTitle}
                </SubTitle>}
                {getParagraph(content)}
            </Fragment>
        );
    };
    const getItem = (text: string[]) => (text.map((item, index) => (
        <ContentLi key={index}>
            <div dangerouslySetInnerHTML={{__html: item}}/>
        </ContentLi>
    )));
    const getOrderList = (subTitle: string, text: string[], index: number, styleProp?: object) => {
        return (
            <Fragment key={index}>
                {subTitle && <SubTitle id={subTitle}>
                    {subTitle}
                </SubTitle>}
                <ol onClick={() => goToSubTitle(subTitle)} style={styleProp}>
                    {getItem(text)}
                </ol>
            </Fragment>
        );
    };

    const getWarning = (text: string[]) => (text.map((item, index) => (
        <Warning key={index} text={item}/>
    )));
    const getWarnings = (subTitle: string, content: string[], index: number) => {
        return (
            <Fragment key={index}>
                {subTitle && <SubTitle id={subTitle}>
                    {subTitle}
                </SubTitle>}
                {getWarning(content)}
            </Fragment>
        );
    };

    const getRow = (text: string[]) => (text.map((item, index) => (
        <div key={index} dangerouslySetInnerHTML={{__html: item}}/>
    )));
    const getRows = (subTitle: string, content: string[], index: number) => {
        return (
            <span key={index} style={{display: 'flex', justifyContent: 'space-between'}}>
                {subTitle && <Stam id={subTitle}>
                    {subTitle}
                </Stam>}
                <div style={{display: 'flex', flexDirection: 'column'}}>
                {getRow(content)}
                </div>
            </span>
        );
    };
    const getIcon = (subTitle: string, content: string[], index: number) => {
        switch (content[0]) {
            case 'ce':
                return (
                    <div key={index} style={{width: '100%'}}>
                        <img
                            width={'66px'}
                            height={'34px'}
                            src={ce} alt="ce"
                        />
                    </div>
                );
            case 'authorizedRepresentative' :
                return (
                    <div key={index} style={{width: '100%', marginTop: '40px'}}>
                        <img width={'70px'}
                             height={'45px'}
                             src={authorizedRepresentative}
                             alt="authorized Representative"
                        />
                    </div>
                );
            case 'manufacturer':
                return (
                    <div key={index} style={{width: '100%', marginTop: '50px'}}>
                        <img width={'66px'}
                             height={'40px'}
                             src={manufacturer}
                             alt="manufacturer"/>
                    </div>
                );
            default:
                return <Fragment key={index}/>;
        }
    };
    const getImg = (subTitle: string, content: string[], index: number) => {
        switch (content[0]) {
            case 'loginView':
                return <ViewImg key={index} src={loginView} alt="login"/>;
            case 'dashboardView' :
                return <ViewImg key={index} src={dashboardView} alt="dashboard"/>;
            case 'dasboardFilterView':
                return <ViewImg key={index} src={dasboardFilterView} alt="dasboard filter"/>;
            case 'dashboardPaginationView':
                return <ViewImg key={index} src={dashboardPaginationView} alt="dashboard pagination"/>;
            case 'dashboardMonitorTableView':
                return <ViewImg key={index} src={dashboardMonitorTableView} alt="dashboard monitor table"/>;
            case 'pateintDeatailsView':
                return <ViewImg key={index} src={pateintDeatailsView} alt="pateint deatails"/>;
            case 'scrollingAnalysisView':
                return <ViewImg key={index} src={scrollingAnalysisView} alt="scrolling analysis"/>;
            case 'ptientRecordingView':
                return <ViewImg key={index} src={ptientRecordingView} alt="scrolling analysis"/>;
            case 'dailyCOPD':
                return <ViewImg key={index} src={dailyCOPD} alt="scrolling analysis"/>;
            case 'menuView':
                return <ViewImg key={index} src={menuView} alt="menu"/>;
            case 'patientManagementView':
                return <ViewImg key={index} src={patientManagementView} alt="patient management"/>;
            case 'patientFormView':
                return <ViewImg key={index} src={patientFormView} alt="editPatient form"/>;
            case 'editPatientFormView':
                return <ViewImg key={index} src={editPatientFormView} alt="editPatient form"/>;
            case 'editMedicalStaffFormView' :
                return <ViewImg key={index} src={editMedicalStaffFormView} alt="edit medicalStaff form "/>;
            case 'medicalStaffFormView':
                return <ViewImg key={index} src={medicalStaffFormView} alt="medical staff form"/>;
            case 'medicalStaffManagementView':
                return <ViewImg key={index} src={medicalStaffManagementView} alt="medical staff management"/>;
            case 'medicalStaffMenuView':
                return <ViewImg key={index} src={medicalStaffMenuView} alt="medical staff menu"/>;
            case 'logoutView':
                return <ViewImg key={index} src={logoutView} alt="log out"/>;
            case 'shortnessOfBreathLevelView':
                return <ViewImg key={index} src={shortnessOfBreathLevelView} alt="shortness of breath level"/>;
            case 'dayToDayComparisonView':
                return <ViewImg key={index} src={dayToDayComparisonView} alt="day to day comparison"/>;
            default :
                return <ViewImg key={index} src="" alt="error"/>;
        }
    };
    const getOrderListOfCollection = (subTitle: string, content: Obj[], index: number, styleProp?: object) => {
        return (
            <Fragment key={index}>
                <ContentLo style={styleProp}>
                    {subTitle && <SubTitle id={subTitle}>
                        {subTitle}
                    </SubTitle>}
                    {content.map((item, index2) => {
                            return (
                                <ContentLi key={index2}>
                                    {item.collection.map((ob, index3) => {
                                        switch (ob.type) {
                                            case 'p':
                                                return getParagraphs(ob.subTitle, ob.content, index3);
                                            case  'ol':
                                                return getOrderList(ob.subTitle, ob.content, index3, ob.styleProp);
                                            case 'warning':
                                                return getWarnings(ob.subTitle, ob.content, index3);
                                            case 'img':
                                                return getImg(ob.subTitle, ob.content, index3);
                                            case 'nestedOl':
                                                return getOrderListOfCollection(
                                                    ob.subTitle,
                                                    ob.content as unknown as Obj[],
                                                    index3,
                                                    ob.styleProp,
                                                    );
                                            default :
                                                return <Fragment key={index3}/>;
                                        }
                                    })}
                                </ContentLi>
                            );
                        },
                    )}
                </ContentLo>
            </Fragment>
        );
    };
    useEffect(() => {
        let newHeadLines: any = [];
        if (value === '') {
            newHeadLines = headLinesArray;
        } else {
            newHeadLines = headLinesArray.map((item: HeadLines) => {
                if (item.title && item.title.titleText.toUpperCase().includes(value.toUpperCase())) {
                    return item;
                } else {
                    const newSubTitles = checkSubTitle(item.subTitles);
                    if (newSubTitles.length > 0) {
                        return {...item, subTitles: newSubTitles};
                    } else {
                        return null;
                    }
                }
            });
        }
        newHeadLines = newHeadLines.filter(item => item !== null);
        if (newHeadLines.length > 0) {
            setheadLines(newHeadLines);
        } else {
            setheadLines([]);
        }
    }, [value]);
    return (
        <Layout title="Help">
            <HeaderWrapper>
                <div style={{display: 'flex', height: '100%'}}>
                    <ImgWrapper marginRight="45px">
                        <Img width="60px" height="73px" src={helpIcon} alt="HelpIcon"/>
                    </ImgWrapper>
                    <TitleWrapper>
                        <Span color="#707070" letterSpacing="0.72px">
                            Home >
                        </Span>
                        <Title>
                            <FormattedMessage id={'HELP'}/>
                        </Title>
                    </TitleWrapper>
                </div>
                <Div>
                    <IconWithTextWrapper href="assets/DashboardUserManual.pdf" download>
                        <Img width="16px" height="22px" src={downloadUserManual} alt="pdf"/>
                        <Link>
                            <FormattedMessage id={'DOWNLOAD_THE_COMPLETE_USER_MANUAL'}/>
                        </Link>
                    </IconWithTextWrapper>
                    <IconWithTextWrapper href="mailto:support@vocalishealth.com" target="_blank" marginRight="30px">
                        <Img width="23px" height="23px" src={contactSupport} alt="contact"/>
                        <Link>
                            <FormattedMessage id={'CONTACT_SUPPORT'}/>
                        </Link>
                    </IconWithTextWrapper>
                </Div>
            </HeaderWrapper>
            <Contanier id="helpContainer">
                <PanelWrapper>
                    <TextFieldWrapper>
                        <IconWrapper>
                            <Img src={searchIcon} opacity="0.3" alt="search Icon"/>
                        </IconWrapper>
                        <TextField
                            onChange={onSearch}
                            type="text"
                            value={value}
                            id="searchTitle"
                            placeHolder="SEARCH"
                            placeHolderPaddingLeft="42px"
                            autoFocus={true}
                        />
                    </TextFieldWrapper>
                    {headLines.map((item: HeadLines, index: number): JSX.Element => (
                        <StyledExpansionPanel
                            onChange={() => handleExpandedChange(index)}
                            key={index}
                            expanded={(expanded[index] !== undefined || value !== '') && item.subTitles.length > 0}
                        >
                            {getHeadLine(item.title, item.subTitles)}
                            {getSubTitle(item.subTitles)}
                        </StyledExpansionPanel>
                    ))}
                </PanelWrapper>
                <Content id={'helpContent'}>
                    <ContentTitle
                        id={dashboardUserManual.section1.title}
                    >
                        {dashboardUserManual.section1.title}
                    </ContentTitle>
                    {dashboardUserManual.section1.paragraph.map((item: Paragraph, index: number) => {
                        switch (item.type) {
                            case 'icon':
                                return getIcon(item.subTitle, item.content, index);
                            case  'row':
                                return getRows(item.subTitle, item.content, index);
                            default :
                                return <Fragment key={index}/>;
                        }
                    })}
                    <ContentTitle
                        id={dashboardUserManual.section2.title}
                    >
                        {dashboardUserManual.section2.title}
                    </ContentTitle>
                    {dashboardUserManual.section2.paragraph.map((item: Paragraph, index: number) => {
                        switch (item.type) {
                            case 'p':
                                return getParagraphs(item.subTitle, item.content, index);
                            case  'ol':
                                return getOrderList(item.subTitle, item.content, index, item.styleProp);
                            case 'warning':
                                return getWarnings(item.subTitle, item.content, index);
                            default :
                                return <Fragment key={index}/>;
                        }
                    })}

                    <ContentTitle id={dashboardUserManual.section3.title}>
                        {dashboardUserManual.section3.title}
                    </ContentTitle>
                    {dashboardUserManual.section3.paragraph.map((item: Paragraph, index: number) => {
                        switch (item.type) {
                            case 'p':
                                return getParagraphs(item.subTitle, item.content, index);
                            case  'ol':
                                return getOrderList(item.subTitle, item.content, index, item.styleProp);
                            case 'warning':
                                return getWarnings(item.subTitle, item.content, index);
                            default :
                                return <Fragment key={index}/>;
                        }
                    })}

                    <ContentTitle id={dashboardUserManual.section4.title}>
                        {dashboardUserManual.section4.title}
                    </ContentTitle>
                    {dashboardUserManual.section4.paragraph.map((item: Paragraph, index: number) => {
                        switch (item.type) {
                            case 'p':
                                return getParagraphs(item.subTitle, item.content, index);
                            case  'ol':
                                return getOrderList(item.subTitle, item.content, index, item.styleProp);
                            case 'warning':
                                return getWarnings(item.subTitle, item.content, index);
                            default :
                                return <Fragment key={index}/>;
                        }
                    })}

                    <ContentTitle id={dashboardUserManual.section5.title}>
                        {dashboardUserManual.section5.title}
                    </ContentTitle>
                    {dashboardUserManual.section5.paragraph.map((item: Paragraph5, index: number) => {
                        switch (item.type) {
                            case 'p':
                                return getParagraphs(item.subTitle, item.content as string[], index);
                            case  'ol':
                                return getOrderListOfCollection(
                                    item.subTitle,
                                    item.content as Obj[],
                                    index,
                                        item.styleProp);
                            case 'warning':
                                return getWarnings(item.subTitle, item.content as string[], index);
                            default :
                                return <Fragment key={index}/>;
                        }
                    })}

                    <ContentTitle id={dashboardUserManual.section6.title}>
                        {dashboardUserManual.section6.title}
                    </ContentTitle>
                    {dashboardUserManual.section6.paragraph.map((item: Paragraph, index: number) => {
                        switch (item.type) {
                            case 'p':
                                return getParagraphs(item.subTitle, item.content, index);
                            case  'ol':
                                return getOrderList(item.subTitle, item.content, index, {});
                            case 'warning':
                                return getWarnings(item.subTitle, item.content, index);
                            default :
                                return <Fragment key={index}/>;
                        }
                    })}
                    <ContentTitle id={dashboardUserManual.section7.title}>
                        {dashboardUserManual.section7.title}
                    </ContentTitle>
                    {dashboardUserManual.section7.paragraph.map((item: Paragraph, index: number) => {
                        switch (item.type) {
                            case 'p':
                                return getParagraphs(item.subTitle, item.content, index);
                            case  'ol':
                                return getOrderList(item.subTitle, item.content, index, {});
                            case 'warning':
                                return getWarnings(item.subTitle, item.content, index);
                            default :
                                return <Fragment key={index}/>;
                        }
                    })}
                </Content>
            </Contanier>
        </Layout>
    );
}

export default memo(Help);
