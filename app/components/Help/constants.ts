
export interface  HeadLines {
    title: Titles;
    subTitles: Titles[] | [];
}
export interface  Titles {
    id: string;
    titleText: string;
    texts?: Titles[] | [];
}
export const headLinesArray: HeadLines[] = [
    {
        title: {id: 'deviceIdentification ' , titleText: 'Device Identification'},
        subTitles: [],
    },
    {
        title: {id: 'deviceDescription ' , titleText: 'Device Description'},
        subTitles: [
            {
                titleText: 'Intended Use',
                id : 'intendedUse',
                texts : [],
            }, {
                titleText: 'Indications for Use',
                id : 'indicationsForUse',
                texts : [],
            }, {
                titleText: 'Overview of VocalisTrack Device',
                id : 'overviewOfVocalisTrackDevice',
                texts : [],
            }, {
                titleText: 'Device Performance and Accuracy of Results',
                id : 'devicePerformanceAndAccuracyofResults',
                texts : [],
            },
        ],
    },
    {
        title: {id: 'safetyGuidelines' , titleText: 'Safety Guidance'},
        subTitles: [],
    },
    {
        title: {id: 'protocolforUse' , titleText: 'Protocol for Use'},
        subTitles: [],
    },
    {
        title: {id: 'operationInstructions' , titleText: 'Operating Instructions'},
        subTitles: [
            {
                titleText: 'Start and login',
                id : 'startAndLogin',
                texts : [],
            }, {
                titleText: 'Patient overview page',
                id : 'patientOverviewPage',
                texts : [
                    {titleText: 'Search filter' , id: 'searchFilter'},
                    {titleText: 'Navigating through patient pages' , id: 'navigatingThroughPatientPages'},
                    {titleText: 'Patient information' , id: 'patientInformation'},
                ],
            },
            {
                titleText: 'Detailed patient view',
                id : 'detailedPatientView',
                texts : [
                    {titleText: 'Entering detailed patient view' , id: 'enteringDetailedPatientView'},
                    {titleText: 'Scrolling analysis history and selecting date' ,
                     id: 'scrollingAnalysisHistoryAndSelectingDate'},
                    {titleText: 'Listen to patient recordings and review daily COPD dairy' ,
                     id: 'listenToPatientRecordingsAndReviewDailyCOPDDairy'},
                ],
            }, {
                titleText: 'Patient management',
                id : 'patientManagement',
                texts : [
                    {titleText: 'Accessing patient management view' , id: 'accessingPatientManagementView'},
                    {titleText: 'Patient management view' , id: 'patientManagementView'},
                    {titleText: 'Adding patient user' , id: 'addingPatientUser'},
                    {titleText: 'Updating patient user' , id: 'updatingPatientUser'},
                ],
            }, {
                titleText: 'Medical Staff management',
                id : 'medicalStaffManagement',
                texts : [
                    {titleText: 'Accessing Medical Staff management' , id: 'accessingPatientManagementView'},
                    {titleText: 'Medical Staff management view' , id: 'medicalStaffManagementView'},
                    {titleText: 'Adding a Medical Staff user' , id: 'addingAMedicalStaffUser'},
                    {titleText: 'Updating a Medical Staff user' , id: 'updatingAMedicalStaffUser'},
                ],
            },
            {
                titleText: 'Logout',
                id : 'logout',
                texts : [],
            },
        ],
    },
    {
        title: {id: 'configuration' , titleText: 'Configuration'},
        subTitles: [],
    },
    {
        title: {id: 'troubleshooting' , titleText: 'Troubleshooting'},
        subTitles: [],
    },

];
