{
  "section1": {
    "title": "Device Identification",
    "paragraph": [
      {
        "subTitle": "Device Name:",
        "type": "row",
        "content": [
         "VocalisTrack Version 1.1"
        ]
      },
      {
        "subTitle": "",
        "type": "icon",
        "content": [
          "ce"
        ]
      },
      {
        "subTitle": "",
        "type": "icon",
        "content": [
          "manufacturer"
        ]
      },
      {
        "subTitle": "Manufacturer:",
        "type": "row",
        "content": [
          "Vocalis Health Ltd.",
          "Address: Sderot Hameginim 32, Haifa, Israel",
          "Email:  <a href='info@vocalishealth.com' target='_blank'>info@vocalishealth.com</a>",
          "Website: <a href='https://www.vocalishealth.com' target='_blank'>  www.vocalishealth.com<a>"
        ]
      },
      {
        "subTitle": "",
        "type": "icon",
        "content": [
          "authorizedRepresentative"
        ]
      },
      {
        "subTitle": "Authorized Representative:",
        "type": "row",
        "content": [
          "Obelis Group",
          "Bd Général Wahis, 53",
          "B-1030 Brussels",
          "Belgium",
          "Tel: +3227325954",
          "Fax: +3227326003",
         " Email: <a href='mailto:mail@obelis.net' target='_blank'> mail@obelis.net </a>"
        ]
      }
    ]
  },
  "section2": {
    "title": "Device Description",
    "paragraph": [
      {
        "subTitle": "Intended Use",
        "type": "p",
        "content": [
          "The VocalisTrack Artificial Intelligence based stand-alone software device is a mobile application that is intended for remote detection of shortness of breath, a common symptom of Chronic Obstructive Pulmonary Disease (COPD) in COPD patients.",
          "It automatically detects improvement, worsening or no change of shortness of breath. The device does not discern or grades dyspnea severity.",
          "The VocalisTrack device is self-administered by the patient.",
          "It is a prescription device."
        ]
      },
      {
        "subTitle": "Indications for Use",
        "type": "p",
        "content": [
          "The VocalisTrack device is indicated for use only on diagnosed COPD patients aged 40 to 80 years.",
          "The VocalisTrack device users should meet none of the following exclusion criteria:"
        ]
      },
      {
        "subTitle": "",
        "type": "ol",
        "content": [
          "Inability to speak from any reason or cause.",
          "Visual disorders significantly limiting the ability to read and operate a smartphone.",
          "Inability to operate a smartphone."
        ]
      },
      {
        "subTitle": "Overview of VocalisTrack Device",
        "type": "p",
        "content": [
          "The product is a stand-alone Artificial-Intelligence (AI) based software device that is intended for detecting shortness of breath, which is a common symptom of Chronic Obstructive Pulmonary Disease (COPD) patients.",
          "The product is self-administered and has the purpose of displaying, to clinicians, the current shortness of breath condition of a patient compared to his shortness of breath from previous time periods. The product provides clinicians with opportunity to listen and compare the recordings archived in the product database.",
          "This Mobile App is a part of the product allowing a patient to conduct a Daily Session consisting of voice recording and answers on questions about patient’s filling. The Mobile App is used by patients.",
          "The Mobile App notifies the patient to conduct his Daily Session, each day at a desired time set by the patient. Using the Mobile App, the system gathers information about the patient in a Daily Session and creates Session Data (as well as Additional Session Data in case it was required). Session Data is then sent to the Backend server where the data is stored, analyzed and available for review by clinician.",
          "The Dashboard displays Patient Condition of each patient as determined by the system. Multiple views are available:"
        ]
      },
      {
        "subTitle": "",
        "type": "ol",
        "content": [
          "<b>General overview:</b> Displays a general overview of all patients in rows, each row contains an overview of one patient.",
          "<b>Detailed view:</b> Displays a detailed view of a patient, where all data about the patient is available in addition to audio files of patient’s recording for deeper inspection by the medical team."

        ]
      },
      {
        "subTitle": "",
        "type": "p",
        "content": [
          "The Mobile App is used by patients, and the Dashboard is used by clinicians. Through the Dashboard, clinicians can add Patient User and Medical User entities to the system, edit and deactivate them.",
          "<b>If the patient indicates that he or she does not feel well, do not rely on the results reported by the Device and respond to the patient.</b>",
          "<b>The questionnaire completed by the patient is merely a diary. We do not evaluate whether the patient's responses are accurate.</b>",
          "<b>Patients must be compliant and use the Application daily and must follow the instructions in the Application and the User Manual when making recordings. They must count from 50 to 70 as quickly as possible, pronouncing each number clearly and counting in the same language each time.</b>"
        ]
      },
      {
        "subTitle": "Device Performance and Accuracy of Results",
        "type": "p",
        "content": [
          "The VocalisTrack device performance was evaluated in a retrospective, single-arm, stand-alone, comparative analytical performance study.",
          "The VocalisTrack device demonstrated high accuracy of device outcomes with an overall agreement rate between the VocalisTrack device and ground truth of 93.51% (95% CI: [85.49%, 97.86%]). The sensitivity and specificity of the VocalisTrack device were 95.45 % (95% CI: [77.16%, 99.88%]) and 92.73 % (95% CI: [82.41%, 97.98%]), respectively."

        ]
      }
    ]
  },
  "section3": {
    "title": "Safety Guidance",
    "paragraph": [
      {
        "subTitle": "",
        "type": "p",
        "content": [
          "To ensure the safety of the patients, it is vital that you strictly follow all directions in this section and all <b> WARNINGS </b> and CAUTIONS given throughout this Operating Manual. "
        ]
      },
      {
        "subTitle": "",
        "type": "warning",
        "content": [
          "VocalisTrack device shall be used only by diagnosed COPD patients aged 40 to 80 years.",
          "VocalisTrack device does not discern or grades dyspnea severity.",
          "The patient must record only his or her own voice. The voice recording must be repeated if someone else’s voice was heard in the background while recording.",
          "VocalisTrack device is not intended to inform clinician(s) about degradation of patient’s breathing condition. The patient needs to contact his/her clinician(s) directly if he/she doesn't feel well.",
          "The app is only supported on the following operation systems: iOS version 11.0.1 and up to 12.1 and Android version 8.0 up to 9.0. Don’t use it on smartphones with other operating systems or with unsupported OS version.",
          "If the patient indicates that he or she does not feel well, do not rely on the results reported by the Device and respond to the patient.",
          "The questionnaire completed by the patient is merely a diary. We do not evaluate whether the patient's responses are accurate. ",
          "Patients must be compliant and use the Application daily and must follow the instructions in the Application and the User Manual when making recordings. They must count from 50 to 70 as quickly as possible, pronouncing each number clearly and counting in the same language each time. "
        ]
      },
      {
        "subTitle": "",
        "type": "p",
        "content": [
          "The Operating Manual includes all the user-related <b>WARNINGS</b> that appear within the relevant sections that describes the device operations."
        ]
      }
    ]
  },
  "section4": {
    "title": "Protocol for Use",
    "paragraph": [
      {
        "subTitle": "",
        "type": "p",
        "content": [
          "The Mobile App of the VocalisTrack device is used according to the following steps:"
        ]
      },
      {
        "subTitle": "",
        "type": "ol",
        "content": [
          "A clinician prescribes the use of the device by a patient.",
          "The clinician instructs the patient on safe and effective use of the device.",
          "The patient freely gives a consent for data collection, store and processing by the device.",
          "The clinician registers the patient user in the device database.",
          "The patient receives the details for installation of Mobile App component of the device on patient’s smartphone and credentials of his/her account (username and password).",
          "The patient installs the Mobile App, defines reminder time and performs the daily sessions.",
          "The clinician reviews the collected data and VocalisTrack analysis results (usually on following patient’s visits)."
        ]
      }
    ]
  },



  "section5": {
    "title": "Operating Instructions",
    "paragraph": [
      {
        "subTitle": "",
        "type": "ol",
        "styleProp": {
          "fontFamily": "Roboto-Light",
          "fontSize": "24px",
          "fontWeight": "bold",
          "color": "#38ABB6"
        },
        "content": [
          {
            "collection": [
              {
                "subTitle": "Start and login",
                "type": "p",
                "content": []
              },
              {
                "subTitle": "",
                "type": "p",
                "content": [
                  "To log in to the Dashboard, please go to the product website and enter your credentials. "
                ]
              },
              {
                "subTitle": "",
                "type": "img",
                "content": [
                   "loginView"
                ]
              },
              {
                "subTitle": "",
                "type": "p",
                "content": [
                  "Once you log in - you will be redirected to the Patient Overview Page."
                ]
              }
            ]
          },
          {
            "collection": [
              {
                "subTitle": "Patient overview page",
                "type": "p",
                "content": []
              },
              {
                "subTitle": "",
                "type": "p",
                "content": [
                "The patient overview page allows you to see and filter a list of all patients in the system and their shortness of breath level (SBL). This view displays a list of all patients in the system, ordered by their patient ID, which can be divided into 3 parts:"
                ]
              },
              {
                "subTitle": "",
                "type": "ol",
                "styleProp": {
                  "listStyleType": "upper-alpha",
                  "fontFamily": "Roboto-Light",
                  "fontSize": "18px",
                  "fontWeight": "normal",
                  "color": "#353535"
                },
                "content": [
                  "Search filter",
                  "Navigation settings",
                  "Patient information"
                ]
              },
              {
                "subTitle": "",
                "type": "img",
                "content": [
                  "dashboardView"
                ]
              },
              {
                "subTitle": "",
                "type": "nestedOl",
                "styleProp": {
                  "listStyleType": "upper-alpha",
                  "fontFamily": "Roboto-Light",
                  "fontSize": "24px",
                  "fontWeight": "bold",
                  "color": "#38ABB6"
                },
                "content": [
                  {
                    "collection": [
                      {
                        "subTitle":"Search filter",
                        "type" : "p",
                        "content": []
                      },
                      {
                        "subTitle":"",
                        "type" : "p",
                        "content": [
                          "You can filter the current view of patients by name, patient ID or Reviewed patients, by entering the desired filter parameters and clicking <b>Search</b>."
                        ]
                      },
                      {
                        "subTitle":"",
                        "type" : "img",
                        "content": [
                          "dasboardFilterView"
                        ]
                      }
                    ]
                  },
                  {
                    "collection": [
                      {
                        "subTitle":"Navigating through patient pages",
                        "type" : "p",
                        "content": []
                      },
                      {
                        "subTitle":"",
                        "type" : "p",
                        "content": [
                          "You can navigate through patient pages to see different patients by clicking <b>Next</b>or <b>Prev</b> from the top right corner of the list:"
                        ]
                      },
                      {
                        "subTitle":"",
                        "type" : "img",
                        "content": [
                          "dashboardPaginationView"
                        ]
                      }
                    ]
                  },
                  {
                    "collection": [
                      {
                        "subTitle": "Patient information",
                        "type": "p",
                        "content": []
                      },
                      {
                        "subTitle": "",
                        "type": "p",
                        "content": [
                          "Shows a simplified view of patient information. To get the detailed view, please double click on the desired patient."                ]
                      },
                      {
                        "subTitle": "",
                        "type": "img",
                        "content": [
                          "dashboardMonitorTableView"
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "collection": [
              {
                "subTitle": "Detailed patient view",
                "type": "p",
                "content": []
              },
              {
                "subTitle": "",
                "type": "nestedOl",
                "styleProp": {
                  "listStyleType": "lower-latin",
                  "fontFamily": "Roboto-Light",
                  "fontSize": "24px",
                  "fontWeight": "bold",
                  "color": "#38ABB6"
                },
                "content": [
                  {
                    "collection": [
                      {
                        "subTitle":"Entering detailed patient view",
                        "type" : "p",
                        "content": []
                      },
                      {
                        "subTitle":"",
                        "type" : "p",
                        "content": [
                          "To view detailed patient analysis and details, simply double click on the patient ID number on the overview screen.",
                          "The detailed patient view will be opened as appears below. Click the <b> Back</b> button to return to the overview screen"
                        ]
                      },
                      {
                        "subTitle":"",
                        "type" : "img",
                        "content": [
                          "pateintDeatailsView"
                        ]
                      },
                      {
                        "subTitle":"",
                        "type" : "p",
                        "content": [
                          "The detailed patient view includes the following windows:"
                        ]
                      },
                      {
                        "subTitle": "",
                        "type": "nestedOl",
                        "styleProp": {
                          "fontFamily": "Roboto-Light",
                          "fontSize": "18px",
                          "fontWeight": "normal",
                          "color": "#353535"
                        },
                        "content": [
                          {
                            "collection": [
                              {
                                "subTitle":"",
                                "type" : "p",
                                "content": [
                                  "Table that displays the shortness of breath comparison in two ways together with the correlated recordings: "
                                ]
                              },
                              {
                                "subTitle":"",
                                "type" : "nestedOl",
                                "styleProp": {
                                  "fontFamily": "Roboto-Light",
                                  "fontSize": "18px",
                                  "fontWeight": "normal",
                                  "color": "#353535",
                                  "listStyleType": "lower-latin"
                                },
                                "content": [
                                  {
                                    "collection": [
                                      {
                                        "subTitle":"",
                                        "type" : "p",
                                        "content": [
                                          "Day to day comparison: SBL of each day is compared to the SBL of the previous day, and the results of the comparisons are displayed graphically using the color scheme described below."
                                        ]
                                      },
                                      {
                                        "subTitle":"",
                                        "type" : "img",
                                        "content": [
                                          "dayToDayComparisonView"
                                        ]
                                      }
                                    ]
                                  },{
                                    "collection": [
                                      {
                                        "subTitle":"",
                                        "type" : "p",
                                        "content": [
                                          "Retrospective comparison: SBL of each prior day is compared to the SBL of today. The results of the comparisons are displayed graphically as described below."
                                        ]
                                      },
                                      {
                                        "subTitle":"",
                                        "type" : "img",
                                        "content": [
                                          "shortnessOfBreathLevelView"
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              }
                            ]
                          },
                          {
                            "collection": [
                              {
                                "subTitle":"",
                                "type" : "p",
                                "content": [
                                  "Patient info: Displays patient’s personal details and contact information."
                                ]
                              }
                            ]
                          },
                          {
                            "collection": [
                              {
                                "subTitle":"",
                                "type" : "p",
                                "content": [
                                  "Daily history diary: Displays the relevant daily COPD dairy."
                                ]
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  },





                  {
                    "collection": [
                      {
                        "subTitle":"Scrolling analysis history and selecting date",
                        "type" : "p",
                        "content": []
                      },
                      {
                        "subTitle":"",
                        "type" : "p",
                        "content": [
                          "You can move the displayed window of analysis forward or backward by clicking the horizontal scroll. To go back to today, simply scroll all the way to right.",
                          "You can use the Checkbox to mark if a patient was reviewed today <b>(Rectangle A)</b."
                        ]
                      },
                      {
                        "subTitle":"",
                        "type" : "img",
                        "content": [
                          "scrollingAnalysisView"
                        ]
                      }
                    ]
                  },
                  {
                    "collection": [
                      {
                        "subTitle": "Daily info tooltip:",
                        "type": "p",
                        "content": []
                      },
                      {
                        "subTitle": "",
                        "type": "p",
                        "content": [
                          "A tooltip can be displayed by hovering over the shortness of breath color indicator. The tooltip contains details about the recordings used to compare the shortness of breath level."                ]
                      }
                    ]
                  },
                  {
                    "collection": [
                      {
                        "subTitle": "Listen to patient recordings and review daily COPD dairy",
                        "type": "p",
                        "content": []
                      },
                      {
                        "subTitle": "",
                        "type": "p",
                        "content": [
                          "You can listen to a desired recording, by clicking on the play button below the retrospective comparison bars. Also, you can review the daily COPD diary, click the desired date on the upper table and the associated dairy will appear on the bottom left part of the screen. "                ]
                      },
                      {
                        "subTitle": "",
                        "type": "img",
                        "content": [
                           "ptientRecordingView"               ]
                      },{
                        "subTitle": "",
                        "type": "img",
                        "content": [
                          "dailyCOPD"
                          ]
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "collection": [
              {
                "subTitle": "Patient management",
                "type": "p",
                "content": []
              },
              {
                "subTitle": "",
                "type": "nestedOl",
                "styleProp": {
                  "listStyleType": "upper-alpha",
                  "fontFamily": "Roboto-Regular",
                  "fontSize": "24px",
                  "fontWeight": "bold",
                  "color": "#38ABB6"
                },
                "content": [
                  {
                    "collection": [
                      {
                        "subTitle":"Accessing patient management view",
                        "type" : "p",
                        "content": []
                      },
                      {
                        "subTitle":"",
                        "type" : "p",
                        "content": [
                         "<b> Patient  management </b> can be accessed from the menu icon in the upper right corner of the screen:"
                        ]
                      },
                      {
                        "subTitle":"",
                        "type" : "img",
                        "content": [
                          "menuView"
                        ]
                      }
                    ]
                  },
                  {
                    "collection": [
                      {
                        "subTitle": "Patient management view",
                        "type": "p",
                        "content": []
                      },
                      {
                        "subTitle": "",
                        "type": "p",
                        "content": [
                          "Patient management view displays a list of all the patients in the system:"                ]
                      },
                      {
                        "subTitle": "",
                        "type": "img",
                        "content": [
                          "patientManagementView"                ]
                      }
                    ]
                  },
                  {
                    "collection": [
                      {
                        "subTitle": "Adding patient user",
                        "type": "p",
                        "content": []
                      },
                      {
                        "subTitle": "",
                        "type": "p",
                        "content": [
                          "You can add a patient user by clicking the <b> Add patient </b> button on the Patient Management screen (A in previous image):"                ]
                      },
                      {
                        "subTitle": "",
                        "type": "img",
                        "content": [
                          "patientFormView"               ]
                      },
                      {
                        "subTitle": "",
                        "type": "p",
                        "content": [
                          "The create patient screen will open. Once you have completed the requested information, click <b> Create </b>."               ]
                      }
                    ]
                  },
                  {
                    "collection": [
                      {
                        "subTitle": "Updating patient user",
                        "type": "p",
                        "content": []
                      },
                      {
                        "subTitle": "",
                        "type": "p",
                        "content": [
                          "You can update a patient user by clicking the <b> edit button </b> appears when you hover the row of the patient in the table (C in patient management view image)."                ]
                      },
                      {
                        "subTitle": "",
                        "type": "img",
                        "content": [
                          "editPatientFormView"               ]
                      },
                      {
                        "subTitle": "",
                        "type": "p",
                        "content": [
                          "Here you can change and update the relevant patient information. Once done, please click on the Update button. Clicking on the Cancel button will cancel all the changes that have been made."               ]
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "collection": [
              {
                "subTitle": "Medical Staff management",
                "type": "p",
                "content": []
              },
              {
                "subTitle": "",
                "type": "nestedOl",
                "styleProp": {
                  "listStyleType": "upper-alpha",
                  "fontFamily": "Roboto-Regular",
                  "fontSize": "24px",
                  "fontWeight": "bold",
                  "color": "#38ABB6"
                },
                "content": [
                  {
                    "collection": [
                      {
                        "subTitle":"Accessing Medical staff management",
                        "type" : "p",
                        "content": []
                      },
                      {
                        "subTitle":"",
                        "type" : "p",
                        "content": [
                          "<b>Medical Staff management </b> can be accessed from the menu icon in the upper right corner of the screen:"
                        ]
                      },
                      {
                        "subTitle":"",
                        "type" : "img",
                        "content": [
                          "medicalStaffMenuView"
                        ]
                      }
                    ]
                  },
                  {
                    "collection": [
                      {
                        "subTitle": "Medical Staff management view",
                        "type": "p",
                        "content": []
                      },
                      {
                        "subTitle": "",
                        "type": "p",
                        "content": [
                          "Medical Staff management view displays a list of all the clinical users in the system:"               ]
                      },
                      {
                        "subTitle": "",
                        "type": "img",
                        "content": [
                          "medicalStaffManagementView"                ]
                      }
                    ]
                  },
                  {
                    "collection": [
                      {
                        "subTitle": "Adding a Medical Staff user",
                        "type": "p",
                        "content": []
                      },
                      {
                        "subTitle": "",
                        "type": "p",
                        "content": [
                          "You can add a user by clicking the <b> Add User </b> button on the User Management screen (A in previous image). Then create user screen will open. Once you have completed the requested information, click <b>Create </b>."                ]
                      },
                      {
                        "subTitle": "",
                        "type": "img",
                        "content": [
                          "medicalStaffFormView"               ]
                      }
                    ]
                  },
                  {
                    "collection": [
                      {
                        "subTitle": "Updating user",
                        "type": "p",
                        "content": []
                      },
                      {
                        "subTitle": "",
                        "type": "p",
                        "content": [
                          "You can update a patient user by clicking the <b> edit button </b> appears when you hover the row of the medical staff user in the table (C in user management view). This will open the edit user screen:"                ]
                      },
                      {
                        "subTitle": "",
                        "type": "img",
                        "content": [
                          "editMedicalStaffFormView"               ]
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "collection": [
              {
                "subTitle": "Logout",
                "type": "p",
                "content": []
              },
              {
                "subTitle": "",
                "type": "p",
                "content": [
                  "<b>Logout</b> can be accessed from the menu icon in the upper right corner of the screen:"                ]
              },
              {
                "subTitle": "",
                "type": "img",
                "content": [
                  "logoutView"
                ]
              }
            ]
          }
        ]
      }
    ]
  },



  "section6": {
    "title": "Configuration",
    "paragraph": [
      {
        "subTitle": "",
        "type": "p",
        "content": [
          "The dashboard of the VocalisTrack device does not require configuration. "
        ]
      }
    ]
  },
  "section7": {
    "title": "Troubleshooting",
    "paragraph": [
      {
        "subTitle": "",
        "type": "p",
        "content": [
          "In case of any issues with operation of this device, contact professional service for VocalisTrack device: <a href='mailto:support@vocalishealth.com' target='_blank'> support@vocalishealth.com <a>"
        ]
      }
    ]
  }
}
