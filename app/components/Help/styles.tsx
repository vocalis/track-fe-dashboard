import styled from 'styled-components';
import React from 'react';
import {ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary, withStyles} from '@material-ui/core';
import styles from 'styles/values';

const Img = styled.img <{opacity?: string}>`
    opacity: ${props => props.opacity ? props.opacity : 1};
`;
const ViewImg = styled.img <{opacity?: string}>`
        max-width: 446px;
    margin-bottom: 40px;
`;
const ImgWrapper = styled.div<{marginRight: string}>`
margin-right: ${props => props.marginRight};
align-self: center;
`;
const Span = styled.div<{color?: string, letterSpacing: string }>`
  color: ${props => props.color};
  letter-spacing: ${props => props.letterSpacing};
  font-weight: normal;
  font-size: 18px;
  display: flex;
     align-items: center;
`;
const Title = styled.div`
  color:#707070;
  font-weight: bold;
  font-family: Roboto-Bold;
  font-size:42px;
`;

const IconWithTextWrapper = styled.a<{marginRight?: string}>`
    text-decoration: none;
    height: 100%;
    display: flex;
    flex-direction: row;
    align-items: center;
    padding-top: 31px;
    margin-right: ${props => props.marginRight ? props.marginRight : '0px'};
    cursor: pointer;
`;
const Link = styled.div`
  color: #38ABB6;
  font-family: Roboto-Regular;
  font-weight: normal;
  font-size: 20px;
  margin-left:10px ;
  cursor: pointer;
`;
const Content = styled.div`
  @media ${styles.sizes.MEDIUM_SCREEN}{
      width: calc( 100% - 398px);
            padding-left: 4.45vw;
             padding-right: 1.45vw;
;
    }
   @media ${styles.sizes.SMALL_SCREEN}{
       width: calc( 100% - 327px);
          padding-left: 5.782vw;
          padding-right: 1.45vw;
  }
     @media ${styles.sizes.LARGE_SCREEN}{
      width: calc( 100% - 398px);
          padding-left: 5.755vw;
           padding-right: 1.45vw;
  }
    position: relative;
        overflow-y: auto;
         overflow-x: hidden;
         height:64vh;
`;

const PanelWrapper = styled.div`
  @media ${styles.sizes.MEDIUM_SCREEN}{
   width: 398px;
  }
   @media ${styles.sizes.SMALL_SCREEN}{
      width: 327px;
      }
     @media ${styles.sizes.LARGE_SCREEN}{
       width: 398px;
  }
         overflow-y: auto;
         overflow-x: hidden;
            height:64vh;
`;

const ContentTitle = styled.div`
color: #38ABB6;
font-family: Roboto-Regular;
font-size: 42px;
padding-bottom: 39px;
`;
const Text = styled.div`
font-family: Roboto-Light;
font-size: 18px;
font-weight: normal;
padding-bottom: 39px;
text-align: left;
color: #353535;
opacity: 1;
`;
const SubTitle = styled.div`
color: #38ABB6;
font-family: Roboto-Regular;
font-size: 24px;
font-weight:bold ;
    height: fit-content;
    padding-bottom: 14px;
width: 100%;
`;
const Stam = styled.div`
color: #38ABB6;
font-family: Roboto-Regular;
font-size: 18px;
font-weight:bold ;
height: 56px;
`;

const LinkText = styled.span`
text-decoration: none;
  &:visited {
   color: #1C1C1C;
  }

`;
const Div = styled.div`
width: calc(100% - 192.17px);
display: flex;
flex-direction: row;
padding-left: 18.490vw;;
justify-content: space-between;
`;
const StyledExpansionPanel = withStyles(() => ({
    root: {
        'boxShadow': 'none',
        '&:before': {
            backgroundColor: 'transparent',
},
    },
}))(ExpansionPanel);


const StyledExpansionPanelSummary = withStyles(() => ({
    root: {
        'fontSize': '18px',
        'fontFamily': 'Roboto-Bold',
        'fontWeight': 'bold',
        'color': '#1C1C1C',
        'textTransform': 'uppercase',
        'minHeight': '20px',
        'padding': '0px',
        '&:hover': {
            backgroundColor: 'rgba(255, 195, 0, 0.1) !important',
        },
},
    expanded: {
        height: '42px',
    },
}))(ExpansionPanelSummary);

const StyledExpansionPanelDetails = withStyles(() => ({
    root: {
        padding: '0px',
    },
}))(ExpansionPanelDetails);
const Ul = styled.ul < {paddingInlineStart: string}>`
    margin: 0px;
    width: 100%;
    padding-inline-start: ${props => props.paddingInlineStart};
`;
const Ol = styled.ol < {paddingInlineStart: string}>`
    margin: 0px;
    width: 100%;
    padding-inline-start: ${props => props.paddingInlineStart};
`;

const ContentLi = styled.li`
`;
const ContentLo = styled.ol`


`;

const Li = styled.li <{ListStyleType: string}>`
list-style-type:${props => props.ListStyleType};
    padding-left: 10px;
    padding-top: 10px;
    color:#1C1C1C ;
    font-size: 18px;
    font-family: Roboto-Regular;
    cursor: pointer;
    &:hover {
    background-color:rgba(255, 195, 0, 0.1);
  }

`;

const TextFieldWrapper = styled.div`
display: flex;
position: relative;
width: 100%;
`;

const  IconWrapper = styled.div`
position: absolute;
z-index: 4;
    top: 8px;
    left: 8px;
`;
const  Contanier = styled.div`
width:100%;
           @media ${styles.sizes.MEDIUM_SCREEN}{
        padding-left: ${styles.sizes.HELP_SIDE_PADDINGE_LEFT};
        padding-right: ${styles.sizes.HELP_SIDE_PADDINGE_ROGHT};
         }
   @media ${styles.sizes.SMALL_SCREEN}{
   padding-right: ${styles.sizes.HELP_SIDE_PADDINGE_ROGHT_SMALL_SCREEN};
  padding-left: ${styles.sizes.HELP_SIDE_PADDINGE_LEFT_SMALL_SCREEN};
  }
     @media ${styles.sizes.LARGE_SCREEN}{
   padding-right: ${styles.sizes.APP_SIDE_PADDING_RIGHT_LARGE_SCREEN};
  padding-left: ${styles.sizes.APP_SIDE_PADDING_LEFT_LARGE_SCREEN};
  }

background-color: #FFFFFF;
display: flex;
flex-direction: row;
padding-top: 73px;
`;

const TitleWrapper = styled.div`
font-size: 35px;
  font-family: Roboto-Regular;
  font-weight: normal;
  align-self: center;
`;

const   HeaderWrapper = styled.div`

           @media ${styles.sizes.MEDIUM_SCREEN}{
        padding-left: ${styles.sizes.HELP_SIDE_PADDINGE_LEFT};
        padding-right: ${styles.sizes.HELP_SIDE_PADDINGE_ROGHT};
         }
   @media ${styles.sizes.SMALL_SCREEN}{
   padding-right: ${styles.sizes.HELP_SIDE_PADDINGE_ROGHT_SMALL_SCREEN};
  padding-left: ${styles.sizes.HELP_SIDE_PADDINGE_LEFT_SMALL_SCREEN};
  }
     @media ${styles.sizes.LARGE_SCREEN}{
   padding-right: ${styles.sizes.APP_SIDE_PADDING_RIGHT_LARGE_SCREEN};
  padding-left: ${styles.sizes.APP_SIDE_PADDING_LEFT_LARGE_SCREEN};
  }

  width: 100%;
  height: 120px;
display: flex;
flex-direction: row;
    align-items: center;
 background: #FDFDFD;
 border-radius: 2px;
  opacity: 1;
    border-bottom: 2px solid #00000007;
`;

export {
    Img,
    TitleWrapper,
    ImgWrapper,
    Span,
    Title,
    IconWithTextWrapper,
    Link,
    Content,
    PanelWrapper,
    SubTitle,
    Text,
    ContentTitle,
    StyledExpansionPanel,
    StyledExpansionPanelSummary,
    StyledExpansionPanelDetails,
    Ul,
    Li,
    Ol,
    LinkText,
    TextFieldWrapper,
    IconWrapper,
    Div,
    Contanier,
    HeaderWrapper,
    ContentLi,
    ContentLo,
    ViewImg,
    Stam,
};
