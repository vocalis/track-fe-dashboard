/**
 *
 * PrivacyNotice
 *
 */
import React, {Fragment, memo, useEffect} from 'react';
import Layout from '../common/Layout';
const privacyNotice = require('./privacyNotice.json');
import {
     Contanier,
    TextWrapper,
    TextTitle,
    SubTitle,
    RightTextWrapper,
    LeftTextWrapper,
} from './styles';
import {useDispatch} from 'react-redux';
import {toggleSideBar} from '../../containers/SideBar/actions';
import './style.css';
interface Props {}
interface Paragraph {
    subTitle: string;
    type: string;
    content: string[];
    rightContent: string[];
    leftContent: string[];
}

interface Section {
    section: {
        title: string;
        paragraph: Paragraph[];
    };
}
function PrivacyNotice(props: Props) {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(toggleSideBar(false));
        window.scrollTo(0, 0);
    });

    const getParagraph = (content: string[]) => {
        return (
            content.map((p, index) => (
                <TextWrapper key={index} dangerouslySetInnerHTML={{__html: p}}/>))
        );
    };
    const  getParagraphs = (subTitle: string, content: string[], index: number) => {
        return(
            <Fragment key={index}>
                {subTitle && <SubTitle  id={subTitle}>
                    {subTitle}
                </SubTitle>}
                {getParagraph(content)}
            </Fragment>
        );
    };
    const getContent = (leftContent, rightContent, index) => {
        return(
            <div key={index} style={{width: '100%' , paddingTop: '10px', display: 'flex'}}>
            <div style={{width: '35%'}}>
            {leftContent.map((p, index) => (
            <LeftTextWrapper key={index} dangerouslySetInnerHTML={{__html: p}}/>))}
            </div>
            <div style={{width: '65%'}}>
            {rightContent.map((p, index) => (
            <RightTextWrapper key={index} dangerouslySetInnerHTML={{__html: p}}/>))
            }
            </div>
        </div>
        );
    };
    return (
      <Layout title="PrivacyNotice">
         <Contanier>
             {privacyNotice.map((section: Section, index: number) => {
                 return (
                     <Fragment key={index}>
                         <TextTitle>
                             {section.section.title}
                         </TextTitle>
                             <TextWrapper>
                             {section.section.paragraph.map((paragraph: Paragraph, index: number) => {
                                 switch (paragraph.type) {
                                     case 'p':
                                         return getParagraphs(paragraph.subTitle, paragraph.content, index);
                                     case  'rightLeftContent':
                                         return getContent(paragraph.leftContent, paragraph.rightContent, index);
                                     default :
                                         return  <Fragment key={index}/>;
                                 }
                             })}
                         </TextWrapper>
                     </Fragment>
                 );
             })}

          </Contanier>
      </Layout>
      );
}

export default memo(PrivacyNotice);
