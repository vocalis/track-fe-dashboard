import styled from 'styled-components';
import React from 'react';
import styles from '../../styles/values';

const Contanier = styled.div`
width:100%;
           @media ${styles.sizes.MEDIUM_SCREEN}{
        padding-left: ${styles.sizes.HELP_SIDE_PADDINGE_LEFT};
        padding-right: ${styles.sizes.HELP_SIDE_PADDINGE_ROGHT};
         }
   @media ${styles.sizes.SMALL_SCREEN}{
   padding-right: ${styles.sizes.HELP_SIDE_PADDINGE_ROGHT_SMALL_SCREEN};
  padding-left: ${styles.sizes.HELP_SIDE_PADDINGE_LEFT_SMALL_SCREEN};
  }
     @media ${styles.sizes.LARGE_SCREEN}{
   padding-right: ${styles.sizes.APP_SIDE_PADDING_RIGHT_LARGE_SCREEN};
  padding-left: ${styles.sizes.APP_SIDE_PADDING_LEFT_LARGE_SCREEN};
  }
background-color: #FFFFFF;
display: flex;
flex-direction: column;
padding-top: 73px;
`;
const TextWrapper = styled.div`
  font-family: Roboto-Light;
  width: 100%;
  line-height:normal;
 padding-top: 10px;
 padding-bottom: 10px;
 font-size: 16px;
`;
const RightTextWrapper = styled.div`
  font-family: Roboto-Light;
  line-height:normal;
   padding-left: 20px;
  padding-bottom: 10px;
   font-size: 16px;
`;
const LeftTextWrapper = styled.div`
  font-family: Roboto-Regular;
  line-height:normal;
  padding-bottom: 10px;
  font-size: 16px;
`;
const TextTitle = styled.div`
text-align: center;
  color: #38ABB6;
    font-family: Roboto-Regular;
        font-size: 32px;
`;
const SubTitle = styled.div`
color: #38ABB6;
font-family: Roboto-Bold;
    height: fit-content;
width: 100%;
text-align: center;
padding-top: 10px;
padding-bottom: 10px;
font-size: 18px;
`;
export {
    SubTitle,
    Contanier,
    TextWrapper,
    TextTitle,
    LeftTextWrapper,
    RightTextWrapper,
};
