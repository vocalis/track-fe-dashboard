import React from 'react';
import {FormattedMessage} from 'react-intl';
import {HomeCardWrapper, HomeCardIcon, HomeCardTitle, HomeCardSubtitle} from './styles';

interface HomeCardProps {
    icon: any;
    title: string;
    subtitle: string;
}

const HomeCard: React.FC<HomeCardProps> = (props: HomeCardProps) => {
    const {icon, title, subtitle} = props;

    return (
        <HomeCardWrapper>
            <HomeCardIcon src={icon} />
            <HomeCardTitle><FormattedMessage id={title}/></HomeCardTitle>
            <HomeCardSubtitle><FormattedMessage id={subtitle}/></HomeCardSubtitle>
        </HomeCardWrapper>
    );
};

export default HomeCard;
