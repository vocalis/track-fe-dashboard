import styled from 'styled-components';
import styles from 'styles/values';

const HomeCardWrapper = styled.div`
  width: 250px;
  height: 300px;
  padding: 82.5px 1.667vw 70.5px 1.667vw;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: ${styles.colors.SHADE.WHITE.WHITE};
  border: ${styles.colors.SHADE.GRAY.BORDER};
  border-radius: 6px;
  box-shadow: 0 9px 41px ${styles.colors.SHADE.BLACK.BLACK_16};
  margin-left: 1.771vw;
  margin-right: 1.771vw;
  &:hover {
    background-color: ${styles.colors.SHADE.WHITE.WHITE};
  }
  @media (min-width: 1200px) {
    width: 320px;
  }
  @media (min-height: 800px) {
    height: 390px;
  }
`;

const HomeCardIcon = styled.img`
  height: 40px;
`;

const HomeCardTitle = styled.div`
  color: ${styles.colors.BRAND.PRIMARY};
  font-size: 27px;
  font-weight: bold;
  margin-top: 20%;
  margin-bottom: 6.5%;
  text-transform: uppercase;
`;

const HomeCardSubtitle = styled.div`
  color: ${styles.colors.BRAND.PRIMARY};
  font-size: 15px;
  text-align: center;
`;

export {HomeCardWrapper, HomeCardIcon, HomeCardTitle, HomeCardSubtitle};
