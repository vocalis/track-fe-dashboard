/**
 *
 * MaterialTable
 *
 */
import React, { memo } from 'react';
import {Table, TableContainer} from '@material-ui/core';
import TabelHeader from './TableHeader';
import TableRows from './TableRows';
import {useStyles} from './styles';
import {HeadCell} from '../../utils/constants';
import {Contanier} from '../../containers/UserManagement/styles';
interface Data {

}
interface Props {
    data: Data[];
    headCells: HeadCell[];
    onRowDoubleClick: (row: object) => void;
    isLoading: boolean;
    searchText: string;
    orderBy: string;
    order: string;
    createSortHandler: (property: any) => (event: React.MouseEvent<unknown>) => void;
}
function MaterialTable(props: Props) {
    const {data , headCells, onRowDoubleClick, isLoading, searchText, order, orderBy, createSortHandler} = props;
    const classes = useStyles();
    return (
        <TableContainer  className={classes.container}>
         <Table className={classes.table} stickyHeader aria-label="sticky table">
        <TabelHeader headCells={headCells} orderBy={orderBy} order={order} createSortHandler={createSortHandler}/>
        <TableRows searchText={searchText} isLoading={isLoading} onRowDoubleClick={onRowDoubleClick} rows={data}/>
        </Table>
        </TableContainer>
  );
}

export default memo(MaterialTable);
