/**
 *
 * MaterialTable
 *
 */
import React, { memo } from 'react';
import {TableHead, TableRow} from '@material-ui/core';
import {HeadCell} from './constants';

import {StyledTableCell, StyledTableSortLabel, Img, Div} from './styles';
export const arrow = require('images/sort_arrow.svg');
interface Props {
    headCells: HeadCell[];
    orderBy: string;
    order: string;
    createSortHandler: (property: any) => (event: React.MouseEvent<unknown>) => void;
}
const Icon = (props) => {
    const {headCellId, orderBy, order} = props;
    return(
        <Div>
            <Img
            width="8.16px"
            height="4.08px"
            src={arrow} alt="arrow"
            transform={'rotateX(180deg)'}
            opacity={headCellId === orderBy && order === 'asc' ? '1' : '0.5'}

        />
        <Img
            width="8.16px"
            height="4.08px"
            src={arrow} alt="arrow"
            transform={'rotateX(0deg)'}
            opacity={headCellId === orderBy &&  order !== 'asc' ? '1' : '0.5'}
        />
    </Div>
    );
};


function TabelHeader(props: Props) {
    const {headCells, order, orderBy, createSortHandler} = props;
    // @ts-ignore
    // @ts-ignore
    // @ts-ignore
    const temporeder = order === 'asc' ? 'asc' : 'desc' ;
    return(
        <TableHead>
            <TableRow>
                {headCells.map(headCell => (
                    <StyledTableCell
                        key={headCell.id}
                        align="left"
                        padding="none"
                        sortDirection={orderBy === headCell.id ? temporeder  : false}
                    >
                        {headCell.id !== 'editIcon' ? (
                            <StyledTableSortLabel
                                active={orderBy === headCell.id}
                                direction={orderBy === headCell.id ? temporeder : 'asc'}
                                 onClick={createSortHandler(headCell.id)}
                               IconComponent={() =>
                                   <Icon
                                   order={temporeder}
                                   orderBy={orderBy}
                                   headCellId={headCell.id}
                                   />}
                        >
                            {headCell.label}

                        </StyledTableSortLabel>) :
                            <StyledTableSortLabel
                            >
                           {headCell.label}
                            </StyledTableSortLabel>
                                }
                    </StyledTableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}

export default memo(TabelHeader);
