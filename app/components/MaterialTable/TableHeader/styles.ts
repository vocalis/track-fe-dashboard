import {TableCell, TableSortLabel, withStyles} from '@material-ui/core';
import styled from 'styled-components';

const StyledTableCell = withStyles(() => ({
    root: {
        backgroundColor: '#EEEEEE',
        paddingLeft: '19px',
        paddingTop: '8px',
        paddingBottom: '8px',
        fontSize: '18px',
        color: '#353535',
        fontFamily: 'Roboto-Light',
        minWidth: '20px',
    },
}))(TableCell);
const StyledTableSortLabel = withStyles(() => ({
    root: {

    },
}))(TableSortLabel);

const Img = styled.img<{
    opacity?: string;
    transform?: string;
    width: string;
    height: string;
}>`
  opacity: ${(props) => (props.opacity ? (props) => props.opacity : 1)};
  cursor: pointer;
  transform: ${(props) =>
    props.transform ? props.transform : 'rotateX(0deg)'};
`;
const Div = styled.div`
    height: 12.24px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    margin-left: 28px;
`;
export {
    StyledTableCell,
    StyledTableSortLabel,
    Img,
    Div,
};
