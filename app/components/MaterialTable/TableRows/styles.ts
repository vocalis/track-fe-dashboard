import {CircularProgress, createStyles, Divider, TableCell, TableRow, Theme, withStyles} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import styled from 'styled-components';
const StyledTableCell = withStyles(() => ({
    root: {
        paddingLeft: '19px',
        paddingTop: '12px',
        paddingBottom: '12px',
        cursor: 'pointer',
        fontSize: '18px',
        color: '#353535',
        fontFamily: 'Roboto-Light',
        minWidth: '30px',
        textTransform: 'capitalize',
    },
}))(TableCell);

const StyledTableRow = withStyles(() => ({
    root: {
        '&:hover .hide': {
            display: 'block',
        },
        '&:hover ': {
            backgroundColor: 'rgba(255, 195, 0, 0.1) !important',
        },
    },
    selected: {
        backgroundColor: ' rgba(255, 195, 0, 0.1) !important',
    },

}))(TableRow);
const StyledCircularProgress = withStyles(() => ({
    root: {
        marginLeft: '10px',
        width: '17px !important',
        height: '17px !important',
    },
    colorSecondary: {
        color: '#000000',
    },
}))(CircularProgress);


const Span = withStyles(() => ({
    root: {
        paddingLeft: '19px',
        paddingTop: '47px',
        paddingBottom: '12px',
        cursor: 'pointer',
        fontSize: '18px',
        color: '#353535',
        fontFamily: 'Roboto-Light',
        minWidth: '30px',
        borderBottom: 'none',
        height: '20px',
    },
}))(TableCell);
// const useStyles = makeStyles((theme: Theme) =>
//     createStyles({
//         hide: {
//             display: 'none',
//             },
//     }),
// );

const Mark = styled.mark`
background-color: #FFC300;
`;
export {
    StyledTableCell,
    StyledTableRow,
    StyledCircularProgress,
    Mark,
    Span,
};
