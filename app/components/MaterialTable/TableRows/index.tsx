/**
 *
 * MaterialTable
 *
 */
import React, {memo, useEffect, useState, Fragment} from 'react';
import {TableBody, TableRow} from '@material-ui/core';
import {
    StyledTableCell,
    StyledTableRow,
    StyledCircularProgress,
    Mark,
    Span,
} from './styles';
import {object} from 'prop-types';
import {FormattedMessage} from 'react-intl';
import './style.css';
interface Props {
    rows: object[];
    onRowDoubleClick: (row: object) => void;
    isLoading: boolean;
    searchText: string;
}

function TableRows(props: Props) {
    const {rows, searchText, onRowDoubleClick, isLoading} = props;
    const tableCells: JSX.Element[] = [];
    const [selectedRow, setSelectedRow] = useState(-1);
    const [tableRows, setTableRows] = useState<JSX.Element[]>([]);
    // const handleClick = (event, row) => {
    //     setSelectedRow(parseInt(event.currentTarget.id, 10));
    // };
    const markText = (text, searchText) => {
        const searchTextIn = `${text}`.toLowerCase();
        const textToSearch = searchText.toLowerCase();
        if (textToSearch !== '' && searchTextIn !== '') {
            const splitedText = `${searchTextIn}`.split(textToSearch);
            if (textToSearch === `${searchTextIn}`) {
                return(<Mark> {textToSearch} </Mark>);
            }
            const splitedArray = splitedText.filter(item => item === '');
            if (splitedText.length === splitedArray.length) {
                splitedText.shift();
            }
            return splitedText.map((str, index) => {
                if (str === '') {
                    return <Mark key={index}>{textToSearch}</Mark>;
                } else {
                        if (str !== '' &&
                            index !== 0 &&
                            splitedText[index - 1] !== '') {
                            return <span key={index} ><Mark>{textToSearch}</Mark>{str}</span>;

                        } else {
                            return  str;
                        }
                }
            });
        } else {
           return  searchTextIn;
        }
    };
    // need to change on selected behavior
    const getCells = (row, index) => {
        const TableCells: JSX.Element[] = [];
        for (const [key, value] of Object.entries(row)) {
                if (searchText === '') {
                    TableCells.push(
                       (
                           <StyledTableCell key={key} padding="none" align="left">
                            <span className={(typeof value !== 'object' || index === selectedRow) ? 'show' : 'hide'} >
                                {value as any}
                            </span>
                        </StyledTableCell>
                       ),
                    );
                } else {
                    TableCells.push(
                        (
                            <StyledTableCell key={key}  padding="none" align="left">
                            {(typeof value !== 'object') ?
                                markText(value, searchText) :
                                <span className={
                                    index === selectedRow ?
                                    'show' :
                                    'hide'
                                }>
                                    {value}
                                </span>
                            }
                        </StyledTableCell>
                        ),
                        );
                }
        }
        return TableCells;
    };

    const getRows = (row, index) => {
        return (
            <StyledTableRow
                id={index}
                key={index}
                hover
                selected={index === selectedRow}
                // onClick={(event) => handleClick(event, row)}
                onDoubleClick={() => onRowDoubleClick(row)}
            >

                {...getCells(row, index)}
            </StyledTableRow>
        );
    };

    useEffect(() => {
        if (!isLoading) {
        rows.map((row, index) => {
            tableCells.push(getRows(row, index));
        });
        } else {
            // @ts-ignore
            tableCells.push([<Fragment key={1}/>]);
        }
        setTableRows(tableCells);
    }, [selectedRow, rows, isLoading]);

    const getTabelBody = () => {
        if (tableRows.length === 0) {
            return (
                <TableBody>
                <TableRow>
                    <Span style={{position: 'absolute'}}>
                        {
                            searchText === '' ?
                                <FormattedMessage id="NO_DATA_TO_SHOW"/> :
                                <FormattedMessage id="NO_RESULT_TO_SHOW_CLEAN_SEARCH_BAR"/>
                        }
                    </Span>
                </TableRow>
                </TableBody>
            );
        } else {
            return (
                <TableBody>
                {tableRows.map((row) => (row))}
            </TableBody>
            );
        }
    };
    return (
        isLoading ? (
                <TableBody>
                    <TableRow>
                    <Span style={{position: 'absolute'}}>
                    <StyledCircularProgress color="secondary"/>
                    <span style={{color: '#353535', marginLeft: '14px'}}><FormattedMessage id="SEARCHING"/></span>
                    </Span>
                    </TableRow>
                </TableBody>
            )
            :
            getTabelBody()


    );
}

export default memo(TableRows);
