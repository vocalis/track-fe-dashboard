import React from 'react';
import { FormattedMessage } from 'react-intl';
import { InputWrapper } from './styles';

interface CheckBoxProps {
    type: string;
}


const VocalisInput: React.FC<CheckBoxProps> = (props: CheckBoxProps) => {
    const { type } = props;


    return <InputWrapper><input type={type} /></InputWrapper>;
};

export default VocalisInput;
