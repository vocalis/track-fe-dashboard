/**
 *
 * Pagination
 *
 */
import React from 'react';
import { FormattedMessage } from 'react-intl';
import {PaginationActionsWrapper, PaginationWrapper, RowPerPageWrapper, Text, PaginationActionIcon} from './styles';

const forwardIcon = require('../../../images/forward.svg');
const fastForwardIcon = require('../../../images/fast-forward.svg');
const backwardIcon = require('../../../images/backward.svg');
const fastBackwardIcon = require('../../../images/fastbackward.svg');


interface Props {
  rawPerPage: number;
  pageNumber: number;
  totalPages: number;
  onForwardClicked: (MouseEvent) => void;
  onFastForwardClicked: (MouseEvent) => void;
  onBackwardClicked: (MouseEvent) => void;
  onFastBackwardClicked: (MouseEvent) => void;
}

function Pagination(props: Props) {
  const {
      rawPerPage,
      pageNumber,
      totalPages,
      onBackwardClicked,
      onFastBackwardClicked,
      onFastForwardClicked,
      onForwardClicked,
  } = props;
  return (
      <PaginationWrapper>
          <PaginationActionsWrapper>
              <PaginationActionIcon src={fastBackwardIcon} onClick={onFastBackwardClicked} />
              <PaginationActionIcon src={backwardIcon} onClick={onBackwardClicked}/>
              <Text style={{marginLeft: '1.809vw'}}>
                  <FormattedMessage id={'PAGE'}/> {pageNumber} <FormattedMessage id={'OF'}/> {totalPages}
              </Text>
              <PaginationActionIcon src={forwardIcon} onClick={onForwardClicked}/>
              <PaginationActionIcon src={fastForwardIcon} onClick={onFastForwardClicked}/>
          </PaginationActionsWrapper>
      </PaginationWrapper>
  );
}

export default Pagination;
