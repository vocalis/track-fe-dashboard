import styled from 'styled-components';
import styles from 'styles/values';
import FONT from '../../../styles/values/fonts';

export const PaginationWrapper = styled.div`
  width: 100%;
  height: 37.5px;
  display: flex;
  justify-content: flex-end;
  flex-direction: row;
  align-items: center;
  padding-right: ${styles.sizes.APP_SIDE_PADDING};
  padding-left: ${styles.sizes.APP_SIDE_PADDING};
  background-color: #F8F8F8;
    @media ${styles.sizes.SMALL_SCREEN}{
   padding-right: ${styles.sizes.APP_SIDE_PADDING_RIGHT_SMALL_SCREEN};
  padding-left: ${styles.sizes.APP_SIDE_PADDING_LEFT_SMALL_SCREEN};
 }
   @media ${styles.sizes.MEDIUM_SCREEN}{
   padding-right: ${styles.sizes.APP_SIDE_PADDING_RIGHT};
  padding-left: ${styles.sizes.APP_SIDE_PADDING_LEFT};
 }
   @media ${styles.sizes.LARGE_SCREEN}{
   padding-right: ${styles.sizes.APP_SIDE_PADDING_RIGHT_LARGE_SCREEN};
  padding-left: ${styles.sizes.APP_SIDE_PADDING_LEFT_LARGE_SCREEN};
 }
`;
export const Text = styled.div`
  font-size: ${FONT.PRIMARY};
  margin-right: 0.586vw;
   cursor: pointer;
`;
export const RowPerPageWrapper = styled.div`
  height: 100%;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
`;

export const PaginationActionsWrapper = styled.div`
  height: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const PaginationActionIcon = styled.img`
  margin-left: 1.809vw;
  cursor: pointer;
`;
