


// status value
import styles from '../../../styles/values';
import _ from 'lodash';
import {QuestionnaireAnswersDataInterface, QuestionnaireInterface} from '../../../models/local';
const betterIcon = require('images/ic_arrow_up.svg');
const sameIcon = require('images/ic_arrow_normal.svg');
const worseIcon = require('images/ic_arrow_down.svg');
const greenDocIcon = require('images/ic_doc_green.svg');
const greyDocIcon = require('images/ic_doc_grey.svg');
const darkGreyDocIcon = require('images/ic_doc_dark_grey.svg');
const flatIcon = require('images/flat.svg');


export const getValue  = (status: 'better' | 'same' | 'worse' | 'deterioration' | 'improvement' | 'none' | null) => {
    switch (status) {
        case 'none':
            return 4;
        case 'better':
        case 'improvement':
            return 3;
        case 'same':
            return 2;
        case 'worse':
        case 'deterioration':
            return 1;
        default:
            return null;
    }
};
// Dot color according to status value
export const valueToColor = (value: number) => {
    switch (value) {
        case 1:
            return styles.colors.TOOLTIP.WORSE_DOT;
        case 2:
            return styles.colors.TOOLTIP.SAME_DOT;
        case 3:
            return styles.colors.TOOLTIP.BETTER_DOT;
        case 4:
            return styles.colors.TOOLTIP.NONE_DOT;
        default:
            return '';
    }
};
// get message text  according to status value
export  const getFormattedMessage = (value: number) => {
    switch (value) {
        case 1:
        case 3:
            return 'SHORTNESS_OF_BREATH_STATUS';
        case 2:
            return 'SHORTNESS_OF_BREATH_SAME';
        case 4:
            return 'SHORTNESS_OF_BREATH_NONE';
        default:
            return 'SHORTNESS_OF_BREATH_NONE';
    }
};
// get status
export const valueToString = (value: number) => {
    switch (value) {
        case 1:
            return 'STATUS_WORSE';
        case 2:
            return 'STATUS_NO_CHANGE';
        case 3:
            return 'STATUS_BETTER';
        case 4:
            return 'STATUS_NONE';
        default:
            return '';
    }
};
// tooltip background -ComparisonHeader
export const valueToBackGround = (value: number) => {
    switch (value) {
        case 1:
            return styles.colors.TOOLTIP.WORSE_BACKGROUND;
        case 2:
            return styles.colors.TOOLTIP.SAME_BACKGROUND;
        case 3:
            return styles.colors.TOOLTIP.BETTER_BACKGROUND;
        case 4:
            return styles.colors.TOOLTIP.NONE_BACKGROUND;
        default:

            return '';
    }
};

export const getDocsIconByAnswers = (answers: { daily: number[], expanded: number[] } | null | undefined) => {
    if (answers) {
        if (answers.expanded && answers.expanded.length > 0) {
            return greenDocIcon;
        } else if (answers.daily && answers.daily.length > 0) {
            return darkGreyDocIcon;
        }
    }
    return greyDocIcon;
};

export const getTrendsIconByStatus = (status:
                                          'better' |
                                          'same' |
                                          'worse' |
                                          'deterioration'
                                          | 'improvement'
                                          | 'none'
                                          | null) => {
    switch (status) {
        case 'better':
        case 'improvement':
            return betterIcon;
        case 'same':
            return sameIcon;
        case 'worse':
        case 'deterioration':
            return worseIcon;
        case 'none':
            return flatIcon;
        default:
            return null;
    }
};



export const matchAnswer = (
    questionnaireAnswers: QuestionnaireAnswersDataInterface[],
    answers: number[],
) => {
    const qAnswer: string[] = [];
    answers.forEach(answer => {
        if ((_.find(questionnaireAnswers, ['id', answer]))) {
            const ans = (_.find(questionnaireAnswers, ['id', answer]));
            qAnswer.push(ans && ans.answer || '');
        }
    });
    return qAnswer;
};
export  const getDomain = (minVal, maxVal, data, type) => {
    let min = minVal;
    let max = maxVal;
    data.map(item => {
        if (item.vital === type) {
            if (item.value < min && item.value > 0) {
                min = item.value;
            }
            if (item.value > max) {
                max = item.value;
            }
        }
    });
    return [Math.ceil(min), Math.ceil(max)];
};
export const getDocsIconGreen = () => (greenDocIcon);
export const getDocsIconDarkGrey = () => (darkGreyDocIcon);
