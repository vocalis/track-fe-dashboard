import styled from 'styled-components';
const QAWrapper = styled.div`
text-align: left;
font-size:12px;
letter-spacing: 0;
color: #353535;
opacity: 1;
`;
const Span = styled.div<{fontWeight?: string, marginBottom?: string}>`
    font-weight:${props => props.fontWeight};
    margin-bottom:${props => props.marginBottom};
`;
const Title = styled.div<{marginBottom?: string}>`
    margin-bottom:${props => props.marginBottom};
  font-size:18px;
font-weight: Bold;
`;
export {
    Span,
    QAWrapper,
    Title,
};

