import React, {Fragment} from 'react';
import {FormattedMessage} from 'react-intl';
import {Span, QAWrapper, Title} from './styles';
import {matchAnswer} from '../commonFunc';
import {
    QuestionnaireDataInterface,
} from '../../../models/local';

interface QASectionProps {
    title: string;
    questions: QuestionnaireDataInterface[];
    answers: number[];
}

const QASection: React.FC<QASectionProps> = (props: QASectionProps) => {
    const {title, questions, answers} = props;
    return (
        <QAWrapper>
            <Title marginBottom="15px">
                <FormattedMessage id={`${title}`}/>
            </Title>
            {questions && questions.map((questionnaire, index) => {
                const {question: questionnaireQuestion, answers: questionnaireAnswers} = questionnaire;
                const answersArr = matchAnswer(questionnaireAnswers, answers);
                if (answersArr && answersArr.length > 0) {
                    return (
                        <Fragment key={index}>
                            <Span marginBottom="6px">
                                {questionnaireQuestion}
                            </Span>
                            <Span fontWeight="bold" marginBottom="10px">
                                {answersArr.map((item, index) =>
                                    <Span marginBottom="2px" key={index}> {item} </Span>,
                                )}
                            </Span>
                        </Fragment>
                    );
                } else {
                    return <Fragment/>;
                }

            })}
        </QAWrapper>
    );
};

export default QASection;
