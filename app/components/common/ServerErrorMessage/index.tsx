/**
 *
 * Search
 *
 */
import React from 'react';

const errorIcon = require('images/errorIcon.svg');
import {injectIntl} from 'react-intl';
import MessageCard from '../../MessageCard';
import {Modal} from '@material-ui/core';
import {StyledDialogContent} from '../../../containers/FormDetails/styles';

interface Props {
    message: string;
    onDoneClick: () => void;
}

function ServerErrorMessage(props: Props) {
    const {onDoneClick, message} = props;
    return (

        <Modal
            open={message !== ''}
            onClose={onDoneClick}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            disableBackdropClick
        >
            <StyledDialogContent>
            <MessageCard
                src={errorIcon}
                messageTitle={'SERVER_ERROR'}
                message=""
                buttonTitle={'Close'}
                messageValue={undefined}
                actionText={undefined}
                onActionClick={() => {
                }}
                onButtonClick={onDoneClick}
                maxWidth="160px"
                isError={true}
            />
            </StyledDialogContent>
        </Modal>
    );
}

export default injectIntl(ServerErrorMessage);
