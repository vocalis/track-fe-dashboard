import styled from 'styled-components';
import styles from 'styles/values';
import { Slider, withStyles } from '@material-ui/core';

const TableWrapper = styled.div`
  width: 100%;
  height: 100%;
  padding-right: 11.042vw;
  padding-left: 11.042vw;
`;

const TableHeader = styled.div`
  padding-bottom: 30px;
  padding-top: 67.5px;
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const TableHeaderItem = styled.div<{
  weight: number;
  alignment: 'flex-start' | 'center';
}>`
  color: ${styles.colors.SHADE.GRAY.TABLE_TEXT};
  font-size: 15px;
  opacity: 40%;
  width: ${props => props.weight}%;
  display: flex;
  flex-direction: column;
  justify-content: ${props => props.alignment};
  align-items: center;
`;

const TableRow = styled.div`
  padding-bottom: 27px;
  padding-top: 24px;
  height: 100px;
  background-color: ${styles.colors.SHADE.WHITE.WHITE};
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  border-bottom: 1px solid ${styles.colors.SHADE.GRAY.BORDER};
`;

const TableRowItem = styled.div<{ weight: number }>`
  width: ${props => props.weight}%;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const TableContainer = styled.div`
  background-color: ${styles.colors.SHADE.WHITE.WHITE};
  border-radius: 6px;
  border: 1px solid ${styles.colors.SHADE.GRAY.TABLE_BORDER};
  display: flex;
  flex-direction: column;
`;

const TableHeaderItemText = styled.span<{onClick?: () => void}>`
  cursor: ${props => props.onClick ? 'pointer' : 'inherit'};
`;



const PrettoSlider = withStyles({
  root: {
    color: '#52af77',
    height: 8,
    width: 180,
    position: 'absolute',
    top: 83,
    right: 217,
  },
  thumb: {
    'height': 24,
    'width': 24,
    'backgroundImage': 'linear-gradient(to right,#38ABB6,#1C565B);',
    'border': '2px solid currentColor',
    'marginTop': -8,
    'marginLeft': -12,
    '&:focus,&:hover,&$active': {
      boxShadow: 'inherit',
    },
  },
  active: {},
  valueLabel: {
    left: 'calc(-50% + 4px)',
  },
  track: {
    height: 8,
    borderRadius: 4,
  },
  rail: {
    height: 8,
    borderRadius: 4,
  },
})(Slider);
export {
  TableWrapper,
  TableHeader,
  TableHeaderItem,
  TableContainer,
  TableRow,
  TableRowItem,
  PrettoSlider,
  TableHeaderItemText,
};
