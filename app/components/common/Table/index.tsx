import React, { useState } from 'react';
import {
  TableWrapper,
  TableHeader,
  TableHeaderItem,
  TableContainer,
  TableRow,
  TableRowItem,
  PrettoSlider, TableHeaderItemText,
} from './styles';

import { FormattedMessage } from 'react-intl';



export interface HeaderItem {
  title: string;
  weight: number;
  align: 'center' | 'flex-start';
  onClick?: () => void;
  showSlider?: boolean;
  onChangeSliderValue?: (value) => void;
}

export interface TableProps {
  headers: HeaderItem[];
  rows: JSX.Element[][];
}

const Table: React.FC<TableProps> = (props: TableProps) => {
  const { headers, rows } = props;
  const [sliderValue, setSliderValue] = useState(0);
  const onChange = (event: any, newValue: number) => {
    setSliderValue(newValue);
    if (headers &&  headers[4].onChangeSliderValue) {
    headers[4].onChangeSliderValue(newValue);
    }
  };
  return (
    <TableWrapper>
      <TableHeader>
        {headers.map(header => (
          <>
            <TableHeaderItem
              key={`header_${header.title}`}
              weight={header.weight}
              alignment={header.align}
            >
              <TableHeaderItemText onClick={header.onClick}>
                <FormattedMessage id={header.title} />
              </TableHeaderItemText>
              {header.showSlider && (
                <PrettoSlider
                    key={`tooltip${header.title}`}
                  valueLabelDisplay="auto"
                  aria-label="pretto slider"
                  value={sliderValue}
                  onChange={onChange}
                />
              )}
            </TableHeaderItem>
          </>
        ))}
      </TableHeader>
      <TableContainer>
        {rows &&
          rows.map((row, i) => (
            <TableRow key={`row_${i}`}>
              {row.map((item, index) => (
                <TableRowItem
                  key={`row_${i}_item_${index}`}
                  weight={headers[index].weight}
                >
                  {item}
                </TableRowItem>
              ))}
            </TableRow>
          ))}
      </TableContainer>
    </TableWrapper>
  );
};

export default Table;
