import styled from 'styled-components';
import styles from 'styles/values';

const DetectorsModalWrapper = styled.div`
    width:1000px;
    height: 657px;
    display: flex;
    flex-direction: column;
`;

const DetectorsModalHeader = styled.div`
    width: 100%;
    height: 63px;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    padding-left: 17px;
    padding-right: 27px;
    box-sizing: border-box;
`;

const DetectorsModalTitle = styled.div`
    height: 63px;
    text-align: left;
    font-size: 18px;
    font-weight: bold;
    letter-spacing: 0;
    color: #353535;
    text-transform: uppercase;
    display: flex;
    align-items: center;
`;

const DetectorsModalCloseIcon = styled.img`
    width: 13px;
    height: 13px;
    cursor: pointer;
`;

const DetectorsModalBody = styled.div`
    width: 100%;
    height: 594px;
    display: flex;
    flex-direction: row;
`;

const DetectorsModalItemsList = styled.div`
    width: 267px;
    height: 100%;
    display: flex;
    flex-direction: column;
    overflow-y: scroll;
    -ms-overflow-style: none;
    ::-webkit-scrollbar {
      display: none;
    }
    overflow: -moz-scrollbars-none;
`;

const DetectorCard = styled.div<{isSelected: boolean}>`
    width: 100%;
    background-color: ${props => props.isSelected ?
    styles.colors.SHADE.YELLOW.CARD_SHADE
    :
    styles.colors.SHADE.WHITE.WHITE};
    cursor: ${props => props.isSelected ? 'auto' : 'pointer'};;
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
`;

const DetectorCardIcon = styled.img`
    height: 38px;
    width: 38px;
    margin-left: 33px;
`;

const DetectorCardData = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
  padding-left: 32px;
  height: 99px;
  text-align: left;
  font-family: Roboto Medium;
  font-size: 18px;
  letter-spacing: 1.2px;
  color: #707070;
`;

const DetectorModalSelectedDataContainer = styled.div`
    height: 100%;
    width: 757px;
    padding-left: 43px;
    padding-right: 10px;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: flex-start;
`;

const DetectorModalBodyMainText = styled.div`
    text-align: left;
    font-size: 15px;
    font-weight: bold;
    font-family: Roboto;
    letter-spacing: 0.5px;
    color: #353535;
    padding-top: 32px;
    padding-bottom: 29px;
`;

const DetectorModalBodySecondaryText = styled.div`
    text-align: left;
    font-family: Roboto Regular;
    font-size: 15px;
    letter-spacing: 0;
    color: #353535;
    opacity: 0.4;
    padding-bottom: 30px;
`;

export {
    DetectorsModalWrapper,
    DetectorsModalHeader,
    DetectorsModalTitle,
    DetectorsModalCloseIcon,
    DetectorsModalBody,
    DetectorsModalItemsList,
    DetectorCard,
    DetectorCardIcon,
    DetectorCardData,
    DetectorModalSelectedDataContainer,
    DetectorModalBodyMainText,
    DetectorModalBodySecondaryText,
};
