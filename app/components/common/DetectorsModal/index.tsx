import React, {useState} from 'react';
import {Modal} from '../index';
import {
    DetectorCard,
    DetectorCardIcon,
    DetectorsModalBody,
    DetectorsModalCloseIcon,
    DetectorsModalHeader,
    DetectorsModalItemsList,
    DetectorsModalTitle,
    DetectorsModalWrapper,
    DetectorCardData,
    DetectorModalSelectedDataContainer,
    DetectorModalBodyMainText, DetectorModalBodySecondaryText,
} from './styles';
import {FormattedMessage} from 'react-intl';

const lungsIcon = require('images/ic_lungs.svg');
const esophagusIcon = require('images/ic_esophagus.svg');
const sleepingIcon = require('images/ic_sleeping.svg');
const alcohoIntoxicationIcon = require('images/alcohol_intoxication.svg');
const depressionIcon = require('images/depression.svg');

interface Props {
    onClose: () => void;
}

const itemsList = [
    {
        title: 'Pulmonary Hypertension',
        data: 'Detects the risk of having a Pulmonary Hypertension, and its severity.',
        icon: lungsIcon,
    },
    {
        title: 'Depression',
        data: 'Detects the risk of having a Depression, and its severity.',
        icon: depressionIcon,
    },
    {
        title: 'Alcohol Intoxication',
        data: 'Detects the risk of having an Alcohol Intoxication.',
        icon: alcohoIntoxicationIcon,
    },
    {
        title: 'COPD (flare-up)',
        data: 'Detects a COPD flare-up.',
        icon: esophagusIcon,
    },
    {
        title: 'OSA',
        data: 'Detects the risk of having an Obstructive Sleep Apnea, and its severity.',
        icon: sleepingIcon,
    },
    {
        title: ' Smoking',
        data: 'ndustry. Lorem Ipsum has been the industter took a ge specimen book. It has',
        icon: 'sleepingIcon',
    },
];

const closeIcon = require('images/close.svg');

const DetectorsModal = (props: Props) => {
    const {onClose} = props;
    const [selectedItemIndex, setSelectedItemIndex] = useState(0);

    const cards = itemsList.map((item, index) => (
        <DetectorCard
            key={`detector_${index}`}
            isSelected={selectedItemIndex === index}
            onClick={() => setSelectedItemIndex((index))}>
            <DetectorCardIcon src={item.icon} />
            <DetectorCardData>
                {item.title}
            </DetectorCardData>
        </DetectorCard>
    ));

    return (
    <Modal>
        <DetectorsModalWrapper>
            <DetectorsModalHeader>
                <DetectorsModalTitle>
                    <FormattedMessage id={'detectors_modal_title'}/>
                </DetectorsModalTitle>
                <DetectorsModalCloseIcon src={closeIcon} onClick={onClose}/>
            </DetectorsModalHeader>
            <DetectorsModalBody>
                <DetectorsModalItemsList>
                    {cards}
                </DetectorsModalItemsList>
                <DetectorModalSelectedDataContainer>
                    <DetectorModalBodyMainText>
                        {itemsList[selectedItemIndex].data}
                    </DetectorModalBodyMainText>
                </DetectorModalSelectedDataContainer>
            </DetectorsModalBody>
        </DetectorsModalWrapper>
    </Modal>
    );
};

export default DetectorsModal;
