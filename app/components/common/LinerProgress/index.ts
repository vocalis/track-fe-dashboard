import {withStyles} from '@material-ui/core';
import LinearProgress from '@material-ui/core/LinearProgress';

export const StyledLinerProgress = withStyles(() => ({
    colorSecondary: {
        backgroundColor: '#38ABB6',
    },
    barColorSecondary: {
        backgroundImage: 'linear-gradient(to right,#38ABB6,#1C565B)',
    },
}))(LinearProgress);
