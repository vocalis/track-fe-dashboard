import styled from 'styled-components';

const ButtonWrapper = styled.div`
    font-size: 15px;
    display: flex;
    justify-content: space-between;
    cursor: pointer;
`;

const LogoIcon = styled.img`
    width: 0.990vw;
    margin-right: 0.586vw;
`;

export {
    ButtonWrapper,
    LogoIcon,
};
