import React from 'react';
import { FormattedMessage } from 'react-intl';
import { ButtonWrapper, LogoIcon } from './styles';

interface ButtonProps {
    title: string;
    icon: string;
    action: any;
}

const HorizontalButton: React.FC<ButtonProps> = (props: ButtonProps) => {
    const { icon, title, action } = props;

    return (
        <ButtonWrapper onClick={action}>
            <LogoIcon src={icon} />
            <FormattedMessage id={title} />
        </ButtonWrapper>
    );
};

export default HorizontalButton;
