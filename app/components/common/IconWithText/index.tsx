import React from 'react';
import { FormattedMessage } from 'react-intl';
import {ContentWrapper, Icon, Title} from './styles';
import {getDocsIconByAnswers, getTrendsIconByStatus} from '../commonFunc';
interface IconWithTextProps  {
    title: string;
    trendsrc?: 'better' | 'same' | 'worse' | 'none' | null ;
    docsrc?: { daily: number[], expanded: number[] } | null | undefined ;
}

const IconWithText: React.FC<IconWithTextProps> = (props: IconWithTextProps) => {
        const{trendsrc, docsrc, title} = props;
        return (
        <ContentWrapper>
            {
                trendsrc ? <Icon src={getTrendsIconByStatus(trendsrc)}/> :
                    <Icon src={getDocsIconByAnswers(docsrc)}/>
            }
            <Title>
                <FormattedMessage id={`${title}`}/>
            </Title>
        </ContentWrapper>
    );
};

export default IconWithText;
