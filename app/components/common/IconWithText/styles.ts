import styled from 'styled-components';
const ContentWrapper = styled.div`
text-align: left;
font-size:  12px;
letter-spacing: 0;
color: #353535;
opacity: 1;
 display: flex;
 padding-left: 7px;
 padding-bottom: 19.5px;
`;
const Title = styled.div`
margin-left: 22.5px;
`;

const Icon = styled.img`
  height: 12px;
`;

export {
    ContentWrapper,
    Title,
    Icon,
};

