import styled from 'styled-components';

const CheckBoxWrapper = styled.div`
    font-size: 15px;
    display: flex;
    justify-content: space-between;
    cursor: pointer;
`;

const Checkbox = styled.img`
    width: 0.990vw;
    margin-right: 0.586vw;
`;

export {
    CheckBoxWrapper,
    Checkbox,
};
