import React from 'react';
import { FormattedMessage } from 'react-intl';
import { CheckBoxWrapper, Checkbox } from './styles';

interface CheckBoxProps {
    title: string;
    value: boolean | null | undefined;
    action: any;
    onDoubleClick?: (event: object) => void;
}

const selected = require('images/checkbox-selected.svg');
const unselected = require('images/checkbox-unselected.svg');

const HorizontalCheckBox: React.FC<CheckBoxProps> = (props: CheckBoxProps) => {
    const { value , title, action, onDoubleClick } = props;
    const valueTemp = value ? selected : unselected;

    return (
        <CheckBoxWrapper onDoubleClick={onDoubleClick} onClick={action}>
            <Checkbox src={valueTemp} />
            <FormattedMessage id={title} />
        </CheckBoxWrapper>
    );
};

export default HorizontalCheckBox;
