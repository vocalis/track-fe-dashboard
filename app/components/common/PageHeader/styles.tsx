import styled from 'styled-components';
import styles from 'styles/values';

const headerHeight = '120px';
const FormikWrapper = styled.div`
  width: 100%;

  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  padding-right: 160px;
   background: #FFFFFF;
padding-left: ${styles.sizes.APP_SIDE_PADDING};
`;
const   HeaderWrapper = styled.div`

           @media ${styles.sizes.MEDIUM_SCREEN}{
        padding-left: ${styles.sizes.HELP_SIDE_PADDINGE_LEFT};
        padding-right: ${styles.sizes.HELP_SIDE_PADDINGE_ROGHT};
         }
   @media ${styles.sizes.SMALL_SCREEN}{
   padding-right: ${styles.sizes.HELP_SIDE_PADDINGE_ROGHT_SMALL_SCREEN};
  padding-left: ${styles.sizes.HELP_SIDE_PADDINGE_LEFT_SMALL_SCREEN};
  }
     @media ${styles.sizes.LARGE_SCREEN}{
   padding-right: ${styles.sizes.APP_SIDE_PADDING_RIGHT_LARGE_SCREEN};
  padding-left: ${styles.sizes.APP_SIDE_PADDING_LEFT_LARGE_SCREEN};
  }

  width: 100%;
  height: 120px;
display: flex;
flex-direction: row;
    align-items: center;
 background: #FDFDFD;
 border-radius: 2px;
  opacity: 1;
    border-bottom: 2px solid #00000007;
`;

const TextWrapper = styled.div`
text-align: left;
font-weight: lighter;
font-size: 18px;
font-family: Roboto;
letter-spacing: 0px;
color: #353535;
opacity: 1;
width: 674px;
height: 50px;
margin-top: 39px;
margin-bottom: 57px;
`;
const Img = styled.img`
height: 23px;
width: 23px;
`;
const ImgWrapper = styled.div`
width:88px;
height: 70px;
margin-right: 22px;
`;
const TitleWrapper = styled.div`
font-size: 35px;
  font-family: Roboto-Regular;
  font-weight: normal;
`;
const ActionWrapper = styled.div`
width: 300px;
    display: flex;
    flex-direction: row;
        align-items: center;
        justify-content: flex-end;
`;
const ButtonWrapper = styled.div`
margin-left: 23px;
`;
const Span = styled.div<{color?: string, letterSpacing: string }>`
  color: ${props => props.color};
  letter-spacing: ${props => props.letterSpacing};
  font-weight: normal;
  font-size: 18px;
  display: flex;
     align-items: center;
`;
const Title = styled.div`
  color:#707070;
  font-weight: bold;
  font-family: Roboto-Bold;
  font-size:42px;
`;
const PaginationWrapper =  styled.div`
margin-top: 51px;
`;
const SearchWrapper = styled.div`
margin-bottom: 27px;
width:524px;
justify-content: space-between;
display: flex;
flex-direction: row;
`;
const Div = styled.div`
width: calc(100% - 192.17px);
display: flex;
flex-direction: row;
padding-left: 18.490vw;;
justify-content: space-between;
`;

const AddActionWrapper = styled.div`
display: flex;
flex-direction: column;
justify-content: center;
margin-bottom: 27px;

`;
const IconWithTextWrapper = styled.div`

`;

export {
    FormikWrapper,
    HeaderWrapper,
    Img,
    TitleWrapper,
    ActionWrapper,
    ButtonWrapper,
    ImgWrapper,
    Span,
    Title,
    TextWrapper,
    SearchWrapper,
    PaginationWrapper,
    Div,
    AddActionWrapper,
    IconWithTextWrapper,
};
