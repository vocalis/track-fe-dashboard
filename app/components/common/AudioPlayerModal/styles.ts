import styled from 'styled-components';
import styles from 'styles/values';

const AudioPlayerModalWrapper = styled.div`
    width: 53.333vw;
    height: 260px;
    display: flex;
    flex-direction: column;
`;

const AudioPlayerModalHeader = styled.div`
    width: 100%;
    height: 70px;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    padding-left: 22px;
    padding-right: 27px;
    box-sizing: border-box;
    width: 100%;
    border-bottom: 1px solid ${styles.colors.SHADE.GRAY.BORDER};
`;

const AudioPlayerModalTitle = styled.div`
    height: 70px;
    text-align: left;
    font-size: 18px;
    font-weight: bold;
    letter-spacing: 0;
    color: ${styles.colors.SHADE.GRAY.TABLE_TEXT};
    text-transform: uppercase;
    display: flex;
    align-items: center;
    padding:
`;


const AudioPlayerModalTitleName = styled.div`
    padding-right: 53px;
    font-size: 18px;
    font-weight: 600;
    letter-spacing: 0;
    color: ${styles.colors.TEXT.BLACK}
`;

const AudioPlayerModalCloseIcon = styled.img`
    width: 13px;
    height: 13px;
    cursor: pointer;
`;

const AudioPlayerModalBody = styled.div`
    width: 100%;
    height: calc(100% - 70px);
    display: flex;
    flex-direction: row;
    align-items: center;
`;

const ProgressWrapper = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    color: ${styles.colors.BRAND.PRIMARY}
`;


export {
    AudioPlayerModalWrapper,
    AudioPlayerModalHeader,
    AudioPlayerModalTitle,
    AudioPlayerModalCloseIcon,
    AudioPlayerModalBody,
    ProgressWrapper,
    AudioPlayerModalTitleName,
};
