import React, {useState} from 'react';
import {AudioPlayer, Modal} from '../index';
import {
    AudioPlayerModalBody,
    AudioPlayerModalCloseIcon,
    AudioPlayerModalHeader,
    AudioPlayerModalTitle, AudioPlayerModalTitleName,
    AudioPlayerModalWrapper, ProgressWrapper,
} from './styles';
import {FormattedMessage} from 'react-intl';
import {CircularProgress} from '@material-ui/core';


interface Props {
    onClose: () => void;
    data: Blob | null;
    recordingId: number;
    name?: string | null;
    id?: number | null;
    age?: string | number | null;
}

const closeIcon = require('images/close.svg');

const AudioPlayerModal = (props: Props) => {
    const {onClose , data, recordingId, name, id, age} = props;

    const getAudioBody = () => {
        if (data) {
            return (
                <AudioPlayer blobData={data}/>
            );
        }
        return (
            <ProgressWrapper>
                <CircularProgress color={'inherit'}/>
            </ProgressWrapper>
        );
    };

    return (
        <Modal>
            <AudioPlayerModalWrapper>
                <AudioPlayerModalHeader>
                    <AudioPlayerModalTitle>
                        <AudioPlayerModalTitleName>
                            {name}
                        </AudioPlayerModalTitleName>
                        <AudioPlayerModalTitleName>
                            <FormattedMessage id={'ID'}/>: {id}
                        </AudioPlayerModalTitleName>
                        <AudioPlayerModalTitleName>
                            <FormattedMessage id={'AGE'}/>: {age}
                        </AudioPlayerModalTitleName>
                    </AudioPlayerModalTitle>
                    <AudioPlayerModalCloseIcon src={closeIcon} onClick={onClose}/>
                </AudioPlayerModalHeader>
                <AudioPlayerModalBody>
                    {getAudioBody()}
                </AudioPlayerModalBody>
            </AudioPlayerModalWrapper>
        </Modal>
    );
};

export default AudioPlayerModal;
