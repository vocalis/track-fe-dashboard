import styled from 'styled-components';

const AudioPlayerWrapper = styled.div`
    height: 114px
    display: flex;
    align-items: center;
    width: 100%;
`;

const PlayButton = styled.img`
    height: 58px;
    margin: 55px;
    cursor: pointer;
`;
export {
    AudioPlayerWrapper,
    PlayButton,
};
