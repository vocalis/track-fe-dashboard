require('wavesurfer.js');

import React, {useState} from 'react';
import {AudioPlayerWrapper, PlayButton} from './styles';
import Wavesurfer from 'react-wavesurfer';
import styles from 'styles/values';

interface AudioPlayerProps {
    blobData: Blob | null;
}

const playSampleIcon = require('images/big_play.svg');
const pauseSampleICon = require('images/big_pause.svg');
// const pauseSampleIcon = require('images/big_pause.svg');

const AudioPlayer: React.FC<AudioPlayerProps> = (props: AudioPlayerProps) => {
    const { blobData } = props;

    const [audioPlayerStatus, setaudioPlayerStatus] = useState(false);

    const toggleAudioPlayer = () => {
        setaudioPlayerStatus(!audioPlayerStatus);
    };

    const loading = () => {
        console.log('loading');
    };
    return (
        <AudioPlayerWrapper>
           {!audioPlayerStatus &&  <PlayButton onClick={toggleAudioPlayer} src={playSampleIcon} />}
            {audioPlayerStatus &&  <PlayButton onClick={toggleAudioPlayer} src={pauseSampleICon} alt="pause"  />}
            <div style={{width: 'calc(100% - 226px)'}}>
                <Wavesurfer
                    responsive={true}
                    audioFile={blobData}
                    pos={0}
                    playing={audioPlayerStatus}
                    ready={loading}
                    options={{
                        waveColor: styles.colors.GRAPHS.GREEN,
                        pixelRatio: 2,
                        interact : true,
                        // minPxPerSec: 10,
                        skipLength: 2,
                        barWidth: 2,
                    }}
                />
            </div>
        </AudioPlayerWrapper>
    );
};

export default AudioPlayer;
