import styled from 'styled-components';
import styles from 'styles/values';

const AppLayout = styled.div`
  height: 100%;
  min-height: 100%;
  background-color: ${styles.colors.GENERAL.APP_BACKGROUND};
`;

const AppBody = styled.div`
  width: 100%;
  height: calc(100% - ${styles.sizes.HEADER_HEIGHT});
  position: absolute;
  left: 0;
  top: ${styles.sizes.HEADER_HEIGHT};
  bottom: 0;
  background-color: #ffffff;
`;
const AppFooter = styled.div`
  width: 100%;
  height: ${styles.sizes.FOOTER_HEIGHT};
  background: white;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
`;
const LayoutBottomLines = styled.img`
  width: 100%;
  height: 145.61px;
  opacity: 0.1;
`;

const BodyWrapper = styled.div`
  width: 100%;
  min-height: calc(100% - ${styles.sizes.FOOTER_HEIGHT});
  padding-bottom: ${styles.sizes.BODY_BOTTOM_PADDING};
  background-color: #ffffff;
`;
const LogoWrapper = styled.div`
  text-align: center;
  margin-top: 19.8px;
  background: #fdfdfd;
  padding-bottom: 37px;
  padding-top: 38px;
`;
const Logo = styled.img`
  width: 229px;
  height: 44px;
`;
const ByWrapper = styled.span`
  font-size: 16px;
  font-family: Roboto-Regular;
  color: #707070;
  opacity: 1;
  margin-right: 16px;
`;
export {
  AppLayout,
  AppBody,
  LayoutBottomLines,
  BodyWrapper,
  AppFooter,
  LogoWrapper,
  Logo,
  ByWrapper,
};
