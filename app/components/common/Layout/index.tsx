import React, {useState} from 'react';
import {
    AppLayout,
    AppBody,
    LayoutBottomLines,
    BodyWrapper,
    AppFooter,
    LogoWrapper,
    Logo,
    ByWrapper,
    } from './styles';
import AppHeader from '../../AppHeader';
import FotterLinks from '../../FotterLinks';
import DetectorsModal from '../DetectorsModal';
import SideBar from '../../../containers/SideBar';
const vtrackLogo = require('images/Vocalis-logo.svg');

const lines = require('images/app_lines.svg');

interface LayoutProps {
    children?: React.ReactNode;
    title: string;
}

const Layout: React.FC<LayoutProps> = (props: LayoutProps) => {
    const {children, title} = props;
    const [showDetectorsModal, setShowDetectorsModal] = useState<boolean>(false);
    return (
        <AppLayout>
            <AppHeader title={title}
                       onLogoClicked={() => setShowDetectorsModal(true)}
                      />
            <AppBody>
                <BodyWrapper>
                    {children}
                </BodyWrapper>
                <AppFooter>
                    <LayoutBottomLines src={lines}/>
                    <div style={{
                        width: '100%',
                        display: 'flex',
                        justifyContent: 'center'}}>
                    <FotterLinks/>
                    </div>
                    <LogoWrapper>

                        <ByWrapper> By</ByWrapper>
                        <a href="https://vocalishealth.com/" target="_blank">
                            <Logo src={vtrackLogo} alt="logo" />
                        </a>
                    </LogoWrapper>
                </AppFooter>
            </AppBody>
            {
                showDetectorsModal &&
                <DetectorsModal onClose={() => setShowDetectorsModal(false)}/>
            }
            <SideBar/>

        </AppLayout>

    );
};

export default Layout;
