import styled from 'styled-components';
import styles from 'styles/values';

const ModalBackdrop = styled.div<{ priority?: number }>`
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: ${(props): number | undefined => props.priority || 100};
    ::before {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: ${styles.colors.GENERAL.MODAL_BACKDROP};
        z-index: 0;
    }
`;

const ModalCard = styled.div`
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    max-width: 90vw;
    max-height: 972px;
    margin: auto;
    overflow: auto;
    z-index: 1;
    background: white;
`;

export {
    ModalBackdrop,
    ModalCard,
};
