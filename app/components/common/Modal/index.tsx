import React from 'react';
import { ModalBackdrop, ModalCard } from './styles';

interface Props {
    children?: React.ReactNode;
    priority?: number;
}

const Modal = (props: Props) => (
    <ModalBackdrop priority={props.priority}>
        <ModalCard>
            {props.children}
        </ModalCard>
    </ModalBackdrop>
);

export default Modal;
