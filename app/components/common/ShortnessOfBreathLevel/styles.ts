import styled from 'styled-components';
import styles from 'styles/values';
import {string} from 'prop-types';

const ShortnessOfBreathLevelWrapper = styled.div`
    height: 637.5px;
    margin-top: 50px;
    width: 100%;
`;

const Title = styled.div`
    text-align: left;
    font-size: 18px;
    font-weight: bold;
    letter-spacing: 0.6px;
    color: ${styles.colors.SHADE.GRAY.TABLE_TEXT};
    margin-bottom: 10px;
`;

const Body = styled.div`
    width: 100%;
    border: 1px solid ${styles.colors.SHADE.GRAY.TABLE_BORDER};
`;

const GraphWrapper = styled.div`
    width: 100%;
    height: 303px;
    border-bottom: 1px solid ${styles.colors.SHADE.GRAY.TABLE_BORDER};
`;

const DetailsWrapper = styled.div`
    width: 100%;
    height: 301px;
`;

const DetailsTitle = styled.div`
    width: 100%;
    height: 65px;
    border-bottom: 1px solid ${styles.colors.SHADE.GRAY.TABLE_BORDER};
    padding-left: 0.833vw;
    display: flex;
    align-items: center;
    justify-content: flex-start;
`;

const DetailsBody = styled.div`
    width: 100%;
    height: calc(100% - 65px);
    display: flex;
`;

const DetailsRightSide = styled.div`
    width: 40%;
    height: 100%;
    padding-left: 0.885vw;
    padding-top: 30px;
`;

const DetailsLeftSideTitle = styled.div`
    text-align: left;
    font-size: 15px;
    font-weight: bold;
    letter-spacing: 0.5px;
    color: ${styles.colors.SHADE.GRAY.TABLE_TEXT};
    text-transform: uppercase;
`;

const DetailsLeftSideBody = styled.div`
    margin-top: 25.5px;
    font-size: 15px;
    letter-spacing: 0;
    color: #353535;
    opacity: 0.77;
`;
const DetailsLeftSideBodyRow = styled.div`
    display: flex;
    align-items: center;
    margin-top: 17px;
`;

const DetailsLeftSideBodyField = styled.div`
    width: 40%;
`;

const DetailsLeftSideBodyValue = styled.div`
    font-weight: bold;
`;

const DetailsLeftSide = styled.div`
    width: 60%;
    height: 100%;
    border-right: 1px solid ${styles.colors.SHADE.GRAY.TABLE_BORDER};
    padding-top: 30px;
    padding-left : 2.135vw;
    overflow-y: auto;
::-webkit-scrollbar {
  width: 7px;
}

  /* Track */
  &::-webkit-scrollbar-track {
    background: #EAEAEA;
  }

  /* Handle */
  &::-webkit-scrollbar-thumb {
    background: #38ABB6;
  }

  /* Handle on hover */
  &::-webkit-scrollbar-thumb:hover {
    background: #555;
  }
`;

const DetailsRightSideTitle = styled.div`
    text-align: left;
    font-size: 15px;
    font-weight: bold;
    letter-spacing: 0.5px;
    color: ${styles.colors.SHADE.GRAY.TABLE_TEXT};
    text-transform: uppercase;
    cursor: pointer;
    display: flex;
`;

const DetailsRightSideBody = styled.div`
    text-align: left;
    font-size: 15px;
    letter-spacing: 0.5px;
    color: ${styles.colors.SHADE.GRAY.TABLE_TEXT};
    margin-top: 26px;
    padding-bottom: 30px;
`;

const QuestionnaireWrapper = styled.div`
    margin-bottom: 33px;
`;

const QuestionnaireQuestion = styled.div`

`;

const QuestionnaireAnswer = styled.div`
    margin-top: 12px;
    font-weight: bold;
    display: flex;
    align-items: center;
`;

const QuestionnaireAnswerIcon = styled.div`
    width: 0;
    height: 0;
    border-top: 4.5px solid transparent;
    border-bottom: 4.5px solid transparent;
    border-left: 6px solid ${styles.colors.SHADE.GRAY.TABLE_TEXT};
    margin-right: 0.391vw;

`;
const ToggleIcon = styled.img<{ direction: string }>`
height: 10px;
padding-left: 2px;
   width: 10px;
   display: flex;
   align-self: center;
     transform: rotate${props => props.direction === 'up' ? '(-90deg)' : '(-270deg)'};
  -webkit-transform: ${props => props.direction === 'up' ? '(-90deg)' : '(-270deg)'};
  margin-left: 3px;
  margin-top: 2px;
`;

export {
    ShortnessOfBreathLevelWrapper,
    Title,
    Body,
    GraphWrapper,
    DetailsWrapper,
    DetailsTitle,
    DetailsBody,
    DetailsLeftSide,
    DetailsLeftSideTitle,
    DetailsLeftSideBody,
    DetailsLeftSideBodyRow,
    DetailsLeftSideBodyField,
    DetailsLeftSideBodyValue,
    DetailsRightSide,
    DetailsRightSideTitle,
    DetailsRightSideBody,
    QuestionnaireWrapper,
    QuestionnaireQuestion,
    QuestionnaireAnswer,
    QuestionnaireAnswerIcon,
    ToggleIcon,
};
