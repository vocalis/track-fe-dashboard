import React, {useCallback, useEffect, useState} from 'react';
import {FormattedMessage} from 'react-intl';
import {
    ShortnessOfBreathLevelWrapper,
    Title,
    Body,
    GraphWrapper,
    DetailsWrapper,
    DetailsTitle,
    DetailsBody,
    DetailsLeftSide,
    DetailsRightSide,
    DetailsLeftSideTitle,
    DetailsLeftSideBody,
    DetailsLeftSideBodyField,
    DetailsLeftSideBodyRow,
    DetailsLeftSideBodyValue,
    DetailsRightSideTitle,
    DetailsRightSideBody,
    QuestionnaireWrapper,
    QuestionnaireQuestion,
    QuestionnaireAnswer,
    QuestionnaireAnswerIcon,
    ToggleIcon,
} from './styles';
import {ShortnessOfBreathGraph} from 'components/Graphs';
import {Patient, PatientAnalysisAnswer} from 'models/local';
import moment, {Moment} from 'moment';
import {testScriptQuestion} from 'jest-cli/build/init/questions';
const img = require('../../../images/Toggle.svg');

interface ShortnessOfBreathLevelProp {
    patient: Patient;
    playSample: (recordingId: number) => void;
    questionnaireData: QuestionnaireInterface | null;
}

interface QuestionnaireInterface {
    daily: QuestionnaireDataInterface[];
    expanded: QuestionnaireDataInterface[];
}

interface QuestionnaireDataInterface {
    id: number;
    question: string;
    answers: QuestionnaireAnswersDataInterface[];
}

interface QuestionnaireAnswersDataInterface {
    id: number;
    answer: string;
}

interface PatientDiary {
    question: string;
    answer: string[];
}

// interface PatientDiaryArray {
//     question: string;
//     answer: string[];
// }

const ShortnessOfBreathLevel: React.FC<ShortnessOfBreathLevelProp> = (props: ShortnessOfBreathLevelProp) => {
    const {patient, playSample, questionnaireData} = props;
    const [dailyDiary, setDailyDiary] = useState<PatientDiary[] | null>(null);
    const [expandedDiary, setExpandedDiary] = useState<PatientDiary[] | null>(null);
    const [dailyDiaryToggle, setDailyDiaryToggle] = useState<boolean>(true);
    const [expandedDiaryToggle, setExpandedDiaryToggle] = useState<boolean>(true);
    const [showDate, setShowDate] = useState<Moment>(moment());
    const getDays = () => {
        if (patient && patient.analysis &&  patient.analysis.length) {
            const lenght = patient.analysis.length;
            const  lastDate  = moment(new Date());
            const  firstDate = moment(patient.analysis[lenght - 1].sourceDate || new Date());
            return(lastDate.diff(firstDate, 'days') + 1);
        }
        return 0;
    };

    const updateDiary = useCallback((diary: PatientAnalysisAnswer, date: Moment) => {
        setDailyDiaryToggle(true);
        setExpandedDiaryToggle(true);
        const diaryAns: PatientDiary[] = [];
        const expandedAns: PatientDiary[] = [];
        if (questionnaireData) {
            for (const questionnaireQuestion of questionnaireData.daily) {
                const question = questionnaireQuestion.question;
                const answers: string[] = [];
                for (const questionnaireAnswer of questionnaireQuestion.answers) {
                    for (const dailyAnswer of diary.daily) {
                        if (questionnaireAnswer.id === dailyAnswer) {
                            answers.push(questionnaireAnswer && questionnaireAnswer.answer);
                        }
                    }
                }
                if (answers.length > 0) {
                    diaryAns.push({
                        question: question,
                        answer: answers,
                    });
                }
            }
            for (const questionnaireQuestion of questionnaireData.expanded) {
                const question = questionnaireQuestion.question;
                const answers: string[] = [];
                for (const questionnaireAnswer of questionnaireQuestion.answers) {
                    for (const dailyAnswer of diary.expanded) {
                        if (questionnaireAnswer.id === dailyAnswer) {
                            answers.push(questionnaireAnswer && questionnaireAnswer.answer);
                        }
                    }
                }
                if (answers.length > 0) {
                    expandedAns.push({
                        question: question,
                        answer: answers,
                    });
                }
            }
        }
        setShowDate(date);
        setDailyDiary(diaryAns);
        setExpandedDiary(expandedAns);
    }, []);

    let totalRecordings = 0;
    if (patient && patient.analysis) {
        for (const analysisItem of  patient.analysis) {
            if (analysisItem) {
                totalRecordings++;
            }
        }
    }

    let lastActivity: Moment | string = '-';
    if (patient && patient.lastActivity) {
        lastActivity = moment(patient.lastActivity).format('MMM DD, YYYY');
    }

    const getDiary = (diary) => {
        if (!diary || diary.length === 0) {
            return 'No diaries were documented ';
        }
        return diary.map((diary: PatientDiary, index: number) => {
            return (
                <QuestionnaireWrapper key={`diary_${index}`}>
                    <QuestionnaireQuestion>
                        {diary.question}
                    </QuestionnaireQuestion>
                    {diary.answer.map((answer, index) => {
                        return (<QuestionnaireAnswer key={index}>
                            <QuestionnaireAnswerIcon/>
                            {answer}
                        </QuestionnaireAnswer>);
                    })}
                </QuestionnaireWrapper>
            );
        });
    };
    const  onDiaryClick = (type: string) => {
         if (type === 'daily') {

                setDailyDiaryToggle(!dailyDiaryToggle);
                } else {
                setExpandedDiaryToggle(!expandedDiaryToggle);

                }
    };
    return (
        <ShortnessOfBreathLevelWrapper>
            <Title><FormattedMessage id={'SHORTNESS_OF_BREATH_LEVEL'}/></Title>
            <Body>
                <GraphWrapper>
                    <ShortnessOfBreathGraph
                        lastActivity={patient.lastActivity}
                        analysis={patient.analysis}
                        playSample={playSample}
                        days={getDays()}
                        updateDiary={updateDiary}
                        questionnaireData={questionnaireData}/>
                </GraphWrapper>
                <DetailsWrapper>
                    <DetailsTitle>
                        {showDate.format('MMM DD, YYYY')}
                    </DetailsTitle>
                    <DetailsBody>
                        <DetailsLeftSide>
                            <DetailsRightSideTitle onClick={() => onDiaryClick('daily')}>
                                <span>
                                <FormattedMessage id={'DAILY_COPY_DIARY'}/>
                                </span>
                                <span>
                                    {`(${dailyDiary && dailyDiary.length || 0})`}
                                </span>
                                <ToggleIcon src={img} direction={dailyDiaryToggle ? 'up' : 'down'}  />
                            </DetailsRightSideTitle>
                            <DetailsRightSideBody>
                                {dailyDiaryToggle && getDiary(dailyDiary)}
                            </DetailsRightSideBody>
                            <DetailsRightSideTitle onClick={() => onDiaryClick('expanded')}>
                                <span>
                                <FormattedMessage id={'EXPANDED_COPY_DIARY'}/>
                            </span>
                                <span>
                                     {`(${expandedDiary && expandedDiary.length || 0})`}
                                </span>
                                <ToggleIcon src={img} direction={expandedDiaryToggle ? 'up' : 'down'}  />
                            </DetailsRightSideTitle>
                            <DetailsRightSideBody>
                                {expandedDiaryToggle && getDiary(expandedDiary)}
                            </DetailsRightSideBody>
                        </DetailsLeftSide>
                        <DetailsRightSide>
                            <DetailsLeftSideTitle>
                                <FormattedMessage id={'PATIENT_INFO'}/>
                            </DetailsLeftSideTitle>
                            <DetailsLeftSideBody>
                                <DetailsLeftSideBodyRow>
                                    <DetailsLeftSideBodyField>
                                        <FormattedMessage id={'TOTAL_RECORDING'}/>
                                    </DetailsLeftSideBodyField>
                                    <DetailsLeftSideBodyValue>
                                        {totalRecordings}
                                    </DetailsLeftSideBodyValue>
                                </DetailsLeftSideBodyRow>
                                <DetailsLeftSideBodyRow>
                                    <DetailsLeftSideBodyField>
                                        <FormattedMessage id={'LAST_ACTIVITY'}/>
                                    </DetailsLeftSideBodyField>
                                    <DetailsLeftSideBodyValue>
                                        {lastActivity}
                                    </DetailsLeftSideBodyValue>
                                </DetailsLeftSideBodyRow>
                                <DetailsLeftSideBodyRow>
                                    <DetailsLeftSideBodyField>
                                        <FormattedMessage id={'EMAIL'}/>
                                    </DetailsLeftSideBodyField>
                                    <DetailsLeftSideBodyValue>
                                        {patient.email}
                                    </DetailsLeftSideBodyValue>
                                </DetailsLeftSideBodyRow>
                                <DetailsLeftSideBodyRow>
                                    <DetailsLeftSideBodyField>
                                        <FormattedMessage id={'PHONE'}/>
                                    </DetailsLeftSideBodyField>
                                    <DetailsLeftSideBodyValue>
                                        {patient && patient.phone || '-'}
                                    </DetailsLeftSideBodyValue>
                                </DetailsLeftSideBodyRow>

                            </DetailsLeftSideBody>
                        </DetailsRightSide>
                    </DetailsBody>
                </DetailsWrapper>
            </Body>
        </ShortnessOfBreathLevelWrapper>
    );
};

export default ShortnessOfBreathLevel;
