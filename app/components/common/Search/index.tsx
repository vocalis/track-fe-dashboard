/**
 *
 * Search
 *
 */
import React, {useState} from 'react';

// import styled from 'styles/styled-components';

import {FormattedMessage, injectIntl} from 'react-intl';
import {Input, InputTitle, InputWrapper, SearchButton, SearchWrapper, Text, Select} from './styles';
import './style.css';
import {MonitorSearchParams} from '../../../containers/MonitorPage/types';
interface Props {
    reviewedPatients: ReadonlyArray<string> | null;
    intl: any;
    searchParams: MonitorSearchParams;
    onSearchClicked: (searchTextValue: string,
                      reviewedPatients: string,
                    ) => void;
}
function Search(props: Props) {
    const {onSearchClicked , intl, searchParams} = props;
    const {reviewed, searchTextValue: searchValue, isFiltering} = searchParams;
    const getSelectedRviewOption = () => {
        if (reviewed === null) {
            return 'all';
        } else {
            if (reviewed === `1970-01-01T00:00:00.000Z`) {
                return 'not_reviewed_today';
            } else {
                    return 'reviewed_today';
            }
        }
    };
    const [searchTextValue, setSearchTextValue] = useState(searchValue || '');
    const [reviewedPatients, setReviewedPatients] = useState(getSelectedRviewOption);
    const [filterData, setFilterData] = useState(false);
    const onSearchClick = () => {
        if (searchTextValue !== '' ||  reviewedPatients !== 'all') {
             setFilterData(true);
            } else {
            setFilterData(false);
        }
        onSearchClicked(
            searchTextValue,
            reviewedPatients,
        );
    };
    const getSearchWrapperBackgroundColor = () => {
        if (isFiltering) {
            return 'filterColor';
        } else {
            if (searchTextValue === '' && reviewedPatients === 'all') {
                return '';
            }
        }
        return 'inputChanged';
    };
    return (
    <SearchWrapper className={getSearchWrapperBackgroundColor()}>
        <div style={{display: 'flex'}}>
        <InputWrapper  width={'15.781vw'}>
            <InputTitle><FormattedMessage id={'FILTER_DATA'}/></InputTitle>
            <Input type={'string'} value={searchTextValue} onChange={event => setSearchTextValue(event.target.value)}/>
        </InputWrapper>
        <InputWrapper width={'15.781vw'} margin={'0vw 1.563vw 0vw 0vw'}>
            <InputTitle><FormattedMessage id={'SEARCH_REVIEWED_PATIENTS'}/></InputTitle>
            <Select  value={reviewedPatients}  onChange={event => setReviewedPatients(event.target.value)} >
                <option value="all">{intl.formatMessage({id : 'ALL_PATIENTS'})}</option>
                <option value="not_reviewed_today">{intl.formatMessage({id : 'NOT_REVIEWED_TODAY'})}</option>
                <option value="reviewed_today">{intl.formatMessage({id : 'REVIEWED_TODAY'})}</option>
            </Select>
        </InputWrapper>
        </div>
        <SearchButton onClick={onSearchClick}>
            <Text><FormattedMessage id={'SEARCH'}/></Text>
        </SearchButton>
    </SearchWrapper>
  );
}

export default injectIntl(Search) ;
