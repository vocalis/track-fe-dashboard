import styled from 'styled-components';
import styles from 'styles/values';
import FONT from '../../../styles/values/fonts';
import { GeneralStyledProps } from '../../../styles/styled-components';

export const SearchWrapper = styled.div`
  width: 100%;
  height: 140px;
  display: flex;
  justify-content: space-between;
  flex-direction: row;
  align-items: center;

  @media ${styles.sizes.SMALL_SCREEN}{
   padding-right: ${styles.sizes.APP_SIDE_PADDING_RIGHT_SMALL_SCREEN};
  padding-left: ${styles.sizes.APP_SIDE_PADDING_LEFT_SMALL_SCREEN};
 }
   @media ${styles.sizes.MEDIUM_SCREEN}{
   padding-right: ${styles.sizes.APP_SIDE_PADDING_RIGHT};
  padding-left: ${styles.sizes.APP_SIDE_PADDING_LEFT};
 }
   @media ${styles.sizes.LARGE_SCREEN}{
   padding-right: ${styles.sizes.APP_SIDE_PADDING_RIGHT_LARGE_SCREEN};
  padding-left: ${styles.sizes.APP_SIDE_PADDING_LEFT_LARGE_SCREEN};
 }
`;
// export const SearchWrapper = styled.div<{backgroundColor: string}>`
//   width: 100%;
//   height: 140px;
//   display: flex;
//   justify-content: space-between;
//   flex-direction: row;
//   align-items: center;
//   padding-right: ${styles.sizes.APP_SIDE_PADDING};
//   padding-left: ${styles.sizes.APP_SIDE_PADDING};
//   background-color:${props => props.backgroundColor === 'paleRed' ? 'rgba(210, 34, 66, 0.1)' : '#FDFDFD'};
// `;
export const InputWrapper = styled.div<GeneralStyledProps>`
  width: ${props => props.width || '8.594vw'};
  height: 100%;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  margin: ${props => props.margin || '0vw 2.188vw 0vw 0vw'};
`;
export const InputTitle = styled.div`
  width: 100%;
  height: 19.5px;
  margin-bottom: 7px;
  display: flex;
  justify-content: flex-start;
  flex-direction: column;
  align-items: flex-start;
  font-size: ${FONT.PRIMARY};
  color: #353535;
  padding-left: 0.039vw;
  padding-bottom: 0.313vw;
`;
export const Input = styled.input`
  width: 100%;
  height: 36px;
  display: flex;
  justify-content: flex-start;
  flex-direction: column;
  align-items: flex-start;
  border: 1px solid #C9C9C9;
  border-radius: 2px;
`;

export const Select = styled.select`
  width: 100%;
  height: 36px;
  display: flex;
  justify-content: flex-start;
  flex-direction: column;
  align-items: flex-start;
  border: 1px solid #C9C9C9;
  border-radius: 2px;
  font-size: 13px;
  padding: 6px;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  background-color: ${styles.colors.SHADE.WHITE.WHITE};
`;

export const SearchButton = styled.button`
  width: 8.594vw;
  height: 36px;
  background-color: #38ABB6;
  border-radius: 2px;
  margin-top: 23px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  cursor: pointer;
`;

export const Text = styled.text`
  font-size: ${FONT.PRIMARY};
  color: #FFFFFF;
`;
