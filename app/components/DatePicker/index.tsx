import React, { Fragment, useCallback, useEffect, useState } from 'react';
import {
  TitleWrapper,
  TextFieldWrapper,
  ErrorWrapper,
  StyledTextField,
  customTheme,
  useStyles,
} from './styles';
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import moment from 'moment';
import { MuiThemeProvider, TextFieldProps } from '@material-ui/core';
import DatePickerHeader from './DatePickerHeader';

interface DatetProps {
  onChange: (id: string, value: string | null) => void;
  label?: JSX.Element;
  id: string;
  isSubmitted: boolean;
  value?: string;
  style?: object;
  fontFamily?: string;
}

const DatePicker: React.FC<DatetProps> = (props: DatetProps) => {
  const { onChange, label, id, isSubmitted, value, style } = props;
  const handleDateChange = (date) => {
    if (date === null) {
      // @ts-ignore
      setSelectedDate(null);
      onChange(id, null);
    } else {
      if (moment(date).format('MM/DD/YYYY') === 'Invalid date') {
        onChange(id, 'Invalid date');
      } else {
          setSelectedDate(moment(date).format('MM/DD/YYYY'));
          onChange(id, moment(date).format('MM/DD/YYYY'));
      }
    }
  };
  const [selectedDate, setSelectedDate] = useState(value);
  const classes = useStyles();
  const renderInput = useCallback(
    (props: TextFieldProps) => {
      const { error, helperText } = props;
      if (helperText !== '') {
        onChange(id, 'Invalid date');
      }
      return (
        <StyledTextField
          {...props}
          className={isSubmitted && error ? classes.redborder : classes.border}
          helperText={
            isSubmitted && error && helperText ? (
              <ErrorWrapper>{'Invalid date'}</ErrorWrapper>
            ) : (
              <Fragment />
            )
          }
        />
      );
    },
    [isSubmitted],
  );
  const renderToolbar = useCallback(
    (props) => {
      return <DatePickerHeader {...props} selectedDate={selectedDate} />;
    },
    [selectedDate],
  );

  useEffect(() => {
    setSelectedDate(value);
  }, [value]);
  return (
    <TextFieldWrapper style={style}>
      <TitleWrapper>{label}</TitleWrapper>
      <MuiThemeProvider theme={customTheme}>
        <MuiPickersUtilsProvider utils={MomentUtils}>
          <KeyboardDatePicker
            clearable
            value={selectedDate}
            placeholder="DD/MM/YYYY"
            onChange={(date) => {
              handleDateChange(date);
            }}
            format="DD/MM/YYYY"
            minDate={moment('1900/01/01').format('YYYY/MM/DD')}
            maxDate={moment().format('YYYY/MM/DD')}
            TextFieldComponent={renderInput}
            InputProps={{ disableUnderline: true }}
            ToolbarComponent={renderToolbar}
            showTodayButton={false}
          />
        </MuiPickersUtilsProvider>
      </MuiThemeProvider>
    </TextFieldWrapper>
  );
};

export default DatePicker;
