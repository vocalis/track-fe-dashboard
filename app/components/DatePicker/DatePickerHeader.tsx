import React, {useEffect, useState} from 'react';
import {CalendarHeader, ImgWrapper, Img, SelectWrapper} from './styles';
// import {Select} from '../Select';
import {RowsPerPage} from '../../containers/UserManagement/constants';
import {SelecetWrapper} from '../../containers/UserManagement/styles';
import moment from 'moment';
import {Select} from '../Select';

const next = require('images/nextCalendar.svg');
const previous = require('images/previousCalendar.svg');

interface Props {

    onChange(month1: moment.Moment): void;
    selectedDate: Date;
}

const DatePickerHeader = (props: Props) => {
    const {selectedDate} = props;
    const handleChange = (name, value) => {
        if (name === 'years') {
            setYear(value);
            props.onChange(moment()
                .set('year', Number(value))
                .set('month', Number(month)));
        } else {
            setMonth(monthArray.indexOf(value));
            props.onChange(moment().set('year', Number(year)).set('month', monthArray.indexOf(value)));
        }
    };

    const monthArray = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    const [months, setMonths] = useState<number []>([]);
    const [years, setYears] = useState<string []>([]);
    const [month, setMonth] = useState<number>(4);
    const [year, setYear] = useState<string>('1993');
    useEffect(() => {
        if (selectedDate) {
            setMonth(moment(selectedDate).get('month'));
            setYear(`${moment(selectedDate).get('year')}`);
        }
    }, [selectedDate]);

    useEffect(() => {
        const monthArray: number[] = [];
        const yearArray: string[] = [];
        for (let i = 0; i < 12; i++) {
            monthArray.push(i);
        }
        for (let i = 1900; i <= moment().get('year'); i++) {
            yearArray.push(`${i}`);
        }
        setMonths([...monthArray]);
        setYears([...yearArray]);
        if (selectedDate) {
            props.onChange(moment(selectedDate));
        } else {
            props.onChange(moment().set('year', 1993).set('month', 5));
        }

    }, []);
    const changeMonth = (event) => {
        if (event.currentTarget.id === 'next') {
            if (month === 11) {
            setMonth(0);
            if (Number(year) >= moment().get('year')) {
                setYear('1900');
                props.onChange(moment().set('year', 1900).set('month', 0));
            } else {
                setYear(`${Number(year) + 1}`);
                props.onChange(moment().set('year', Number(year) + 1).set('month', 0));
            }
            } else {
                setMonth(month + 1);
                props.onChange(moment().set('year', Number(year)).set('month', month + 1));
            }
        } else {
            if (month === 0) {
                setMonth(11);
                if (Number(year) === 1900) {
                    setYear(`${moment().get('year')}`);
                    props.onChange(moment().set('year', moment().get('year')).set('month', 11));
                } else {
                    setYear(`${Number(year) - 1}`);
                    props.onChange(moment().set('year', Number(year) - 1).set('month', 11));
                }
            } else {
                setMonth(month - 1);
                props.onChange(moment().set('year', Number(year)).set('month', month - 1));
            }
        }
    };
    return (
        <CalendarHeader>
            <ImgWrapper onClick={changeMonth} id="previous">
                <Img     transform="rotate(180deg)" src={previous} alt="next day"/>
            </ImgWrapper>
            <SelectWrapper>
            <Select
                onChange={handleChange}
                id="months"
                name="months"
                value={monthArray}
                selectedValue={monthArray[month]}
                withLabel={false}
                width="100px"
                height="36px"
                classes={{
                    borderRadius: '0px',
                    padding: '0px',
                    height: '36px'}}
            />
                <Select
                    onChange={handleChange}
                    id="years"
                    name="years"
                    value={[...years]}
                    selectedValue={year}
                    withLabel={false}
                    width="100px"
                    height="36px"
                    classes={{
                        borderRadius: '0px',
                        padding: '0px',
                        height: '36px'}}
                />
            </SelectWrapper>
            <ImgWrapper id="next" onClick={changeMonth}>
                <Img  transform="rotate(0deg)" src={next} alt="previous day"/>
            </ImgWrapper>
        </CalendarHeader>

    );

};

export default DatePickerHeader;
