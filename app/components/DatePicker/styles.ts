import {withStyles, TextField, createStyles, createMuiTheme, ThemeOptions} from '@material-ui/core';
import styled from 'styled-components';
import {makeStyles} from '@material-ui/core/styles';
import styles from '../../styles/values';

const ErrorWrapper = styled.span<{ isPassword?: boolean, errorPossition?: string }>`
    font-size: 16px;
    color: #D22242;
    font-family: Roboto-Regular;
        font-weight: normal;
        margin-top: 10px;
    margin-left: -9px;
`;
const TextFieldWrapper = styled.div`
position: relative;
width: 100%;
`;
const TitleWrapper = styled.div`

`;

const StyledTextField = withStyles(() => ({
    root: {
        height: '41px',
        width: '100%',
        color: 'red',
        padding: '4px',
        fontSize: '18px',
        marginTop: '11px',
        paddingBottom: '10px',
        paddingLeft: '9px',
        boxSizing: 'border-box',
        fontFamily: 'Roboto-Regular',
        underline: 'none',
        borderRadius: '8px',
    },
}))(TextField);


const useStyles = makeStyles(() =>
    createStyles({
        border: {
            border: '1px solid #DBDBDB',
        },
        redborder: {
            border: '1px solid #D22242',
        },
    }),
);

// @ts-ignore
// @ts-ignore

interface CustomType  {

}


const theme = {
    props  : {
        MuiButtonBase: {
            // The properties to apply
            disableRipple: true, // No more ripple, on the whole application!
        },
    },
    overrides: {
      MuiPickersDay: {
            day: {
                'border': '1px solid #c5c5c5',
                'background': ' #ededed',
                'fontWeight': 'normal',
                'color': '#2b2b2b',
                'borderRadius': 0,
                'margin': ' 2px',

                'width': '45px',
                'height': '30px',
                '&:hover ': {
                    backgroundColor: 'rgba(255, 195, 0, 0.1) !important',
                },
            },
            daySelected: {
                color: '#2b2b2b',
                backgroundColor: 'rgba(255, 195, 0, 0.1)',
            },
            current: {
                color: '#2b2b2b',
            },
        },
      MuiPickersCalendarHeader: {
            dayLabel: {
                color: '#454545',
                fontFamily: 'Roboto-Bold',
                width: '45px',
            },
        },
      MuiPickersBasePicker: {
            pickerView: {
                maxWidth: '375px',
                minWidth: '350px',
            },
        },
      MuiButton: {
            textPrimary: {
                color: '#2b2b2b',
            },
        },
    },
};
const customTheme = createMuiTheme(theme as ThemeOptions);
const CalendarHeader = styled.div`
    background-color: rgba(231, 231, 231, 1);
    width: 100%;
    height: 48px;
    position: absolute;
        z-index: 2;
        display: flex;
    justify-content: space-between;
    align-items: center;
`;


const ImgWrapper = styled.div`
width: 48px;
    height: 36px;
    align-items: center;
    display: flex;
    justify-content: center;
    align-items: center;
   background-color:#e7e7e7;
      &:hover {
         border: 1px solid #cccccc;
    background-color: #EDEDED;
    cursor: pointer;
  }
`;
const Img = styled.img<{ transform: string }>`
    height: 16px;
    width: 16px;
     transform:${props => props.transform}
`;
const SelectWrapper = styled.div`
    display: flex;
`;
export {
    TextFieldWrapper,
    TitleWrapper,
    ErrorWrapper,
    StyledTextField,
    customTheme,
    useStyles,
    CalendarHeader,
    ImgWrapper,
    Img,
    SelectWrapper,
};

