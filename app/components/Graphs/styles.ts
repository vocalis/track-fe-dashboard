import styled from 'styled-components';
import styles from 'styles/values';

const GraphWrapper = styled.div`
    width: auto;
    height: 100%;
`;

export {
    GraphWrapper,
};
