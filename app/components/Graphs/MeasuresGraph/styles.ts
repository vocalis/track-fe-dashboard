import styled from 'styled-components';
import styles from 'styles/values';

const GraphWrapper = styled.div`
    width: auto;
    height: 100%;
`;

const TooltipWrapper = styled.div`
    width:200px;
    height: 100px;
`;

const Scroller = styled.rect`
    fill: green;
`;
export {
    GraphWrapper,
    TooltipWrapper,
    Scroller,
};
