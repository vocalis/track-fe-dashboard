import React from 'react';
import {GraphWrapper} from './styles';
import { ResponsiveContainer, ComposedChart, Line, XAxis, YAxis, Tooltip} from 'recharts';
import styles from 'styles/values';
import GraphTooltip from '../GraphTooltip';
import moment from 'moment';
import _ from 'lodash';
import {PatientVital} from '../../../models/local';
import {LineGraph} from '../index';
import {LineProp} from '../LineGraph';
import {getDomain} from '../../common/commonFunc';

interface MeasuresGraphProps {
    data?: PatientVital[];
    days?: number;
}


const MeasuresGraph: React.FC<MeasuresGraphProps> = (props: MeasuresGraphProps) => {
    const {data, days} = props;

    const domain1 = getDomain(35, 40, data, 'temperature');
    const domain2 = getDomain(30, 60, data, 'o2');
    const lines: LineProp[] = [
        {
            field: 'o2',
            color: styles.colors.GRAPHS.PINK,
            unit: 'O2 Sat',
            domain: {
                orientation: 'left',
                values: [domain2[0], domain2[1]],
            },
        },
        {
            field: 'temperature',
            color: styles.colors.GRAPHS.BLUE,
            unit: 'Temp',
            domain: {
                orientation: 'right',
                values: [domain1[0], domain1[1]],
            },
        },
    ];


    return (
        <LineGraph lines={lines} data={data} days={days}/>
    );
};

export default MeasuresGraph;
