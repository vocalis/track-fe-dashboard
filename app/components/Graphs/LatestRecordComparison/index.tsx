import React from 'react';
import {GraphWrapper} from './styles';
import {
    ResponsiveContainer, Bar, XAxis,
    YAxis, Tooltip, CartesianGrid, BarChart,
} from 'recharts';
import styles from 'styles/values';
import GraphTooltip from '../GraphTooltip';
import { PatientAnalysis } from 'models/local';
import moment from 'moment';
import _ from 'lodash';
import LatestRecordComparisonTooltip from '../LatestRecordComparisonTooltip';
import './style.css';
interface LatestRecordComparisonProps {
    analysis?: PatientAnalysis[];
    days?: number;
    lastActivity: Date | null;
}

const geStatustValue = (status: string | null): number => {
    if (status === 'worse' || status === 'deterioration') {
        return 1;
    }
    if (status === 'same') {
        return 2;
    }
    if (status === 'better' || status === 'improvement') {
        return 3;
    }
    return 0;
};

const tickFormatter = (value) => {
    switch (value) {
        case 1:
            return 'Worse';
        case 2:
            return 'Same';
        case 3:
            return 'Better';
        default:
            return '';
    }
};

const LatestRecordComparison: React.FC<LatestRecordComparisonProps> = (props: LatestRecordComparisonProps) => {
    const {analysis, days, lastActivity} = props;

    const date = moment();
    const graphData: Array<{
        date: Date,
        day: string,
        value: number,
        refDate: Date | null,
        lastActivity: Date | null}> = [];
    for (let i = 0; i < (days || 10); i++) {
        const dayData = analysis ? _.find<PatientAnalysis | null>(analysis,
                item => (!!(item && item.sourceDate && moment(item.sourceDate).isSame(date, 'day')))) : null;
        graphData.push({
            date: date.toDate(),
            day: date.format('D'),
            lastActivity: lastActivity,
            refDate: dayData && dayData.referenceDate ? dayData.referenceDate : null,
            value: geStatustValue(dayData && dayData.sourceStatus ? dayData.sourceStatus : null),
        });
        date.subtract(1, 'day');
    }
    graphData.reverse();
    return (
        <GraphWrapper  style={{width: '100%', cursor: 'pointer', height: '100%' }}>
            <ResponsiveContainer>
                <BarChart className="barChart"  data={graphData}
                               margin={{ top: 20, right: 20, bottom: 0, left: 20 }}>
                    <Tooltip
                        wrapperStyle={{
                            zIndex: 10,
                            left: -174,
                            top: -145}}
                        allowEscapeViewBox={{x: true, y: true}}
                        cursor={{
                            fill: styles.colors.GRAPHS.YELLOW,
                        }}
                        content={<LatestRecordComparisonTooltip />} />
                    <XAxis
                        padding={{left: 20, right: 20}}
                        // minTickGap={10}
                        axisLine={false}
                        tickLine={false}
                        // tickMargin={20}
                        dataKey="day"/>
                    <YAxis
                        stroke={styles.colors.GRAPHS.BLACK}
                        domain={[0, 3]}
                        ticks={[1 , 2, 3]}
                        tickFormatter={tickFormatter}
                        // padding={{top: 15, bottom: 15}}
                        tickMargin={20}
                        tickLine={false}
                        axisLine={false}/>
                    <CartesianGrid
                        vertical={false}
                        opacity={0.5}
                        // horizontalPoints={[220, 160, 90, 25]}
                    />
                    <Bar
                        unit={'unit2'}
                        dataKey="value"
                        barSize={15}
                        fill={styles.colors.GRAPHS.GREEN}
                        radius={[10, 10, 0, 0]} />
                </BarChart>
            </ResponsiveContainer>
        </GraphWrapper>
    );
};

export default LatestRecordComparison;
