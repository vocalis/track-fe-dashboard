import React, {useEffect, useState} from 'react';
import styles from 'styles/values';
import {LineGraph} from '../index';
import {PatientVital} from '../../../models/local';
import {LineProp} from '../LineGraph';
import {getDomain} from '../../common/commonFunc';

interface WeightGraphProps {
    data?: PatientVital[];
    days?: number;
}


const WeightGraph: React.FC<WeightGraphProps> = (props: WeightGraphProps) => {
    const {data, days} = props;
    const domain = getDomain(30, 60, data, 'pulse');
    const lines: LineProp[] = [
        {
            field: 'pulse',
            color: styles.colors.GRAPHS.GRAY,
            unit: 'Pulse',
            domain: {
                orientation: 'left',
                values: [domain[0], domain[1]],
            },
            label : 'Pulse',
        },
    ];

    return (
        <LineGraph lines={lines} data={data} days={days} enableBrush={true}/>
    );
};

export default WeightGraph;
