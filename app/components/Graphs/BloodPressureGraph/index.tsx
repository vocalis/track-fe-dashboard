import React from 'react';
import {FormattedMessage} from 'react-intl';
import { ResponsiveContainer, ComposedChart, Line, Bar, Area, XAxis,
    YAxis, ReferenceLine, ReferenceDot, Tooltip, Legend, CartesianGrid, Brush,
    LineChart } from 'recharts';
import styles from 'styles/values';
import GraphTooltip from '../GraphTooltip';
import {GraphWrapper} from '../styles';
import {PatientVital} from '../../../models/local';
import moment from 'moment';
import _ from 'lodash';

interface BloodPressureGraphProps {
    data?: PatientVital[];
    days?: number;
}

const BloodPressureGraph: React.FC<BloodPressureGraphProps> = (props: BloodPressureGraphProps) => {
    const {data, days} = props;
    const date = moment();
    const graphData: Array<{
        date: string,
        bloodPressure?: [number, number] | null,
    }> = [];

    for (let i = 0; i < (days || 10); i++) {
        const bloodPressureHigh = _.find(data,
            item => item.vital === 'bloodPressureHigh' && moment(item.date).isSame(date, 'day'));
        const bloodPressureLow = _.find(data,
            item => item.vital === 'bloodPressureLow' && moment(item.date).isSame(date, 'day'));
        graphData.push(
            {
                date: date.format('D'),
                bloodPressure : bloodPressureLow && bloodPressureLow.value &&
                    bloodPressureHigh && bloodPressureHigh.value &&
                    [bloodPressureLow.value, bloodPressureHigh.value] || null,
            },
        );
        date.subtract(1, 'day');
    }

    graphData.reverse();


    return (
        <GraphWrapper style={{ width: '100%', height: '100%' }}>
            <ResponsiveContainer>
                <ComposedChart data={graphData}
                               margin={{ bottom: 0, top: 20, right: 20, left: 20 }}>
                    <Tooltip allowEscapeViewBox={{x: true, y: true}} content={<GraphTooltip/>}/>
                    <XAxis
                        allowDataOverflow={true}
                        padding={{left: 20, right: 20}}
                        // minTickGap={10}
                        axisLine={false}
                        tickLine={false}
                        tickMargin={0}
                        dataKey="date"/>
                    <YAxis
                        stroke={styles.colors.GRAPHS.BLACK}
                        // allowDataOverflow={true}
                        domain={[60, 150]}
                        tickCount={4}
                        padding={{top: 0, bottom: 0}}
                        tickMargin={20}
                        tickLine={false}
                        axisLine={false}/>
                    <CartesianGrid vertical={false}  opacity={0.5}/>
                    <Bar dataKey="bloodPressure" barSize={12} fill={styles.colors.GRAPHS.GRAY} radius={12} />
                    <Brush
                        fill={styles.colors.SCROLLER.TRACK}
                        stroke={styles.colors.SCROLLER.HANDLE}
                        startIndex={graphData.length - 11}
                        endIndex={graphData.length - 1}
                        height={5}
                        padding={{top: 20}}
                    />
                </ComposedChart>
            </ResponsiveContainer>
        </GraphWrapper>
    );
};

export default BloodPressureGraph;
