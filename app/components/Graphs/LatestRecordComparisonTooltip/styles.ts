import styled from 'styled-components';
import styles from 'styles/values';

const LatestRecordComparisonTooltipWrapper = styled.div`
    width: 330px;
    height: 115px;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: flex-start;
    border-radius: 5px;
    padding: 16.5px 18.5px;
    border: 1px solid #EAEAEA;
    background-color: ${styles.colors.SHADE.WHITE.WHITE};
    &::before {
        position: absolute;
        left: calc(50% - 11px);
        bottom: -10px;
        content: '';
        width: 0;
        height: 0;
        border-top: 11px solid ${styles.colors.SHADE.WHITE.WHITE};
        border-right: 10px solid transparent;
        border-left: 10px solid transparent;
  }
`;

export {LatestRecordComparisonTooltipWrapper};
