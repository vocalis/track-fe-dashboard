import React from 'react';

import './styles.ts';
import { LatestRecordComparisonTooltipWrapper} from './styles';
import LatestRecordComparisonTooltipContent from '../LatestRecordComparisonTooltipContent';
import moment from 'moment';

interface Props {
    payload?: {
        payload: {
            date: Date;
            refDate: Date | null;
            value: number;
            lastActivity: Date | null;
        },
    };
    active?: boolean;
    label?: string;
}

const LatestRecordComparisonTooltip = (props: Props) => {
    const {payload, active} = props;
    if (active &&
        payload  &&
        payload[0] &&
        payload[0].payload &&
        payload[0].payload.value &&
        payload[0].payload.lastActivity) {
        const lastActivity = moment(payload[0].payload.lastActivity).format('YYYY-MM-DD');
        const date = moment(payload[0].payload.date).format('YYYY-MM-DD');
        if (date !== lastActivity) {
            return(
                <LatestRecordComparisonTooltipWrapper>
                    <LatestRecordComparisonTooltipContent
                        date={payload[0].payload.date}
                        refDate={payload[0].payload.lastActivity}
                        status={payload[0].payload.value}/>
                </LatestRecordComparisonTooltipWrapper>
        );
        }
    }
    return null;
};

export default LatestRecordComparisonTooltip;
