import styled from 'styled-components';
import styles from 'styles/values';

const GraphWrapper = styled.div`
  width: 100%;
  height: 26%;
  display: flex;
  flex-direction: row;
  font-size: 14px;

`;
const GraphBody = styled.div`
  height: 100%;
  flex: 1;
  display: flex;
  flex-direction: row;
  position: relative;
`;
const GraphLine = styled.div<{index: number}>`
  width: 100%;
  height: 1px;
  background-color: #EAEAEA;
  position: relative;
  left: 0;
  bottom: calc(-90.7% + ${props => props.index * 18.507}%);
`;
const Bar = styled.div<{value: 'better' | 'same' | 'worse' | 'deterioration' | 'improvement' | null}>`
  width: 12px;
  height: ${props => {
      switch (props.value) {
          case 'worse':
          case 'deterioration':
              return '21.4%';
          case 'same':
              return '39.8%;%';
          case 'better':
          case 'improvement':
          default:
              return '58.4%';
      }
  }};
  background-color: ${props => props.value ? styles.colors.GRAPHS.GREEN : styles.colors.GRAPHS.DISABLED};
  border-top-left-radius: 6px;
  border-top-right-radius: 6px;
`;

const GraphLines = styled.div`
  z-index: 3;
  width: 100%;
  height: 100%;
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
`;
const TooltipWrapper = styled.div`
    background: #FFFFFF 0% 0% no-repeat padding-box;
border: 1px solid #EAEAEA;
opacity: 1;
width:510px;
height:560px;
padding-left:37.5px;
padding-right:32.5px;
padding-top:22.5px;

`;
const TitleWrapper = styled.div<{marginBottom: string , size: string}>`
text-align: left;
font-weight: bold;
    font-size: ${props => props.size};
letter-spacing: 0;
color: #353535;
opacity: 1;
margin-bottom:${props => props.marginBottom};
`;
const ContentWrapper = styled.div`
text-align: left;
font-size:  12px;
color: #353535;
opacity: 1;
max-width:587px;
`;
const Separator = styled.div`
width:100%;
border: 1px solid #EAEAEA;
opacity: 1;
    margin-top: 26px;
    margin-bottom: 29px;
`;
const BarWrapper = styled.div`
display:flex;
position: absolute;
    bottom: 14px;
    height: 100%;
    align-items: flex-end;
    width: 306px;
    justify-content: space-between;
    z-index: 10;
`;
const StatusWrapper = styled.div`
width: 156px;
height: 143px;
text-align: left;
font-size:12px;
letter-spacing: 0;
color: #353535;
opacity: 1;
margin-top: 4px;
`;
const SummaryWrapper = styled.div`
display: flex;
    align-content: space-between;
`;
const Span = styled.div<{marginTop?: string}>`
width: 125px;
margin-top:${props => props.marginTop};
`;

export {
    GraphWrapper,
    ContentWrapper,
    TooltipWrapper,
    TitleWrapper,
    Separator,
    GraphBody,
    GraphLines,
    GraphLine,
    BarWrapper,
    StatusWrapper,
    SummaryWrapper,
    Bar,
    Span,
};
