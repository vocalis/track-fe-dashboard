import React from 'react';

import {
 TooltipWrapper,
    TitleWrapper,
    ContentWrapper,
    Separator,
    GraphWrapper,
    GraphLines,
    GraphBody,
    BarWrapper,
    GraphLine,
    StatusWrapper,
    SummaryWrapper,
    Bar,
    Span,
} from './styles';
import {FormattedMessage} from 'react-intl';
const ShortnessOfBreathLevelTooltip = () => {
    return (
        <TooltipWrapper>
            <TitleWrapper size="18px" marginBottom="18px">
                <FormattedMessage id={'SHORTNESS_OF_BREATH_LEVEL_TITLE'}/>
            </TitleWrapper>
            <ContentWrapper>
                <FormattedMessage id={'SHORTNESS_OF_BREATH_LEVEL_TEXT1'}/>
            </ContentWrapper>
                <Separator/>
            <ContentWrapper>
                <FormattedMessage id={'SHORTNESS_OF_BREATH_LEVEL_TEXT2'}/>
            </ContentWrapper>
            <GraphWrapper>
            <GraphBody>
                <GraphLines>
                    <GraphLine index={0} />
                    <GraphLine index={1} />
                    <GraphLine index={2} />
                    <GraphLine index={3} />
                </GraphLines>
                <BarWrapper>
                    <Bar value={'better'}/>
                    <Bar value={'same'}/>
                    <Bar value={'worse'}/>
                </BarWrapper>
            </GraphBody>
            </GraphWrapper>
            <SummaryWrapper>
                <StatusWrapper>
                    <TitleWrapper size="15px" marginBottom="12px">
                        <FormattedMessage id={'BETTER'}/>
                    </TitleWrapper>
                    <Span>
                    <FormattedMessage id={'SHORTNESS_OF_BREATH_LEVEL_TEXT3'}/>
                    </Span>
                </StatusWrapper>
                <StatusWrapper>
                    <TitleWrapper size="15px"  marginBottom="12px">
                        <FormattedMessage id={'SAME'}/>
                    </TitleWrapper>
                    <Span>
                    <FormattedMessage id={'SHORTNESS_OF_BREATH_LEVEL_TEXT4'}/>
                    </Span>
                    <Span marginTop="12px">
                        <FormattedMessage id={'SHORTNESS_OF_BREATH_LEVEL_TEXT4_CONT'}/>
                    </Span>
                </StatusWrapper>

                <StatusWrapper>
                    <TitleWrapper size="15px"  marginBottom="12px">
                        <FormattedMessage id={'WORSE'}/>
                    </TitleWrapper>
                    <Span>
                    <FormattedMessage id={'SHORTNESS_OF_BREATH_LEVEL_TEXT5'}/>


                    </Span>
                </StatusWrapper>
            </SummaryWrapper>
        </TooltipWrapper>

    );
};
export default ShortnessOfBreathLevelTooltip;
