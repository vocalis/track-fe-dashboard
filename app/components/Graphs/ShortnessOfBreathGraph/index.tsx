import React, {useCallback, useEffect, useRef, useState} from 'react';
import {FormattedMessage} from 'react-intl';
import Tooltip from '@tippy.js/react';
import _ from 'lodash';
import moment, {Moment} from 'moment';
import {
    BarRowLabel,
    BarsRow,
    DateRow,
    GraphAxis,
    GraphBody,
    GraphWrapper,
    QARow,
    TrendRow,
    AudioRow,
    GraphPrimaryTitle,
    GraphSecondaryTitle,
    HelpCircle,
    GraphOverlay,
    GraphLines,
    GraphLine,
    GraphItems,
    GraphRowItem,
    TrendIcon,
    DocIcon,
    Bar,
    PlayIcon,
    StartOfMonth,
    ActivePlayIcon,
    GraphLastItem,
    DayRow,
    TooltipWrapper,

} from './styles';
import 'tippy.js/dist/tippy.css';
import 'tippy.js/themes/light-border.css';
import {
    PatientAnalysis,
    PatientAnalysisAnswer,
    QuestionnaireInterface,
} from 'models/local';
import LatestRecordComparisonTooltipContent from '../LatestRecordComparisonTooltipContent';
import ComparisonTooltipContent from '../ComparisonTooltipContent';
import ShortnessOfBreathLevelTooltip from '../ShortnessOfBreathLevelTooltip';
import DayToDayComparisonTooltip from '../DayToDayComparisonTooltip';
import {
    getDocsIconByAnswers,
    getTrendsIconByStatus,
    getValue,
} from '../../common/commonFunc';

const DAYS_COUNT = 60;
const helpIcon = require('images/ic_help_circle.svg');
const playEnabledIcon = require('images/ic_play_enabled.svg');
const playDisabledIcon = require('images/ic_play_disabled.svg');

interface Props {
    analysis: PatientAnalysis[];
    days?: number;
    playSample: (recordingId: number) => void;
    updateDiary: (diary: PatientAnalysisAnswer, date: Moment) => void;
    questionnaireData: QuestionnaireInterface | null;
    lastActivity: Date | null;
}

interface PatientDiary {
    question: string;
    answer: string;
}

const getStartOfMonth = date => {
    if (date.date() === 1) {
        return <StartOfMonth>{date.format('MMM YYYY')}</StartOfMonth>;
    }
    return '';
};

const ShortnessOfBreathGraph = (props: Props) => {
    const {analysis, days, playSample, updateDiary, questionnaireData, lastActivity} = props;
    const [showChosenDate, setShowChosenDate] = useState<Moment | null>(null);
    const [graphItemsdata, setGraphItemsdata] = useState< JSX.Element[]>([]);
    const [months, setMonths] = useState('');
    const [graphWidth, setGraphWidth] = useState<number>(0);
    const scrollRef = useRef<HTMLDivElement>(null);
    const firstScrollerPossition = useRef<number>(0);
    const containerRef = useRef<HTMLDivElement>(null);
    const temp = useRef<string>('');
    let lastDate = moment();
    let firstDate = moment();
    let subDays;
    let itemWidth;
    useEffect(() => {
        const clientRect = containerRef && containerRef.current &&
            containerRef.current.getBoundingClientRect &&
            containerRef.current.getBoundingClientRect() || null;
        if (clientRect) {
            setGraphWidth(clientRect.width - 100);
        }
    });

    // @ts-ignore
    useEffect(() => {
        if (scrollRef && scrollRef.current && scrollRef.current.lastElementChild) {
            const ItemsWidth = scrollRef.current.clientWidth + 14;
            itemWidth = scrollRef.current.lastElementChild.clientWidth;
            subDays = ItemsWidth / (itemWidth || 1);
            lastDate = moment().subtract(1, 'day');
            firstDate = moment().subtract(subDays, 'day');
            if ((days && days < subDays) || Number(lastDate.format('MM')) - Number(firstDate.format('MM')) ===
            0) {
                setMonths(`${lastDate.format('MMM')}`);
            } else {
                if (Number(lastDate.format('MM')) - Number(firstDate.format('MM')) ===
                    1) {
                    setMonths(`${firstDate.format('MMM')}/${lastDate.format('MMM')}`);
                } else {
                    setMonths(`${firstDate.format('MMM')}/${firstDate
                        .add(1, 'month')
                        .format('MMM')}/${lastDate.format('MMM')}`);
                }
            }
            scrollRef.current.addEventListener('scroll', scrollerListener);
        }
        return () => scrollRef &&
            scrollRef.current &&
            scrollRef.current.removeEventListener('scroll', scrollerListener);
    }, []);

    const scrollerListener = event => {
        if (firstScrollerPossition.current === 0) {
            firstScrollerPossition.current = event.currentTarget.scrollLeft + 14;
        }
        let items =
            (firstScrollerPossition.current - event.currentTarget.scrollLeft) /
            itemWidth;
        items = items > 1 ? Math.ceil(items) : 0;
        if (items > 0) {
            lastDate = moment().subtract(items, 'day');
            firstDate = moment(lastDate).subtract(subDays - 1, 'day');
            if (
                Number(lastDate.format('MM')) - Number(firstDate.format('MM')) ===
                0
            ) {
                if (temp.current !== `${firstDate.format('MMM')}`) {
                    temp.current = `${firstDate.format('MMM')}`;
                    setMonths(temp.current);
                }
            } else if (
                Number(lastDate.format('MM')) - Number(firstDate.format('MM')) ===
                1
            ) {
                if (temp.current !== `${firstDate.format('MMM')}/${lastDate.format('MMM')}`) {
                    temp.current = `${firstDate.format('MMM')}/${lastDate.format('MMM')}`;
                    setMonths(temp.current);
                }
            } else {
                const firstMonthCopy = moment(JSON.parse(JSON.stringify(firstDate)));
                const nextMonth = firstMonthCopy.add(1, 'month');
                if (temp.current !== `${firstDate.format('MMM')}/${nextMonth
                    .format('MMM')}/${lastDate.format('MMM')}`) {
                    temp.current = `${firstDate.format('MMM')}/${nextMonth
                        .format('MMM')}/${lastDate.format('MMM')}`;
                    setMonths(temp.current);
                }
            }
        }
    };

    useEffect(() => {
        if (scrollRef && scrollRef.current && scrollRef.current.scrollTo) {
            scrollRef.current.scrollTo(1000000, 0);
        }
    }, []);

    const getPlayIcon = (sourceRecordingId: number | null) =>
        sourceRecordingId ? (
            <ActivePlayIcon
                onClick={() => playSample(sourceRecordingId)}
                src={playEnabledIcon}
            />
        ) : (
            <PlayIcon src={playDisabledIcon}/>
        );

    const displayDate = (analysisItem, date) => {
        updateDiary(
            (analysisItem && analysisItem.answers) || {daily: [], expanded: []},
            date,
        );
        setShowChosenDate(date);
    };
    const data: JSX.Element[] = [];
    const date = moment();
    let isLastRecord = false;
    let minDays = 0;
    if ((graphWidth / 40) > (days || DAYS_COUNT)) {
        minDays = Math.round(graphWidth / 40);
    }
    const getBarRow = (date, analysisItem, value, isToday) => {
        let backgroundColor = '';
        if (isToday) {
            if (analysisItem && value) {
                backgroundColor =  'rgba(207, 238, 243, 0.77)';
                isLastRecord = true;
            } else {
                backgroundColor = 'rgba(246, 255, 204, 0.52)';
            }
        } else {
            if (!isLastRecord && analysisItem && value) {
                backgroundColor = !isLastRecord && analysisItem && value ?
                    'rgba(207, 238, 243, 0.77)' :
                    '';
                isLastRecord = true;
            }

        }
        return(
            <BarsRow backgroundColor={backgroundColor}>
                {getStartOfMonth(date)}
                {analysisItem && value ? (
                    <Tooltip
                        content={
                            <TooltipWrapper>
                                <LatestRecordComparisonTooltipContent
                                    date={analysisItem.sourceDate}
                                    refDate={lastActivity}
                                    status={value}
                                />
                            </TooltipWrapper>
                        }
                        enabled={
                            lastActivity !== null &&
                            moment(analysisItem.sourceDate).format('DD/MM/YYYY') !==
                            moment(lastActivity).format('DD/MM/YYYY')
                        }
                        arrow
                        placement="top"
                        theme={'light-border'}
                    >
                        <Bar value={analysisItem.sourceStatus}/>
                    </Tooltip>
                ) : (
                    <Bar value={null}/>
                )}
            </BarsRow>
        );
    };
    // @ts-ignore
    const itemDays = (minDays || Math.max(days, DAYS_COUNT));
    // date.subtract(itemDays - 1, 'day');

    useEffect(() => {
        for (let i = 0; i < itemDays; i++) {
            // console.log(lastActivity);
            // debugger;
            // const lastComparisonDate = analysis[0].sourceDate
            // console.log()
            const analysisItem = _.find<PatientAnalysis>(analysis, item => {
                    return  (item.sourceDate ? moment(item.sourceDate).isSame(date, 'day') : false);
                },

            );
            const trendIcon = getTrendsIconByStatus(
                analysisItem ? analysisItem.status : null,
            );
            const docsIcon = getDocsIconByAnswers(
                analysisItem ? analysisItem.answers : null,
            );
            const playIcon = getPlayIcon(
                analysisItem ? analysisItem.sourceRecordingId : null,
            );
            const value = getValue(analysisItem ? analysisItem.sourceStatus : null);
            const getTrendQAStatus = getValue(
                analysisItem ? analysisItem.status : null,
            );
            const answers = analysisItem && analysisItem.answers;
            const sourceDate = date.clone();
            const x = showChosenDate  && showChosenDate.isSame(date, 'day');
            const item = (
                <GraphRowItem
                    onClick={() => displayDate(analysisItem, sourceDate)}
                    key={`shortness_column_${i}`}
                    isDateChosen={
                        (showChosenDate && showChosenDate.isSame(date, 'day')) || false
                    }
                >
                    <DayRow today={i === itemDays - 1}>Today</DayRow>
                    <TrendRow  >
                        {trendIcon && analysisItem ? (
                            <Tooltip
                                maxWidth={600}
                                content={
                                    <ComparisonTooltipContent
                                        questionnaireData={questionnaireData}
                                        answers={answers}
                                        status={getTrendQAStatus}
                                        sourceDate={analysisItem.sourceDate}
                                        referenceDate={analysisItem.referenceDate}
                                    />
                                }
                                arrow
                                placement="left-end"
                                theme={'light-border'}
                            >
                                <TrendIcon src={trendIcon}/>
                            </Tooltip>
                        ) : (
                            ''
                        )}
                    </TrendRow>
                    <QARow>
                        {docsIcon && analysisItem ? (
                            <Tooltip
                                maxWidth={600}
                                content={
                                    <ComparisonTooltipContent
                                        questionnaireData={questionnaireData}
                                        answers={answers}
                                        status={getTrendQAStatus}
                                        sourceDate={analysisItem.sourceDate}
                                        referenceDate={analysisItem.referenceDate}
                                    />
                                }
                                placement="left-end"
                                arrow
                                theme={'light-border'}
                            >
                                <DocIcon src={docsIcon}/>
                            </Tooltip>
                        ) : (
                            <DocIcon src={docsIcon}/>
                        )}
                    </QARow>
                    {getBarRow(date, analysisItem, value, 0 === i)}
                    <DateRow>{date.format('D')}</DateRow>
                    <AudioRow>{playIcon}</AudioRow>
                </GraphRowItem>
            );
            // if (i < itemDays - 1) {
            //     data.push(item);
            // } else {
            //     todayItem = item;
            // }
            data.push(item);
            date.subtract(1, 'day');
        }
        setGraphItemsdata([...data]);
    }, [showChosenDate]);


    const getGraphAxisCallback = useCallback(() => {
        return getGraphAxis();
    }, [months]);
    const getGraphBodyCallback = useCallback(() => {
        return getGrapBody();
    }, [analysis, days, playSample, updateDiary, questionnaireData, graphItemsdata]);


    const getGrapBody = () => {
        return (
            <GraphBody>
            <GraphLines>
                <GraphLine index={0}/>
                <GraphLine index={1}/>
                <GraphLine index={2}/>
                <GraphLine index={3}/>
            </GraphLines>
            <GraphPrimaryTitle>
                <FormattedMessage id={'DAY_TO_DAY_COMPARISON'}/>
                <Tooltip
                    maxWidth={600}
                    content={<DayToDayComparisonTooltip/>}
                    arrow
                    placement="auto-start"
                    theme={'light-border'}
                >
                    <HelpCircle src={helpIcon}/>
                </Tooltip>
            </GraphPrimaryTitle>
            <GraphSecondaryTitle>
                <FormattedMessage id={'LATEST_RECORD_COMPARISON'}/>
                <Tooltip
                    maxWidth={600}
                    content={<ShortnessOfBreathLevelTooltip/>}
                    arrow
                    placement="auto-start"
                    theme={'light-border'}
                >
                    <HelpCircle src={helpIcon}/>
                </Tooltip>
            </GraphSecondaryTitle>
            <GraphItems
                className="GraphItems"
                ref={scrollRef}>
                {graphItemsdata}
            </GraphItems>
            {/*<GraphLastItem>{todayItem}</GraphLastItem>*/}
        </GraphBody>
        );
    };

    const getGraphAxis = () => {
        return (
            <GraphAxis>
            <TrendRow alignLeft={true}>
                <FormattedMessage id={'LABEL_TREND'}/>
            </TrendRow>
            <QARow alignLeft={true}>
                <FormattedMessage id={'LABEL_QA'}/>
            </QARow>
            <BarsRow alignLeft={true} height="49.3%" paddingBottom={'21px'}>
                <BarRowLabel>
                    <FormattedMessage id={'LABEL_BETTER'}/>
                </BarRowLabel>
                <BarRowLabel>
                    <FormattedMessage id={'LABEL_SAME'}/>
                </BarRowLabel>
                <BarRowLabel>
                    <FormattedMessage id={'LABEL_WORSE'}/>
                </BarRowLabel>
            </BarsRow>
            <DateRow height="11.9%" alignLeft={true}>
                {months}
            </DateRow>
            <AudioRow alignLeft={true}>
                <FormattedMessage id={'LABEL_AUDIO'}/>
            </AudioRow>
        </GraphAxis>
        );
    };
    return (
        <GraphWrapper ref={containerRef}>
            {getGraphAxisCallback()}
            {getGraphBodyCallback()}
        </GraphWrapper>
    );
};

export default ShortnessOfBreathGraph;
