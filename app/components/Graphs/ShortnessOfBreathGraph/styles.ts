import styled from 'styled-components';
import styles from 'styles/values';

const GraphWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: row;
  font-size: 14px;
`;

const GraphAxis = styled.div`
  height: 100%;
  border-right: 1px solid #eaeaea;
  padding-left: 7px;
  padding-right: 7px;
  text-align: left;
  letter-spacing: 0;
  color: #353535;
  width: 100px;
`;

const GraphBody = styled.div`
  height: 100%;
  flex: 1;
  display: flex;
  flex-direction: row;
  position: relative;
`;

const TrendRow = styled.div<{ alignLeft?: boolean }>`
  height: 16%;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  align-items: ${props => (props.alignLeft ? 'flex-start' : 'center')};
  line-height: 12px;
`;

const TrendIcon = styled.img`
  height: 12px;
`;

const QARow = styled.div<{ alignLeft?: boolean }>`
  height: 9.6%;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  align-items: ${props => (props.alignLeft ? 'flex-start' : 'center')};
  line-height: 15px;
`;

const DocIcon = styled.img`
  height: 15px;
`;

const ActivePlayIcon = styled.img`
  height: 13px;
  cursor: pointer;
  margin-top: 2px;
`;

const PlayIcon = styled.img`
  display: none;
  height: 13px;
  margin-top: 2px;
`;

const BarsRow = styled.div<{ alignLeft?: boolean; height?: string; paddingBottom?: string, backgroundColor?: string }>`
  height: ${props => (props.height ? props.height : '53.1%')};
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  align-items: ${props => (props.alignLeft ? 'flex-start' : 'center')};
  padding-bottom: ${props => (props.paddingBottom ? props.paddingBottom : '0px')}; ;
  background-color: ${props => props.backgroundColor ? props.backgroundColor : ''};
  cursor: pointer;
`;

const Bar = styled.div<{
  value: 'better' | 'same' | 'worse' | 'deterioration' | 'improvement' | null;
}>`
  width: 12px;
  height: ${props => {
    switch (props.value) {
      case 'worse':
      case 'deterioration':
        return '21.4%';
      case 'same':
        return '42.8%';
      case 'better':
      case 'improvement':
      default:
        return '64.2%';
    }
  }};
  background-color: ${props =>
    props.value ? styles.colors.GRAPHS.GREEN : styles.colors.GRAPHS.DISABLED};
  border-top-left-radius: 6px;
  border-top-right-radius: 6px;
`;

const BarRowLabel = styled.div`
  height: 21.4%;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
`;

const DateRow = styled.div<{ alignLeft?: boolean; height?: string }>`
  height: ${props => (props.height ? props.height : '12.5%')};
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: ${props => (props.alignLeft ? 'flex-start' : 'center')};
`;

const AudioRow = styled.div<{ alignLeft?: boolean }>`
  height: 9.8%;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: ${props => (props.alignLeft ? 'flex-start' : 'center')};
`;

const GraphPrimaryTitle = styled.div`
  position: absolute;
  z-index: 12;
  top: 3%;
  width: calc(100% - 13px);
  left: 13px;
  text-align: left;
  font: Bold 14px/17px Roboto;
  letter-spacing: 0;
  color: #353535;
`;

const GraphSecondaryTitle = styled.div`
  z-index: 12;
  position: relative;
  height: 20px;
  top: 30%;
  left: 13px;
  text-align: left;
  font: Bold 14px/17px Roboto;
  letter-spacing: 0;
  color: #353535;
      height: 20px;
    width: fit-content;
 background-color: transparent;
`;

const GraphOverlay = styled.div`
  z-index: 10;
  width: 100%;
  height: 100%;
  position: absolute;
  left: 0;
  top: 0;
`;

const GraphItems = styled.div`
  z-index: 11;
  width: calc(100% - 40px);
  height: 100%;
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  display: flex;
  flex-direction: row-reverse;
  overflow-y: auto;

  &::-webkit-scrollbar {
    height: 7px;
  }

  /* Track */
  &::-webkit-scrollbar-track {
    background: #eaeaea;
  }

  /* Handle */
  &::-webkit-scrollbar-thumb {
    background: #38abb6;
  }

  /* Handle on hover */
  &::-webkit-scrollbar-thumb:hover {
    background: #555;
  }
`;

const GraphLastItem = styled.div`
  z-index: 11;
  width: 40px;
  height: calc(100% - 7px);
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  display: flex;
  flex-direction: row;
  overflow-y: auto;
  position: absolute;
  left: calc(100% - 40px);
  box-shadow: 0px 3px 6px #00000029;
`;

const GraphLines = styled.div`
  z-index: 3;
  width: 100%;
  height: 100%;
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
`;

const GraphLine = styled.div<{ index: number }>`
  width: 100%;
  height: 1px;
  background-color: #eaeaea;
  position: relative;
  left: 0;
  bottom: calc(-74.7% + ${props => props.index * 10.507}%);
`;

const GraphRowItem = styled.div<{ isDateChosen: boolean }>`
  height: calc(100% - 7px);
  width: 40px;
  min-width: 40px;
  padding-left: 7px;
  padding-right: 7px;
  background-color: ${props =>
    props.isDateChosen && styles.colors.GRAPHS.YELLOW};
`;

const HelpCircle = styled.img`
  z-index: 20;
  padding-left: 7px;
`;

const StartOfMonth = styled.div`
  position: absolute;
  top: 92px;
  width: fit-content;
  padding-left: 33px;
  font-size: 10.5px;
`;

const DayRow = styled.div<{ today: boolean }>`
  display: ${props => (props.today ? 'block' : 'none')};
  font-size: 10.5px;
  color: ${styles.colors.TEXT.GREEN};
  position: absolute;
  top: 13px;
  right: 5px;
`;

const TooltipWrapper = styled.div`
  padding: 10px;
`;

export {
  GraphWrapper,
  GraphAxis,
  GraphBody,
  TrendRow,
  QARow,
  BarsRow,
  BarRowLabel,
  DateRow,
  AudioRow,
  GraphOverlay,
  GraphPrimaryTitle,
  GraphSecondaryTitle,
  HelpCircle,
  GraphLines,
  GraphLine,
  GraphItems,
  GraphRowItem,
  TrendIcon,
  DocIcon,
  PlayIcon,
  Bar,
  StartOfMonth,
  ActivePlayIcon,
  GraphLastItem,
  DayRow,
  TooltipWrapper,
};
