import styled from 'styled-components';
import styles from 'styles/values';


const DateTitle = styled.div`
    color: ${styles.colors.SHADE.BLACK.BLACK};
    font-size: 15px;
    font-weight: bold;
`;

const PopupContent = styled.div`
    color: ${styles.colors.SHADE.BLACK.BLACK};
    font-size: 12px;
`;

export { DateTitle, PopupContent };
