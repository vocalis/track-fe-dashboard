import {DateTitle, PopupContent} from './styles';
import moment from 'moment';
import {FormattedMessage, injectIntl} from 'react-intl';
import React from 'react';
import styles from 'styles/values';

interface Props {
    date: Date;
    refDate: Date;
    status: number;
    intl: any;
}

const valueToString = (value: number) => {
    switch (value) {
        case 1:
            return 'STATUS_WORSE';
        case 2:
            return 'STATUS_NO_CHANGE';
        case 3:
            return 'STATUS_BETTER';
        default:
            return '';
    }
};

const valueToColor = (value: number) => {
    switch (value) {
        case 1:
            return styles.colors.COMPARISON.WORSE;
        case 2:
            return styles.colors.COMPARISON.SAME;
        case 3:
            return styles.colors.COMPARISON.BETTER;
        default:
            console.log('color', '');
            return '';
    }
};

const LatestRecordComparisonTooltipContent = (props: Props) => {
    const {date, refDate, status, intl} = props;
    const dateVal = moment(date).format('MMM DD, YYYY');
    const statusVal = intl.formatMessage({id: valueToString(status)});
    const refDateVal = moment(refDate).format('MMM DD, YYYY');
    return (
        <>
            <DateTitle>{dateVal}</DateTitle>
            <PopupContent>
                <FormattedMessage
                    id={status === 2 ?
                        'LATEST_RECORD_COMPARISON_TOOLTIP_INFO_SAME'
                        :
                        'LATEST_RECORD_COMPARISON_TOOLTIP_INFO'}
                    values={
                        {
                            date: dateVal,
                            status: <b style={{color: valueToColor(status)}}>{statusVal}</b>,
                            refDate: refDateVal,
                        }
                    }
                />
            </PopupContent>
         </>
    );
};

export default injectIntl(LatestRecordComparisonTooltipContent);
