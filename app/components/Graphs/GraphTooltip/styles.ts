import styled from 'styled-components';
import styles from 'styles/values';
import { GeneralStyledProps } from '../../../styles/styled-components';

const TooltipWrapper = styled.div`
  width: auto;
  height: auto;
  padding: 18px;
  background: ${styles.colors.GRAPHS.BLACK} 0% 0% no-repeat padding-box;
  box-shadow: 0px 3px 6px #00000029;
  border-radius: 5px;
  &::before {
    position: absolute;
    left: -9px;
    top: calc(50% - 10px);
    content: "";
    width: 0;
    height: 0;
    border-top: 10px solid transparent;
    border-bottom: 10px solid transparent;
    border-right: 10px solid ${styles.colors.GRAPHS.BLACK};
  }
`;

const TooltipField = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const TooltipFieldColor = styled.div<GeneralStyledProps>`
  height: 7.5px;
  width: 7.5px;
  background-color: ${props => props.color || '#A2DFFD'};
  border-radius: 50%;
  display: inline-block;
`;

const TooltipFieldName = styled.div`
  display: flex;
  align-items: center;
  font-size: 15px;
  letter-spacing: 0;
  color: ${styles.colors.TEXT.GRAY};
  opacity: 1;
  padding-left: 9px;
`;

const TooltipFieldNameText = styled.div`
  margin-left: 9px;
`;

const TooltipFieldValue = styled.div`
  font-size: 15px;
  font-weight: bold;
  letter-spacing: 0;
  padding-left: 4px;
  color: ${styles.colors.TEXT.WHITE};
  opacity: 1;
`;
export {
  TooltipWrapper,
  TooltipField,
  TooltipFieldName,
  TooltipFieldNameText,
  TooltipFieldValue,
  TooltipFieldColor,
};
