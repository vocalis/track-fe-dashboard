import React from 'react';
import {FormattedMessage} from 'react-intl';
import {
    TooltipField,
    TooltipFieldColor,
    TooltipFieldName,
    TooltipFieldNameText,
    TooltipFieldValue,
    TooltipWrapper,
} from './styles';


interface GraphTooltipProps {
    payload?: any;
    active?: boolean;
    label?: string;
    coordinate?: {
        x: number;
    };
    viewBox?: {
        width: number;
        left: number;
        right: number;
        x: number;
    };
}

interface TooltipPayload {
    fill: string;
    dataKey: string;
    name: string;
    color: string;
    value: number;
    payload: any;
    unit: string;
}

const GraphTooltip: React.FC<GraphTooltipProps> = (props: GraphTooltipProps) => {
    const {active, payload, label} = props;
    const getUnit = (name) => {
        switch (name) {
            case 'temperature':
                return <span>&#176;</span>;
                break;
            case 'o2':
                return <span>%</span>;
                break;
            case 'pulse':
                return <span> bpm</span>;
                break;
            case 'bloodPressure':
                return <span> mmHg</span>;
                break;
            default:
            return <span>''</span>;
        }
    };
    const getTooltipValues = () => {
        return payload.map((field: TooltipPayload, index: number) => {
            if (Array.isArray(field.value)) {

                return (
                    <div>
                        <TooltipField key={`tooltip_${index}_High`}>
                            <TooltipFieldName>
                                {/*<TooltipFieldColor color={field.color}/>*/}
                                <TooltipFieldNameText>Systolic</TooltipFieldNameText>
                            </TooltipFieldName>
                            <TooltipFieldValue>{field.value[1]}{getUnit(field.name)}</TooltipFieldValue>
                        </TooltipField>
                        <TooltipField key={`tooltip_${index}_Low`}>
                            <TooltipFieldName>
                                {/*<TooltipFieldColor color={field.color}/>*/}
                                <TooltipFieldNameText>Diastolic</TooltipFieldNameText>
                            </TooltipFieldName>
                            <TooltipFieldValue>{field.value[0]}{getUnit(field.name)}</TooltipFieldValue>
                        </TooltipField>
                    </div>
                );
            }
            return (
                <TooltipField key={`tooltip_${index}`}>
                    <TooltipFieldName>
                        <TooltipFieldColor color={field.color}/>
                        <TooltipFieldNameText>{field.unit}</TooltipFieldNameText>
                    </TooltipFieldName>
                    <TooltipFieldValue>{field.value}{getUnit(field.name)}</TooltipFieldValue>
                </TooltipField>
            );
        });
    };

    if (active) {
        return (
            <TooltipWrapper>
                {getTooltipValues()}
            </TooltipWrapper>
        );
    }

    return null;
};

export default GraphTooltip;
