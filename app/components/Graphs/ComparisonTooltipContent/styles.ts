import styled from 'styled-components';

const ComparisonTooltipContentWrapper = styled.div`
    width: 500px;
`;
const ComparisonHeader = styled.div<{color: string}>`
    width: 100%;

    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: flex-start;
    background-color: ${props => props.color};
    padding: 14px 18px 10px;
        padding-right: 150px;
    text-align: left;
    font-size: 12px;
color: #353535;
`;
const ComparisonHeaderContainer = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: flex-start;
    padding-left: 15px;
`;
const ComparisonContent = styled.div`
  padding-top:22px;
    padding-left: 18px;
`;
const Dot = styled.div<{color: string}>`
  width: 12px;
  height: 12px;
  border-radius: 50%;
  display: inline-block;
  background-color: ${props => props.color};
`;
const SectionWrapper = styled.div`
display: flex;
padding-right: 16px;

`;
const Icon = styled.img`
  height: 12px;
  width: 15px;
  margin-right: 16px;
  margin-top: 7px;
`;
const Separator = styled.div`
width:100%;
border: 1px solid #EAEAEA;
opacity: 1;
    margin-bottom: 10px;
`;

export {
    ComparisonTooltipContentWrapper,
    ComparisonHeader,
    Dot,
    ComparisonHeaderContainer,
    ComparisonContent,
    SectionWrapper,
    Icon,
    Separator,
};
