import {
  ComparisonHeader,
  ComparisonHeaderContainer,
  ComparisonTooltipContentWrapper,
  Dot,
  ComparisonContent,
  SectionWrapper,
  Icon,
  Separator,
} from './styles';
import { FormattedMessage, injectIntl } from 'react-intl';
import React, { Fragment } from 'react';
import QASection from '../../common/QASection';
import moment from 'moment';
import {
  getDocsIconDarkGrey,
  getDocsIconGreen,
  getFormattedMessage,
  valueToBackGround,
  valueToColor,
  valueToString,
} from '../../common/commonFunc';
import {
  AnswersInterface,
  QuestionnaireInterface,
} from '../../../models/local';

interface Props {
  status: number;
  intl: any;
  answers: AnswersInterface;
  referenceDate: Date;
  sourceDate: Date;
  questionnaireData: QuestionnaireInterface;
}

const ComparisonTooltipContent = (props: Props) => {
  const {
    intl,
    questionnaireData,
    answers,
    status,
    sourceDate,
    referenceDate,
  } = props;
  const refDate = moment(referenceDate).format('D MMM, YYYY');
  const souDate = moment(sourceDate).format('D MMM, YYYY');
  const getDailyAnswers: number[] = (answers && answers.daily) || [];
  const getDailyQuestions =
    (questionnaireData && questionnaireData.daily) || [];
  const getExpandedQuestions =
    (questionnaireData && questionnaireData.expanded) || [];
  const getExpandedAnswers: number[] = (answers && answers.expanded) || [];
  let statusVal = intl.formatMessage({ id: valueToString(status) });
  statusVal = <b style={{ color: valueToColor(status) }}>{statusVal}</b>;
  const styledSouDate = <b>{souDate}</b>;
  // const styledRefDate =<b>{refDate}</b>
  const styledRefDate =
    moment(sourceDate).diff(moment(referenceDate), 'days') > 1 ? (
      <b>{refDate}</b>
    ) : (
      intl.formatMessage({ id: 'THE_DAY_BEFORE' })
    );
  return (
    <ComparisonTooltipContentWrapper>
      <ComparisonHeader color={valueToBackGround(status)}>
        <Dot color={valueToColor(status)} />
        <ComparisonHeaderContainer>
          <FormattedMessage
            id={`${getFormattedMessage(status)}`}
            values={{ styledRefDate, styledSouDate, statusVal }}
          />
        </ComparisonHeaderContainer>
      </ComparisonHeader>
      <ComparisonContent>
        <SectionWrapper>
          <Icon src={getDocsIconDarkGrey()} />
          <QASection
            title={'DAILY_COPD_DAILY_TITLE'}
            questions={getDailyQuestions}
            answers={getDailyAnswers}
          />
        </SectionWrapper>
        <Separator />
        {getExpandedAnswers.length > 0 ? (
          <SectionWrapper>
            <Icon src={getDocsIconGreen()} />
            <QASection
              title={'EXPANDED_DAILY_COPD_DAILY_TITLE'}
              questions={getExpandedQuestions}
              answers={getExpandedAnswers}
            />
          </SectionWrapper>
        ) : (
          <Fragment />
        )}
      </ComparisonContent>
    </ComparisonTooltipContentWrapper>
  );
};

export default injectIntl(ComparisonTooltipContent);
