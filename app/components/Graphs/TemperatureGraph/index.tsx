import React, {useEffect, useState} from 'react';
import styles from 'styles/values';
import {LineGraph} from '../index';
import {PatientVital} from '../../../models/local';
import {LineProp} from '../LineGraph';
import _ from 'lodash';
import {getDomain} from '../../common/commonFunc';
interface TemperatureGraphProps {
    data?: PatientVital[];
    days?: number;
    unit?: string;
}

const TemperatureGraph: React.FC<TemperatureGraphProps> = (props: TemperatureGraphProps) => {
    const {data, days, unit} = props;

    const convertCelsiusToFahrenheit = (value) => {
        if (value !== undefined && value !== null) {
            return Math.round(value * 9 / 5 + 32);
        }
        return value;
    };
    const domain = getDomain(30, 40, data, 'temperature');
    const lines: LineProp[] = [
        {
            field: 'temperature',
            color: styles.colors.GRAPHS.GRAY,
            unit: 'Temp',
            convert : unit === 'F' ? convertCelsiusToFahrenheit : null,
            domain: {
                orientation: 'left',
                values: (unit === 'C'
                        ?
                        [domain[0], domain[1]]
                        :
                        [convertCelsiusToFahrenheit(domain[0]), convertCelsiusToFahrenheit(domain[1])]
                ),
            },
        },
    ];






    return (
        <LineGraph
            lines={lines}
            data={data}
            days={days}
            enableBrush={true}/>
    );
};

export default TemperatureGraph;
