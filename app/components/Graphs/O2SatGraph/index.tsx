import React, {useEffect, useState} from 'react';
import {ResponsiveContainer, ComposedChart, Line, XAxis, YAxis, Tooltip, Brush} from 'recharts';
import styles from 'styles/values';
import GraphTooltip from '../GraphTooltip';
import {GraphWrapper} from '../styles';
import {LineGraph} from '../index';
import {PatientVital} from '../../../models/local';
import {LineProp} from '../LineGraph';
import {getDomain} from '../../common/commonFunc';

interface O2SatGraphProps {
    data?: PatientVital[];
    days?: number;
}


const O2SatGraph: React.FC<O2SatGraphProps> = (props: O2SatGraphProps) => {
    const {data, days} = props;
    const domain = getDomain(30, 60, data, 'o2');
    const lines: LineProp[] = [
        {
            field: 'o2',
            color: styles.colors.GRAPHS.GRAY,
            unit: 'O2 Sat',
            domain: {
                orientation: 'left',
                values: [domain[0], domain[1]],
            },
        },
];




    return (
        <LineGraph lines={lines} data={data} days={days} enableBrush={true}/>
    );
};

export default O2SatGraph;
