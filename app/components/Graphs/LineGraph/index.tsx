import React from 'react';
import {
  ResponsiveContainer,
  ComposedChart,
  Line,
  XAxis,
  YAxis,
  Tooltip,
  CartesianGrid,
  Brush,
  Scatter,
} from 'recharts';
import styles from 'styles/values';
import GraphTooltip from '../GraphTooltip';
import moment from 'moment';
import _ from 'lodash';
import { PatientVital } from '../../../models/local';
import { GraphWrapper } from '../styles';

interface MeasuresGraphProps {
  data?: PatientVital[];
  days?: number;
  lines: LineProp[];
  enableBrush?: boolean;
}

export interface LineProp {
  field: string;
  color: string;
  unit: string;
  convert?: ((value: any) => any) | null;
  label?: string;
  domain: {
    orientation: 'left' | 'right';
    values: [number, number];
  };
}

const isEven = (n: number) => {
  return n % 2 === 0;
};

const LineGraph: React.FC<MeasuresGraphProps> = (props: MeasuresGraphProps) => {
  const { data, days, lines, enableBrush } = props;
  const unit = '';
  const objCounter = {
    o2: 0,
    temperature: 0,
    pulse: 0,
  };
  const date = moment();
  const graphData: Array<{
    date: string;
    o2?: number;
    temperature?: number;
    pulse?: number;
  }> = [];

  for (let i = 0; i < (days || 10); i++) {
    const pointData = {};
    for (const line of lines) {
      const point = _.find(
        data,
        item =>
          item.vital === line.field && moment(item.date).isSame(date, 'day'),
      );
      pointData[line.field] = (point && point.value) || undefined;
      if (point && point.value) {
        objCounter[line.field]++;
      }
      if (line.convert) {
        pointData[line.field] = line.convert(pointData[line.field]);
      }
    }
    graphData.push({
      date: date.format('D'),
      ...pointData,
    });
    date.subtract(1, 'day');
  }

  graphData.reverse();

  const tickFormatter = value => {
    return value;
  };

  const getLines = () => {
    if (!lines) {
      return [];
    }
    return lines.map((line: LineProp, index: number) => {
      if (objCounter[line.field] > 1) {
        return (
          <Line
            yAxisId={`${line.field}_${index}`}
            dot={false}
            connectNulls={true}
            key={`line_${line.field}_${index}`}
            unit={line.unit}
            type="monotone"
            dataKey={line.field}
            stroke={line.color}
            strokeWidth={3}
          />
        );
      } else {
        return (
          <Scatter
            yAxisId={`${line.field}_${index}`}
            key={`line_${line.field}_${index}`}
            // unit={line.unit}
            dataKey={line.field}
            stroke={line.color}
          />
        );
      }
    });
  };

  const getYAxis = () => {
    if (!lines) {
      return [];
    }
    return lines.map((line: LineProp, index: number) => {
      const customDomain: [number, number] | undefined =
        line.domain && line.domain.values;

      if (customDomain && isEven(customDomain[0]) !== isEven(customDomain[1])) {
        customDomain[1]++;
      }
      return (
        <YAxis
          yAxisId={`${line.field}_${index}`}
          dataKey={line.field}
          key={`yAxis_${line.field}_${index}`}
          stroke={styles.colors.GRAPHS.BLACK}
          tickFormatter={tickFormatter}
          allowDataOverflow={true}
          orientation={line.domain && line.domain.orientation}
          domain={['auto', 'auto']}
          tickCount={4}
          // padding={{top: 15, bottom: 0}}
          tickMargin={20}
          tickLine={false}
          axisLine={false}
        />
      );
    });
  };

  return (
    <GraphWrapper style={{ width: '100%', height: '100%' }}>
      <ResponsiveContainer>
        <ComposedChart
          data={graphData}
          margin={{ top: 35, right: 20, bottom: 0, left: 20 }}
        >
          <Tooltip
            wrapperStyle={{
              zIndex: 10,
              top: '-25%',
            }}
            allowEscapeViewBox={{ x: true, y: true }}
            content={<GraphTooltip />}
          />
          <XAxis
            allowDataOverflow={true}
            padding={{ left: 20, right: 20 }}
            // minTickGap={10}
            axisLine={false}
            tickLine={false}
            // tickMargin={20}
            dataKey="date"
          />
          <CartesianGrid vertical={false} opacity={0.5} />
          {getYAxis()}
          {/*<Bar dataKey="pv" barSize={5} fill={styles.colors.GRAPHS.GRAY} radius={10} />*/}
          {getLines()}
          {enableBrush ? (
            <Brush
              fill={styles.colors.SCROLLER.TRACK}
              stroke={styles.colors.SCROLLER.HANDLE}
              startIndex={graphData.length - 11}
              endIndex={graphData.length - 1}
              height={5}
              padding={{ top: 20 }}
            />
          ) : (
            ''
          )}
        </ComposedChart>
      </ResponsiveContainer>
    </GraphWrapper>
  );
};

export default LineGraph;
