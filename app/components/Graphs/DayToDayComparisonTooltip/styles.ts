import styled from 'styled-components';
const GraphWrapper = styled.div`
  width: 100%;
  height: 26%;
  display: flex;
  flex-direction: row;
  font-size: 14px;

`;

const TooltipWrapper = styled.div`
    background: #FFFFFF 0% 0% no-repeat padding-box;
border: 1px solid #EAEAEA;
opacity: 1;
width:530px;
height:483px;
padding-left:37.5px;
padding-right:30.5px;
padding-top:22.5px;

`;
const TrendAndQAWrapper = styled.div`


`;

const TitleWrapper = styled.div<{marginBottom: string , size: string}>`
text-align: left;
font-weight: bold;
    font-size: ${props => props.size};
letter-spacing: 0;
color: #353535;
opacity: 1;
margin-bottom:${props => props.marginBottom};
`;
const ContentWrapper = styled.div`
text-align: left;
font-size:  12px;
color: #353535;
opacity: 1;
max-width:587px;
`;
const Separator = styled.div`
width:100%;
border: 1px solid #EAEAEA;
opacity: 1;
    margin-top: 26px;
    margin-bottom: 29px;
`;

export {
    GraphWrapper,
    ContentWrapper,
    TooltipWrapper,
    TitleWrapper,
    Separator,
    TrendAndQAWrapper,
};
