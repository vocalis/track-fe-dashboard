import React from 'react';

import {
 TooltipWrapper,
    TitleWrapper,
    ContentWrapper,
    Separator,
    TrendAndQAWrapper,
} from './styles';
import {FormattedMessage} from 'react-intl';
import IconWithText from '../../common/IconWithText';
const DayToDayComparisonTooltip = () => {
    return (
        <TooltipWrapper>
            <TitleWrapper size="18px" marginBottom="18px">
                <FormattedMessage id={'DAY_TO_DAY_COMPARISON'}/>
            </TitleWrapper>
            <ContentWrapper>
                <FormattedMessage id={'DAY_TO_DAY_COMPARISON_TEXT1'}/>
            </ContentWrapper>
                <Separator/>
            <TrendAndQAWrapper>
                <IconWithText trendsrc="better" title="DAY_TO_DAY_BETER"/>
                <IconWithText trendsrc="same" title="DAY_TO_DAY_SAME"/>
                <IconWithText trendsrc="worse" title="DAY_TO_DAY_WORSE"/>
                <IconWithText trendsrc="none" title="DAY_TO_DAY_NONE"/>
                <IconWithText docsrc={{daily: [1], expanded: []}} title="COPD_DIARY"/>
                <IconWithText docsrc={{daily: [], expanded: [1]}}  title="EXPENDED_COPD_DIARY"/>
            </TrendAndQAWrapper>

            <ContentWrapper>
                <FormattedMessage id={'DAY_TO_DAY_COMPARISON_TEXT3'}/>
            </ContentWrapper>
        </TooltipWrapper>

    );
};
export default DayToDayComparisonTooltip;
