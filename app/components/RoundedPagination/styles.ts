import { withStyles} from '@material-ui/core';
import {Pagination} from '@material-ui/lab';
import styled from 'styled-components';
import {makeStyles} from '@material-ui/core/styles';

const StyledPagination = withStyles(() => ({
    root: {
            color: 'red !important',
    },
}))(Pagination);

const useStyles = makeStyles({
    ul: {
        listStyle: 'none',
        padding: 6,
        margin: 6,
        display: 'flex',
        opacity: 1,
    },
    selected: {
        background: 'rgba(56, 171, 182, 0.5)',
        border: '1px solid #DBDBDB',
        borderRadius: '6px',
        height: '38px',
        minWidth: '38px',
        opacity: 1,
    },
    active: {
    color: '#000000' ,
    opacity: 1,
},
});


const Button = styled.button<{isDisabled: boolean}>`
  background: #FFFFFF;
border: 1px solid #DBDBDB;
border-radius: 6px;
height: 38px;
min-width: 38px;
opacity: 0.5;
font-size: 18px;
font-family: Roboto-Light;
margin-right: 6px;
cursor:${props => props.isDisabled ? '' : 'pointer'};

`;
const Img = styled.img<{name: string}>`
width: 12px;
height: 18px;
`;

export {
    StyledPagination,
    useStyles,
    Button,
    Img,
};
