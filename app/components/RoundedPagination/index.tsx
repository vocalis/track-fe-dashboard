/**
 *
 * Contact
 *
 */
import React, {memo, Fragment} from 'react';
import {Button, Img, useStyles} from './styles';
import {usePagination} from '@material-ui/lab';
import _ from 'lodash';
const next = require('images/next.svg');
const previous = require('images/previous.svg');
interface Props {
    count: number;
    selectedpage: number;
    onChange: (page: string) => void;
}
function RoundedPagination(props: Props) {
    const {count, selectedpage, onChange} = props;
    const classes = useStyles();
    const {items} = usePagination({count});
    const handleClick = (e) => {
        debouncedChangePageClick(e.target);
    };
    const onChangePageClick = (e) => {
            onChange(e.target.name);
    };
    const debouncedChangePageClick = _.debounce(onChangePageClick, 3000);
    return (
        count ?  (
            <ul onClick={onChangePageClick} className={classes.ul}>
            {items.map(({page, type, selected, ...item}, index) => {
                let children;
                if (type === 'start-ellipsis' || type === 'end-ellipsis') {
                    children = (
                        <Button isDisabled={true} disabled={true}>
                            ...
                        </Button>
                    );
                } else if (type === 'page') {
                    children = (
                        <Button
                            isDisabled={false}
                            name={`${page}`}
                            className={selectedpage === page ? classes.selected : ''}
                            {...item}>
                            {page}
                        </Button>
                    );
                } else {
                    children = (
                        <Button
                            name={type}
                            type="button"
                            isDisabled={item.disabled}
                            className={!item.disabled ? classes.active : ''}
                            {...item}>
                            {type === 'next' && <Img   name={type} src={next} alt={type}/>}
                            {type === 'previous' && <Img name={type} src={previous} alt={type}/>}
                        </Button>
                    );
                }

                return <li  key={index}>{children}</li>;
            })}
        </ul>
        ) : <Fragment/>
    );
}

export default memo(RoundedPagination);

