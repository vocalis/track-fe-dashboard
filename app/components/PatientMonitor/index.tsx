import React, { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import {
  PatientMonitorHeader,
  PatientMonitorWrapper,
  PatientMonitorBody,
  PatientData,
  PatientActions,
  Separator,
  PatientName,
  PatientTitleText,
  PatientGraphWrapper,
  PatientGraphBody,
} from './styles';
import { HorizontalCheckBox } from 'components/common';
import { Patient } from '../../models/local';
import { LatestRecordComparison, MeasuresGraph } from '../Graphs';
import './style.css';
import { Modal } from '@material-ui/core';
import MessageCard from '../MessageCard';
import { StyledDrawer } from '../../containers/SideBar/styles';
import ConfirmReviewPopup from './ConfirmReviewPopup';
import Button from '../Button';
import { GraphWrapper } from '../Graphs/LatestRecordComparison/styles';
import {StyledDialogContent} from '../../containers/FormDetails/styles';
interface PatientMonitorProp {
  patient: Patient;
  onPatientReviewClicked: (patientId: number) => void;
  onPatientSelected: (patientId: number) => void;
}

const PatientMonitor: React.FC<PatientMonitorProp> = (
  props: PatientMonitorProp,
) => {
  const { patient, onPatientReviewClicked, onPatientSelected } = props;
  const { name, patientId, reviewed } = patient;
  const [showPatientReviewPopup, setShowPatientReviewPopup] = useState(false);

  const onConfirmPatientReview = () => {
    setShowPatientReviewPopup(false);
    onPatientReviewClicked(patientId);
  };
  const reviewPatient = () => {
    setShowPatientReviewPopup(true);
  };
  const closePopupModal = () => {
    setShowPatientReviewPopup(false);
  };
  const onNameDoubleClick = () => {
    onPatientSelected(patient.patientId);
  };
  const preventDoubleClick = (event) => {
    event.stopPropagation();
  };
  return (
      <>
        <Modal
            open={showPatientReviewPopup}
            onClose={closePopupModal}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            disableBackdropClick
        >
          <StyledDialogContent>
          <ConfirmReviewPopup
              message={reviewed ? 'CONFIRM_NOT_REVIEW' : 'CONFIRM_REVIEW'}
              onActionClick={closePopupModal}
              onButtonClick={onConfirmPatientReview}
              messageWidth={reviewed ? '300px' : '100%'}
          />
          </StyledDialogContent>
        </Modal>
    <PatientMonitorWrapper className="container">
      <span className="hoverBody">
        <PatientMonitorHeader
          className="hoverBody"
          onDoubleClick={onNameDoubleClick}
        >
          <PatientData>
            <PatientName>{name}</PatientName>
            <Separator>|</Separator>
            <PatientTitleText>
              <FormattedMessage id="ID_NUMBER" values={{ number: patientId }} />
            </PatientTitleText>
            <Separator>|</Separator>
            <PatientTitleText>
              <FormattedMessage id="AGE" />: {patient.age || '-'}
            </PatientTitleText>
          </PatientData>
          <PatientActions>
            <HorizontalCheckBox
              onDoubleClick={preventDoubleClick}
              action={reviewPatient}
              value={patient.reviewed}
              title="REVIEWED_TODAY"
            />
          </PatientActions>
        </PatientMonitorHeader>
        <PatientMonitorBody
          className="hoverBody"
          onDoubleClick={onNameDoubleClick}
        >
          <PatientGraphWrapper>
            <PatientGraphBody>
              <LatestRecordComparison
                days={25}
                analysis={patient.analysis}
                lastActivity={patient.lastActivity}
              />
            </PatientGraphBody>
          </PatientGraphWrapper>
          {/*<PatientGraphWrapper>*/}
          {/*    <PatientGraphBody>*/}
          {/*        <MeasuresGraph data={patient.vitals}/>*/}
          {/*    </PatientGraphBody>*/}
          {/*</PatientGraphWrapper>*/}
        </PatientMonitorBody>
      </span>
    </PatientMonitorWrapper>
        </>
  );
};

export default PatientMonitor;
