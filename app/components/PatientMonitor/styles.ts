import styled from 'styled-components';
import styles from 'styles/values';

const PatientMonitorWrapper = styled.div`
cursor: pointer ;
    height: 270px;
    border: 1px solid ${styles.colors.SHADE.GRAY.TABLE_BORDER};
    margin-top: 20px;
    border-radius: 6px;
        @media ${styles.sizes.SMALL_SCREEN}{
  margin-right: ${styles.sizes.APP_SIDE_PADDING_RIGHT_SMALL_SCREEN};
  margin-left: ${styles.sizes.APP_SIDE_PADDING_LEFT_SMALL_SCREEN};
  width:1000px
 }
   @media ${styles.sizes.MEDIUM_SCREEN}{
   margin-right: ${styles.sizes.APP_SIDE_PADDING_RIGHT};
  margin-left: ${styles.sizes.APP_SIDE_PADDING_LEFT};
  width:1099px
 }
   @media ${styles.sizes.LARGE_SCREEN}{
   margin-right: ${styles.sizes.APP_SIDE_PADDING_RIGHT_LARGE_SCREEN};
  margin-left: ${styles.sizes.APP_SIDE_PADDING_LEFT_LARGE_SCREEN};
    width:1200px;
}
`;

const PatientMonitorHeader = styled.div`
    height: 54px;
    padding-left: 1.094vw;
    padding-right: 1.042vw;
    border-bottom: 1px solid ${styles.colors.SHADE.GRAY.TABLE_BORDER};
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`;

const PatientMonitorBody = styled.div`
    width: 100%;
    display: flex;
    flex: 1 1;
    height: calc(100% - 54px);
    padding: 12px 1.250vw;
    background-color : ${styles.colors.GENERAL.PATIENT_BACKGROUND};
    justify-content: space-between;
    align-items: center;
`;

const PatientData = styled.div`
    flex: 1 1;
    font-size: 18px;
    color : ${styles.colors.SHADE.GRAY.TABLE_TEXT};
    display: flex;
`;

const PatientActions = styled.div`
    flex: 1 1;
    font-size: 13.5px;
    color : ${styles.colors.SHADE.GRAY.TABLE_TEXT};
    margin-left: 12.656vw;
    display: flex;
    justify-content: flex-end;
`;

const Separator = styled.div`
  color: ${styles.colors.SHADE.GRAY.TABLE_TEXT};
  margin-left: 1.042vw;
  margin-right: 1.042vw;
`;

const PatientName = styled.div`
    font-weight: bold;
    cursor: pointer;
    user-select: none;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
`;

const PatientTitleText = styled.div`

`;

const PatientGraphWrapper = styled.div`
    background: ${styles.colors.SHADE.WHITE.WHITE} 0% 0% no-repeat padding-box;
    border: 1px solid #EAEAEA;
    border-radius: 8px;
    opacity: 1;
    //width: calc(50% - 0.625vw);
    width: 100%;
    height: 100%;
`;

const PatientGraphHeader = styled.div`
    height: 50px;
    border-bottom: 1px solid #EAEAEA;
    padding-left: 0.547vw;
    text-align: left;
    font-size: 13.5px;
    font-weight: bold;
    letter-spacing: 0.45px;
    color: #353535;
    opacity: 1;
    justify-content: flex-start;
    align-items: center;
    display: flex;
`;

const PatientGraphBody = styled.div`
    height: 100%;
`;
const PopupWrapper = styled.div`
   width:500px ;
min-height: 500px;
 background-color: #FFFFFF;
 display: flex;
 flex-direction: column;
 box-shadow: 0px 3px 18px #00000029;
border: 1px solid #EAEAEA;
margin: auto;
margin-top: 15px;
align-items: center;
padding: 30px;
 justify-content: space-between;
`;
const MessageWrapper = styled.div<{messageWidth: string}>`
          text-align: center;
        font-family: Roboto-Regular;
        font-size: 36px;
        letter-spacing: 0px;
          color:#38ABB6;
          width:${props => props.messageWidth}
        `;
const ButtonWrapper = styled.div`
`;
const ActionWrapper = styled.div`
text-align: center;
font-family: Roboto-Regular;
font-size: 20px;
letter-spacing: 0px;
color: #38ABB6;
opacity: 1;
cursor: pointer;
border: 1px solid transparent;
padding-top: 5px;
border-radius: 6px;

width: 260px;
height: 42px;
&:hover{
background: #FCFCFC;
border: 1px solid #DBDBDB;
opacity: 1;
}

`;
export {
    PatientMonitorWrapper,
    PatientMonitorHeader,
    PatientMonitorBody,
    PatientData,
    PatientActions,
    Separator,
    PatientName,
    PatientTitleText,
    PatientGraphWrapper,
    PatientGraphHeader,
    PatientGraphBody,
    PopupWrapper,
    MessageWrapper,
    ButtonWrapper,
    ActionWrapper,
};
