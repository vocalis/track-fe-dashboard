import React from 'react';
import Button from '../Button';
import { FormattedMessage } from 'react-intl';
import {
    PopupWrapper,
    MessageWrapper,
    ButtonWrapper,
    ActionWrapper,
} from './styles';
interface Props {
    message: string;
    onActionClick: () => void;
    onButtonClick: () => void;
    messageWidth: string;
}

function ConfirmReviewPopup(props: Props) {
        const{message,
              onActionClick,
              onButtonClick,
              messageWidth,
        } = props;
        return(
        <PopupWrapper>
            <MessageWrapper messageWidth={messageWidth}>
                <FormattedMessage id={`${message}`}/>
            </MessageWrapper>
            <ButtonWrapper>
                <Button onClick={onButtonClick} title="OK"/>
            </ButtonWrapper>
            <ActionWrapper onClick={onActionClick}>
               <FormattedMessage id="CANCEL"/>
            </ActionWrapper>
        </PopupWrapper>
    );
}
export default ConfirmReviewPopup;
