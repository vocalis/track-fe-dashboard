/**
 *
 * Contact
 *
 */
import React, {memo, useEffect} from 'react';
import Layout from '../common/Layout';
import {
    Contanier,
    ContentTitle,
    Text,
    Link,
} from './styles';
import {FormattedMessage} from 'react-intl';
import {useDispatch} from 'react-redux';
import {toggleSideBar} from '../../containers/SideBar/actions';

interface Props {}

function Contact(props: Props) {
    const dispatch = useDispatch();
    useEffect(() => {
        window.scrollTo(0, 0);
        dispatch(toggleSideBar(false));
    });
    return (
        <Layout title="Contact">
            <Contanier>
                <ContentTitle>
                    <FormattedMessage id={'CONTACT_US'} />
                </ContentTitle>
                <Text width="fit-content">
                    <FormattedMessage id={'CONTACT_PARAGRAPH_1'}
                                      values={{lineBreaking: <br/>}}
                    />
                </Text>
                <Text width="fit-content">
                    <FormattedMessage id={'CONTACT_PARAGRAPH_2'}
                                      values={{
                                          lineBreaking: <br/>,
                                          email:
                                              <Link
                                                 href="mailto:support@vocalishealth.com"
                                                 target="_blank" >
                                                  support@vocalishealth.com
                                              </Link>}}
                    />
                </Text>

            </Contanier>
        </Layout>
    );
}

export default memo(Contact);
