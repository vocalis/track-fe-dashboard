
import styled from 'styled-components';
import React from 'react';
import styles from 'styles/values';
const ContentTitle = styled.div`
font-family: Roboto-Regular;
font-size: 42px;
padding-bottom: 39px;
`;
const Text = styled.div<{width?: string}>`
font-family: Roboto-Regular;
font-size: 18px;
padding-bottom: 39px;
text-align: left;
color: #353535;
opacity: 1;
width: ${props => props.width ? props.width : '100%'};
`;


const  Contanier = styled.div`
width:100%;
           @media ${styles.sizes.MEDIUM_SCREEN}{
        padding-left: ${styles.sizes.HELP_SIDE_PADDINGE_LEFT};
        padding-right: ${styles.sizes.HELP_SIDE_PADDINGE_ROGHT};
         }
   @media ${styles.sizes.SMALL_SCREEN}{
   padding-right: ${styles.sizes.HELP_SIDE_PADDINGE_ROGHT_SMALL_SCREEN};
  padding-left: ${styles.sizes.HELP_SIDE_PADDINGE_LEFT_SMALL_SCREEN};
  }
     @media ${styles.sizes.LARGE_SCREEN}{
   padding-right: ${styles.sizes.APP_SIDE_PADDING_RIGHT_LARGE_SCREEN};
  padding-left: ${styles.sizes.APP_SIDE_PADDING_LEFT_LARGE_SCREEN};
  }

background-color: #FFFFFF;
display: flex;
flex-direction: column;
padding-top: 73px;
`;



const Link = styled.a`
text-decoration: none;
 color: #1e70bf !important;
 &:visited {color: #1e70bf !important};
`;
export {
    Text,
    ContentTitle,
    Contanier,
    Link,
};
