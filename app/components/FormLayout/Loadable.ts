/**
 *
 * Asynchronously loads the component for FormLayout
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
