/**
 *
 * FormLayout
 *
 */
import React, { memo, Fragment, useState } from 'react';
import {
  FormWrapper,
  PersonalInfoWrapper,
  ContactInfoWrapper,
  TitleWrapper,
  TextFieldWrapper,
  SelectWrapper,
  CloseIconWrapper,
  ButtonWrapper,
} from './styles';
import { FormattedMessage, injectIntl } from 'react-intl';
import TextField from '../TextField';
import { FormikErrors } from 'formik';
import { ValuesProps } from '../../models/local';
import { Select } from '../Select';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import DatePicker from '../DatePicker';
import { Modal } from '@material-ui/core';
import Button from '../Button';
const closeIcon = require('images/close.svg');
interface Props {
  values: object;
  setFieldValue: (name: string, val: string | number | null) => void;
  errors?: FormikErrors<ValuesProps>;
  isSubmitted: boolean;
  PersonalTextFieldPropsArray: any;
  ContactTextFieldPropsArray: any;
  intl: any;
  statusProps: any;
  editPage: boolean;
  onChangePasswordRequest: (password) => {};
}

function FormLayout(props: Props) {
  const { intl } = props;
  const {
    values,
    setFieldValue,
    errors,
    PersonalTextFieldPropsArray,
    ContactTextFieldPropsArray,
    isSubmitted,
    statusProps,
    editPage,
    onChangePasswordRequest,
  } = props;
  const [showChangePssswordModal, setChangePssswordModal] = useState(false);
  const onChangePasswordClicked = () => {
    setChangePssswordModal(!showChangePssswordModal);
    onChangePasswordRequest(values['password']);
    setFieldValue('password', '');
  };
  const togglePasswordModal = () => {
    setChangePssswordModal(!showChangePssswordModal);
    if (showChangePssswordModal) {
      setFieldValue('password', '');
    }
  };
  const getTextFieldValue = (
    editPage: boolean,
    editable: boolean,
    value: string,
    id: string,
  ) => {
    if (editPage && !editable && id === 'password') {
      return '******';
    } else {
      return value;
    }
  };
  return (
    <FormWrapper>
      <Modal
        open={showChangePssswordModal}
        onClose={() => {}}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        disableBackdropClick
      >
        <div
          style={{
            width: '500px',
            minHeight: '300px',
            backgroundColor: '#FDFDFD',
            display: 'flex',
            flexDirection: 'column',
            boxShadow: '0px 3px 18px #00000029',
            border: '1px solid #EAEAEA',
            margin: 'auto',
            marginTop: '15px',
            alignItems: 'center',
            padding: '20px',
          }}
        >
          <CloseIconWrapper>
            <img
              style={{ cursor: 'pointer' }}
              onClick={togglePasswordModal}
              src={closeIcon}
            />
          </CloseIconWrapper>
          <TextField
            onChange={setFieldValue}
            type="string"
            label={<FormattedMessage id={'NEW_PASSWORD'} />}
            id="password"
            isSubmitted={true}
            style={{ marginTop: '38px' }}
            fontFamily="Roboto-Regular"
            helpText="HELP_TEXT"
            error={errors && errors['password']}
            showChangePssswordModal={showChangePssswordModal}
          />
          <ButtonWrapper>
            <Button
              disableButton={
                values['password'] === '' ||
                (errors && errors['password'] !== undefined)
              }
              onClick={onChangePasswordClicked}
              title={'CHANGE'}
            />
          </ButtonWrapper>
        </div>
      </Modal>
      <PersonalInfoWrapper>
        <TitleWrapper>
          <FormattedMessage id="PERSONAL_INFORMATION" />
        </TitleWrapper>
        <TextFieldWrapper>
          {PersonalTextFieldPropsArray.map((props, index) => {
            if (props.type !== 'date') {
              return (
                <TextField
                  key={index}
                  onChange={setFieldValue}
                  type={props.type}
                  label={<FormattedMessage id={props.label} />}
                  id={props.id}
                  error={errors && errors[props.id]}
                  isSubmitted={isSubmitted}
                  value={getTextFieldValue(
                    editPage,
                    props.editable,
                    values[props.id],
                    props.id,
                  )}
                  style={{ marginTop: '38px' }}
                  fontFamily="Roboto-Regular"
                  disabled={editPage && !props.editable}
                  editPage={editPage}
                />
              );
            } else {
              return (
                <Fragment key={index}>
                  <MuiPickersUtilsProvider utils={MomentUtils}>
                    <DatePicker
                      onChange={setFieldValue}
                      label={<FormattedMessage id={props.label} />}
                      id={props.id}
                      isSubmitted={isSubmitted}
                      value={values[props.id]}
                      style={{ marginTop: '38px' }}
                      fontFamily="Roboto-Regular"
                    />
                  </MuiPickersUtilsProvider>
                </Fragment>
              );
            }
          })}
        </TextFieldWrapper>
      </PersonalInfoWrapper>
      <ContactInfoWrapper>
        <TitleWrapper>
          <FormattedMessage id="CONTACT_INFORMATION" />
        </TitleWrapper>
        <TextFieldWrapper>
          {ContactTextFieldPropsArray.map((props) => {
            return (
              <TextField
                key={`Contact${props.id}`}
                onChange={setFieldValue}
                type={props.type}
                label={<FormattedMessage id={props.label} />}
                id={props.id}
                isSubmitted={isSubmitted}
                value={getTextFieldValue(
                  editPage,
                  props.editable,
                  values[props.id],
                  props.id,
                )}
                style={{ marginTop: '38px' }}
                error={errors && errors[props.id]}
                helpText={props.id === 'password' ? 'HELP_TEXT' : ''}
                fontFamily="Roboto-Regular"
                errorPossition="-76px"
                disabled={editPage && !props.editable}
                onChangePasswordClicked={togglePasswordModal}
                editPage={editPage}
              />
            );
          })}
          {editPage && (
            <SelectWrapper>
              <Select
                key={statusProps.id}
                onChange={setFieldValue}
                id={statusProps.id}
                name={statusProps.name}
                value={statusProps.values}
                selectedValue={values[statusProps.id]}
                withLabel={true}
                width="398px"
                height="42px"
                fontFamily="Roboto-Regular"
              />
            </SelectWrapper>
          )}
        </TextFieldWrapper>
      </ContactInfoWrapper>
    </FormWrapper>
  );
}

export default memo(injectIntl(FormLayout));
