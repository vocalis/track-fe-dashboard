import styled from 'styled-components';

const FormWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: row;
`;
const PersonalInfoWrapper = styled.div`
  width: 552px;
`;
const ContactInfoWrapper = styled.div`
  width: 313px;
`;
const TitleWrapper = styled.div`
  font-size: large;
  font-weight: lighter;
  color: slategray;
  margin-top: 40px;
`;
const TextFieldWrapper = styled.div`
  position: relative;
  margin-top: 50px;
  width: 398px;
  font-family: Roboto-Regular;
  font-size: 18px;
  color: #38abb6;
`;
const SelectWrapper = styled.div`
  width: 398px;
  margin-top: 82px;
  font-family: Roboto-Regular;
  font-size: 18px;
`;
const CloseIconWrapper = styled.div`
    position: relative;
    left: 0px;
    width: 100%;
    top: 0px;
`;
const ButtonWrapper = styled.div`
position: relative;
bottom: 3px;
margin-top: 66px;
`;
export {
  FormWrapper,
  PersonalInfoWrapper,
  ContactInfoWrapper,
  TitleWrapper,
  TextFieldWrapper,
  SelectWrapper,
  CloseIconWrapper,
  ButtonWrapper,
};
