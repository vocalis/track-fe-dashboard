/**
 *
 * Asynchronously loads the component for MessageCard
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
