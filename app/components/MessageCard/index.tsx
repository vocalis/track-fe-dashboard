/**
 *
 * MessageCard
 *
 */
import React, {memo, RefObject} from 'react';
import {
  CardWrapper,
  IconWrapper,
  Img,
  MessageTitleWrapper,
  ButtonWrapper,
  MessageWrapper,
  Div,
} from './styles';
import Button from '../Button';
import {FormattedMessage} from 'react-intl';

interface Props {
  src: string;
  onButtonClick: () => void;
  onActionClick: () => void | undefined;
  message: string | undefined;
  buttonTitle: string;
  messageTitle: string ;
  actionText: string | undefined;
  messageValue: string | undefined;
  maxWidth: string;
  isError: boolean;

}

const  MessageCard = React.forwardRef((props: Props, ref: RefObject<HTMLButtonElement>) => {
    const {
        src,
        buttonTitle,
        onButtonClick,
        message,
        messageTitle,
        onActionClick,
        actionText,
        messageValue,
        maxWidth,
        isError,
    } = props;
    return (
        <CardWrapper>
            <IconWrapper>
                <Img src={src} alt="icon"/>
            </IconWrapper>
            <MessageTitleWrapper maxWidth={maxWidth} isError={isError}>
                <FormattedMessage id={messageTitle} values={{createdId: messageValue}}/>
            </MessageTitleWrapper>
            {message && <MessageWrapper>
                <FormattedMessage id={`${message}`}/>
            </MessageWrapper>}
            <ButtonWrapper haveMessage={message}>
                <Button ref={ref} width="260px" height="42px" onClick={onButtonClick} title={buttonTitle}/>
            </ButtonWrapper>
            {actionText && <Div onClick={onActionClick}>
                <FormattedMessage id={actionText}/>
            </Div>}
        </CardWrapper>
    );
});

export default memo(MessageCard);
