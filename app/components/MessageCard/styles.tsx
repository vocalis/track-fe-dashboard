import styled from 'styled-components';
import React from 'react';
import {ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary, withStyles} from '@material-ui/core';
import styles from '../../styles/values';
import {string} from 'prop-types';


const CardWrapper = styled.div`
width:500px ;
min-height: 500px;
 background-color: #FFFFFF;
 display: flex;
 flex-direction: column;
 box-shadow: 0px 3px 18px #00000029;
border: 1px solid #EAEAEA;
margin: auto;
margin-top: 15px;
align-items: center;
`;
const IconWrapper = styled.div`
width:96px ;
height: 64px;
margin-top: 50.5px;
`;
const Img = styled.img`
width:96px ;
height: 64px;
`;
const MessageTitleWrapper = styled.div<{maxWidth: string, isError: boolean }>`
text-align: center;
font-family: Roboto-Regular;
font-size: 36px;
letter-spacing: 0px;
color:${props => props.isError ? '#D22242' : '#38ABB6'}
opacity: 1;
margin-top: 41px;
max-width: ${props => props.maxWidth };
`;
const MessageWrapper = styled.div`
margin-top: 37.4px;
text-align: center;
font-family: Roboto-Regular;
font-size: 20px;
letter-spacing: 0px;
color: #707070;;
opacity: 1;
`;
const ButtonWrapper = styled.div<{haveMessage ?: string} >`
margin-top: ${props => props.haveMessage ? '65px' : '71px'}
`;
const Div = styled.div`
margin-top: 46px;
text-align: center;
font-family: Roboto-Regular;
font-size: 20px;
letter-spacing: 0px;
color: #38ABB6;
opacity: 1;
cursor: pointer;
border: 1px solid transparent;
padding-top: 5px;
&:hover{
background: #FCFCFC;
border: 1px solid #DBDBDB;
border-radius: 6px;
opacity: 1;
width: 260px;
height: 42px;
}
`;

export {
    CardWrapper,
   IconWrapper,
    Img,
    MessageTitleWrapper,
    ButtonWrapper,
    MessageWrapper,
    Div,
};
