import React from 'react';
import {FormattedMessage} from 'react-intl';
import {
    PatientScreenWrapper,
    PatientScreenHeader,
    PatientName,
    Separator,
    PatientID,
    PatientData,
    PatientActions,
    PatientScreenBody,
    PatientSubSection,
    PatientSubSectionTitle,
    PatientSubSectionBody,
    PatientSubSectionPerc, LoadingWrapper,
} from './styles';

import { HorizontalButton, HorizontalCheckBox } from 'components/common';
import {Patient} from '../../models/local';
import {StyledLinerProgress} from '../common/LinerProgress';

const resetIcon = require('images/reset_data.svg');
const playSampleIcon = require('images/play.svg');

interface PatientScreenProp {
    patient: Patient;
    onResetDataClicked: (patientId: number) => void;
    onPatientReviewClicked: (patientId: number) => void;
    onPlaySampleClicked: (patientId: number) => void;
}

const PatientScreen: React.FC<PatientScreenProp> = (props: PatientScreenProp) => {
    const { patient, onResetDataClicked, onPatientReviewClicked, onPlaySampleClicked } = props;
    const { name, patientId } = patient;

    const resetData = () => {
        onResetDataClicked(patientId);
    };

    const playSample = () => {
        onPlaySampleClicked(patientId);
    };

    const reviewPatient = () => {
        onPatientReviewClicked(patientId);
    };

    const getDetectorBody = (detectorType) => {
        const detector = patient.detectors.find(detectorData => detectorData.detector === detectorType);
        if (detector) {
            return (
                <PatientSubSectionBody>
                    {detector.value}
                    <PatientSubSectionPerc>
                        ({detector.confidence || '-'}{detector.detector === 'age' ?  '±' : '%'})
                    </PatientSubSectionPerc>
                </PatientSubSectionBody>
            );
        }
        return (
            <PatientSubSectionBody>
                -
            </PatientSubSectionBody>
        );
    };

    return (
        <PatientScreenWrapper>
            {
                patient.loading &&
                    <LoadingWrapper>
                        <StyledLinerProgress color="secondary"  />
                    </LoadingWrapper>
            }
                <PatientScreenHeader>
                    <PatientData>
                        <PatientName>
                            {name}
                        </PatientName>
                        <Separator>|</Separator>
                            <PatientID><FormattedMessage id="ID_NUMBER" values={{number: patientId}}/></PatientID>
                    </PatientData>
                    <PatientActions>
                        <HorizontalButton action={resetData} icon={resetIcon} title="RESET_DATA" />
                        <HorizontalButton action={playSample} icon={playSampleIcon} title="PLAY_SAMPLE" />
                        <HorizontalCheckBox action={reviewPatient} value={patient.reviewed} title="REVIEWED_TODAY" />
                    </PatientActions>
                </PatientScreenHeader>
                <PatientScreenBody>
                    {/*<PatientSubSection>*/}
                    {/*    <PatientSubSectionTitle><FormattedMessage id={'SMOKER'}/></PatientSubSectionTitle>*/}
                    {/*    {getDetectorBody('smoker')}*/}
                    {/*</PatientSubSection>*/}
                    <PatientSubSection>
                        <PatientSubSectionTitle><FormattedMessage id={'AGE'}/></PatientSubSectionTitle>
                        {getDetectorBody('age')}
                    </PatientSubSection>
                    <PatientSubSection>
                        <PatientSubSectionTitle><FormattedMessage id={'GENDER'}/></PatientSubSectionTitle>
                        {getDetectorBody('gender')}
                    </PatientSubSection>
                </PatientScreenBody>
        </PatientScreenWrapper>
    );
};

export default PatientScreen;
