import styled from 'styled-components';
import styles from 'styles/values';

const PatientScreenWrapper = styled.div`
    height: 202px;
    border: 1px solid ${styles.colors.SHADE.GRAY.TABLE_BORDER};
    margin-top 20px;
    border-radius: 6px;
    margin-right: 11.042vw;
    margin-left: 11.042vw;
    position: relative;
`;

const PatientScreenHeader = styled.div`
    height: 75px;
    padding-left: 1.094vw;
    padding-right: 1.042vw;
    border-bottom: 1px solid ${styles.colors.SHADE.GRAY.TABLE_BORDER};
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`;

const PatientName = styled.div`
    font-weight: bold;
`;

const Separator = styled.div`
  color: ${styles.colors.SHADE.GRAY.TABLE_TEXT};
  margin-left: 1.042vw;
  margin-right: 1.042vw;
`;

const PatientID = styled.div`

`;

const PatientData = styled.div`
    flex: 1 1;
    font-size: 18px;
    color : ${styles.colors.SHADE.GRAY.TABLE_TEXT};
    display: flex;
`;

const PatientActions = styled.div`
    flex: 1 1;
    font-size: 18px;
    color : ${styles.colors.SHADE.GRAY.TABLE_TEXT};
    margin-left: 12.656vw;
    display: flex;
    justify-content: space-between;
`;

const PatientScreenBody = styled.div`
    width: 100%;
    display: flex;
    flex: 1 1;
    height: calc(100% - 75px);
`;

const PatientSubSection = styled.div`
    border-right: 1px solid ${styles.colors.SHADE.GRAY.TABLE_BORDER};
    width: ${100 / 2}%;
    height: 100%;
    padding-top: 26px;
    padding-left: 2.037vw;
`;

const PatientSubSectionTitle = styled.div`
    font-size: 18px;
    font-weight: bold;
    letter-spacing: 0.05em;
    text-align: left;
    color: #707070;
`;

const PatientSubSectionBody = styled.div`
    font-weight: 300;
    font-size: 33px;
    text-align: left;
    color: #707070;
    display: flex;
`;

const PatientSubSectionPerc = styled.div`
    margin-left: 1.289vw;
`;

const LoadingWrapper = styled.div`
    position: absolute;
    width: 100%;
    top: 0;
    z-index: 10;
`;



export {
    PatientScreenWrapper,
    PatientScreenHeader,
    PatientName,
    Separator,
    PatientID,
    PatientData,
    PatientActions,
    PatientScreenBody,
    PatientSubSection,
    PatientSubSectionTitle,
    PatientSubSectionBody,
    LoadingWrapper,
    PatientSubSectionPerc,
};
