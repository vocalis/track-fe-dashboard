/**
 *
 * PublicRoute
 *
 */
import React, { memo } from 'react';

import {Redirect, Route} from 'react-router';

interface Props {}

function PublicRoute({component: Component, isAuthenticated, ...rest}: any) {
    return (
        <Route {...rest} render={(props) =>
            isAuthenticated && props.history.location.pathname === '/login' ? <Redirect to="/"/>
            : <Component {...props}/>
        }/>

    );
}

export default memo(PublicRoute);
