import {
    about,
    contact,
    createMedicalStaff,
    createPatient,
    editMedicalStaff,
    editPatient,
    help, login,
    medicalStaff,
    patients,
    privacyNotice,
    termsOfService,
} from './paths';
import {userRole} from '../utils/constants';

interface RouteConfig {
    path: string;
    exact?: boolean;
    role?: string;
}

export interface Routes {
    // home: RouteConfig;
    triage: RouteConfig;
    monitor: RouteConfig;
    screen: RouteConfig;
    patient: RouteConfig;
    login: RouteConfig;
    patientsManagement: RouteConfig;
    patientManagementCreate: RouteConfig;
    patientManagementEdit: RouteConfig;
    medicalStaffManagement: RouteConfig;
    medicalStaffManagementCreate: RouteConfig;
    medicalStaffManagementEdit: RouteConfig;
    about: RouteConfig;
    contact: RouteConfig;
    help: RouteConfig;
    termsOfService: RouteConfig;
    privacyNotice: RouteConfig;
    MonitorWrapper: RouteConfig;
}

const routes: Routes = {

    monitor: {
        path: '/' || '',
        exact: true,
        role: userRole.patient,
    },
    triage: {
        path: '/triage',
        exact: false,
    },
    MonitorWrapper: {
        path: '/monitor',
        exact: true,
        role: userRole.patient,
    },
    patient: {
        path: '/monitor/:id',
        exact: true,
        role: userRole.patient,
    },
    screen: {
        path: '/screen',
        exact: false,
    },
    login: {
        path: login,
        exact: true,
        role: userRole.none,
    },
    patientsManagement: {
        path: patients,
        exact: true,
        role: userRole.staff,
    },
    patientManagementCreate: {
        path: createPatient,
        exact: true,
        role: userRole.staff,
    },
    patientManagementEdit: {
        path: editPatient,
        exact: true,
        role: userRole.staff,
    },
    medicalStaffManagement: {
        path: medicalStaff,
        exact: true,
        role: userRole.admin,
    },
    medicalStaffManagementCreate: {
        path: createMedicalStaff,
        exact: true,
        role: userRole.admin,
    },
    medicalStaffManagementEdit: {
        path: editMedicalStaff,
        exact: true,
        role: userRole.admin,
    },
    about: {
        path: about,
        exact: true,
        role: userRole.none,
    },
    contact: {
        path: contact,
        exact: true,
        role: userRole.none,
    },
    help: {
        path: help,
        exact: true,
        role: userRole.none,
    },
    privacyNotice: {
        path: privacyNotice,
        exact: true,
        role: userRole.none,
    },
    termsOfService: {
        path: termsOfService,
        exact: true,
        role: userRole.none,
    },
};

export default routes;
