export const about = '/about';
export const login = '/login';
export const contact = '/contact';
export const help = '/help';
export const patients = '/patients';
export const createPatient = '/patient/create';
export const editPatient = '/patient/edit/:id';
export const medicalStaff = '/medicalStaff';
export const createMedicalStaff = '/medicalStaff/create';
export const editMedicalStaff = '/medicalStaff/edit/:id';
export const editMedicalStaffWithParam = '/medicalStaff/edit';
export const editPatientWithParam = '/patient/edit';
export const termsOfService =  '/termsOfService';
export const privacyNotice =  '/privacyNotice';


export const patientManagement = 'patient_Management';
export const mediclStaffManagement = 'medical_Staff_Management';
export const helpId = 'help';
export const contactUs = 'contact';
export const aboutUs = 'about';
export const privacy = 'privacy_Notice';
export const terms = 'terms_of_Service';

export const mapingPathsToTabId = (path) => {
    switch (path) {
        case about:
            return aboutUs;
        case contact :
            return contactUs;
        case help :
            return helpId;
        case createPatient :
            return patientManagement;
        case patients :
            return patientManagement;
        case medicalStaff :
            return mediclStaffManagement;
        case createMedicalStaff :
            return mediclStaffManagement;
        case termsOfService :
            return terms;
        case privacyNotice :
            return privacy;
        default:
            if (path.includes(editMedicalStaffWithParam)) {
                return mediclStaffManagement;
            }
            if (path.includes(editPatientWithParam)) {
                return patientManagement;
            }
            return  '/';
    }
};
