/**
 *
 * Asynchronously loads the component for PrivateRouter
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
