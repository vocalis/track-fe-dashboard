/*
 * PrivateRouter Messages
 *
 * This contains all the text for the PrivateRouter component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.PrivateRoute';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the PrivateRouter component!',
  },
});
