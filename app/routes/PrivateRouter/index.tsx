/**
 *
 * PrivateRouter
 *
 */
import React, {Component, memo} from 'react';
import {Redirect, Route, RouteProps} from 'react-router';
import {isTSRestType} from '@babel/types';
import {stringToNumber} from '../../utils/commonFunc';

interface Authentication {
    isAuthenticated: boolean;
    authenticatedUserRole: string;
}
function PrivateRoute({component: Component, authenticatedUserRole, role, isAuthenticated, ...rest}: any) {
    return (
        <Route  {...rest} render={(props) => {
            if (isAuthenticated) {
                if (stringToNumber(authenticatedUserRole) >= stringToNumber(role)) {
                    return <Component  {...props}/>;
                } else {
                    return <Redirect to="/NotFound"/>;
                }

            } else {
                return <Redirect to="/login"/>;
            }
        }}
        />
    );
}
export default memo(PrivateRoute);

