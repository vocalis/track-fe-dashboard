import {
  ServerAuthenticatedUser,
  ServerMedicalStaff,
  ServerMedicalStaffDetails,
  ServerMedicalStaffTable,
  ServerPatient,
  ServerPatientAnalysis,
  ServerPatientDetails,
  ServerPatients,
  ServerPatientsManagementTable,
  ServerPatientVital,
} from './server';
import {
  AuthenticatedUser,
  MedicalStaff,
  MedicalStaffDetails,
  MedicalStaffManagementState,
  Patient,
  PatientAnalysis,
  PatientDetails,
  Patients,
  PatientsManagementState,
  PatientVital,
} from './local';
import moment from 'moment';
import { medicalStaff } from '../routes/paths';
import { StatusProps } from '../containers/FormDetails/constants';

const getPatients = (serverPatients: ServerPatient[]): Patient[] => {
  if (!serverPatients) {
    return [];
  }
  return serverPatients.map<Patient>((serverPatient) => ({
    loading: false,
    patientId: serverPatient.patientId || serverPatient.id,
    name: `${serverPatient.firstName} ${serverPatient.lastName}`,
    email: serverPatient.email,
    phone: serverPatient.phone,
    birthDate:
      (serverPatient.birthDate && moment(serverPatient.birthDate).toDate()) ||
      null,
    age:
      (serverPatient.birthDate &&
        moment().diff(moment(serverPatient.birthDate), 'years')) ||
      null,
    lastActivity:
      (serverPatient.lastActivity &&
        moment(serverPatient.lastActivity).toDate()) ||
      null,
    reviewed: moment(`${serverPatient.reviewDate}`).isSame(moment(), 'day'),
    reviewedDate: moment(`${serverPatient.reviewDate}`).toDate(),
    detectors: serverPatient.detectors,
    analysis: getAnalysis(serverPatient.analysis),
    vitals: getVitals(serverPatient.vitals || []),
  }));
};

const getPatient = (serverPatient: ServerPatient): Patient | null => {
  if (!serverPatient) {
    return null;
  }
  return getPatients([serverPatient])[0];
};

const getVitals = (serverVitals: ServerPatientVital[]): PatientVital[] => {
  return serverVitals
    .filter((serverVital) => serverVital.value !== -1)
    .map<PatientVital>((serverVital) => ({
      date: moment(serverVital.date).toDate(),
      value: serverVital.value,
      vital: serverVital.vital,
    }));
};

const getAnalysis = (
  serverAnalysis: ServerPatientAnalysis[],
): PatientAnalysis[] =>
  serverAnalysis.map<PatientAnalysis>((serverPatientAnalysis) => ({
    status: serverPatientAnalysis.status,
    referenceDate: serverPatientAnalysis.referenceDate
      ? moment(serverPatientAnalysis.referenceDate).toDate()
      : null,
    sourceStatus: serverPatientAnalysis.sourceStatus || 'same',
    source: serverPatientAnalysis.source,
    sourceDate: serverPatientAnalysis.sourceDate
      ? moment(serverPatientAnalysis.sourceDate).toDate()
      : null,
    referenceRecordingId: serverPatientAnalysis.referenceRecordingId,
    sourceRecordingId: serverPatientAnalysis.sourceRecordingId,
    answers: serverPatientAnalysis.answers,
  }));

const getPatientDetails = (
  serverPatientDetails: ServerPatientDetails,
): PatientDetails | null => {
  if (!serverPatientDetails) {
    return null;
  }
  return {
    email: serverPatientDetails.email ? serverPatientDetails.email : '',
    firstName: serverPatientDetails.firstName
      ? serverPatientDetails.firstName
      : '',
    lastName: serverPatientDetails.lastName
      ? serverPatientDetails.lastName
      : '',
    birthDate: serverPatientDetails.birthDate
      ? moment(serverPatientDetails.birthDate).format('YYYY-MM-DD')
      : null,
    phone: serverPatientDetails.phoneNumber
      ? serverPatientDetails.phoneNumber
      : '',
    status: serverPatientDetails.enabled
      ? StatusProps.values[0]
      : StatusProps.values[1],
    password: '',
    username: serverPatientDetails.username
      ? serverPatientDetails.username
      : '',
  };
};
const getMedicalStaffDetails = (
  serverMedicalStaffDetails: ServerMedicalStaffDetails,
): MedicalStaffDetails | null => {
  if (!serverMedicalStaffDetails) {
    return null;
  }
  return {
    email: serverMedicalStaffDetails.email
      ? serverMedicalStaffDetails.email
      : '',
    firstName: serverMedicalStaffDetails.firstName
      ? serverMedicalStaffDetails.firstName
      : '',
    lastName: serverMedicalStaffDetails.lastName
      ? serverMedicalStaffDetails.lastName
      : '',
    birthDate: serverMedicalStaffDetails.birthDate
      ? moment(serverMedicalStaffDetails.birthDate).format('YYYY-MM-DD')
      : null,
    phone: serverMedicalStaffDetails.phoneNumber
      ? serverMedicalStaffDetails.phoneNumber
      : '',
    status: serverMedicalStaffDetails.enabled
      ? StatusProps.values[0]
      : StatusProps.values[1],
    password: '',
  };
};

const getMedicalStaffTable = (
  serverMedicalStaff: ServerMedicalStaffTable,
): MedicalStaffManagementState | null => {
  if (!serverMedicalStaff) {
  }
  const params = {
    pagesCount: serverMedicalStaff.meta.totalPages,
    selectedPage: serverMedicalStaff.meta.currentPage,
    rowsPerPage: serverMedicalStaff.meta.itemsPerPage,
    rows: serverMedicalStaff.meta.totalItems,
    resultCount: serverMedicalStaff.meta.totalFilteredItems,
    orderBy: 'id',
    sortDirection: 'asc',
  };
  const data = serverMedicalStaff.data.map<MedicalStaff>((medicalStaff) => ({
    Id: medicalStaff.id ?  medicalStaff.id : -1,
    email: medicalStaff.email ? medicalStaff.email.toLowerCase() : '',
    firstName: medicalStaff.firstName ? medicalStaff.firstName.toLowerCase() : '',
    lastName: medicalStaff.lastName ? medicalStaff.lastName.toLowerCase() : '',
    status: medicalStaff.enabled ? 'Active'.toLowerCase() : 'Not Active'.toLowerCase(),
    created: moment(medicalStaff.creationDate).format('MMM,DD,YYYY').toLowerCase(),
  }));
  return {
    data,
    params,
  };
};

const getPatientTable = (
  serverPatients: ServerPatientsManagementTable,
): PatientsManagementState | null => {
  if (!serverPatients) {
  }
  const params = {
    pagesCount: serverPatients.meta.totalPages,
    selectedPage: serverPatients.meta.currentPage,
    rowsPerPage: serverPatients.meta.itemsPerPage,
    rows: serverPatients.meta.totalItems,
    resultCount: serverPatients.meta.totalFilteredItems,
    orderBy: 'id',
    sortDirection: 'asc',
  };
  const data = serverPatients.data.map<Patients>((patient) => ({
    Id: patient.id ? patient.id : -1,
    firstName: patient.firstName ? patient.firstName.toLowerCase() : '',
    lastName: patient.lastName ? patient.lastName.toLowerCase() : '',
    status: patient.enabled ? 'Active'.toLowerCase() : 'Not Active'.toLowerCase(),
    lastActivity: patient.lastActivity
      ? moment(patient.lastActivity).format('MMM DD,YYYY').toLowerCase()
      : '',
    // birthDate: patient.birthDate,
    // email: patient.email,
    // creationDate: patient.creationDate,
    // createdBy: patient.createdBy,
    // lastLoginDate: patient.lastLoginDate,
    // reviewDate: patient.reviewDate,
    // phoneNumber: patient.phoneNumber,
  }));
  return {
    data,
    params,
  };
};
const getAuthenticatedUser = (
  serverAuthenticatedUser: ServerAuthenticatedUser,
): AuthenticatedUser | null => {
  if (!serverAuthenticatedUser) {
    return null;
  }
  return {
    userId: serverAuthenticatedUser.user,
    role: serverAuthenticatedUser.roles[0],
    accessToken: serverAuthenticatedUser.accessToken,
    refreshToken: serverAuthenticatedUser.refreshToken,
    expiresAt: serverAuthenticatedUser.expiresAt,
  };
};
export {
  getPatients,
  getPatient,
  getPatientDetails,
  getMedicalStaffTable,
  getPatientTable,
  getAuthenticatedUser,
  getMedicalStaffDetails,
};
