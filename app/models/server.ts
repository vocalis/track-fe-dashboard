import {Patients, UserManagemntparams} from './local';


interface ServerPatientAnalysis {
    sourceRecordingId: number;
    referenceRecordingId: number | null;
    status: 'better' | 'same' | 'worse' | null;
    sourceDate: string | null;
    referenceDate: string | null;
    answers: {
        daily: number[],
        expanded: number[],
    };
    sourceStatus: 'better' | 'same' | 'worse' | null;
    source: boolean;
}

interface ServerPatientDetectors {
    detector: string;
    value: string;
    confidence: number;
    created: string;
    recordingId: number;
}


interface ServerPatient {
    patientId: number;
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    reviewDate: string;
    phone: string;
    lastActivity: string;
    birthDate: string | null;
    detectors: ServerPatientDetectors[];
    analysis: ServerPatientAnalysis[];
    vitals?: ServerPatientVital[];
}
interface ServerPatientDetails {
    id: number;
    email: string;
    username?: string;
    firstName: string;
    lastName: string;
    birthDate: string | null;
    phoneNumber: string;
    password: string;
    enabled: boolean;
    creationDate: Date;
    createdBy: number;
    lastLoginDate: Date;
    lastActivity: Date;
    reviewDate: Date;
}
interface ServerMedicalStaffDetails {
    id: number;
    email: string;
    firstName: string;
    lastName: string;
    lastLoginDate: Date;
    enabled: boolean;
    creationDate: Date;
    birthDate: string;
    phoneNumber: string;
}
interface ServerMedicalStaff {
    id: number;
    email: string;
    firstName: string;
    lastName: string;
    lastLoginDate: Date;
    enabled: boolean;
    creationDate: Date;
    // birthDate: string;
    // phone: string;
    // password: string;
}
interface ServerTriage {
    id: number;
    name: string;
    pcc: string[];
    score: number;
    alert: boolean;
}

interface ServerPatientVital {
    date: string;
    vital: string;
    value: number;
}
interface ServerMedicalStaffTable {
    data: ServerMedicalStaff[];
    meta: UsersManagemntparams;
}
interface ServerPatientsManagementTable {
    data: ServerPatients[];
    meta: UsersManagemntparams;
}
interface UsersManagemntparams {
    currentPage: number;
    itemCount: number;
    itemsPerPage: number;
    totalItems: number;
    totalPages: number;
    totalFilteredItems: number;
}
interface ServerPatients {
    firstName: string;
    lastName: string;
    lastActivity: Date;
    enabled: boolean;
    id: number;
    // email: string;
    // createdBy: number;
    // birthDate: Date;
    // creationDate: Date;
    // lastLoginDate: Date;
    // phoneNumber: string;
    // reviewDate: Date;
}
interface ServerAuthenticatedUser {
    user: number;
    roles: string[];
    accessToken: string;
    refreshToken: string;
    expiresAt: Date;
}
export {
    ServerPatient,
    ServerPatientAnalysis,
    ServerTriage,
    ServerPatientDetectors,
    ServerPatientVital,
    ServerPatientDetails,
    ServerPatients,
    ServerMedicalStaff,
    ServerAuthenticatedUser,
    ServerMedicalStaffDetails,
    ServerPatientsManagementTable,
    ServerMedicalStaffTable,
};

