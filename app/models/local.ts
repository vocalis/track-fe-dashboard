import {RowsPerPage} from '../containers/UserManagement/constants';

interface PatientAnalysis {
    sourceRecordingId: number | null;
    referenceRecordingId: number | null;
    status: 'better' | 'same' | 'worse' | null;
    sourceDate: Date | null;
    referenceDate: Date | null;
    answers: PatientAnalysisAnswer;
    sourceStatus: 'better' | 'same' | 'worse' | null;
    source: boolean;
}
interface MedicalStaffManagementState {
    data: MedicalStaff [];
    params: UserManagemntparams;
}
interface MedicalStaff {
    Id: number;
    email: string;
    firstName: string;
    lastName: string;
    status: string;
    created: string;
}
interface AuthenticatedUser {
    userId: number;
    role: string;
    accessToken: string;
    refreshToken: string;
    expiresAt: Date;
}
interface PatientsManagementState {
    data: Patients[];
    params: UserManagemntparams ;
}

interface UserManagemntparams {
    pagesCount: number;
    selectedPage: number;
    rowsPerPage: number;
    rows: number;
    resultCount: number;
    orderBy: string;
    sortDirection: string;
    searchText?: string;
    }
interface Patients {
    firstName: string;
    lastName: string;
    status: string;
    Id: number;
    lastActivity: string;
    // email: string;
    // createdBy: number;
    // birthDate: Date;
    // creationDate: Date;
    // lastLoginDate: Date;

    // reviewDate: Date;
    // phoneNumber: string;
}
interface PatientAnalysisAnswer {
    daily: number[];
    expanded: number[];
}
export interface ValuesProps {
    firstName: string;
    lastName: string;
    email: string;
    phone: string;
    birthDate: string | null;
    password: string;
}


interface PatientDetectors {
    'detector': string;
    'value': string;
    'confidence': number;
    'created': string;
    'recordingId': number;
}
interface Patient {
    loading: boolean;
    patientId: number;
    name: string;
    lastActivity: Date | null;
    birthDate: Date | null;
    email: string;
    phone: string;
    age: number | null;
    reviewedDate: Date | null;
    reviewed?: boolean | null | undefined;
    detectors: PatientDetectors[];
    analysis: PatientAnalysis[];
    vitals?: PatientVital[];
}
interface PatientDetails {
    username?: string;
    email: string;
    firstName: string;
    lastName: string;
    birthDate: string | null;
    phone: string;
    status: string;
    password?: string;
}



interface MedicalStaffDetails {
    email: string;
    firstName: string;
    lastName: string;
    birthDate: string | null;
    phone: string;
    status: string;
    password?: string;
}

interface PatientVital {
    date: Date;
    vital: string;
    value: number;
}
interface QuestionnaireInterface {
    daily: QuestionnaireDataInterface[];
    expanded: QuestionnaireDataInterface[];
}
interface  AnswersInterface {
    daily: number[];
    expanded: number[];
}
interface QuestionnaireDataInterface {
    id: number;
    question: string;
    answers: QuestionnaireAnswersDataInterface[];
}

interface QuestionnaireAnswersDataInterface {
    id: number;
    answer: string;
}
interface LoginUserData {
    email: string;
    password: string;
}
export {
    Patient,
    PatientAnalysis,
    PatientVital,
    PatientAnalysisAnswer,
    QuestionnaireInterface,
    QuestionnaireDataInterface,
    QuestionnaireAnswersDataInterface,
    AnswersInterface,
    LoginUserData,
    PatientDetails,
    MedicalStaff,
    Patients,
    MedicalStaffManagementState,
    PatientsManagementState,
    UserManagemntparams,
    AuthenticatedUser,
    MedicalStaffDetails,
};

