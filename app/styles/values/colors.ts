export default {
    SHADE: {
        WHITE: {
            WHITE: '#FFFFFF',
        },
        GRAY: {
            BORDER: '#EAEAEA',
            TABLE_BORDER: '#EAEAEA',
            TABLE_TEXT: '#353535',
        },
        BLACK: {
            BLACK: 'rgba(0,0,0,1)',
            BLACK_16: 'rgba(0,0,0,0.16)',
        },
        RED: {
            ERROR: '#D22242',
        },
        YELLOW: {
            CARD_SHADE: 'rgba(255,195,0,0.16)',
        },
    },
    GRAPHS: {
        GRAY : '#707070',
        PINK: '#FDA2B1',
        BLUE: '#A2DFFD',
        GREEN: '#38ABB6',
        BLACK: '#353535',
        YELLOW: 'rgb(255,195,0,0.1)',
        DISABLED: '#EAEAEA',
    },
    TEXT: {
        GRAY: '#EAEAEA',
        WHITE: '#FFFFFF',
        BLACK: '#353535',
        LIGHTGRAY: '#B3B3B3',
        GREEN: '#38ABB6',
    },
    BRAND: {
        PRIMARY: '#38ABB6',
        ACCENT: '#d32f2f',
    },
    GENERAL: {
        APP_BACKGROUND: '#FDFDFD',
        PATIENT_BACKGROUND: '#F8F8F8',
        MODAL_BACKDROP: 'rgba(53,53,53,0.6)',
    },
    GRADIENT: {
        HOME: {
            START: '#38ABB6',
            END: '#1C565B',
        },
        HEADER: {
            START: '#38ABB6',
            END: '#1C565B',
        },
    },
    SCROLLER: {
        HANDLE: '#38ABB6',
        TRACK: '#EAEAEA',
    },
    COMPARISON: {
        BETTER: '#00D948',
        SAME: '#353535',
        WORSE: '#CF1F2E',
    },
    TOOLTIP: {
        BETTER_BACKGROUND: 'rgba(76, 175, 80, 0.3)',
        SAME_BACKGROUND: '#FFFFFF',
        WORSE_BACKGROUND: 'rgba(210, 34, 66, 0.1)',
        NONE_BACKGROUND: '#FFFFFF',
        BETTER_DOT: '#00D948',
        SAME_DOT: '#353535',
        WORSE_DOT: '#CF1F2E',
        NONE_DOT: 'rgba(0,0,0,1)',
    },
};
