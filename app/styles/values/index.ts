import colors from './colors';
import fonts from './fonts';
import transitions from './transitions';
import sizes from './sizes';

export default {
    colors,
    fonts,
    sizes,
    transitions,
};
