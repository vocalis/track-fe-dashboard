interface PatientServerAnalysis {
    'sourceRecordingId': number;
    'referenceRecordingId': number | null;
    'status': string;
    'sourceDate': string | null;
    'referenceDate': string | null;
    'answers': {
        'daily': number[],
        'expanded': [],
    };
    'sourceStatus': string;
    'source': boolean;
}

interface PatientServer {
    'patientId': number;
    'firstName': string;
    'lastName': string;
    'email': string;
    'reviewed': string;
    'analysis': { [name: string]: PatientServerAnalysis };
}

export {
    PatientServer,
};

