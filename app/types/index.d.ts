import { Reducer, Store } from 'redux';
import { RouterState } from 'connected-react-router';
import { ContainerState as LanguageProviderState } from 'containers/LanguageProvider/types';
import {ContainerState as MonitorState} from 'containers/MonitorPage/types';
import {ContainerState as TriageState} from 'containers/TriagePage/types';
import {ContainerState as ScreenState} from 'containers/ScreenPage/types';
import {ContainerState as PatientState} from 'containers/PatientPage/types';
import {ContainerState as AppState} from 'containers/App/types';
import {ContainerState as LoginState} from 'containers/Login/types';
import {ContainerState as AuthProviderState} from 'containers/AuthProvider/types';
import {ContainerState as FormDetailsState} from 'containers/FormDetails/types';
import {ContainerState as sideBarState} from 'containers/SideBar/types';
import {ContainerState as userManagement} from 'containers/UserManagement/types';

export interface InjectedStore extends Store {
  injectedReducers: any;
  injectedSagas: any;
  runSaga(saga: (() => IterableIterator<any>) | undefined, args: any | undefined): any;
}

export interface InjectReducerParams {
  key: keyof ApplicationRootState;
  reducer: Reducer<any, any>;
}

export interface InjectSagaParams {
  key: keyof ApplicationRootState;
  saga: () => IterableIterator<any>;
  mode?: string | undefined;
}

// Your root reducer type, which is your redux state types also
export interface ApplicationRootState {
  readonly router: RouterState;
  readonly language: LanguageProviderState;
  readonly app: AppState;
  readonly monitor: MonitorState;
  readonly patient: PatientState;
  readonly screen: ScreenState;
  readonly triage: TriageState;
  readonly vocalisHealth: MonitorState;
  readonly login: LoginState;
  readonly authProvider: AuthProviderState;
  readonly formDetails: FormDetailsState;
  readonly  userManagement: userManagement;
  readonly  sideBar: sideBarState;
  // for testing purposes
  readonly test: any;
}
